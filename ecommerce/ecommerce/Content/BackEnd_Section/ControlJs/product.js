﻿var successmsg = null;

//function InsertProductDetail() {
//    if (CheckBlankValidation("ProductName")) return !1;
//    if (CheckBlankValidation("ProductCode")) return !1;
//    if (CheckNumberBlankValidation("PurchasePrice")) return !1;
//    if (CheckNumberBlankValidation("SalePrice")) return !1;
//    if (CheckBlankValidation('Discount')) return !1;
//    if (CheckDropDownBlankValidation('UnitId')) return !1;
//    if (CheckDropDownBlankValidation('MenuId')) return !1;
//    //if (CheckDropDownBlankValidation('CategoryId')) return !1;
//    //if (CheckDropDownBlankValidation('SubCategoryId')) return !1;
//    if (CheckDropDownBlankValidation('BrandId')) return !1;
//    $(this).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");
//}

$("#btnProductSave").click(function () {
    //if (CheckBlankValidation("txtProductName")) return !1;
    //if (CheckBlankValidation("txtProductCode")) return !1;
    //if (CheckBlankValidation("txtPurchasePrice")) return !1;
    //if (CheckBlankValidation("txtSalePrice")) return !1;
    //if (CheckDropDownBlankValidation('ddlUnit')) return !1;
    //if (CheckDropDownBlankValidation('ddlMenu')) return !1;
    ////if (CheckDropDownBlankValidation('ddlCategory')) return !1;
    ////if (CheckDropDownBlankValidation('ddlSubCategory')) return !1;
    //if (CheckDropDownBlankValidation('ddlBrand')) return !1;
    ////if (CheckBlankValidation("txtDescription")) return !1;
    //$(this).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");

    if (CheckBlankValidation("ProductName")) return !1;
    if (CheckBlankValidation("ProductCode")) return !1;
    if (CheckNumberBlankValidation("PurchasePrice")) return !1;
    if (CheckNumberBlankValidation("SalePrice")) return !1;
    //if (CheckBlankValidation('Discount')) return !1;
    if (CheckDropDownBlankValidation('UnitId')) return !1;
    if (CheckDropDownBlankValidation('MenuId')) return !1;
    //if (CheckDropDownBlankValidation('CategoryId')) return !1;
    //if (CheckDropDownBlankValidation('SubCategoryId')) return !1;
    if (CheckDropDownBlankValidation('BrandId')) return !1;
    $(this).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");


    var product = {};

    product.ProductId = GetParameterValues('productid');
    product.ProductName = $("#ProductName").val();
    product.ProductCode = $("#ProductCode").val();
    product.BrandId = $("#BrandId").val();
    product.MenuId = $("#MenuId").val();
    product.CategoryId = $("#CategoryId").val();
    product.SubCategoryId = $("#SubCategoryId").val();
    product.Offer = $("#Offer").val();
    product.PurchasePrice = $("#PurchasePrice").val();
    product.SalePrice = $("#SalePrice").val();
    product.DiscountPer = $("#DiscountPer").val();
    product.UnitId = $("#UnitId").val();
    product.DeliveryTime = $("#DeliveryTime").val();
    //product.Description = $("#txtDescription").val();
    product.IsFeaturedProducts = $("#IsFeaturedProducts").val();

    $.ajax({
        type: "Post",
        url: "/ProductMaster/ProcessInsertProduct",
        data: '{product: ' + JSON.stringify(product) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "insert") {
                    $("#hdnProdtId").val(data[1]);
                    $(".prodsuccessmessage").click();
                    $(".promodelheading").css("color", "#00a300").html("Success");
                    $(".prosucessmsg").css("color", "#00a300").html(data[2]);
                }
                else if (data[0] == "update") {
                    $("#hdnProdtId").val("");
                    $(".prodsuccessmessage").click();
                    $(".promodelheading").css("color", "#00a300").html("Success");
                    $(".prosucessmsg").css("color", "#00a300").html(data[1]);
                }
                else {
                    alert("fail");
                }
                $("#btnProductSave").html("Submit");
            }
        }
    });
});

$(".deletebackprodt").click(function () {
    var delproductid = $(this).data('productid');
    if (delproductid != null) {
        DeleteFun('btnProductDelete', delproductid);
    }
});

$(document.body).on('click', "#btnProductDelete", function (e) {
    var proid = $(this).data('valid');
    $(".delproinnercls_" + proid).removeClass("fa-trash").addClass(" fa-spin fa-spinner");
    $(".delcancelbtn").click();
    DeleteProductDetail(proid, 'delete');
});

function DeleteProductDetail(proid, acttype) {
    $.ajax({
        type: "Post",
        url: "/ProductMaster/DeleteProductDetail",
        data: '{productid: ' + JSON.stringify(proid) + ',actiontype: ' + JSON.stringify(acttype) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data == "true") {
                    window.location.reload();
                }
            }
        }
    });
}

function DeleteFun(delId, valid) {
    var row = "<div class='modal fade' id='DeleteModel' role='dialog'>"
        + "<div class='modal-dialog modal-sm' style='margin:15% auto!important;'>"
        + "<div class='modal-content' style='text-align: center;border-radius: 5px !important;width: 350px'>"
        + "<div class='modal-header' style='padding: 12px;'>"
        + "<h5 class='modal-title modelheading'><span id='iconid'></span><span class='spanmsg'></span>Delete Confirmation</h5>"
        + "</div>"
        + "<div class='modal-body' style='padding-left:20px!important;'>"
        + "<h6 class='sucessmsg'>Are you sure, want to delete this record?</h6>"
        + "</div>"
        + "<div class='modal-footer'>"
        + "<button type='button' class='btn btn-success' data-valid='" + valid + "' id='" + delId + "'>Delete</button>"
        + "<button type='button' class='btn btn-danger delcancelbtn' data-dismiss='modal'>Cancel</button>"
        + "</div>"
        + "</div>"
        + "</div>"
        + "</div>";

    $("#custommodal").append(row);
    $(".dodelete").click();
}

$(".prodtsuccesscancel").click(function () {
    var proid = $("#hdnProdtId").val();
    if (proid != null && proid != "") {
        window.location.href = "/product-master/add-edit-product?productid=" + proid + "&status=existing";
    }
});

$(function () {
    //GetMenuList();
    //GetProductDetailById();
});

function GetProductDetailById() {
    var proid = GetParameterValues('productid');
    if (proid != null) {
        $.ajax({
            type: "Post",
            url: "/ProductMaster/ProcessGetProductDetail",
            data: '{productid: ' + JSON.stringify(proid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null && data != undefined) {
                    $("#txtProductName").val(data.ProductName);
                    $("#txtProductCode").val(data.ProductCode);
                    $("#txtPurchasePrice").val(data.PurchasePrice);
                    $("#txtSalePrice").val(data.SalePrice);
                    $("#txtDiscount").val(data.Discount);
                    $("#txtOffer").val(data.Offer);
                    $("#ddlUnit").val(data.Unit);
                    $("#txtDeliveryTime").val(data.DeliveryTime);
                    $("#ddlMenu").val(data.Menu);
                    $("#ddlCategory").val(data.Category);
                    $("#ddlSubCategory").val(data.SubCategory);
                    $("#ddlBrand").val(data.Brand);
                    if (data.IsFeaturedProducts) {
                        $("#ddlFeaturedProduct").val("true");
                    } else {
                        $("#ddlFeaturedProduct").val("false");
                    }
                    $("#txtDescription").val(data.Description);
                }
            }
        });
    }
}

$('#txtProductSearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var countryList = $(".productlistcss");
    if (_this.val() != "") {
        $.each(countryList, function (i, item) {
            var countryname = $(this).data('productname').toLowerCase();
            if (countryname.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.each(countryList, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});

function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

function InsertImageDetails() {
    var proid = GetParameterValues('productid');
    if (CheckProductIdExists(proid)) {
        if (CheckBlankValidation("txtImgName")) return !1;
        if (CheckBlankValidation("txtProImage")) return !1;

        $.ajax({
            type: "Post",
            url: "/ProductMaster/SaveProductImageDetails",
            data: '{imagename: ' + JSON.stringify($("#txtImgName").val()) + ',productid: ' + JSON.stringify(proid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != "") {
                    var imgId = data;

                    if (imgId != "0") {
                        var fileUpload = $("#txtProImage").get(0);
                        var files = fileUpload.files;
                        //Create FormData object
                        var fileData = new FormData();
                        //Looping over all files and add it to FormData object
                        for (var i = 0; i < files.length; i++) {
                            fileData.append(files[i].name, files[i]);
                        }

                        //fileData.append(imgId); 

                        $.ajax({
                            url: '/ProductMaster/UpdateLastInsertedImageName',
                            type: "POST",
                            contentType: false, // Not to set any content header  
                            processData: false, // Not to process data  
                            data: fileData,
                            //data: { fileData, imageId: imgId, hotcode: hotelco },
                            success: function (result) {
                                if (result != "") {
                                    $("#txtImgName").val("");
                                    $("#txtProImage").val("");
                                    GetExistingImageDetail();
                                    //    if (result = "true") {
                                    //        ResetImageSection();
                                    //        GetExistingImageDetail(hotelco);
                                    //        if (imgMsg == "true") { $(".modelheading").css("color", "#028904"); $(".spanmsg").html(" Success"); $("#iconid").addClass("fa fa-check-circle"); $(".sucessmsg").html("Record has been submitted sucessfully."); }
                                    //        $(".close").click();
                                    //        $(".successmessage").click();
                                    //    }
                                    //    else {
                                    //        $(".close").click();
                                    //        $(".modelheading").css("color", "#ff0000"); $(".spanmsg").html(" Image Submission Failed!"); $("#iconid").addClass("fa fa-exclamation-triangle"); $(".sucessmsg").html("Record submission has been failed.");
                                    //        $(".successmessage").click();
                                    //    }
                                    //}
                                    //else {
                                    //    $(".close").click();
                                    //    $(".modelheading").css("color", "#ff0000"); $(".spanmsg").html(" Image Submission Failed!"); $("#iconid").addClass("fa fa-exclamation-triangle"); $(".sucessmsg").html("Record submission has been failed.");
                                    //    $(".successmessage").click();
                                    //alert("Success");
                                }
                            }
                        });
                    }
                    else {
                        //if (imgMsg == "true") { $(".modelheading").css("color", "#028904"); $(".spanmsg").html(" Success"); $("#iconid").addClass("fa fa-check-circle"); $(".sucessmsg").html("Record has been submitted sucessfully."); }
                        //if (data == "nohotelcode") { $(".modelheading").css("color", "#ff0000"); $(".spanmsg").html(" Already Exist!"); $("#iconid").addClass("fa fa-exclamation-triangle"); $(".sucessmsg").html("There is something wrong, Please try again!"); }
                        //if (imgMsg == "nohotelcode") { $(".modelheading").css("color", "#ff0000"); $(".spanmsg").html(" Hotel Not Available!"); $("#iconid").addClass("fa fa-exclamation-triangle"); $(".sucessmsg").html("You have to add hotel first!"); }
                        //$(".close").click();
                        //$(".successmessage").click();
                        alert("Failed");
                    }
                }
            }
        });
    }
}

//function GetAllProductImages() {
//    var proid = GetParameterValues('productid');
//    if (proid != null && proid == "") {
//        $.ajax({
//            type: "Post",
//            url: "/ProductMaster/GetAllImagesByProductId",
//            data: '{productid: ' + JSON.stringify(proid) + '}',
//            contentType: "application/json; charset=utf-8",
//            datatype: "json",
//            success: function (data) {
//                if (data != null) {
//                    $("#BindImageList").append(data);
//                }
//            }
//        });
//    }
//}

$("#tabImages").click(function () {
    GetExistingImageDetail();
});

function GetExistingImageDetail() {
    var productid = GetParameterValues('productid');
    try {
        if (productid != null && productid != null) {
            $('#BindImageList').html("");

            $.ajax({
                type: "Post",
                url: "/ProductMaster/GetAllImagesByProductId",
                data: '{productid: ' + JSON.stringify(productid) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (data) {
                    if (data != "") {
                        $('#BindImageList').html(data);
                    }
                }
            });
        }
    }
    catch (err) {
        alert(err);
    }
}

$(document.body).on('click', ".imgactionevt", function (e) {
    var imgid = $(this).data("imageid");
    var imgaction = $(this).data("imgaction");
    var imgproductid = $(this).data("productid");

    $(this).html("Please Wait...");

    if (imgaction == "edit") {
    }
    else {
        $.ajax({
            type: "Post",
            url: "/ProductMaster/ImageActionEvent",
            data: '{imgid: ' + JSON.stringify(imgid) + ', productid: ' + JSON.stringify(imgproductid) + ', action: ' + JSON.stringify(imgaction) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != "") {
                    if (data == "true") {
                        GetExistingImageDetail();
                    }
                }
            }
        });
    }
});

function GetMenuList() {
    $("#ddlMenu").prop("disabled", true);
    $("#ddlMenu").children().remove();
    $("#ddlMenu").append($('<option>').val(0).text('Please wait...'));
    $.ajax({
        type: "Post",
        url: "/ProductMaster/GetMenuList",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != "") {
                $('#ddlMenu').html(data);
            }
        }
    });
}

function GetCategoryList() {
    $("#ddlCategory").prop("disabled", true);
    $("#ddlCategory").children().remove();
    $("#ddlCategory").append($('<option>').val(0).text('Please wait...'));
    var menuid = $('#ddlMenu').val();
    $.ajax({
        type: "Post",
        url: "/ProductMaster/BindCategoryById",
        data: '{menuid: ' + JSON.stringify(menuid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $("#ddlCategory").children().remove();
                $("#ddlCategory").append(data);
                $("#ddlCategory").prop("disabled", false);
            }
        }
    });
}

function InsertDescriptionDetails() {
    var proid = GetParameterValues('productid');
    if (CheckProductIdExists(proid)) {
        if (CheckBlankValidation("txtPDescription1")) return !1;

        $("#btnDescSubmit").html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");

        var descriptionsList = [];
        descriptionsList[0] = $("#txtPDescription1").val();
        descriptionsList[1] = $("#txtPDescription2").val();
        descriptionsList[2] = $("#txtPDescription3").val();
        descriptionsList[3] = $("#txtPDescription4").val();
        descriptionsList[4] = $("#txtPDescription5").val();

        $.ajax({
            type: "Post",
            url: "/ProductMaster/InsertDescriptionDetails",
            data: '{productid: ' + JSON.stringify(proid) + ',desclist: ' + JSON.stringify(descriptionsList) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data[0] == "true") {
                        $(".failedmessage").click();
                        $(".failedheading").css("color", "#00a300").html("Success");
                        $(".failedmsg").css("color", "#00a300").html(data[1]);
                    }
                    else {
                        alert("fail");
                    }
                    $("#btnDescSubmit").html("Submit");
                }
            }
        });
    }
}

function CheckProductIdExists(proid) {
    if (proid == null && proid == undefined) {
        $(".failedmessage").click();
        $(".failedheading").css("color", "#ff0000").html("Failed !");
        $(".failedmsg").css("color", "#ff0000").html("Please add product detail first!");
        return false;
    }
    return true;
}

function FillCategory() {
    $("#CategoryId").prop("disabled", true);
    $("#CategoryId").children().remove();
    $("#CategoryId").append($('<option>').val(0).text('Please wait...'));
    var menuid = $('#MenuId').val();
    $.ajax({
        url: '/ProductMaster/BindCategoryByMenuId',
        type: "GET",
        dataType: "JSON",
        data: { menuid: menuid },
        success: function (data) {
            $("#CategoryId").children().remove();
            $("#CategoryId").append($('<option>').val(0).text('Select Category'));
            $.each(data, function (i, data) {
                $("#CategoryId").append($('<option></option>').val(data.Value).html(data.Text));
            });
            $("#CategoryId").prop("disabled", false);
        }
    });
}

function FillSubCategory() {
    $("#SubCategoryId").prop("disabled", true);
    $("#SubCategoryId").children().remove();
    $("#SubCategoryId").append($('<option>').val(0).text('Please wait...'));

    var menuid = $('#MenuId').val();
    var catid = $('#CategoryId').val();
    $.ajax({
        url: '/ProductMaster/BindSubCategoryByCatId',
        type: "GET",
        dataType: "JSON",
        data: { menuid: menuid, catid: catid },
        success: function (data) {
            $("#SubCategoryId").children().remove();
            $("#SubCategoryId").append($('<option>').val(0).text('Select Sub Category'));
            $.each(data, function (i, data) {
                $("#SubCategoryId").append($('<option></option>').val(data.Value).html(data.Text));
            });
            $("#SubCategoryId").prop("disabled", false);
        }
    });
}

$("#tabDescriptions").click(function () {
    GetExistingDescDetail();
});

function GetExistingDescDetail() {
    var productid = GetParameterValues('productid');
    try {
        if (productid != null && productid != null) {
            $.ajax({
                type: "Post",
                url: "/ProductMaster/GetExistingDescDetail",
                data: '{productid: ' + JSON.stringify(productid) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (data) {
                    if (data != null) {
                        if (data[0] != null) {
                            $("#txtPDescription1").val(data[0]);
                        }
                        if (data[1] != null) {
                            $("#txtPDescription2").val(data[1]);
                        }
                        if (data[2] != null) {
                            $("#txtPDescription3").val(data[2]);
                        }
                        if (data[3] != null) {
                            $("#txtPDescription4").val(data[3]);
                        }
                        if (data[4] != null) {
                            $("#txtPDescription5").val(data[4]);
                        }
                    }
                }
            });
        }
    }
    catch (err) {
        alert(err);
    }
}