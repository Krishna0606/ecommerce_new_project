﻿var deleteactionid = null;

$(function () {
    IsMemberDeleted();

    var memberid = GetParameterValues('memberid');
    if (memberid != undefined && memberid != "") {
        $("#UserId").attr("disabled", "disabled");
        $("#Password").attr("disabled", "disabled");
    }
});

function IsMemberDeleted() {
    if (localStorage.getItem("memberdeleted") == "true") {
        localStorage.removeItem("memberdeleted");
        SuccessModal("Record deleted successfully.");
    }
}

function SuccessModal(msg) {
    $(".successmessage").click();
    $(".modelheading").css("color", "#00a300").html("Success");
    $(".sucessmsg").css("color", "#00a300").html(msg);
}

function FillState() {
    $("#StateId").prop("disabled", true);
    $("#StateId").children().remove();
    $("#StateId").append($('<option>').val(0).text('Please wait...'));
    var countryId = $('#CountryId').val();
    $.ajax({
        url: '/AreaMaster/BindState',
        type: "GET",
        dataType: "JSON",
        data: { CountryID: countryId },
        success: function (data) {
            $("#StateId").children().remove();
            $("#StateId").append($('<option>').val(0).text('-- Select State --'));
            $.each(data, function (i, data) {
                $("#StateId").append($('<option></option>').val(data.Value).html(data.Text));
            });
            $("#StateId").prop("disabled", false);
        }
    });
}

function FillCity() {
    $("#CityId").prop("disabled", true);
    $("#CityId").children().remove();
    $("#CityId").append($('<option>').val(0).text('Please wait...'));
    var stateId = $('#StateId').val();
    $.ajax({
        url: '/AreaMaster/BindCity',
        type: "GET",
        dataType: "JSON",
        data: { stateid: stateId },
        success: function (data) {
            $("#CityId").children().remove();
            $("#CityId").append($('<option>').val(0).text('-- Select City --'));
            $.each(data, function (i, data) {
                $("#CityId").append($('<option></option>').val(data.Value).html(data.Text));
            });
            $("#CityId").prop("disabled", false);
        }
    });
}

function MemRegSubmitEvt() {
    if (CheckBlankValidation("FirstName")) return !1;
    if (CheckBlankValidation("LastName")) return !1;
    if (CheckBlankValidation("EmailId")) return !1;
    if (CheckEmailValidatoin("EmailId")) return !1;
    if (CheckBlankValidation("MobileNo")) return !1;
    if (CheckBlankValidation("Address")) return !1;
    if (CheckBlankValidation("PinCode")) return !1;
    if (CheckDropDownBlankValidation("CountryId")) return !1;
    if (CheckDropDownBlankValidation("StateId")) return !1;
    if (CheckDropDownBlankValidation("CityId")) return !1;
    //if (CheckBlankValidation("City")) return !1;
    if (CheckBlankValidation("AreaName")) return !1;
    if (CheckBlankValidation("AddharNo")) return !1;
    if (CheckBlankValidation("PanNo")) return !1;
    //if (CheckBlankValidation("City")) return !1;
    if (CheckBlankValidation("UserId")) return !1;
    if (CheckBlankValidation("Password")) return !1;

    var thisbutton = $("#btnMemRegSubmit");

    var supplier = {};
    supplier.LoginId = $("#hdnMemberId").val();
    if (supplier.LoginId != "" && supplier.LoginId != undefined) {
        supplier.ActionType = "modify";
    }
    supplier.FirstName = $("#FirstName").val();
    supplier.LastName = $("#LastName").val();
    supplier.EmailId = $("#EmailId").val();
    supplier.MobileNo = $("#MobileNo").val();
    supplier.CountryId = $("#CountryId").val();
    supplier.StateId = $("#StateId").val();
    supplier.CityId = $("#CityId").val();
    supplier.AreaName = $("#AreaName").val();
    supplier.PinCode = $("#PinCode").val();
    supplier.Address = $("#Address").val();
    //supplier.ProfileImage = $('#fluProfileImage').val();;
    //supplier.Profession = $("#Profession").val();
    supplier.DOB = $("#DOB").val();
    supplier.AddharNo = $("#AddharNo").val();
    supplier.PanNo = $("#PanNo").val();
    //supplier.GSTNo = "";
    //supplier.AddharImage = "";
    //supplier.PanImage = "";
    supplier.BankName = $("#BankName").val();
    supplier.AccountNo = $("#AccountNo").val();
    supplier.BranchName = $("#BranchName").val();
    supplier.BrnachCode = $("#BrnachCode").val();
    supplier.IFSCCode = $("#IFSCCode").val();
    supplier.BankAddress = $("#BankAddress").val();
    supplier.PayeeName = $("#PayeeName").val();
    supplier.UserId = $("#UserId").val();
    supplier.Password = $("#Password").val();


    if (!$(".useridexist").hasClass("available")) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");

        $.ajax({
            type: "Post",
            url: "/MemberMaster/CreateMemRegistration",
            data: '{supplier: ' + JSON.stringify(supplier) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data[0] == "true") {
                        $(".successmessage").click();
                        $(".modelheading").css("color", "#00a300").html("Success");
                        $(".sucessmsg").css("color", "#00a300").html(data[1]);
                        RestMemRegForm();
                    }
                    else {
                        $(".successmessage").click();
                        $(".modelheading").css("color", "#dc3545").html("Failed");
                        $(".sucessmsg").css("color", "#dc3545").html(data[1]);
                    }
                    $(thisbutton).html("Submit");
                }
            }
        });
    }
}

function RestMemRegForm() {
    $("#FirstName").val("");
    $("#LastName").val("");
    $("#EmailId").val("");
    $("#MobileNo").val("");
    $("#Address").val("");
    $("#PinCode").val("");
    $("#CountryId").prop('selectedIndex', 0);
    $("#StateId").prop('selectedIndex', 0);
    $("#CityId").prop('selectedIndex', 0);
    $("#DOB").val("");
    $("#AddharNo").val("");
    $("#PanNo").val("");
    $("#BankName").val("");
    $("#AccountNo").val("");
    $("#BranchName").val("");
    $("#IFSCCode").val("");
    $("#BankAddress").val("");
    $("#PayeeName").val("");
    $("#UserId").val("");
    $("#Password").val("");
}

function CheckUserId() {
    setTimeout(function () {
        $.ajax({
            type: "Post",
            url: "/MemberMaster/CheckUserIdExist",
            data: '{userid: ' + JSON.stringify($("#UserId").val()) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data[0] == "true") {
                        $(".useridexist").html(data[1]);
                        $(".useridexist").addClass(" available").removeClass("hidden");
                    }
                    else {
                        $(".useridexist").html("");
                        $(".useridexist").removeClass(" available").addClass("hidden");
                    }
                }
            }
        });
    }, 100);
}

$(".deletemember").click(function () {
    var countryid = $(this).data('memberid');
    deleteactionid = countryid;
    DeleteFun('btnMemberDelete');
});

function DeleteFun(delId) {
    var row = "<div class='modal fade' id='DeleteModel' role='dialog'>"
        + "<div class='modal-dialog modal-sm' style='margin:15% auto!important;'>"
        + "<div class='modal-content' style='text-align: center;border-radius: 5px !important;width: 350px'>"
        + "<div class='modal-header' style='padding: 12px;'>"
        + "<h5 class='modal-title modelheading'><span id='iconid'></span><span class='spanmsg'></span>Delete Confirmation</h5>"
        + "</div>"
        + "<div class='modal-body' style='padding-left:20px!important;'>"
        + "<h6 class='sucessmsg'>Are you sure, want to delete this record?</h6>"
        + "</div>"
        + "<div class='modal-footer'>"
        + "<button type='button' class='btn btn-success' id='" + delId + "'>Delete</button>"
        + "<button type='button' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
        + "</div>"
        + "</div>"
        + "</div>"
        + "</div>";

    $("#custommodal").append(row);
    $(".dodelete").click();
}

$(document.body).on('click', "#btnMemberDelete", function (e) {
    var supplier = {};
    supplier.LoginId = deleteactionid;

    $.ajax({
        type: "Post",
        url: "/MemberMaster/CreateMemRegistration",
        data: '{supplier: ' + JSON.stringify(supplier) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    //$(".successmessage").click();
                    //$(".modelheading").css("color", "#00a300").html("Success");
                    //$(".sucessmsg").css("color", "#00a300").html(data[1]);
                    localStorage.setItem("memberdeleted", true);
                    window.location.href = "/member-master/member-list";
                }
                else {
                    alert("fail");
                }
            }
        }
    });
});

$('#txtMemberSearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var countryList = $(".memberlistcss");
    if (_this.val() != "") {
        $.each(countryList, function (i, item) {
            var countryname = $(this).data('membername').toLowerCase();
            if (countryname.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.each(countryList, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});

function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}