﻿$(".btnSetPosition").click(function () {
    var reviewid = $(this).data("reviewid");
    var thisreviewposval = $("#txtPosition_" + reviewid).val();

    var thisbutton = $("#btnSetPosition_" + reviewid);

    $(thisbutton).html("<i class='fa fa-pulse fa-spinner processspin' style='color: #fff;'></i>").prop("disabled", true);

    $.ajax({
        type: "Post",
        url: "/ReviewMaster/UpdateReviewPosition",
        data: '{reviewid:' + JSON.stringify(reviewid) + ',posval:' + JSON.stringify(thisreviewposval) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data = "true") {
                    setTimeout(function () {
                        $(thisbutton).html("Set").prop("disabled", false);
                    }, 1000);
                }
                else {
                    alert("Error Occured!");
                }
            }
        }
    });
});

$(".ddlisapproved").change(function () {
    var reviewid = $(this).data("reviewid");
    $(this).after("<span class='processdivsec'><i class='fa fa-pulse fa-spinner' style='color: #ff6795;'></i></span>").prop("disabled", true);

    $.ajax({
        type: "Post",
        url: "/ReviewMaster/UpdateReviewIsApproved",
        data: '{reviewid:' + JSON.stringify(reviewid) + ',approved:' + JSON.stringify($("#ddlIsApproved_" + reviewid).val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data = "true") {
                    setTimeout(function () {
                        $("#ddlIsApproved_" + reviewid).prop("disabled", false);
                        $(".processdivsec").html("");
                    }, 1000);
                }
                else {
                    alert("Error Occured!");
                }
            }
        }
    });
});