﻿function CheckBoxCheckedValidation(i) {
    if (!$("#" + i).is(':checked')) {
        return "false";
    }
    else { return "true"; }
}

$("#btnBackLogin").click(function () {
    var thisbutton = $("#btnBackLogin");
    var username = $("#txtusername").val();
    var password = $("#txtpassword").val();
    var isrember = CheckBoxCheckedValidation("ckb1");

    if (username == null || username == "") { $(".usernamesec").css("border-bottom", "1px solid red"); $("#txtusername").focus(); return false; } else { $(".usernamesec").css("border-bottom", "1px solid #b2b2b2"); }
    if (username != "" && (password == null || password == "")) { $(".userpasssec").css("border-bottom", "1px solid red"); $("#txtpassword").focus(); return false; } else { $(".userpasssec").css("border-bottom", "1px solid #b2b2b2"); }

    $(thisbutton).html("Please Wait...");
    $.ajax({
        type: "Post",
        url: "/Account/ProcessBackendLogin",
        data: '{username: ' + JSON.stringify(username) + ',password: ' + JSON.stringify(password) + ',isrember: ' + JSON.stringify(isrember) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    localStorage.setItem("backendloginsuccess", data[1]);
                    window.location.href = "/dashboard";
                }
                else {
                    $(".errormsg").removeClass("hidden").html(data[1]);
                    localStorage.removeItem("backendloginsuccess");
                    $(thisbutton).html("Login");
                    //localStorage.getItem("lastname");
                }
            }
        }
    });
});