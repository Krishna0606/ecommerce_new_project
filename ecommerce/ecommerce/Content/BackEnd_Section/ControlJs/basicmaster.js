﻿var deleteactionid = null;
function MenuSubmitEvt() {
    var thisbutton = $("#btnSubmit");
    if (CheckBlankValidation("MenuName")) return !1;
    var hdnImg = $("#hdnImage").val();
    if (hdnImg == "") {
        if (CheckBlankValidation("txtMenuImage")) return !1;
    }
    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");

    $.ajax({
        type: "Post",
        url: "/BasicMaster/ProcessInsertMenu",
        data: '{menuid: ' + JSON.stringify($("#hdnmeunId").val()) + ',menuname: ' + JSON.stringify($("#MenuName").val()) + ',isfeatured: ' + JSON.stringify($("#IsFeatured").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "insert") {
                    SaveMenuImage(data[1], "txtMenuImage", "InsertMenuImage", "insert");
                }
                else if (data[0] == "update") {
                    var file = $('#txtMenuImage').val().substring($('#txtMenuImage').val().lastIndexOf('.') + 1).toLowerCase();                    
                    if (file != "") {
                        SaveMenuImage(data[1], "txtMenuImage", "InsertMenuImage", "update");
                    }
                    else {
                        ShowMessagePopup("Success", "#00a300", "Record updated successfully.", "#00a300");
                    }
                }
                else {
                    alert("fail");
                }
            }
            $("#btnSubmit").html("Submit");
        }
    });
}

function SaveMenuImage(id, fileuploadid, methodname, actiontype) {
    if (id != "0") {
        var fileUpload = $("#" + fileuploadid).get(0);
        var files = fileUpload.files;
        //Create FormData object
        var fileData = new FormData();
        //Looping over all files and add it to FormData object
        for (var i = 0; i < files.length; i++) {
            fileData.append(files[i].name, files[i]);
        }

        $.ajax({
            url: '/BasicMaster/' + methodname,
            type: "POST",
            contentType: false, // Not to set any content header  
            processData: false, // Not to process data  
            data: fileData,
            //data: { fileData, imageId: imgId, hotcode: hotelco },
            success: function (data) {
                if (data == "true") {
                    if (actiontype == "insert") {
                        $("#MenuName").val('');
                        $("#txtMenuImage").val('');
                        $("#IsFeatured").prop('selectedIndex', "false");
                        ShowMessagePopup("Success", "#00a300", "Record inserted successfully.", "#00a300");
                    }
                    else if (actiontype == "update") {
                        ShowMessagePopup("Success", "#00a300", "Record updated successfully.", "#00a300");
                    }
                }
            }
        });
    }
}

$("#txtMenuImage").change(function () {
    var val = $(this).val();
    switch (val.substring(val.lastIndexOf('.') + 1).toLowerCase()) {
        case 'jpg': case 'png':
            break;
        default:
            $(this).val('');
            ShowMessagePopup("Image Incorrect!", "#e72c2c", "Image should be 'jpg' or 'png' only.", "#e72c2c");
            break;
    }
});

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    $(".successmessage").click();
    $(".modelheading").css("color", headcolor).html(headerHeading);
    $(".sucessmsg").css("color", bodycolor).html(bodyMsg);
}

function CategorySubmitEvt() {
    var thisbutton = $("#btnSubmit");
    if (CheckDropDownBlankValidation('Menu')) return !1;
    if (CheckBlankValidation("CategoryName")) return !1;
    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");
    $.ajax({
        type: "Post",
        url: "/BasicMaster/ProcessInsertCategory",
        data: '{categoryid: ' + JSON.stringify($("#hdnCategoryId").val()) + ',categoryname: ' + JSON.stringify($("#CategoryName").val()) + ',menu: ' + JSON.stringify($("#Menu").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    $("#CategoryName").val('');
                    $(".successmessage").click();
                    $(".modelheading").css("color", "#00a300").html("Success");
                    $(".sucessmsg").css("color", "#00a300").html(data[1]);
                }
                else {
                    alert("fail");
                }
                $(thisbutton).html("Submit");
            }
        }
    });
}

$(function () {
    IsMenuDeleted();
    IsCategoryDeleted();

    //=============Sub Category==========
    RunStartSubCategorySection();
    IsBrandDeleted();
    IsUnitDeleted();
})

function IsMenuDeleted() {
    if (localStorage.getItem("menudeleted") == "true") {
        localStorage.removeItem("menudeleted");
        SuccessModal("Record deleted successfully.");
    }
}

function IsCategoryDeleted() {
    if (localStorage.getItem("categorydeleted") == "true") {
        localStorage.removeItem("categorydeleted");
        SuccessModal("Record deleted successfully.");
    }
}

function IsBrandDeleted() {
    if (localStorage.getItem("branddeleted") == "true") {
        localStorage.removeItem("branddeleted");
        SuccessModal("Record deleted successfully.");
    }
}

function IsUnitDeleted() {
    if (localStorage.getItem("unitdeleted") == "true") {
        localStorage.removeItem("unitdeleted");
        SuccessModal("Record deleted successfully.");
    }
}

function SuccessModal(msg) {
    $(".successmessage").click();
    $(".modelheading").css("color", "#00a300").html("Success");
    $(".sucessmsg").css("color", "#00a300").html(msg);
}

$('#txtmenuSearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var MenuList = $(".Menulistcss");
    if (_this.val() != "") {
        $.each(MenuList, function (i, item) {
            var Menuname = $(this).data('menuname').toLowerCase();
            if (Menuname.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.each(MenuList, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});

$('#txtCategorySearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var Categorylist = $(".categorylistcss");
    if (_this.val() != "") {
        $.each(Categorylist, function (i, item) {
            var Categoryname = $(this).data('categoryname').toLowerCase();
            if (Categoryname.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.each(Categorylist, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});

$(".deleteMenu").click(function () {
    var menuid = $(this).data('menuid');
    deleteactionid = menuid;
    DeleteFun('btnMenuDelete');
});

$(".deleteBrand").click(function () {
    var brandid = $(this).data('brandid');
    deleteactionid = brandid;
    DeleteFun('btnBrandDelete');
});

$(".deleteUnit").click(function () {
    var unitid = $(this).data('unitid');
    deleteactionid = unitid;
    DeleteFun('btnUnitDelete');
});

$(".deletecategory").click(function () {
    var categoryid = $(this).data('categoryid');
    deleteactionid = categoryid;
    DeleteFun('btnCategoryDelete');
});

$(document.body).on('click', "#btnMenuDelete", function (e) {
    $.ajax({
        type: "Post",
        url: "/BasicMaster/ProcessInsertMenu",
        data: '{menuid: ' + JSON.stringify(deleteactionid) + ',menuname: ' + null + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    localStorage.setItem("menudeleted", true);
                    window.location.href = "/basic-master/menu-list";
                }
                else {
                    alert("fail");
                }
            }
        }
    });
});

$(document.body).on('click', "#btnBrandDelete", function (e) {
    $.ajax({
        type: "Post",
        url: "/BasicMaster/ProcessInsertBrand",
        data: '{brandid: ' + JSON.stringify(deleteactionid) + ',brandname: ' + null + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    localStorage.setItem("branddeleted", true);
                    window.location.href = "/basic-master/brand-list";
                }
                else {
                    alert("fail");
                }
            }
        }
    });
});

$(document.body).on('click', "#btnUnitDelete", function (e) {
    $.ajax({
        type: "Post",
        url: "/BasicMaster/ProcessInsertUnit",
        data: '{unitid: ' + JSON.stringify(deleteactionid) + ',unitname: ' + null + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    localStorage.setItem("unitdeleted", true);
                    window.location.href = "/basic-master/Unit-list";
                }
                else {
                    alert("fail");
                }
            }
        }
    });
});

$(document.body).on('click', "#btnCategoryDelete", function (e) {
    $.ajax({
        type: "Post",
        url: "/BasicMaster/ProcessInsertCategory",
        data: '{categoryid: ' + JSON.stringify(deleteactionid) + ',categoryname: ' + null + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    localStorage.setItem("categorydeleted", true);
                    window.location.href = "/basic-master/category-list";
                }
                else {
                    alert("fail");
                }
            }
        }
    });
});

function DeleteFun(delId) {
    var row = "<div class='modal fade' id='DeleteModel' role='dialog'>"
        + "<div class='modal-dialog modal-sm' style='margin:15% auto!important;'>"
        + "<div class='modal-content' style='text-align: center;border-radius: 5px !important;width: 350px'>"
        + "<div class='modal-header' style='padding: 12px;'>"
        + "<h5 class='modal-title modelheading'><span id='iconid'></span><span class='spanmsg'></span>Delete Confirmation</h5>"
        + "</div>"
        + "<div class='modal-body' style='padding-left:20px!important;'>"
        + "<h6 class='sucessmsg'>Are you sure, want to delete this record?</h6>"
        + "</div>"
        + "<div class='modal-footer'>"
        + "<button type='button' class='btn btn-success' id='" + delId + "'>Delete</button>"
        + "<button type='button' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
        + "</div>"
        + "</div>"
        + "</div>"
        + "</div>";

    $("#custommodal").append(row);
    $(".dodelete").click();
}

//===========================Sub Category List====================================
function RunStartSubCategorySection() {
    BindMenuList("ddlSubCatMenuName");
}

function SubCatSubmitEvt() {
    var thisbutton = $("#btnSubCatSubmit");
    if (CheckDropDownBlankValidation('ddlSubCatMenuName')) return !1;
    if (CheckDropDownBlankValidation('ddlSubCatCategoryName')) return !1;
    if (CheckBlankValidation("txtSubCategoryName")) return !1;
    $(thisbutton).prop("disabled", true);
    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");
    var subcatid = GetParameterValues("subcategoryid");
    if (subcatid == undefined) {
        subcatid = null;
    }
    $.ajax({
        type: "Post",
        url: "/BasicMaster/ProcessInsertSubCategory",
        data: '{subcatid: ' + JSON.stringify(subcatid) + ',menuid: ' + JSON.stringify($("#ddlSubCatMenuName").val()) + ',catid: ' + JSON.stringify($("#ddlSubCatCategoryName").val()) + ',subcatname: ' + JSON.stringify($("#txtSubCategoryName").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    $(".successmessage").click();
                    $(".modelheading").css("color", "#00a300").html("Success");
                    $(".sucessmsg").css("color", "#00a300").html(data[1]);
                    if (subcatid == null && subcatid == "") {
                        ResetSubCatSetion();
                    }
                }
                else {
                    alert("Failed!");
                }
            }
            $(thisbutton).html("Submit");
        }
    });
}

function ResetSubCatSetion() {
    $("#ddlSubCatMenuName").prop('selectedIndex', 0);
    $("#ddlSubCatCategoryName").html("<option value='0'>Select Category</option>");
    $("#txtSubCategoryName").val("");
    $("#btnSubCatSubmit").html("Submit");
}

function BindMenuList(selectid) {
    $("#" + selectid).prop("disabled", true);
    $("#" + selectid).children().remove();
    $("#" + selectid).append($('<option>').val(0).text('Please wait...'));
    $.ajax({
        url: '/Common/BindJsonMenuList',
        type: "GET",
        dataType: "JSON",
        //data: '{menuid: ' + JSON.stringify($("#hdnCategoryId").val()) + '}',
        success: function (data) {
            if (data != null) {
                $("#" + selectid).children().remove();
                $("#" + selectid).html(data);
                $("#" + selectid).prop("disabled", false);
                var subcatid = GetParameterValues("subcategoryid");
                if (subcatid != null && subcatid != "") {
                    UpdateSubCatSection(subcatid);
                }
            }
        }
    });
}

$("#ddlSubCatMenuName").change(function () {
    BindCategoryList("ddlSubCatCategoryName", $("#ddlSubCatMenuName").val(), null);
});

function BindCategoryList(selectid, selectedmenuid, catid) {
    $("#" + selectid).prop("disabled", true);
    $("#" + selectid).children().remove();
    $("#" + selectid).append($('<option>').val(0).text('Please wait...'));
    $.ajax({
        type: "Post",
        url: "/Common/BindJsonCategoryList",
        data: '{menuid: ' + JSON.stringify(selectedmenuid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $("#" + selectid).children().remove();
                $("#" + selectid).html(data);
                $("#" + selectid).prop("disabled", false);
                if (catid != null) {
                    $("#" + selectid).val(catid);
                    //$("#" + selectid).prop('selectedIndex', catid);
                }
            }
        }
    });
}

function UpdateSubCatSection(subcatid) {
    $.ajax({
        type: "Post",
        url: "/BasicMaster/GetSubCategoryById",
        data: '{subcategoryid: ' + JSON.stringify(subcatid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $("#ddlSubCatMenuName").val(data[0]);
                BindCategoryList("ddlSubCatCategoryName", data[0], data[1]);

                //$("#ddlSubCatMenuName").prop('selectedIndex', data[0]);

                //$("#ddlSubCatCategoryName").val(data[1]);
                // $('#ddlSubCatCategoryName option:selected').attr("selected", data[1]);

                $("#txtSubCategoryName").val(data[2]);
            }
        }
    });
}

function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

$('#txtSubCategorySearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var SubCatList = $(".subcategorylistcss");
    if (_this.val() != "") {
        $.each(SubCatList, function (i, item) {
            var subcategoryname = $(this).data('subcategoryname').toLowerCase();
            if (subcategoryname.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.each(SubCatList, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});
//===========================Brand List====================================
$('#txtbrnadSearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var Brnadlist = $(".Brandlistcss");
    if (_this.val() != "") {
        $.each(Brnadlist, function (i, item) {
            var brandname = $(this).data('brandname').toLowerCase();
            if (brandname.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.each(Brnadlist, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});

function BrandSubmitEvt() {
    var thisbutton = $("#btnSubmit");
    if (CheckBlankValidation("BrandName")) return !1;
    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");
    $.ajax({
        type: "Post",
        url: "/BasicMaster/ProcessInsertBrand",
        data: '{brandid: ' + JSON.stringify($("#hdnbrandId").val()) + ',brandname: ' + JSON.stringify($("#BrandName").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    $("#BrandName").val('');
                    $(".successmessage").click();
                    $(".modelheading").css("color", "#00a300").html("Success");
                    $(".sucessmsg").css("color", "#00a300").html(data[1]);
                }
                else {
                    alert("fail");
                }
                $(thisbutton).html("Submit");
            }
        }
    });
}

$('#txtUnitSearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var Unitlist = $(".Unitlistcss");
    if (_this.val() != "") {
        $.each(Unitlist, function (i, item) {
            var unitname = $(this).data('unitname').toLowerCase();
            if (unitname.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.each(Unitlist, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});

function UnitSubmitEvt() {
    var thisbutton = $("#btnSubmit");
    if (CheckBlankValidation("UnitName")) return !1;
    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");
    $.ajax({
        type: "Post",
        url: "/BasicMaster/ProcessInsertUnit",
        data: '{unitid: ' + JSON.stringify($("#hdnunitId").val()) + ',unitname: ' + JSON.stringify($("#UnitName").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    $("#UnitName").val('');
                    $(".successmessage").click();
                    $(".modelheading").css("color", "#00a300").html("Success");
                    $(".sucessmsg").css("color", "#00a300").html(data[1]);
                }
                else {
                    alert("fail");
                }
                $(thisbutton).html("Submit");
            }
        }
    });
}