﻿function InsertHomeBanner() {
    if (CheckBlankValidation("fluBannerImg")) return !1;

    var banner = {};
    banner.Heading = $("#txtHeading").val();
    banner.Content = $("#txtContent").val();
    banner.RedirectUrl = $("#txtRedirectUrl").val();
    banner.IsShow = $("#ddlIsShow").val();
    banner.BannerType = $("#ddlBannerType").val();
    banner.BannerOrder = $("#txtOrder").val();

    $("#btnHomeBannerSubmit").html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");

    $.ajax({
        type: "Post",
        url: "/SettingMaster/InsertBannerDetail",
        data: '{banner: ' + JSON.stringify(banner) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data == "true") {
                var fileUpload = $("#fluBannerImg").get(0);
                var files = fileUpload.files;
                //Create FormData object
                var fileData = new FormData();
                //Looping over all files and add it to FormData object
                for (var i = 0; i < files.length; i++) {
                    fileData.append(files[i].name, files[i]);
                }

                //fileData.append(imgId); 

                $.ajax({
                    url: '/SettingMaster/UpdateLastInsertedBannerImageName',
                    type: "POST",
                    contentType: false, // Not to set any content header  
                    processData: false, // Not to process data  
                    data: fileData,
                    //data: { fileData, imageId: imgId, hotcode: hotelco },
                    success: function (result) {
                        if (result != "") {
                            alert("Success");
                        }
                    }
                });
            }
            else if (data == "false") {
                alert("Failed");
            }
            else {
                alert("Error");
            }
        }
    });
}

$(".btnSetBannerOrderClass").click(function () {
    var bannerid = $(this).data("bannerid");    
    var thisreviewposval = $("#txtBannerOrder_" + bannerid).val();
    var thisbutton = $("#btnSetBannerOrder_" + bannerid);
    $(thisbutton).html("<i class='fa fa-pulse fa-spinner' style='color: #fff;'></i>").prop("disabled", true);

    $.ajax({
        type: "Post",
        url: "/SettingMaster/UpdateBannerOrder",
        data: '{bannerid:' + JSON.stringify(bannerid) + ',posval:' + JSON.stringify(thisreviewposval) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data = "true") {
                    setTimeout(function () {
                        $(thisbutton).html("Set").prop("disabled", false);
                    }, 1000);
                }
                else {
                    alert("Error Occured!");
                }
            }
        }
    });
});