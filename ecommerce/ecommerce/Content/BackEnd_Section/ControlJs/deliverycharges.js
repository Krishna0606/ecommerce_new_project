﻿$(function () {
    //BindDCState();

    //var dcid = GetParameterValues("dcid");
    //if (dcid != null && dcid != undefined) {
    //    BindDcDetails(dcid);
    //}

    if ($("#Price").val() == "0") {
        $("#Price").val('');
    }

    if (localStorage.getItem("dcdeleted") == "true") {
        localStorage.removeItem("dcdeleted");
        SuccessModal("Record deleted successfully.");
    }
});

function SuccessModal(msg) {
    $(".successmessage").click();
    $(".modelheading").css("color", "#00a300").html("Success");
    $(".sucessmsg").css("color", "#00a300").html(msg);
}

function BindDcDetails(dcid) {
    $.ajax({
        type: "Post",
        url: "/SettingMaster/BindDcDetails",
        data: '{dcid: ' + JSON.stringify(dcid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $("#txtDCPrice").val(data.Price);
                $("#txtDCPinCode").val(data.PinCode);

                BindDCState();
                BindDCCity(data.StateId);
                setInterval(function () {
                    $("#txtDCStateName").val(data.StateId);
                    $("#txtDCCityName").val(data.CityId);
                }, 100);
            }
        }
    });
}

function BindDCState() {
    $.ajax({
        type: "Post",
        url: "/SettingMaster/BindDCState",
        //data: '{stateid: ' + JSON.stringify($("#hdnStateId").val()) + ',statename: ' + JSON.stringify($("#StateName").val()) + ',statecode: ' + JSON.stringify($("#StateCode").val()) + ',countryid: ' + JSON.stringify($("#CountryId").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $("#txtDCStateName").html("");
                $("#txtDCStateName").html(data);
            }
        }
    });
}

function BindDCCity(stid) {
    $.ajax({
        type: "Post",
        url: "/SettingMaster/BindDCCity",
        data: '{stateid: ' + JSON.stringify(stid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $("#txtDCCityName").html("");
                $("#txtDCCityName").html(data);
            }
        }
    });
}

//$('#txtDCStateName').on('change', function () {
//    BindDCCity($("#txtDCStateName").val());
//});

function InsertDeliveryCharges() {
    var thisbutton = $("#btnDeliveryChargesSubmit");

    if (CheckBlankValidation("Price")) return !1;
    if (CheckBlankValidation("PinCode")) return !1;

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");

    var fieldsval = {};
    var dcid = GetParameterValues("dcid");
    if (dcid != null) {
        fieldsval.DeliveryChargesId = dcid;
    }
    fieldsval.Price = $("#Price").val();
    fieldsval.StateId = $("#StateId").val();
    fieldsval.StateName = $("#StateId option:selected").text();
    fieldsval.CityId = $("#CityId").val();
    fieldsval.CityName = $("#CityId option:selected").text();
    fieldsval.PinCode = $("#PinCode").val();

    $.ajax({
        type: "Post",
        url: "/SettingMaster/InsertDeliveryCharges",
        data: '{dcharges: ' + JSON.stringify(fieldsval) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    if (dcid == null) {
                        Reset();
                    }
                    $(".successmessage").click();
                    $(".modelheading").css("color", "#00a300").html("Success");
                    $(".sucessmsg").css("color", "#00a300").html(data[1]);
                }
                else {
                    alert("fail");
                }
                $(thisbutton).html("Submit");
            }
        }
    });
}

function Reset() {
    $("#Price").val('');
    $("#StateId").prop('selectedIndex', 0);
    $("#CityId").children().remove();
    $("#CityId").append($('<option>').val(0).text('Select City'));
    $("#PinCode").val('');
    //BindDCState();
    //$("#txtDCCityName").html("");
    //$("#txtDCCityName").html("<option value='0'>Select City</option>");

}

function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

function FillCity() {
    $("#CityId").prop("disabled", true);
    $("#CityId").children().remove();
    $("#CityId").append($('<option>').val(0).text('Please wait...'));
    var stateId = $('#StateId').val();
    $.ajax({
        //type: "Post",
        //url: "/SettingMaster/BindDCCity",
        //data: '{stateid: ' + JSON.stringify(stateId) + '}',
        //contentType: "application/json; charset=utf-8",
        //datatype: "json",

        url: '/SettingMaster/BindDCCity',
        type: "GET",
        dataType: "JSON",
        data: { stateid: stateId },
        success: function (data) {
            $("#CityId").children().remove();
            $("#CityId").append($('<option>').val(0).text('Select City'));
            $.each(data, function (i, data) {
                $("#CityId").append($('<option></option>').val(data.Value).html(data.Text));
            });
            $("#CityId").prop("disabled", false);
        }
    });
}

var deleteddcid = null;

$(".deleteddccharges").click(function () {
    deleteddcid = $(this).data('dccharges');
    DeleteFun('btnDeliveryChargesDelete');
    //InsertDeliveryCharges(dcchargesid);
});

$(document.body).on('click', "#btnDeliveryChargesDelete", function (e) {
    var dcchargesid = $(this).data('dcid');
    var fieldsval = {};
    fieldsval.DeliveryChargesId = deleteddcid;
    $.ajax({
        type: "Post",
        url: "/SettingMaster/InsertDeliveryCharges",
        data: '{dcharges: ' + JSON.stringify(fieldsval) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    //$(".successmessage").click();
                    //$(".modelheading").css("color", "#00a300").html("Success");
                    //$(".sucessmsg").css("color", "#00a300").html(data[1]);
                    localStorage.setItem("dcdeleted", true);
                    window.location.href = "/setting-master/delivery-charges-list";
                }
                else if (data[0] == "logout") {
                    window.location.href = data[1];
                }
                else {
                    alert("fail");
                }
            }
        }
    });
});

function DeleteFun(delId) {
    var row = "<div class='modal fade' id='DeleteModel' role='dialog'>"
        + "<div class='modal-dialog modal-sm' style='margin:15% auto!important;'>"
        + "<div class='modal-content' style='text-align: center;border-radius: 5px !important;width: 350px'>"
        + "<div class='modal-header' style='padding: 12px;'>"
        + "<h5 class='modal-title modelheading'><span id='iconid'></span><span class='spanmsg'></span>Delete Confirmation</h5>"
        + "</div>"
        + "<div class='modal-body' style='padding-left:20px!important;'>"
        + "<h6 class='sucessmsg'>Are you sure, want to delete this record?</h6>"
        + "</div>"
        + "<div class='modal-footer'>"
        + "<button type='button' class='btn btn-success' id='" + delId + "'>Delete</button>"
        + "<button type='button' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
        + "</div>"
        + "</div>"
        + "</div>"
        + "</div>";

    $("#custommodal").append(row);
    $(".dodelete").click();
}