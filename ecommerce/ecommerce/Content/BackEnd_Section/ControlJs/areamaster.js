﻿var deleteactionid = null;

$(function () {
    IsCountryDeleted();
    IsStateDeleted();
    IsCityDeleted();
});

function IsCountryDeleted() {
    if (localStorage.getItem("countrydeleted") == "true") {
        localStorage.removeItem("countrydeleted");
        SuccessModal("Record deleted successfully.");
    }
}

function IsStateDeleted() {
    if (localStorage.getItem("statedeleted") == "true") {
        localStorage.removeItem("statedeleted");
        SuccessModal("Record deleted successfully.");
    }
}

function IsCityDeleted() {
    if (localStorage.getItem("cityydeleted") == "true") {
        localStorage.removeItem("cityydeleted");
        SuccessModal("Record deleted successfully.");
    }
}

function SuccessModal(msg) {
    $(".successmessage").click();
    $(".modelheading").css("color", "#00a300").html("Success");
    $(".sucessmsg").css("color", "#00a300").html(msg);
}

function CountrySubmitEvt() {
    var thisbutton = $("#btnSubmit");
    if (CheckBlankValidation("CountryName")) return !1;
    if (CheckBlankValidation("CountryCode")) return !1;
    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");
    $.ajax({
        type: "Post",
        url: "/AreaMaster/ProcessInsertCountry",
        data: '{countryid: ' + JSON.stringify($("#hdnCountryId").val()) + ',countryname: ' + JSON.stringify($("#CountryName").val()) + ',countrycode: ' + JSON.stringify($("#CountryCode").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    $("#CountryName").val('');
                    $("#CountryCode").val('');
                    $(".successmessage").click();
                    $(".modelheading").css("color", "#00a300").html("Success");
                    $(".sucessmsg").css("color", "#00a300").html(data[1]);
                }
                else {
                    alert("fail");
                }
                $(thisbutton).html("Submit");
            }
        }
    });
}

$('#txtCountrySearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var countryList = $(".countrylistcss");
    if (_this.val() != "") {
        $.each(countryList, function (i, item) {
            var countryname = $(this).data('countryname').toLowerCase();
            if (countryname.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.each(countryList, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});

$(".deletecountry").click(function () {
    var countryid = $(this).data('countryid');
    deleteactionid = countryid;
    DeleteFun('btnCountryDelete');
});

$(document.body).on('click', "#btnCountryDelete", function (e) {
    $.ajax({
        type: "Post",
        url: "/AreaMaster/ProcessInsertCountry",
        data: '{countryid: ' + JSON.stringify(deleteactionid) + ',countryname: ' + null + ',countrycode: ' + null + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    //$(".successmessage").click();
                    //$(".modelheading").css("color", "#00a300").html("Success");
                    //$(".sucessmsg").css("color", "#00a300").html(data[1]);
                    localStorage.setItem("countrydeleted", true);
                    window.location.href = "/area-master/country-list";
                }
                else {
                    alert("fail");
                }
            }
        }
    });
});

$(document.body).on('click', "#btnStateDelete", function (e) {
    $.ajax({
        type: "Post",
        url: "/AreaMaster/ProcessInsertState",
        data: '{stateid: ' + JSON.stringify(deleteactionid) + ',statename: ' + null + ',statecode: ' + null + ',countryid: ' + null + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    //$(".successmessage").click();
                    //$(".modelheading").css("color", "#00a300").html("Success");
                    //$(".sucessmsg").css("color", "#00a300").html(data[1]);
                    localStorage.setItem("statedeleted", true);
                    window.location.href = "/area-master/state-list";
                }
                else {
                    alert("fail");
                }
            }
        }
    });
});

$(document.body).on('click', "#btnCityDelete", function (e) {
    $.ajax({
        type: "Post",
        url: "/AreaMaster/ProcessInsertCity",
        data: '{cityid: ' + JSON.stringify(deleteactionid) + ',cityname: ' + null + ',citycode: ' + null + ',countryid: ' + null + ',stateId: ' + null + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    //$(".successmessage").click();
                    //$(".modelheading").css("color", "#00a300").html("Success");
                    //$(".sucessmsg").css("color", "#00a300").html(data[1]);
                    localStorage.setItem("cityydeleted", true);
                    window.location.href = "/area-master/city-list";
                }
                else {
                    alert("fail");
                }
            }
        }
    });
});


function StateSubmitEvt() {
    var thisbutton = $("#btnStateSubmit");
    if (CheckDropDownBlankValidation('CountryId')) return !1;
    if (CheckBlankValidation("StateName")) return !1;
    if (CheckBlankValidation("StateCode")) return !1;
    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");
    $.ajax({
        type: "Post",
        url: "/AreaMaster/ProcessInsertState",
        data: '{stateid: ' + JSON.stringify($("#hdnStateId").val()) + ',statename: ' + JSON.stringify($("#StateName").val()) + ',statecode: ' + JSON.stringify($("#StateCode").val()) + ',countryid: ' + JSON.stringify($("#CountryId").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    $("#StateName").val('');
                    $("#StateCode").val('');
                    $("#CountryId").prop('selectedIndex', 0);
                    $(".successmessage").click();
                    $(".modelheading").css("color", "#00a300").html("Success");
                    $(".sucessmsg").css("color", "#00a300").html(data[1]);
                }
                else {
                    alert("fail");
                }
                $(thisbutton).html("Submit");
            }
        }
    });
}

$(".deletestate").click(function () {
    var stateid = $(this).data('stateid');
    deleteactionid = stateid;
    DeleteFun('btnStateDelete');
});

$('#txtStateSearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var stateList = $(".statelistcss");
    if (_this.val() != "") {
        $.each(stateList, function (i, item) {
            var statename = $(this).data('statename').toLowerCase();
            if (statename.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.each(stateList, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});


function CitySubmitEvt() {
    var thisbutton = $("#btnCitySubmit");
    if (CheckDropDownBlankValidation('CountryId')) return !1;
    if (CheckDropDownBlankValidation('StateId')) return !1;
    if (CheckBlankValidation("CityName")) return !1;
    if (CheckBlankValidation("CityCode")) return !1;
    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");
    var cityid = $("#hdnCityId").val();
    var cityname = $("#CityName").val();
    var citycode = $("#CityCode").val();
    var countryid = $("#CountryId").val();
    var stateid = $("#StateId").val();

    $.ajax({
        type: "Post",
        url: "/AreaMaster/ProcessInsertCity",
        data: '{cityid: ' + JSON.stringify(cityid) + ',cityname: ' + JSON.stringify(cityname) + ',citycode: ' + JSON.stringify(citycode) + ',countryid: ' + JSON.stringify(countryid) + ',stateId: ' + JSON.stringify(stateid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {                   
                    if (cityid == 0) {
                        $("#CityName").val('');
                        $("#CityCode").val('');
                        $("#CountryId").prop('selectedIndex', 0);
                        $("#StateId").prop('selectedIndex', 0);
                    }
                    $(".successmessage").click();
                    $(".modelheading").css("color", "#00a300").html("Success");
                    $(".sucessmsg").css("color", "#00a300").html(data[1]);
                }
                else {
                    alert("fail");
                }
                $(thisbutton).html("Submit");
            }
        }
    });
}

function FillState() {
    $("#StateId").prop("disabled", true);
    $("#StateId").children().remove();
    $("#StateId").append($('<option>').val(0).text('Please wait...'));
    var countryId = $('#CountryId').val();
    $.ajax({
        url: '/AreaMaster/BindState',
        type: "GET",
        dataType: "JSON",
        data: { CountryID: countryId },
        success: function (data) {
            $("#StateId").children().remove();
            $("#StateId").append($('<option>').val(0).text('-- Select State --'));
            $.each(data, function (i, data) {
                $("#StateId").append($('<option></option>').val(data.Value).html(data.Text));
            });
            $("#StateId").prop("disabled", false);
        }
    });
}

$(".deletecity").click(function () {
    var cityid = $(this).data('cityid');
    deleteactionid = cityid;
    DeleteFun('btnCityDelete');
});

$('#txtCitySearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var cityList = $(".citylistcss");
    if (_this.val() != "") {
        $.each(cityList, function (i, item) {
            var cityname = $(this).data('cityname').toLowerCase();
            if (cityname.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.each(cityList, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});

function DeleteFun(delId) {
    var row = "<div class='modal fade' id='DeleteModel' role='dialog'>"
        + "<div class='modal-dialog modal-sm' style='margin:15% auto!important;'>"
        + "<div class='modal-content' style='text-align: center;border-radius: 5px !important;width: 350px'>"
        + "<div class='modal-header' style='padding: 12px;'>"
        + "<h5 class='modal-title modelheading'><span id='iconid'></span><span class='spanmsg'></span>Delete Confirmation</h5>"
        + "</div>"
        + "<div class='modal-body' style='padding-left:20px!important;'>"
        + "<h6 class='sucessmsg'>Are you sure, want to delete this record?</h6>"
        + "</div>"
        + "<div class='modal-footer'>"
        + "<button type='button' class='btn btn-success' id='" + delId + "'>Delete</button>"
        + "<button type='button' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
        + "</div>"
        + "</div>"
        + "</div>"
        + "</div>";

    $("#custommodal").append(row);
    $(".dodelete").click();
}