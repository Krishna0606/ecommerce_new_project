﻿
$(function () {
    //localStorage.removeItem("selectedstateid");
    //localStorage.removeItem("selectedstatename");
    //if (localStorage.getItem("selectedstateid") == null && localStorage.getItem("selectedstatename") == null) {
    //    setTimeout(function () {
    //        GetStateDetails();
    //        $('.locationbtnpopup').click();
    //    }, 2000);
    //}

    //localStorage.removeItem("pincodeval");
    if (localStorage.getItem("pincodeval") == null) {
        setTimeout(function () {            
            $('.locationbtnpopup').click();
        }, 2000);
    }
    else {
        $(".toplocaton").html("<li><i class='fa fa-map-marker' style='color: #ff6795;font-size:20px;'></i>&nbsp;Current Location : <span style='color:#ff6795;font-weight:bold;'>" + localStorage.getItem("pincodeval") + "</span>&nbsp;<i class='fa fa-refresh locationreset' title='Location Reset' style='cursor:pointer;color: #6f0063;font-size:18px;'></i></li>");
    }

    CheckCurrUrl();
    if (localStorage.getItem("frontloginsuccess") != null) {
        $(".usernotloggedin").addClass("hidden");
        $(".userloggedin").removeClass("hidden");
        CheckFrontUserLogin();
    }
});

$(document.body).on('click', ".locationreset", function (e) {
    GetStateDetails();
    $('.locationbtnpopup').click();
});

$(".selectedstateclick").click(function () {
    //var stateval = $("#ddlhomestate").val();
    //if (stateval > 0) {
    //    $(this).html("Please wait...");
    //    localStorage.setItem("selectedstateid", stateval);
    //    localStorage.setItem("selectedstatename", $("#ddlhomestate option:selected").text());
    //    window.location.reload();
    //}
    if (CheckFocusCancellationBlankValidation('txtPincodeInput')) return !1;
    var pincodeval = $("#txtPincodeInput").val();
    if (pincodeval != "" && pincodeval != null) {
        $(this).html("Please wait...");
        localStorage.setItem("pincodeval", pincodeval);        
        window.location.reload();
    }
});

function GetStateDetails() {
    $.ajax({
        type: "Post",
        url: "/AreaMaster/GetStateDetails",
        data: '{stateid: ' + JSON.stringify(null) + ',status: ' + JSON.stringify(true) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $("#ddlhomestate").html(data);
            }
        }
    });
}

$(document.body).on('click', "#btnFrontlogin", function (e) {
    if (CheckFocusCancellationBlankValidation('FrontLoginId')) return !1;
    if (CheckFocusCancellationBlankValidation('FrontPassword')) return !1;

    $("#btnFrontlogin").html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>").prop("disabled", true);

    $.ajax({
        type: "Post",
        url: "/Account/ProcessFrontEndLogin",
        data: '{userid: ' + JSON.stringify($("#FrontLoginId").val()) + ',password: ' + JSON.stringify($("#FrontPassword").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    localStorage.setItem("frontloginsuccess", "userlogin");
                    //$(".closefsignin").click();
                    window.location.reload(true);
                }
                else {
                    $("#btnFrontlogin").prop("disabled", false);
                    $("#errormsgdiv").html("");
                    if (data.length == 2) {
                        $("#errormsgdiv").append("<p class='col-sm-12 form-group text-danger'>" + data[1] + "</p>");
                    }
                    if (data.length == 3) {
                        $("#NotVerifiedSection").removeClass("hidden");
                        $("#errormsgdiv").append("<p class='col-sm-12 form-group text-danger'>" + data[1] + "</p>");
                    }
                    localStorage.setItem("frontloginsuccess", null);
                    $("#btnFrontlogin").html("Login now");
                }
            }
        }
    });
});

function CheckDetailsInCart(ptype) {
    $.ajax({
        type: "Post",
        url: "/Default/CheckDetailsInCart",
        data: '{pagetype: ' + JSON.stringify(ptype) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    $(".cartcounter").html(data[1]).addClass("shake");
                    $("#detailsofcart").html(data[2]);
                    $("#cartprice").html(data[3]);
                }
                else {
                    $(".cartcounter").html(data[1]);
                    $("#detailsofcart").html(data[2]);
                    $("#cartprice").html(data[3]);
                    $(".cartcounter").removeClass("shake");
                }
            }
            else {
                $(".cartcounter").removeClass("shake");
            }
        }
    });
}

function CheckUserLogin() {
    if (localStorage.getItem("frontloginsuccess") != null) {
        $(".blanckman").addClass("hidden");
        $(".loginsection").addClass("hidden");
        $(".logoutsection").removeClass("hidden");
        CheckDetailsInCart('front');
    }
    else {
        $(".loginsection").removeClass("hidden");
        $(".logoutsection").addClass("hidden");
        localStorage.removeItem("frontloginsuccess");
    }
}

function CheckFrontUserLogin() {
    $.ajax({
        type: "Post",
        url: "/Default/CheckFrontUserLogin",
        //data: '{pagetype: ' + JSON.stringify(ptype) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    localStorage.setItem("frontloginsuccess", data[1]);
                    CheckUserLogin();
                }
                else {
                    localStorage.removeItem("frontloginsuccess");
                    window.location.href = "/";
                }
            }
        }
    });
}

function LogoutFrontendLogin() {
    $.ajax({
        type: "Post",
        url: "/Default/LogoutFrontendLogin",
        //data: '{pagetype: ' + JSON.stringify(ptype) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data == "true") {
                    localStorage.removeItem("frontloginsuccess");
                    window.location.href = "/";
                }
            }
        }
    });
}

$("#frontlogout").click(function () {
    //localStorage.removeItem("selectedstateid");
    //localStorage.removeItem("selectedstatename");
    localStorage.removeItem("pincodeval");
    LogoutFrontendLogin();
});


//==================Sign up Section==============================
function FrontRegSubmitEvt() {
    var thisbutton = $("#Signbtn")
    if (CheckFocusCancellationBlankValidation('txtSignUpFirstName')) return !1;
    if (CheckFocusCancellationBlankValidation('txtSignUpLastName')) return !1;
    if (CheckFocusCancellationBlankValidation('txtSignUpEmail')) return !1;
    if (CheckEmailValidatoin("txtSignUpEmail")) return !1;
    if (CheckFocusCancellationBlankValidation('txtSignUpMobile')) return !1;
    if (CheckFocusCancellationBlankValidation('txtSignUpUserId')) return !1;
    if (CheckFocusCancellationBlankValidation('txtSignUpPassword')) return !1;
    if (CheckFocusCancellationBlankValidation('txtSignUpCnfPassword')) return !1;
    if (CheckSamePasswordValidation("txtSignUpPassword", "txtSignUpCnfPassword")) return !1;
    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");
    $.ajax({
        type: "Post",
        url: "/MemberMaster/UserRegistration",
        data: '{firstname: ' + JSON.stringify($("#txtSignUpFirstName").val()) + ',lastname: ' + JSON.stringify($("#txtSignUpLastName").val()) + ',emailid: ' + JSON.stringify($("#txtSignUpEmail").val()) + ',mobileno: ' + JSON.stringify($("#txtSignUpMobile").val()) + ',userid: ' + JSON.stringify($("#txtSignUpUserId").val()) + ',regpassword: ' + JSON.stringify($("#txtSignUpPassword").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data[0] == "true") {
                $("#txtMsgHeadersignup").addClass("hidden");
                $("#Signinform").addClass("hidden");
                $("#txtMsgHeadersignveryfy").removeClass("hidden");
                $("#Verifyform").removeClass("hidden");
                $("#txtuseridotp").val($("#txtSignUpUserId").val());
                $("#txtuseridotp").prop("disabled", true);
                $("#txtpasswordotp").val($("#txtSignUpPassword").val());
                $("#txtpasswordotp").prop("disabled", true);

                $(".hasotpdetails").removeClass("hidden");
                $(".hidesendotpbutton").addClass("hidden");
            }
            else {
                $("#errorsingup").append("<div class='col-sm-12 form-group'><p>Error! there is an error occurred.</p></div>");
            }
        }
    });
}

function SendVerificationCode() {
    $("#perrormsg").html("");
    if (CheckFocusCancellationBlankValidation('txtuseridotp')) return !1;
    if (CheckFocusCancellationBlankValidation('txtpasswordotp')) return !1;

    $.ajax({
        type: "Post",
        url: "/Account/SendVerificationCode",
        data: '{userid: ' + JSON.stringify($("#txtuseridotp").val()) + ',password: ' + JSON.stringify($("#txtpasswordotp").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data[0] == "true") {
                $("#txtuseridotp").prop("disabled", true);
                $("#txtpasswordotp").prop("disabled", true);
                $(".hidesendotpbutton").addClass("hidden");
                $(".hasotpdetails").removeClass("hidden");
            }
            else {
                $("#perrormsg").html(data[1]).css("color", "red");
            }
        }
    });
}

function ClickNotVerified() {
    $("#txtMsgHeader").addClass("hidden");
    $("#loginform").addClass("hidden");
    $("#txtMsgHeadersignup").addClass("hidden");
    $("#Verifyform").removeClass("hidden");
    $("#txtMsgHeadersignveryfy").removeClass("hidden");
}

function FrontVerifySubmitEvt() {
    $("#perrormsg").html("");
    if (CheckFocusCancellationBlankValidation('txtuseridotp')) return !1;
    if (CheckFocusCancellationBlankValidation('txtpasswordotp')) return !1;
    if (CheckFocusCancellationBlankValidation('txtemailotp')) return !1;
    if (CheckFocusCancellationBlankValidation('txtsmsotp')) return !1;

    var uesrid = null; var password = null;

    if ($("#txtSignUpUserId").val() != "" && $("#txtSignUpPassword").val() != "") {
        uesrid = $("#txtSignUpUserId").val();
        password = $("#txtSignUpPassword").val();
    }
    else {
        uesrid = $("#txtuseridotp").val();
        password = $("#txtpasswordotp").val();
    }

    $("#VarifyOtp").html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>").prop("disabled", true);

    $.ajax({
        type: "Post",
        url: "/Account/ProcessVerifyOtpValid",
        data: '{emailotp: ' + JSON.stringify($("#txtemailotp").val()) + ',mobileotp: ' + JSON.stringify($("#txtsmsotp").val()) + ',userid: ' + JSON.stringify(uesrid) + ',password: ' + JSON.stringify(password) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    $.ajax({
                        type: "Post",
                        url: "/Account/ProcessFrontEndLogin",
                        data: '{userid: ' + JSON.stringify(uesrid) + ',password: ' + JSON.stringify(password) + '}',
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        success: function (data) {
                            if (data != null) {
                                if (data[0] == "true") {
                                    localStorage.setItem("frontloginsuccess", "userlogin");
                                    //$(".closefsignin").click();
                                    window.location.reload(true);
                                }
                            }
                        }
                    });
                }
                else {
                    $("#perrormsg").html(data[1]).css("color", "red");
                    $("#VarifyOtp").html("Verify").prop("disabled", false);
                }
            }
        }
    });
}

function CheckFrontUserId() {
    setTimeout(function () {
        $.ajax({
            type: "Post",
            url: "/MemberMaster/CheckUserIdExist",
            data: '{userid: ' + JSON.stringify($("#txtSignUpUserId").val()) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data[0] == "true") {
                        $(".useridexist").html(data[1]);
                        $(".useridexist").addClass(" available").removeClass("hidden");
                    }
                    else {
                        $(".useridexist").html("");
                        $(".useridexist").removeClass(" available").addClass("hidden");
                    }
                }
            }
        });
    }, 100);
}

$(document.body).on('change', ".cartquant", function (e) {
    var cartid = $(this).data('cartid');
    var rowquantity = $(this).val();
    var rowprice = $("#price_" + cartid).data("rowprice");
    $("#txtCartQuantity_" + cartid).prop("disabled", true);
    $(".spiiner_" + cartid).removeClass("hidden");

    setTimeout(function () {
        $.ajax({
            type: "Post",
            url: "/Default/UpdateAddToCart",
            data: '{cartid: ' + JSON.stringify(cartid) + ',quantity: ' + JSON.stringify(rowquantity) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $("#txtCartQuantity_" + cartid).prop("disabled", false);
                if (data != null) {
                    if (data == "true") {
                        var rowTotalPrice = rowquantity * parseFloat(rowprice);
                        $("#price_" + cartid).html(rowTotalPrice.toFixed(2));

                        var otherTotalPrice = 0;
                        //$.each(".rowprice", function (i, val) {
                        $(".rowprice").each(function (i, val) {
                            var looprowcartid = $(this).data('rowcartid');
                            if (looprowcartid != cartid) {
                                var looprowquantity = $("#txtCartQuantity_" + looprowcartid).val();
                                var looprowprice = $(this).data('rowprice');

                                otherTotalPrice = otherTotalPrice + (looprowquantity * parseFloat(looprowprice));
                            }
                        });

                        rowTotalPrice = (rowTotalPrice + otherTotalPrice).toFixed(2);
                        $("#spsubtotal").html(rowTotalPrice);
                        //$("#sptotal").html(Math.ceil(rowTotalPrice) + ".00");
                        //$("#cartprice").html(Math.ceil(rowTotalPrice) + ".00");
                        $("#sptotal").html(rowTotalPrice);
                        $("#cartprice").html(rowTotalPrice);
                    }
                }
                $(".spiiner_" + cartid).addClass("hidden");
            }
        });
    }, 100);
});

$(document.body).on('keyup', ".cartquant", function (e) {
    var cartid = $(this).data('cartid');
    var rowquantity = $(this).val();
    if (rowquantity <= 0) {
        $(this).val("1");
        rowquantity = 1;
    }
    var rowprice = $("#price_" + cartid).data("rowprice");
    $("#txtCartQuantity_" + cartid).prop("disabled", true);
    $(".spiiner_" + cartid).removeClass("hidden");

    setTimeout(function () {
        $.ajax({
            type: "Post",
            url: "/Default/UpdateAddToCart",
            data: '{cartid: ' + JSON.stringify(cartid) + ',quantity: ' + JSON.stringify(rowquantity) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $("#txtCartQuantity_" + cartid).prop("disabled", false);
                if (data != null) {
                    if (data == "true") {
                        var rowTotalPrice = rowquantity * parseFloat(rowprice);
                        $("#price_" + cartid).html(rowTotalPrice.toFixed(2));

                        var otherTotalPrice = 0;
                        //$.each(".rowprice", function (i, val) {
                        $(".rowprice").each(function (i, val) {
                            var looprowcartid = $(this).data('rowcartid');
                            if (looprowcartid != cartid) {
                                var looprowquantity = $("#txtCartQuantity_" + looprowcartid).val();
                                var looprowprice = $(this).data('rowprice');

                                otherTotalPrice = otherTotalPrice + (looprowquantity * parseFloat(looprowprice));
                            }
                        });

                        rowTotalPrice = (rowTotalPrice + otherTotalPrice).toFixed(2);
                        $("#spsubtotal").html(rowTotalPrice);
                        //$("#sptotal").html(Math.ceil(rowTotalPrice) + ".00");
                        //$("#cartprice").html(Math.ceil(rowTotalPrice) + ".00");
                        $("#sptotal").html(rowTotalPrice);
                        $("#cartprice").html(rowTotalPrice);
                    }
                }
                $(".spiiner_" + cartid).addClass("hidden");
            }
        });
    }, 100);
});

$(document.body).on('click', ".delrowcart", function (e) {
    var cartid = $(this).data('delrowcartid');
    $(".delrowicon_" + cartid).removeClass("fa-trash").addClass(" fa-spin fa-spinner");

    setTimeout(function () {
        $.ajax({
            type: "Post",
            url: "/Default/DeleteAddToCart",
            data: '{cartid: ' + JSON.stringify(cartid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data == "true") {
                        CheckDetailsInCart("logedin");
                    }
                    else {
                        $(".delrowicon_" + cartid).removeClass(" fa-spin fa-spinner").addClass(" fa-trash");
                    }
                }
            }
        });
    }, 200);
});

function CheckCurrUrl() {
    var pathname = window.location.pathname;
    if (pathname == "/default/home" || pathname == "/") {
        $(".showmenubar").addClass(" in ");
    } else {
        $(".showmenubar").removeClass("in");
    }

    if (pathname == "/place-order/checkout") {
        $(".popupcartshow").addClass("hidden");
    }
}

$(".popupcartclose").click(function () {
    var pathname = window.location.pathname;
    if (pathname == "/place-order/checkout") {
        window.location.reload();
    }
});

//======================Checkout======================================

$(document.body).on('click', "#btnCheckout", function (e) {
    $(this).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>").prop("disabled", true);
});



//======================Search on Master Page======================================

//$("#txtSearchProduct").autocomplete({
//    minLength: 3,
//    source: function (request, response) {
//        $(".searchwaiting").removeClass("hidden");
//        $.ajax({
//            url: "/Default/GetProductSearchData",
//            type: "POST",
//            dataType: "json",
//            data: { prefix: request.term },
//            success: function (data) {
//                response($.map(data, function (item) {
//                    return {
//                        label: item.Text,
//                        value: $("#txtSearchProduct").val("Please wait...").prop("disable", true)
//                    };
//                }));
//                $(".searchwaiting").addClass("hidden");
//            }
//        });
//    },
//    select: function (event, ui) {
//        $("#txtSearchProduct").val("");
//        $("#txtSearchProduct").val("Please wait...").prop("disable", true);
//    },
//    response: function (event, ui) {
//        if (!ui.content.length) {
//            var noResult = { value: "", label: "No product matching your request" };
//            ui.content.push(noResult);
//        }
//    }
//}).data("ui-autocomplete")._renderItem = function (div, item) {
//    return $("<div class='product-thumb insp-cat col-xs-12' style='margin-bottom: 5px;padding: 10px;'>")
//        .data("<div class='insp-search'></div>", item)
//        .append(item.label)
//        .appendTo(div);
//};

//style = 'padding: 5px;border-bottom: 1px solid #ff6795;' > <i class='fa fa-cart-plus' style='font-size: 20px;color: #ff6795;'></i> & nbsp;


$(".txtsearchproduct").keyup(function () {
    var searchinput = $(this).val();
    if (searchinput != "") {
        if (searchinput.length >= 3) {
            $(".searchwaiting").removeClass("hidden");
            $.ajax({
                url: "/Default/GetProductSearchData",
                type: "POST",
                dataType: "json",
                data: { prefix: searchinput },
                success: function (data) {
                    if (data != null) {
                        if (data[0] == "false") {
                            $(".insp-search-result").css("height", "85px");
                        }
                        else {
                            $(".insp-search-result").css("height", "450px");
                        }
                        $(".insp-search-result").html(data[1]);
                        $(".insp-search-result").removeClass("hidden");
                        $(".topsearchicon").removeClass("fa-search").addClass(" fa-close");
                    }
                    $(".searchwaiting").addClass("hidden");
                }
            });
        }
        else {
            $(".topsearchicon").addClass(" fa-search").removeClass("fa-close");
        }
    }
    else {
        $(".topsearchicon").addClass(" fa-search").removeClass("fa-close");
    }
});

$(".btnTopSearchActive").click(function () {
    if ($(".topsearchicon").hasClass("fa-close")) {
        $(".txtsearchproduct").val("");
        $(".topsearchicon").addClass(" fa-search").removeClass("fa-close");
        $(".insp-search-result").addClass("hidden");
        $(".insp-search-result").html("");
    }
});


function CloseLoginPopup() {
    window.location.reload();
}

var _tick = null;
function StartCountDown(startSecond) {
    clearInterval(_tick);
    $("#secRemaing").html("");
    $("#minRemaing").html("");
    $(".strcountdown").removeClass("hidden");
    var remSeconds = startSecond;
    var secondCounters = remSeconds % 60;
    function formarteNumber(number) { if (number < 10) return '0' + number; else return '' + number; }
    function startTick() {
        $("#secRemaing").text(formarteNumber(secondCounters));
        $("#minRemaing").text(formarteNumber(parseInt(remSeconds / 60)));
        if (secondCounters == 0) { secondCounters = 60; }
        _tick = setInterval(function () {
            if (remSeconds > 0) {
                remSeconds = remSeconds - 1;
                secondCounters = secondCounters - 1;
                $("#secRemaing").text(formarteNumber(secondCounters));
                $("#minRemaing").text(formarteNumber(parseInt(remSeconds / 60)));
                if (secondCounters == 0) { secondCounters = 60; }
            }
            else {
                clearInterval(_tick);
            }
        }, 1000);
    }
    startTick();
}