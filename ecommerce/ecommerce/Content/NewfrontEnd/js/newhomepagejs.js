﻿//$(".frontaddtocart").click(function () 

$(document.body).on('click', ".frontaddtocart", function (e) {
    if (localStorage.getItem("frontloginsuccess") != null) {
        //$(".addprodincart").click();
        var thisproid = $(this).data('proid');
        var proDetailsspan = $("#frontprodetails_" + thisproid);

        $(".fasbasket_" + thisproid).removeClass("fa-shopping-basket").addClass(" fa-spinner fa-spin ");
        $(".fasbasket_" + thisproid).prop("disabled", true);

        var addcart = {};
        addcart.ProductId = $(proDetailsspan).data('productid');
        addcart.ProductName = $(proDetailsspan).data('productname');
        addcart.ProductImgUrl = $(proDetailsspan).data('productimgurl');
        addcart.SalePrice = $(proDetailsspan).data('saleprice');
        addcart.Discount = $(proDetailsspan).data('discountper');
        addcart.DiscountPrice = $(proDetailsspan).data('discountprice');
        addcart.UnitId = $(proDetailsspan).data('unitid');
        addcart.UnitName = $(proDetailsspan).data('unitname');
        addcart.Quantity = $("#fqty_" + addcart.ProductId).val();

        $.ajax({
            type: "Post",
            url: "/Default/ProcessToAddToCart",
            data: '{addcart: ' + JSON.stringify(addcart) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data == "true") {
                        CheckDetailsInCart("logedin");
                        $("#fqty_" + addcart.ProductId).val("1");
                    }
                }
                setTimeout(function () {
                    $(".fasbasket_" + thisproid).addClass(" fa-shopping-basket ").removeClass(" fa-spinner fa-spin");
                    $(".fasbasket_" + thisproid).prop("disabled", false);
                }, 500);
            }
        });
    }
    else {
        $("#btnFrontUserLogin").click();
    }
});

$(function () {
    BindHomePageProducts();
});

function BindHomePageProducts() {
    //var stateid = null; var statename = null;
    //if (localStorage.getItem("selectedstateid") != null && localStorage.getItem("selectedstatename") != null) {
    //    stateid = localStorage.getItem("selectedstateid");
    //    statename = localStorage.getItem("selectedstatename");
    //}

    var pincodeval = null; 
    if (localStorage.getItem("pincodeval") != null) {
        pincodeval = localStorage.getItem("pincodeval");
    }

    $.ajax({
        type: "Post",
        url: "/Default/HomeAllProductBind",
        //data: '{stateid: ' + JSON.stringify(stateid) + ',statename: ' + JSON.stringify(statename) + '}',
        data: '{pincode: ' + JSON.stringify(pincodeval) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $(".bindtopcategory").html(data[0]);
                $(".bindfeaturedmenus").html(data[1]);
                $(".bindallfeatureproduct").html(data[2]);                
            }
        }
    });
}