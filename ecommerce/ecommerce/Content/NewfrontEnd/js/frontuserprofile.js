﻿$(".chngebtn").click(function () {
    $(".boxinfo").removeClass("hidden");
    $(".updateinfo").addClass("hidden");
});
$(".updatechange").click(function () {
    $(".boxinfo").addClass("hidden");
    $(".updateinfo").removeClass("hidden");
});
$(".mangaeadd").click(function () {
    $(".mAddrs").removeClass("hidden");
    $(".prflinfo").addClass("hidden");
});
$(".profileinfor").click(function () {
    $(".mAddrs").addClass("hidden");
    $(".prflinfo").removeClass("hidden");
});
$(".changeadd").click(function () {
    $(".oldaddress").addClass("hidden");
    $(".newaddchange").removeClass("hidden");
});
$(".cancel").click(function () {
    $(".oldaddress").removeClass("hidden");
    $(".newaddchange").addClass("hidden");
});

$(function () {
    if (localStorage.getItem("frontloginsuccess") != null) {
        UserShippingAddress();
    }
    else {
        localStorage.removeItem("frontloginsuccess");
        window.location.href = "/";
    }
});

function UserShippingAddress() {
    $(".oldadd").html("");
    $.ajax({
        type: "Post",
        url: "/UserProfile/UserprofilepageDetail",      
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {                    
                    $(".oldadd").html(data[1]);                   
                }
                else {
                    if (data[1] == "logout") {
                        localStorage.removeItem("frontloginsuccess");
                        window.location.href = "/";
                    }
                }
            }
        }
    });
}
function BindShippingAddress(loginid, userid) {
    $(".oldadd").html("");
    $.ajax({
        type: "Post",
        url: "/UserProfile/GetJsonGetShippingAddress",
        data: '{loginid: ' + JSON.stringify(loginid) + ',userid: ' + JSON.stringify(userid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    $(".oldadd").html(data[1]);
                    $(".oldaddress").removeClass("hidden");
                    $(".newaddchange").addClass("hidden");
                }
                else {
                    if (data[1] == "logout") {
                        localStorage.removeItem("frontloginsuccess");
                        window.location.href = "/";
                    }
                }
            }
        }
    });
}

$("#btnCheckoutAddAddress").click(function () {
    if (CheckFocusChekoutBlankValidation("ShippingAddress_FirstName")) return !1;
    if (CheckFocusChekoutBlankValidation("ShippingAddress_LastName")) return !1;
    if (CheckFocusChekoutBlankValidation("ShippingAddress_EmailId")) return !1;
    if (CheckEmailValidatoin("ShippingAddress_EmailId")) return !1;
    if (CheckFocusChekoutBlankValidation("ShippingAddress_MobileNo")) return !1;
    if (CheckFocusChekoutBlankValidation("ShippingAddress_Address")) return !1;
    if (CheckFocusChekoutBlankValidation("ShippingAddress_City")) return !1;
    if (CheckFocusChekoutBlankValidation("ShippingAddress_StateId")) return !1;
    if (CheckFocusChekoutBlankValidation("ShippingAddress_PinCode")) return !1;

    $("#btnCheckoutAddAddress").html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>").prop("disabled", true);

    var shippingadd = {};

    shippingadd.FirstName = $("#ShippingAddress_FirstName").val();
    shippingadd.LastName = $("#ShippingAddress_LastName").val();
    shippingadd.EmailId = $("#ShippingAddress_EmailId").val();
    shippingadd.MobileNo = $("#ShippingAddress_MobileNo").val();
    shippingadd.AlternateMobileNo = $("#ShippingAddress_AlternateMobileNo").val();
    shippingadd.Address = $("#ShippingAddress_Address").val();
    shippingadd.City = $("#ShippingAddress_City").val();
    shippingadd.StateId = $("#ShippingAddress_StateId").val();
    shippingadd.State = $("#ShippingAddress_StateId option:selected").text();
    shippingadd.PinCode = $("#ShippingAddress_PinCode").val();
    shippingadd.Landmark = $("#ShippingAddress_Landmark").val();
    shippingadd.AddressType = $("#ShippingAddress_AddressType").val();
    shippingadd.IsDefault = true;


    $.ajax({
        type: "Post",
        url: "/UserProfile/CreateShippingAddress",
        data: '{shippingadd: ' + JSON.stringify(shippingadd) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    BindShippingAddress(data[1], data[2]);
                    ClearAllFields();
                }
                else {
                    alert("Failed!")
                }

                $("#btnCheckoutAddAddress").html("SAVE AND DELIVER HERE").prop("disabled", false);
            }
        }
    });
});

function ClearAllFields() {
    $("#ShippingAddress_FirstName").val("");
    $("#ShippingAddress_LastName").val("");
    $("#ShippingAddress_EmailId").val("");
    $("#ShippingAddress_MobileNo").val("");
    $("#ShippingAddress_AlternateMobileNo").val("");
    $("#ShippingAddress_Address").val("");
    $("#ShippingAddress_City").val("");
    $("#ShippingAddress_StateId").val("");
    $("#ShippingAddress_PinCode").val("");
    $("#ShippingAddress_Landmark").val("");
}