﻿
function MinusQytPro() {
    var currqyt = parseInt($("#input-quantity").val());
    if (currqyt > 1) {
        $(".btnqytminus").prop("disabled", false);
        $("#input-quantity").val(currqyt - 1);
    }
    else {
        $(".btnqytminus").prop("disabled", true);
    }
}

function PlusQytPro() {
    var currqyt = parseInt($("#input-quantity").val()) + 1;

    if (currqyt > 1) {
        $(".btnqytminus").prop("disabled", false);
        $("#input-quantity").val(currqyt);
    }
    else {
        $(".btnqytminus").prop("disabled", true);
    }
}

$("#input-quantity").keyup(function () {
    var currqyt = parseInt($(this).val());

    if (currqyt > 1) {
        $(".btnqytminus").prop("disabled", false);
    }
    else if (currqyt <= 0) {
        $("#input-quantity").val(1);
    }
    else {
        $(".btnqytminus").prop("disabled", true);
    }
});

$(".addtocartdeailpage").click(function () {
    if (localStorage.getItem("frontloginsuccess") != null) {
        //$(".addprodincart").click();
        var thisproid = $(this).data('proid');
        var proDetailsspan = $("#frontprodetails_" + thisproid);

        $(".fasbasket_" + thisproid).removeClass("fa-shopping-cart").addClass(" fa-spinner fa-spin ");
        $(".fasbasket_" + thisproid).prop("disabled", true);

        var addcart = {};
        addcart.ProductId = $(proDetailsspan).data('productid');
        addcart.ProductName = $(proDetailsspan).data('productname');
        addcart.ProductImgUrl = $(proDetailsspan).data('productimgurl');
        addcart.SalePrice = $(proDetailsspan).data('saleprice');
        addcart.Discount = $(proDetailsspan).data('discountper');
        addcart.DiscountPrice = $(proDetailsspan).data('discountprice');
        addcart.UnitId = $(proDetailsspan).data('unitid');
        addcart.UnitName = $(proDetailsspan).data('unitname');
        addcart.Quantity = $(".fqty_" + addcart.ProductId).val();

        $.ajax({
            type: "Post",
            url: "/Default/ProcessToAddToCart",
            data: '{addcart: ' + JSON.stringify(addcart) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data == "true") {
                        CheckDetailsInCart("logedin");
                        $("#fqty_" + addcart.ProductId).val("1");
                    }
                }
                setTimeout(function () {
                    $(".fasbasket_" + thisproid).addClass(" fa-shopping-cart ").removeClass(" fa-spinner fa-spin");
                    $(".fasbasket_" + thisproid).prop("disabled", false);
                }, 500);
            }
        });
    }
    else {
        $("#btnFrontUserLogin").click();
    }
});

$(".writeareviewbutton").click(function () {
    if (localStorage.getItem("frontloginsuccess") != null) {
        var productid = $(this).data("productwritereviewid");

        if ($(this).hasClass("openreviewform")) {
            $("#reviewform").addClass("hidden");
            $("#reviewdatasection").removeClass("hidden");
            $(this).html("Write A Review").removeClass("openreviewform");
            GetReviewList(productid);
        }
        else {
            $("#reviewform").removeClass("hidden");
            $("#reviewdatasection").addClass("hidden");
            $(this).html("Show Reviews").addClass("openreviewform");
        }
    }
    else {
        $("#btnFrontUserLogin").click();
    }
});

function SubmitReview() {
    var thisbutton = $("#btnReviewSubmit");
    if (CheckBlankValidation("txtReviewName")) return !1;
    if (CheckBlankValidation("txtReviewEmail")) return !1;
    if (CheckEmailValidatoin("txtReviewEmail")) return !1;
    if (CheckBlankValidation("txtReviewContent")) return !1;

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin' style='color: #fff;'></i>").prop("disabled", true);

    var productid = $(thisbutton).data("productreviewid");

    $.ajax({
        type: "Post",
        url: "/Default/InsertReviewDetail",
        data: '{productid:' + JSON.stringify(productid) + ',name: ' + JSON.stringify($("#txtReviewName").val()) + ',email: ' + JSON.stringify($("#txtReviewEmail").val()) + ',content: ' + JSON.stringify($("#txtReviewContent").val()) + ',rating: ' + JSON.stringify($(".selectrating:checked").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data == "true") {
                    $("#reviewformmessage").html("<h4 style='color:#ff6795;'>Your review has been submitted successfully.</h4>").removeClass("hidden");
                    ClearAll();
                }
                else {
                }
            }
            $(thisbutton).html("Submit").prop("disabled", false);
        }
    });
}

function ClearAll() {
    $("#reviewform").addClass("hidden");
    $("#txtReviewName").val("");
    $("#txtReviewEmail").val("");
    $("#txtReviewContent").val("");
    $('input:radio[name=rating]')[0].checked = true;
}

function GetReviewList(productid) {
    $("#reviewdatasection").html("");
    $.ajax({
        type: "Post",
        url: "/Default/GetReviewDetail",
        data: '{productid:' + JSON.stringify(productid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null && data != "") {
                $("#reviewdatasection").html(data);
            }
            else {
                $("#reviewdatasection").html("There are no reviews for this product.");
            }
        }
    });
}

$("#clickreviewssection").click(function () {
    $("#reviewform").addClass("hidden");
    $("#reviewdatasection").removeClass("hidden");
    $("#reviewformmessage").html("");
    $(".writeareviewbutton").html("Write A Review").removeClass("openreviewform");

    var productid = $(this).data("productreviewsection");
    GetReviewList(productid);
});