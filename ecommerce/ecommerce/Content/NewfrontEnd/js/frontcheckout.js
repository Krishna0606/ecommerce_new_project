﻿$(".changeadd").click(function () {
    $(".oldadd").addClass("hidden");
    $(".newaddchange").removeClass("hidden");
    //$("#changeaddress").addClass("hidden")
});
$(".cancel").click(function () {
    $(".oldadd").removeClass("hidden");
    $(".newaddchange").addClass("hidden");
    //$("#changeaddress").removeClass("hidden")
});

$(function () {
    if (localStorage.getItem("frontloginsuccess") != null) {
        CheckCheckoutDel();
    }
    else {
        localStorage.removeItem("frontloginsuccess");
        window.location.href = "/";
    }
});

function CheckCheckoutDel() {
    $(".oldadd").html("");
    $.ajax({
        type: "Post",
        url: "/Default/CheckCheckoutPageDetail",
        //data: '{addcart: ' + JSON.stringify(addcart) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    CheckDetailsInCart("logedin");
                    $(".oldadd").html(data[1]);
                    $("#bindcartdelintable").html(data[2]);
                    $("#finalPayAmountSection").html(data[3]);
                }
                else {
                    if (data[1] == "logout") {
                        localStorage.removeItem("frontloginsuccess");
                        window.location.href = "/";
                    }
                }
            }
        }
    });
}

function BindFinalCheckoutAmount() {
    $("#finalPayAmountSection").html("");
    $.ajax({
        type: "Post",
        url: "/Default/BindFinalPriceDetailOnCheckout",
        data: '{loginid: ' + JSON.stringify(0) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data != "logout") {
                    $("#finalPayAmountSection").html(data);
                }
                else {
                    localStorage.removeItem("frontloginsuccess");
                    window.location.href = "/";
                }
            }
        }
    });
}

function BindShippingAddress(loginid, userid) {
    $(".oldadd").html("");
    $.ajax({
        type: "Post",
        url: "/Default/GetJsonGetShippingAddress",
        data: '{loginid: ' + JSON.stringify(loginid) + ',userid: ' + JSON.stringify(userid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    $(".oldadd").html(data[1]);
                    $(".oldadd").removeClass("hidden");
                    $(".newaddchange").addClass("hidden");
                }
                else {
                    if (data[1] == "logout") {
                        localStorage.removeItem("frontloginsuccess");
                        window.location.href = "/";
                    }
                }
            }
        }
    });
}

$("#btnCheckoutAddAddress").click(function () {
    if (CheckFocusChekoutBlankValidation("ShippingAddress_FirstName")) return !1;
    if (CheckFocusChekoutBlankValidation("ShippingAddress_LastName")) return !1;
    if (CheckFocusChekoutBlankValidation("ShippingAddress_EmailId")) return !1;
    if (CheckEmailValidatoin("ShippingAddress_EmailId")) return !1;
    if (CheckFocusChekoutBlankValidation("ShippingAddress_MobileNo")) return !1;
    if (CheckFocusChekoutBlankValidation("ShippingAddress_Address")) return !1;
    if (CheckFocusChekoutBlankValidation("ShippingAddress_City")) return !1;
    if (CheckFocusChekoutBlankValidation("ShippingAddress_StateId")) return !1;
    if (CheckFocusChekoutBlankValidation("ShippingAddress_PinCode")) return !1;

    $("#btnCheckoutAddAddress").html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>").prop("disabled", true);

    var shippingadd = {};

    shippingadd.FirstName = $("#ShippingAddress_FirstName").val();
    shippingadd.LastName = $("#ShippingAddress_LastName").val();
    shippingadd.EmailId = $("#ShippingAddress_EmailId").val();
    shippingadd.MobileNo = $("#ShippingAddress_MobileNo").val();
    shippingadd.AlternateMobileNo = $("#ShippingAddress_AlternateMobileNo").val();
    shippingadd.Address = $("#ShippingAddress_Address").val();
    shippingadd.City = $("#ShippingAddress_City").val();
    shippingadd.StateId = $("#ShippingAddress_StateId").val();
    shippingadd.State = $("#ShippingAddress_StateId option:selected").text();
    shippingadd.PinCode = $("#ShippingAddress_PinCode").val();
    shippingadd.Landmark = $("#ShippingAddress_Landmark").val();
    shippingadd.AddressType = $("#ShippingAddress_AddressType").val();
    shippingadd.IsDefault = true;


    $.ajax({
        type: "Post",
        url: "/UserProfile/CreateShippingAddress",
        data: '{shippingadd: ' + JSON.stringify(shippingadd) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    BindShippingAddress(data[1], data[2]);
                    ClearAllFields();
                }
                else {
                    alert("Failed!")
                }

                $("#btnCheckoutAddAddress").html("SAVE AND DELIVER HERE").prop("disabled", false);
            }
        }
    });
});

function ClearAllFields() {
    $("#ShippingAddress_FirstName").val("");
    $("#ShippingAddress_LastName").val("");
    $("#ShippingAddress_EmailId").val("");
    $("#ShippingAddress_MobileNo").val("");
    $("#ShippingAddress_AlternateMobileNo").val("");
    $("#ShippingAddress_Address").val("");
    $("#ShippingAddress_City").val("");
    $("#ShippingAddress_StateId").val("");
    $("#ShippingAddress_PinCode").val("");
    $("#ShippingAddress_Landmark").val("");
}

$(document.body).on('change', ".checkoutcartquant", function (e) {
    var cartid = $(this).data('cartid');
    var rowquantity = $(this).val();
    var rowprice = $("#checkoutprice_" + cartid).data("checkoutrowprice");
    $("#txtCheckoutCartQuantity_" + cartid).prop("disabled", true);
    $(".spiiner_" + cartid).removeClass("hidden");

    setTimeout(function () {
        $.ajax({
            type: "Post",
            url: "/Default/UpdateAddToCart",
            data: '{cartid: ' + JSON.stringify(cartid) + ',quantity: ' + JSON.stringify(rowquantity) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $("#txtCheckoutCartQuantity_" + cartid).prop("disabled", false);
                if (data != null) {
                    if (data == "true") {
                        var rowTotalPrice = rowquantity * parseFloat(rowprice);
                        $("#checkoutprice_" + cartid).html(rowTotalPrice.toFixed(2));

                        var otherTotalPrice = 0;
                        //$.each(".rowprice", function (i, val) {
                        $(".checkoutrowprice").each(function (i, val) {
                            var looprowcartid = $(this).data('checkoutrowcartid');
                            if (looprowcartid != cartid) {
                                var looprowquantity = $("#txtCheckoutCartQuantity_" + looprowcartid).val();
                                var looprowprice = $(this).data('checkoutrowprice');

                                otherTotalPrice = otherTotalPrice + (looprowquantity * parseFloat(looprowprice));
                            }
                        });

                        rowTotalPrice = (rowTotalPrice + otherTotalPrice).toFixed(2);
                        $("#checkoutspsubtotal").html(rowTotalPrice);
                        //$("#sptotal").html(Math.ceil(rowTotalPrice) + ".00");
                        //$("#cartprice").html(Math.ceil(rowTotalPrice) + ".00");
                        //$("#sptotal").html(rowTotalPrice);
                        //$("#cartprice").html(rowTotalPrice);
                        CheckDetailsInCart("logedin");
                        BindFinalCheckoutAmount();
                    }
                }
                $(".spiiner_" + cartid).addClass("hidden");
            }
        });
    }, 100);
});

$(document.body).on('keyup', ".checkoutcartquant", function (e) {
    var cartid = $(this).data('cartid');
    var rowquantity = $(this).val();

    if (rowquantity <= 0) {
        rowquantity = 1;
        $(this).val(rowquantity);
    }

    var rowprice = $("#checkoutprice_" + cartid).data("checkoutrowprice");
    $("#txtCheckoutCartQuantity_" + cartid).prop("disabled", true);
    $(".spiiner_" + cartid).removeClass("hidden");

    setTimeout(function () {
        $.ajax({
            type: "Post",
            url: "/Default/UpdateAddToCart",
            data: '{cartid: ' + JSON.stringify(cartid) + ',quantity: ' + JSON.stringify(rowquantity) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $("#txtCheckoutCartQuantity_" + cartid).prop("disabled", false);
                if (data != null) {
                    if (data == "true") {
                        var rowTotalPrice = rowquantity * parseFloat(rowprice);
                        $("#checkoutprice_" + cartid).html(rowTotalPrice.toFixed(2));

                        var otherTotalPrice = 0;
                        //$.each(".rowprice", function (i, val) {
                        $(".checkoutrowprice").each(function (i, val) {
                            var looprowcartid = $(this).data('checkoutrowcartid');
                            if (looprowcartid != cartid) {
                                var looprowquantity = $("#txtCheckoutCartQuantity_" + looprowcartid).val();
                                var looprowprice = $(this).data('checkoutrowprice');

                                otherTotalPrice = otherTotalPrice + (looprowquantity * parseFloat(looprowprice));
                            }
                        });

                        rowTotalPrice = (rowTotalPrice + otherTotalPrice).toFixed(2);
                        $("#checkoutspsubtotal").html(rowTotalPrice);
                        //$("#sptotal").html(Math.ceil(rowTotalPrice) + ".00");
                        //$("#cartprice").html(Math.ceil(rowTotalPrice) + ".00");
                        //$("#sptotal").html(rowTotalPrice);
                        //$("#cartprice").html(rowTotalPrice);
                        CheckDetailsInCart("logedin");
                        BindFinalCheckoutAmount();
                    }
                }
                $(".spiiner_" + cartid).addClass("hidden");
            }
        });
    }, 100);
});

$(document.body).on('click', ".checkoutdelrowcart", function (e) {
    var cartid = $(this).data('checkoutdelrowcartid');
    $(".checkoutdelrowicon_" + cartid).removeClass("fa-trash").addClass(" fa-spin fa-spinner");

    setTimeout(function () {
        $.ajax({
            type: "Post",
            url: "/Default/DeleteAddToCart",
            data: '{cartid: ' + JSON.stringify(cartid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data == "true") {
                        CheckCheckoutDel();
                    }
                    else {
                        $(".checkoutdelrowicon_" + cartid).removeClass(" fa-spin fa-spinner").addClass(" fa-trash");
                    }
                }
            }
        });
    }, 200);
});

$(document.body).on('click', ".selectaddclass", function (e) {
    var addressid = $(this).data('shipaddid');
    $(this).prop("disabled", true);
    $(".ispinho_" + addressid).removeClass("fa-edit").addClass(" fa-spin fa-spinner");

    setTimeout(function () {
        $.ajax({
            type: "Post",
            url: "/Default/SetDefaultAddress",
            data: '{addressid: ' + JSON.stringify(addressid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $(this).prop("disabled", false);
                if (data != null) {
                    if (data[0] == "true") {
                        BindShippingAddress(data[1], data[2]);
                    }
                    else {
                        if (data[0] == "logout") {
                            localStorage.removeItem("frontloginsuccess");
                            window.location.href = "/";
                        }
                    }
                }
            }
        });
    }, 200);
});

$(document.body).on('click', "#btnPlaceOrderContinue", function (e) {
    if (localStorage.getItem("frontloginsuccess") != null) {
        $(this).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>").prop("disabled", true);

        $.ajax({
            type: "Post",
            url: "/Default/InsertOrderDetails",
            //data: '{addressid: ' + JSON.stringify(addressid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                //$(this).prop("disabled", false);
                if (data != null) {
                    if (data[0] == "true") {
                        //$(".headermsgcheckout").html("your order has been placed successfully.").addClass("text-success");
                        $(".messagecheckout").html("your order has been placed successfully.").addClass("text-success");                       
                        $(".footercheckshow").removeClass("hidden");
                        $(".succesplaceorder").click();
                    }
                    else if (data[0] == "noshippingaddress") {
                        $("#btnPlaceOrderContinue").html("CONTINUE").prop("disabled", false);
                        $(".headermsgcheckout").html("Missing Delivery Address!").addClass("text-danger");
                        $(".messagecheckout").html("Delivery address missing, please add address first.");
                        $(".headernotshow").removeClass("hidden");
                        $(".errofootershow").removeClass("hidden");
                        $(".succesplaceorder").click();
                    }
                    else if (data[0] == "false") {
                        $("#btnPlaceOrderContinue").html("CONTINUE").prop("disabled", false);
                        $(".headermsgcheckout").html("Failed!").addClass("text-danger");
                        $(".messagecheckout").html("Error Occurred");
                        $(".headernotshow").removeClass("hidden");
                        $(".errofootershow").removeClass("hidden");
                        $(".succesplaceorder").click();
                    }
                }
            }
        });
    }
    else {
        localStorage.removeItem("frontloginsuccess");
        window.location.href = "/";
    }
});