﻿
$(document.body).on('click', ".frontaddtocart", function (e) {
    if (localStorage.getItem("frontloginsuccess") != null) {
        //$(".addprodincart").click();
        var thisproid = $(this).data('proid');
        var proDetailsspan = $("#frontaddprodetails_" + $(this).data('proid'));

        $(".fasbasket_" + thisproid).removeClass("fa-shopping-basket").addClass(" fa-spinner fa-spin ");
        $(".fasbasket_" + thisproid).prop("disabled", true);

        var addcart = {};
        addcart.ProductId = $(proDetailsspan).data('productid');
        addcart.ProductName = $(proDetailsspan).data('productname');
        addcart.ProductImgUrl = $(proDetailsspan).data('productimgurl');
        addcart.SalePrice = $(proDetailsspan).data('saleprice');
        addcart.Discount = $(proDetailsspan).data('discountper');
        addcart.DiscountPrice = $(proDetailsspan).data('discountprice');
        addcart.UnitId = $(proDetailsspan).data('unitid');
        addcart.UnitName = $(proDetailsspan).data('unitname');
        addcart.Quantity = $("#fqty_" + addcart.ProductId).val();

        $.ajax({
            type: "Post",
            url: "/Default/ProcessToAddToCart",
            data: '{addcart: ' + JSON.stringify(addcart) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data == "true") {
                        CheckDetailsInCart("logedin");
                        $("#fqty_" + addcart.ProductId).val("1");
                    }
                }
                setTimeout(function () {
                    $(".fasbasket_" + thisproid).addClass(" fa-shopping-basket ").removeClass(" fa-spinner fa-spin");
                    $(".fasbasket_" + thisproid).prop("disabled", false);
                }, 500);
            }
        });
    }
    else {
        $("#btnFrontUserLogin").click();
    }
});

var pincodeval = null;

$(function () {
    GetProducts();
});

function Getbreadcrumb() {
    var menuid = null; var categryid = null; var subcategryid = null;

    var pathname = window.location.pathname.split('/');
    if (pathname.length == 4) { menuid = pathname[3]; }
    else if (pathname.length == 6) { menuid = pathname[3]; categryid = pathname[5]; }
    else if (pathname.length == 8) { menuid = pathname[3]; categryid = pathname[5]; subcategryid = pathname[7]; }

    $.ajax({
        type: "Post",
        url: "/Default/GetBreadCrumb",
        data: '{menuid: ' + JSON.stringify(menuid) + ',categryid: ' + JSON.stringify(categryid) + ',subcategryid: ' + JSON.stringify(subcategryid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null && data != "") {
                $(".breadcrumb").html(data);
            }
        }
    });
}

function GetProducts() {
    FilterActivationSpinnerStart();
    $(".bindproductlist").html("");
    var menuid = null; var categryid = null; var subcategryid = null;

    var pathname = window.location.pathname.split('/');
    if (pathname.length == 4) { menuid = pathname[3]; }
    else if (pathname.length == 6) { menuid = pathname[3]; categryid = pathname[5]; }
    else if (pathname.length == 8) { menuid = pathname[3]; categryid = pathname[5]; subcategryid = pathname[7]; }

    //if (localStorage.getItem("selectedstateid") != null && localStorage.getItem("selectedstatename") != null) {
    //    selectedstateid = localStorage.getItem("selectedstateid");
    //}

    if (localStorage.getItem("pincodeval") != null) {
        pincodeval = localStorage.getItem("pincodeval");
    }

    $.ajax({
        type: "Post",
        url: "/Default/GetProductList",
        data: '{pincode: ' + JSON.stringify(pincodeval) + ',menuid: ' + JSON.stringify(menuid) + ',categryid: ' + JSON.stringify(categryid) + ',subcategryid: ' + JSON.stringify(subcategryid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {            
            Getbreadcrumb();
            if (data != null && data != "") {
                $(".bindproductlist").html(data);
                $(".proupsortby").removeClass("hidden");
                YesProLocation();
            }
            else {
                $(".proupsortby").addClass("hidden");
                NoProLocation();
            }
            GetLeftSideFilter(pincodeval);
            FilterActivationSpinnerEnd();
        }
    });
}

function YesProLocation() {
    $(".noproductlocation").html("");
}
function NoProLocation() {
    $(".noproductlocation").html("<h3>Products are not available in related location '" + localStorage.getItem("pincodeval") + "'.</h3><h3>You can find other location products.</h3><h3>OR</h3><h3>Change your location click on Reset button.</h3>");
}

function GetLeftSideFilter(pincode) {
    $(".leftsidefilter").html("");
    $.ajax({
        type: "Post",
        url: "/Default/GetLeftSideFilter",
        //data: '{stateid: ' + JSON.stringify(tempselectedstateid) + '}',
        data: '{pincode: ' + JSON.stringify(pincode) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $(".leftsidefilter").html(data);
            }
        }
    });
}

var filter = {};

$(document.body).on('click', ".statecheckbox", function (e) {
    var stateCheck = [];

    $(".statecheckbox").each(function () {
        if ($(this).prop("checked") == true) {
            var thisstateid = $(this).data('stateid');
            stateCheck.push(thisstateid);
        }
    });

    filter.StateId = stateCheck;

    BindProductByFilterAction();
});

$(document.body).on('click', ".brandcheckbox", function (e) {
    var brandCheck = [];

    $(".brandcheckbox").each(function () {
        if ($(this).prop("checked") == true) {
            var thisstateid = $(this).data('brandid');
            brandCheck.push(thisstateid);
        }
    });

    filter.BrandId = brandCheck;

    BindProductByFilterAction();
});

$(document.body).on('change', "#ddlMinPrice", function (e) {
    filter.MinPrice = $(this).val();
    BindProductByFilterAction();
});

$(document.body).on('change', "#ddlMaxPrice", function (e) {
    filter.MaxPrice = $(this).val();
    BindProductByFilterAction();
});

$(document.body).on('click', ".areacheckbox", function (e) {
    var areaCheck = [];

    $(".areacheckbox").each(function () {
        if ($(this).prop("checked") == true) {
            var thisarea = $(this).data('areaname');
            areaCheck.push(thisarea);
        }
    });

    filter.Area = areaCheck;

    BindProductByFilterAction();
});

function BindProductByFilterAction() {
    //if (localStorage.getItem("selectedstateid") != null && localStorage.getItem("selectedstatename") != null) {
    //    filter.SelectedStateId = localStorage.getItem("selectedstateid");
    //}
    if (localStorage.getItem("pincodeval") != null) {
        filter.PinCode = localStorage.getItem("pincodeval");
    }

    FilterActivationSpinnerStart();
    $(".bindproductlist").html("");

    CheckFilterActivated();

    $.ajax({
        type: "Post",
        url: "/Default/BindProductByFilterAction",
        data: '{filter: ' + JSON.stringify(filter) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null && data != "") {
                $(".bindproductlist").html(data);
                $(".proupsortby").removeClass("hidden");
                YesProLocation();
            }
            else {
                $(".proupsortby").addClass("hidden");
                NoProLocation();
            }
            FilterActivationSpinnerEnd();
        }
    });
}

function CheckFilterActivated() {
    var isFilteractive = false;

    $(".statecheckbox").each(function () {
        if ($(this).prop("checked") == true) {
            isFilteractive = true;
        }
    });

    $(".areacheckbox").each(function () {
        if ($(this).prop("checked") == true) {
            isFilteractive = true;
        }
    });

    $(".brandcheckbox").each(function () {
        if ($(this).prop("checked") == true) {
            isFilteractive = true;
        }
    });

    if (isFilteractive) {
        $(".filteractivated").removeClass("hidden");
    }
    else {
        $(".filteractivated").addClass("hidden");
    }
}

$(document.body).on('click', ".filteractivated", function (e) {
    $(this).addClass("hidden");
    filter = {};

    $(".statecheckbox").each(function () {
        $(this).prop("checked", false);
    });

    $(".areacheckbox").each(function () {
        $(this).prop("checked", false);
    });

    $(".brandcheckbox").each(function () {
        $(this).prop("checked", false);
    });

    GetProducts();
});

function FilterActivationSpinnerStart() {
    $(".filteractivation").click();
}

function FilterActivationSpinnerEnd() {
    $(".closefilteractivation").click();
}