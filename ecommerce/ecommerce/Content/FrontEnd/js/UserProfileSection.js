﻿
$("#btnShippingAddress").click(function () {
    if (CheckFocusBlankValidation("FirstName")) return !1;
    if (CheckFocusBlankValidation("LastName")) return !1;
    if (CheckFocusBlankValidation("EmailId")) return !1;
    if (CheckEmailValidatoin("EmailId")) return !1;
    if (CheckFocusBlankValidation("MobileNo")) return !1;
    if (CheckFocusBlankValidation("Address")) return !1;
    if (CheckFocusBlankValidation("PinCode")) return !1;

    var shippingadd = {};

    shippingadd.FirstName = $("#FirstName").val();
    shippingadd.LastName = $("#LastName").val();
    shippingadd.EmailId = $("#EmailId").val();
    shippingadd.MobileNo = $("#MobileNo").val();
    shippingadd.AlternateMobileNo = $("#AlternateMobileNo").val();
    shippingadd.Address = $("#Address").val();
    shippingadd.City = $("#City").val();
    shippingadd.StateId = $("#State").val();
    shippingadd.State = $("#State").text();
    shippingadd.PinCode = $("#PinCode").val();
    shippingadd.Landmark = $("#Landmark").val();
    shippingadd.AddressType = $("#AddressType").val();
    shippingadd.IsDefault = $("#IsDefault").val();
    

    $.ajax({
        type: "Post",
        url: "/UserProfile/CreateShippingAddress",
        data: '{shippingadd: ' + JSON.stringify(shippingadd) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data == "true") {

                }
            }
        }
    });
});