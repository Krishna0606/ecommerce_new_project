﻿
$(".frontaddtocart").click(function () {
    if (localStorage.getItem("frontloginsuccess") != null) {
        $(".addprodincart").click();

        var proDetailsspan = $("#frontprodetails_" + $(this).data('proid'));

        var addcart = {};
        addcart.ProductId = $(proDetailsspan).data('productid');
        addcart.ProductName = $(proDetailsspan).data('productname');
        addcart.ProductImgUrl = $(proDetailsspan).data('productimgurl');
        addcart.SalePrice = $(proDetailsspan).data('saleprice');
        addcart.Discount = $(proDetailsspan).data('discountper');
        addcart.DiscountPrice = $(proDetailsspan).data('discountprice');
        addcart.UnitId = $(proDetailsspan).data('unitid');
        addcart.UnitName = $(proDetailsspan).data('unitname');
        addcart.Quantity = 1;

        $.ajax({
            type: "Post",
            url: "/Default/ProcessToAddToCart",
            data: '{addcart: ' + JSON.stringify(addcart) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data == "true") {
                        CheckDetailsInCart("logedin");
                    }
                }
                setTimeout(function () { $(".closeaddprodincart").click(); }, 500);
            }
        });
    }
    else {
        $("#btnFrontUserLogin").click();
    }
});


$(document.body).on('change', ".cartquant", function (e) {
    var cartid = $(this).data('cartid');
    var rowquantity = $(this).val();
    var rowprice = $("#price_" + cartid).data("rowprice");
    $("#txtCartQuantity_" + cartid).prop("disabled", true);
    $(".spiiner_" + cartid).removeClass("hidden");

    setTimeout(function () {
        $.ajax({
            type: "Post",
            url: "/Default/UpdateAddToCart",
            data: '{cartid: ' + JSON.stringify(cartid) + ',quantity: ' + JSON.stringify(rowquantity) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $("#txtCartQuantity_" + cartid).prop("disabled", false);
                if (data != null) {
                    if (data == "true") {
                        var rowTotalPrice = rowquantity * parseFloat(rowprice);

                        var otherTotalPrice = 0;
                        //$.each(".rowprice", function (i, val) {
                        $(".rowprice").each(function (i, val) {
                            var looprowcartid = $(this).data('rowcartid');
                            if (looprowcartid != cartid) {
                                var looprowquantity = $("#txtCartQuantity_" + looprowcartid).val();
                                var looprowprice = $(this).data('rowprice');

                                otherTotalPrice = otherTotalPrice + (looprowquantity * parseFloat(looprowprice));
                            }
                        });

                        rowTotalPrice = rowTotalPrice + otherTotalPrice;
                        $("#spsubtotal").html(rowTotalPrice);
                        $("#sptotal").html(Math.ceil(rowTotalPrice) + ".00");
                    }
                }                
                $(".spiiner_" + cartid).addClass("hidden");
            }
        });
    }, 100);
});

$(document.body).on('keyup', ".cartquant", function (e) {
    var cartid = $(this).data('cartid');
    var rowquantity = $(this).val();
    var rowprice = $("#price_" + cartid).data("rowprice");
    $("#txtCartQuantity_" + cartid).prop("disabled", true);
    $(".spiiner_" + cartid).removeClass("hidden");

    setTimeout(function () {
        $.ajax({
            type: "Post",
            url: "/Default/UpdateAddToCart",
            data: '{cartid: ' + JSON.stringify(cartid) + ',quantity: ' + JSON.stringify(rowquantity) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $("#txtCartQuantity_" + cartid).prop("disabled", false);
                if (data != null) {
                    if (data == "true") {
                        var rowTotalPrice = rowquantity * parseFloat(rowprice);

                        var otherTotalPrice = 0;
                        //$.each(".rowprice", function (i, val) {
                        $(".rowprice").each(function (i, val) {
                            var looprowcartid = $(this).data('rowcartid');
                            if (looprowcartid != cartid) {
                                var looprowquantity = $("#txtCartQuantity_" + looprowcartid).val();
                                var looprowprice = $(this).data('rowprice');

                                otherTotalPrice = otherTotalPrice + (looprowquantity * parseFloat(looprowprice));
                            }
                        });

                        rowTotalPrice = rowTotalPrice + otherTotalPrice;
                        $("#spsubtotal").html(rowTotalPrice);
                        $("#sptotal").html(Math.ceil(rowTotalPrice) + ".00");
                    }
                }                
                $(".spiiner_" + cartid).addClass("hidden");
            }
        });
    }, 100);
});

$(document.body).on('click', ".delrowcart", function (e) {
    var cartid = $(this).data('delrowcartid');
    $(".delrowicon_" + cartid).removeClass("fa-trash").addClass(" fa-spin fa-spinner");

    setTimeout(function () {
        $.ajax({
            type: "Post",
            url: "/Default/DeleteAddToCart",
            data: '{cartid: ' + JSON.stringify(cartid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {                
                if (data != null) {
                    if (data == "true") {
                        CheckDetailsInCart("logedin");
                    }
                    else {
                        $(".delrowicon_" + cartid).removeClass(" fa-spin fa-spinner").addClass(" fa-trash");
                    }
                }
            }
        });
    }, 200);
});