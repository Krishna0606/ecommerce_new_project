﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

$(".addtocart").click(function () {    
    if (localStorage.getItem("frontloginsuccess") != null) {
        //if (CheckFocusCancellationBlankValidation('UnitId')) return !1;

        $(".addprodincart").click();

        var proDetailsspan = $("#prodetails");

        var addcart = {};
        addcart.ProductId = $(proDetailsspan).data('productid');
        addcart.ProductName = $(proDetailsspan).data('productname');
        addcart.ProductImgUrl = $(proDetailsspan).data('productimgurl');
        addcart.SalePrice = $(proDetailsspan).data('saleprice');
        addcart.Discount = $(proDetailsspan).data('discount');
        addcart.DiscountPrice = "";
        addcart.UnitId = $(proDetailsspan).data('unitid');
        addcart.UnitName = $(proDetailsspan).data('unitname');
        addcart.Quantity = 1;

        $.ajax({
            type: "Post",
            url: "/Default/ProcessToAddToCart",
            data: '{addcart: ' + JSON.stringify(addcart) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data == "true") {
                        CheckDetailsInCart("logedin");
                    }
                    setTimeout(function () { $(".closeaddprodincart").click(); }, 500);
                }
            }
        });
    }
    else {
        $("#btnFrontUserLogin").click();
    }
});


function CheckDetailsInCart(ptype) {
    $.ajax({
        type: "Post",
        url: "/Default/CheckDetailsInCart",
        data: '{pagetype: ' + JSON.stringify(ptype) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    $(".cartcounter").html(data[1]).addClass("shake");
                    $("#detailsofcart").html(data[2]);
                }
                else {
                    $(".cartcounter").html(data[1]);
                    $("#detailsofcart").html(data[2]);
                    $(".cartcounter").removeClass("shake");
                }
            }
            else {
                $(".cartcounter").removeClass("shake");
            }
        }
    });
}