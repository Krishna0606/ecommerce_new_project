﻿$(".frontaddtocart").click(function () {
    if (localStorage.getItem("frontloginsuccess") != null) {
        $(".addprodincart").click();

        var proDetailsspan = $("#frontaddprodetails_" + $(this).data('proid'));

        var addcart = {};
        addcart.ProductId = $(proDetailsspan).data('productid');
        addcart.ProductName = $(proDetailsspan).data('productname');
        addcart.ProductImgUrl = $(proDetailsspan).data('productimgurl');
        addcart.SalePrice = $(proDetailsspan).data('saleprice');
        addcart.Discount = $(proDetailsspan).data('discountper');
        addcart.DiscountPrice = $(proDetailsspan).data('discountprice');
        addcart.UnitId = $(proDetailsspan).data('unitid');
        addcart.UnitName = $(proDetailsspan).data('unitname');
        addcart.Quantity = 1;

        $.ajax({
            type: "Post",
            url: "/Default/ProcessToAddToCart",
            data: '{addcart: ' + JSON.stringify(addcart) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data == "true") {
                        CheckDetailsInCart("logedin");
                    }
                }
                setTimeout(function () { $(".closeaddprodincart").click(); }, 500);
            }
        });
    }
    else {
        $("#btnFrontUserLogin").click();
    }
});