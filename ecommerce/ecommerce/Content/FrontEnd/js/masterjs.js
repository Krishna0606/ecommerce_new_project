﻿var ischecked = false;
$(function () {
    //alert(localStorage.getItem("frontloginsuccess"));
    //alert(sessionStorage.getItem("frontlogoutreload"));
    //CheckUserLogin();
    if (localStorage.getItem("frontloginsuccess") != null) {
        CheckFrontUserLogin();
    }
});

function CheckDetailsInCart(ptype) {
    $.ajax({
        type: "Post",
        url: "/Default/CheckDetailsInCart",
        data: '{pagetype: ' + JSON.stringify(ptype) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    $(".cartcounter").html(data[1]).addClass("shake");
                    $("#detailsofcart").html(data[2]);
                }
                else {
                    $(".cartcounter").html(data[1]);
                    $("#detailsofcart").html(data[2]);
                    $(".cartcounter").removeClass("shake");
                }
            }
            else {
                $(".cartcounter").removeClass("shake");
            }
        }
    });
}

$(document.body).on('click', "#btnFrontlogin", function (e) {
    if (CheckFocusCancellationBlankValidation('FrontLoginId')) return !1;
    if (CheckFocusCancellationBlankValidation('FrontPassword')) return !1;

    $("#btnFrontlogin").html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>").prop("disabled", true);

    $.ajax({
        type: "Post",
        url: "/Account/ProcessFrontEndLogin",
        data: '{userid: ' + JSON.stringify($("#FrontLoginId").val()) + ',password: ' + JSON.stringify($("#FrontPassword").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    localStorage.setItem("frontloginsuccess", "userlogin");
                    $(".closefsignin").click();
                    window.location.reload(true);
                }
                else {
                    $("#btnFrontlogin").prop("disabled", false);
                    $("#errormsgdiv").html("");
                    $("#errormsgdiv").append("<p class='col-sm-12 form-group text-danger'>" + data[1] + "</p>");
                    localStorage.setItem("frontloginsuccess", null);
                    $("#btnFrontlogin").html("Login now");
                }
            }
        }
    });
});

function CheckUserLogin() {
    if (localStorage.getItem("frontloginsuccess") != null) {
        $(".blanckman").addClass("hidden");
        $(".loginsection").addClass("hidden");
        $(".logoutsection").removeClass("hidden");
        CheckDetailsInCart('front');
    }
    else {
        $(".loginsection").removeClass("hidden");
        $(".logoutsection").addClass("hidden");
        localStorage.removeItem("frontloginsuccess");
    }
}

function CheckFrontUserLogin() {
    $.ajax({
        type: "Post",
        url: "/Default/CheckFrontUserLogin",
        //data: '{pagetype: ' + JSON.stringify(ptype) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    localStorage.setItem("frontloginsuccess", data[1]);  
                    CheckUserLogin();
                }
                else {
                    localStorage.removeItem("frontloginsuccess");   
                    window.location.href = "/";
                }
            }
        }
    });
}

function LogoutFrontendLogin() {
    $.ajax({
        type: "Post",
        url: "/Default/LogoutFrontendLogin",
        //data: '{pagetype: ' + JSON.stringify(ptype) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data == "true") {
                    localStorage.removeItem("frontloginsuccess");
                    window.location.href = "/";
                }
            }
        }
    });
}

$("#frontlogout").click(function () {
    LogoutFrontendLogin();
});


//==================Sign up Section==============================
function FrontRegSubmitEvt() {
    var thisbutton = $("#Signbtn")
    if (CheckFocusCancellationBlankValidation('txtSignUpFirstName')) return !1;
    if (CheckFocusCancellationBlankValidation('txtSignUpLastName')) return !1;
    if (CheckFocusCancellationBlankValidation('txtSignUpEmail')) return !1;
    if (CheckEmailValidatoin("txtSignUpEmail")) return !1;
    if (CheckFocusCancellationBlankValidation('txtSignUpMobile')) return !1;
    if (CheckFocusCancellationBlankValidation('txtSignUpUserId')) return !1;
    if (CheckFocusCancellationBlankValidation('txtSignUpPassword')) return !1;
    if (CheckFocusCancellationBlankValidation('txtSignUpCnfPassword')) return !1;
    if (CheckSamePasswordValidation("txtSignUpPassword", "txtSignUpCnfPassword")) return !1;
    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");
    $.ajax({
        type: "Post",
        url: "/MemberMaster/UserRegistration",
        data: '{firstname: ' + JSON.stringify($("#txtSignUpFirstName").val()) + ',lastname: ' + JSON.stringify($("#txtSignUpLastName").val()) + ',emailid: ' + JSON.stringify($("#txtSignUpEmail").val()) + ',mobileno: ' + JSON.stringify($("#txtSignUpMobile").val()) + ',userid: ' + JSON.stringify($("#txtSignUpUserId").val()) + ',regpassword: ' + JSON.stringify($("#txtSignUpPassword").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data[0] == "true") {
                $.ajax({
                    type: "Post",
                    url: "/Account/ProcessFrontEndLogin",
                    data: '{userid: ' + JSON.stringify($("#txtSignUpUserId").val()) + ',password: ' + JSON.stringify($("#txtSignUpPassword").val()) + '}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (data) {
                        if (data != null) {
                            if (data[0] == "true") {
                                localStorage.setItem("frontloginsuccess", "userlogin");
                                //$(".closefsignin").click();
                                window.location.reload(true);
                            }
                        }
                    }
                });
            }
            else {
                $("#errorsingup").append("<div class='col-sm-12 form-group'><p>Error! there is an error occurred.</p></div>");
            }
        }
    });
}

function CheckFrontUserId() {
    setTimeout(function () {
        $.ajax({
            type: "Post",
            url: "/MemberMaster/CheckUserIdExist",
            data: '{userid: ' + JSON.stringify($("#txtSignUpUserId").val()) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data[0] == "true") {
                        $(".useridexist").html(data[1]);
                        $(".useridexist").addClass(" available").removeClass("hidden");
                    }
                    else {
                        $(".useridexist").html("");
                        $(".useridexist").removeClass(" available").addClass("hidden");
                    }
                }
            }
        });
    }, 100);
}