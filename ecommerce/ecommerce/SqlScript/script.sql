USE [master]
GO
/****** Object:  Database [Gentelella]    Script Date: 5/28/2020 3:36:31 PM ******/
CREATE DATABASE [Gentelella]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Gentelella', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Gentelella.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Gentelella_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Gentelella_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Gentelella] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Gentelella].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Gentelella] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Gentelella] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Gentelella] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Gentelella] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Gentelella] SET ARITHABORT OFF 
GO
ALTER DATABASE [Gentelella] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Gentelella] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Gentelella] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Gentelella] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Gentelella] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Gentelella] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Gentelella] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Gentelella] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Gentelella] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Gentelella] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Gentelella] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Gentelella] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Gentelella] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Gentelella] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Gentelella] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Gentelella] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Gentelella] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Gentelella] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Gentelella] SET  MULTI_USER 
GO
ALTER DATABASE [Gentelella] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Gentelella] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Gentelella] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Gentelella] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Gentelella] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Gentelella]
GO
/****** Object:  Table [dbo].[HomeBanner]    Script Date: 5/28/2020 3:36:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HomeBanner](
	[HomeBannerId] [int] IDENTITY(1,1) NOT NULL,
	[Heading] [varchar](500) NULL,
	[Content] [varchar](max) NULL,
	[IsShow] [bit] NULL,
	[Status] [bit] NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[HomeBannerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Brand]    Script Date: 5/28/2020 3:36:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Brand](
	[BrandId] [int] IDENTITY(1,1) NOT NULL,
	[BrnadName] [varchar](200) NULL,
	[Status] [bit] NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[BrandId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Category]    Script Date: 5/28/2020 3:36:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Category](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](200) NULL,
	[Menu] [int] NULL,
	[Status] [bit] NULL DEFAULT ((1)),
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_City]    Script Date: 5/28/2020 3:36:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_City](
	[CityId] [int] IDENTITY(1,1) NOT NULL,
	[CityName] [varchar](200) NULL,
	[CityCode] [varchar](50) NULL,
	[CountryId] [int] NULL,
	[StateId] [int] NULL,
	[Status] [bit] NULL DEFAULT ((1)),
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[CityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Country]    Script Date: 5/28/2020 3:36:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Country](
	[CountryId] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [varchar](200) NULL,
	[CountryCode] [varchar](10) NULL,
	[Status] [bit] NULL DEFAULT ((1)),
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_DeliveryCharges]    Script Date: 5/28/2020 3:36:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_DeliveryCharges](
	[DeliveryChargesId] [int] IDENTITY(1,1) NOT NULL,
	[Price] [decimal](18, 2) NULL,
	[StateId] [varchar](10) NULL,
	[StateName] [varchar](100) NULL,
	[CityId] [varchar](10) NULL,
	[CityName] [varchar](100) NULL,
	[PinCode] [varchar](10) NULL,
	[SupplierId] [int] NULL,
	[SupplierLoginId] [varchar](50) NULL,
	[SupplierName] [varchar](100) NULL,
	[Status] [bit] NULL DEFAULT ((1)),
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[DeliveryChargesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Login]    Script Date: 5/28/2020 3:36:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Login](
	[LoginId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](10) NULL,
	[FirstName] [varchar](200) NULL,
	[LastName] [varchar](200) NULL,
	[EmailId] [varchar](200) NULL,
	[MobileNo] [varchar](15) NULL,
	[Country] [int] NULL,
	[State] [int] NULL,
	[City] [varchar](200) NULL,
	[PinCode] [varchar](10) NULL,
	[Address] [varchar](max) NULL,
	[ProfileImage] [varchar](200) NULL,
	[Profession] [varchar](200) NULL,
	[DOB] [varchar](20) NULL,
	[AddharNo] [varchar](50) NULL,
	[PanNo] [varchar](50) NULL,
	[GSTNo] [varchar](50) NULL,
	[AddharImage] [varchar](200) NULL,
	[PanImage] [varchar](200) NULL,
	[BankName] [varchar](200) NULL,
	[AccountNo] [varchar](200) NULL,
	[BranchName] [varchar](100) NULL,
	[BrnachCode] [varchar](20) NULL,
	[IFSCCode] [varchar](20) NULL,
	[BankAddress] [varchar](max) NULL,
	[PayeeName] [varchar](200) NULL,
	[UserId] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[UserType] [varchar](20) NULL,
	[IsApproved] [bit] NULL DEFAULT ((0)),
	[Status] [bit] NULL DEFAULT ((1)),
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
	[SupplierName] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[LoginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Menu]    Script Date: 5/28/2020 3:36:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Menu](
	[Menuid] [int] IDENTITY(1,1) NOT NULL,
	[MenuName] [varchar](100) NULL,
	[Status] [bit] NULL DEFAULT ((1)),
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[Menuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Product]    Script Date: 5/28/2020 3:36:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Product](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [varchar](500) NULL,
	[ProductCode] [varchar](50) NULL,
	[Brand] [int] NULL,
	[Menu] [int] NULL,
	[Category] [int] NULL,
	[SubCategory] [int] NULL,
	[Offer] [varchar](10) NULL,
	[PurchasePrice] [decimal](18, 2) NULL DEFAULT ((0)),
	[SalePrice] [decimal](18, 2) NULL DEFAULT ((0)),
	[Discount] [decimal](18, 2) NULL DEFAULT ((0)),
	[Unit] [int] NULL,
	[DeliveryTime] [varchar](50) NULL,
	[Description] [varchar](max) NULL,
	[IsFeaturedProducts] [bit] NULL DEFAULT ((0)),
	[SupplierId] [int] NULL,
	[SupplierLoginId] [varchar](100) NULL,
	[Status] [bit] NULL DEFAULT ((1)),
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
	[UpdatedDate] [datetime] NULL DEFAULT (getdate()),
	[SupplierName] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_ProductDescription]    Script Date: 5/28/2020 3:36:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_ProductDescription](
	[ProductDescriptionId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[Description1] [varchar](max) NULL,
	[Description2] [varchar](max) NULL,
	[Description3] [varchar](max) NULL,
	[Description4] [varchar](max) NULL,
	[Description5] [varchar](max) NULL,
	[Status] [bit] NULL DEFAULT ((1)),
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[ProductDescriptionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_ProductImages]    Script Date: 5/28/2020 3:36:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_ProductImages](
	[ImageId] [int] IDENTITY(1,1) NOT NULL,
	[ImageName] [varchar](200) NULL,
	[ImageUrl] [varchar](200) NULL,
	[ProductId] [int] NULL,
	[Status] [bit] NULL DEFAULT ((1)),
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[ImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_State]    Script Date: 5/28/2020 3:36:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_State](
	[StateId] [int] IDENTITY(1,1) NOT NULL,
	[StateName] [varchar](200) NULL,
	[StateCode] [varchar](50) NULL,
	[CountryId] [int] NULL,
	[Status] [bit] NULL DEFAULT ((1)),
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[StateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_SubCategory]    Script Date: 5/28/2020 3:36:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_SubCategory](
	[SubCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[SubCategoryName] [varchar](200) NULL,
	[Menu] [int] NULL,
	[Category] [int] NULL,
	[Status] [bit] NULL DEFAULT ((1)),
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[SubCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Unit]    Script Date: 5/28/2020 3:36:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_Unit](
	[Unitid] [int] IDENTITY(1,1) NOT NULL,
	[Unit] [varchar](200) NULL,
	[Status] [bit] NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Unitid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[T_Category] ON 

INSERT [dbo].[T_Category] ([CategoryId], [CategoryName], [Menu], [Status], [CreatedDate]) VALUES (1, N'zxbv', 3, 0, CAST(N'2020-05-25 15:01:24.567' AS DateTime))
INSERT [dbo].[T_Category] ([CategoryId], [CategoryName], [Menu], [Status], [CreatedDate]) VALUES (2, N'Fresh Vegetables', 4, 1, CAST(N'2020-05-25 16:53:07.673' AS DateTime))
INSERT [dbo].[T_Category] ([CategoryId], [CategoryName], [Menu], [Status], [CreatedDate]) VALUES (3, N'Herbs & Seasonings', 4, 1, CAST(N'2020-05-25 16:53:20.433' AS DateTime))
INSERT [dbo].[T_Category] ([CategoryId], [CategoryName], [Menu], [Status], [CreatedDate]) VALUES (4, N'Exotic Fruits & Veggies', 4, 1, CAST(N'2020-05-25 16:53:30.163' AS DateTime))
INSERT [dbo].[T_Category] ([CategoryId], [CategoryName], [Menu], [Status], [CreatedDate]) VALUES (5, N'Fresh Fruits', 4, 1, CAST(N'2020-05-25 16:53:40.857' AS DateTime))
INSERT [dbo].[T_Category] ([CategoryId], [CategoryName], [Menu], [Status], [CreatedDate]) VALUES (6, N'Atta, Flours & Sooji', 5, 1, CAST(N'2020-05-25 16:54:11.400' AS DateTime))
INSERT [dbo].[T_Category] ([CategoryId], [CategoryName], [Menu], [Status], [CreatedDate]) VALUES (7, N'Salt, Sugar & Jaggery', 5, 1, CAST(N'2020-05-25 16:54:20.520' AS DateTime))
INSERT [dbo].[T_Category] ([CategoryId], [CategoryName], [Menu], [Status], [CreatedDate]) VALUES (8, N'Dals & Pulses', 5, 1, CAST(N'2020-05-25 16:54:29.327' AS DateTime))
INSERT [dbo].[T_Category] ([CategoryId], [CategoryName], [Menu], [Status], [CreatedDate]) VALUES (9, N'Breads & Buns', 6, 1, CAST(N'2020-05-25 16:54:40.327' AS DateTime))
INSERT [dbo].[T_Category] ([CategoryId], [CategoryName], [Menu], [Status], [CreatedDate]) VALUES (10, N'Dairy', 6, 1, CAST(N'2020-05-25 16:54:48.817' AS DateTime))
INSERT [dbo].[T_Category] ([CategoryId], [CategoryName], [Menu], [Status], [CreatedDate]) VALUES (11, N'Cakes & Pastries', 6, 1, CAST(N'2020-05-25 16:54:58.443' AS DateTime))
SET IDENTITY_INSERT [dbo].[T_Category] OFF
SET IDENTITY_INSERT [dbo].[T_City] ON 

INSERT [dbo].[T_City] ([CityId], [CityName], [CityCode], [CountryId], [StateId], [Status], [CreatedDate]) VALUES (1, N'Uttam Nagar', N'UN', 17, 3, 1, CAST(N'2020-05-28 13:20:09.463' AS DateTime))
INSERT [dbo].[T_City] ([CityId], [CityName], [CityCode], [CountryId], [StateId], [Status], [CreatedDate]) VALUES (2, N'GuruGram', N'GG', 17, 5, 1, CAST(N'2020-05-28 13:25:39.283' AS DateTime))
INSERT [dbo].[T_City] ([CityId], [CityName], [CityCode], [CountryId], [StateId], [Status], [CreatedDate]) VALUES (3, N'Meerut', N'MRT', 17, 4, 1, CAST(N'2020-05-28 13:26:19.850' AS DateTime))
INSERT [dbo].[T_City] ([CityId], [CityName], [CityCode], [CountryId], [StateId], [Status], [CreatedDate]) VALUES (4, N'Ghaziabad', N'GZB', 17, 4, 1, CAST(N'2020-05-28 13:26:46.933' AS DateTime))
INSERT [dbo].[T_City] ([CityId], [CityName], [CityCode], [CountryId], [StateId], [Status], [CreatedDate]) VALUES (5, N'Kanpur', N'KNP', 17, 4, 1, CAST(N'2020-05-28 13:27:22.903' AS DateTime))
SET IDENTITY_INSERT [dbo].[T_City] OFF
SET IDENTITY_INSERT [dbo].[T_Country] ON 

INSERT [dbo].[T_Country] ([CountryId], [CountryName], [CountryCode], [Status], [CreatedDate]) VALUES (1, N'India', N'IN', 0, CAST(N'2020-05-21 16:51:08.650' AS DateTime))
INSERT [dbo].[T_Country] ([CountryId], [CountryName], [CountryCode], [Status], [CreatedDate]) VALUES (10, N'India', N'IN', 0, CAST(N'2020-05-22 18:00:48.333' AS DateTime))
INSERT [dbo].[T_Country] ([CountryId], [CountryName], [CountryCode], [Status], [CreatedDate]) VALUES (11, N'zdhgb', N'xfghncbn ', 0, CAST(N'2020-05-22 22:09:39.480' AS DateTime))
INSERT [dbo].[T_Country] ([CountryId], [CountryName], [CountryCode], [Status], [CreatedDate]) VALUES (12, N'India', N'IN', 0, CAST(N'2020-05-22 23:16:03.600' AS DateTime))
INSERT [dbo].[T_Country] ([CountryId], [CountryName], [CountryCode], [Status], [CreatedDate]) VALUES (13, N'India', N'CS', 0, CAST(N'2020-05-22 23:21:35.357' AS DateTime))
INSERT [dbo].[T_Country] ([CountryId], [CountryName], [CountryCode], [Status], [CreatedDate]) VALUES (14, N'Bublb', N'KKKKKKKKK', 0, CAST(N'2020-05-22 23:21:39.480' AS DateTime))
INSERT [dbo].[T_Country] ([CountryId], [CountryName], [CountryCode], [Status], [CreatedDate]) VALUES (15, N'India', N'CS', 0, CAST(N'2020-05-22 23:24:08.760' AS DateTime))
INSERT [dbo].[T_Country] ([CountryId], [CountryName], [CountryCode], [Status], [CreatedDate]) VALUES (16, N'India', N'IN', 0, CAST(N'2020-05-22 23:26:01.253' AS DateTime))
INSERT [dbo].[T_Country] ([CountryId], [CountryName], [CountryCode], [Status], [CreatedDate]) VALUES (17, N'India', N'CS', 1, CAST(N'2020-05-22 23:30:24.377' AS DateTime))
INSERT [dbo].[T_Country] ([CountryId], [CountryName], [CountryCode], [Status], [CreatedDate]) VALUES (18, N'Cystal', N'CS', 0, CAST(N'2020-05-22 23:30:30.870' AS DateTime))
SET IDENTITY_INSERT [dbo].[T_Country] OFF
SET IDENTITY_INSERT [dbo].[T_DeliveryCharges] ON 

INSERT [dbo].[T_DeliveryCharges] ([DeliveryChargesId], [Price], [StateId], [StateName], [CityId], [CityName], [PinCode], [SupplierId], [SupplierLoginId], [SupplierName], [Status], [CreatedDate]) VALUES (1, CAST(40.00 AS Decimal(18, 2)), N'3', N'Delhi', N'4', N'Delhi', N'110059', 3, N'krishna007', N'krishna kant', 1, CAST(N'2020-05-28 12:19:54.377' AS DateTime))
INSERT [dbo].[T_DeliveryCharges] ([DeliveryChargesId], [Price], [StateId], [StateName], [CityId], [CityName], [PinCode], [SupplierId], [SupplierLoginId], [SupplierName], [Status], [CreatedDate]) VALUES (2, CAST(0.00 AS Decimal(18, 2)), N'0', N'', N'0', N'', N'', 3, N'krishna007', N'krishna kant', 0, CAST(N'2020-05-28 13:48:02.727' AS DateTime))
INSERT [dbo].[T_DeliveryCharges] ([DeliveryChargesId], [Price], [StateId], [StateName], [CityId], [CityName], [PinCode], [SupplierId], [SupplierLoginId], [SupplierName], [Status], [CreatedDate]) VALUES (3, CAST(0.00 AS Decimal(18, 2)), N'0', N'', N'0', N'', N'', 3, N'krishna007', N'krishna kant', 0, CAST(N'2020-05-28 13:49:58.090' AS DateTime))
INSERT [dbo].[T_DeliveryCharges] ([DeliveryChargesId], [Price], [StateId], [StateName], [CityId], [CityName], [PinCode], [SupplierId], [SupplierLoginId], [SupplierName], [Status], [CreatedDate]) VALUES (4, CAST(0.00 AS Decimal(18, 2)), N'0', N'', N'0', N'', N'', 3, N'krishna007', N'krishna kant', 0, CAST(N'2020-05-28 13:50:41.420' AS DateTime))
INSERT [dbo].[T_DeliveryCharges] ([DeliveryChargesId], [Price], [StateId], [StateName], [CityId], [CityName], [PinCode], [SupplierId], [SupplierLoginId], [SupplierName], [Status], [CreatedDate]) VALUES (5, CAST(0.00 AS Decimal(18, 2)), N'0', N'', N'0', N'', N'', 3, N'krishna007', N'krishna kant', 0, CAST(N'2020-05-28 13:51:26.390' AS DateTime))
INSERT [dbo].[T_DeliveryCharges] ([DeliveryChargesId], [Price], [StateId], [StateName], [CityId], [CityName], [PinCode], [SupplierId], [SupplierLoginId], [SupplierName], [Status], [CreatedDate]) VALUES (6, CAST(40.00 AS Decimal(18, 2)), N'5', N'Hariyana', N'2', N'GuruGram', N'110059', 3, N'krishna007', N'krishna kant', 1, CAST(N'2020-05-28 13:52:22.523' AS DateTime))
INSERT [dbo].[T_DeliveryCharges] ([DeliveryChargesId], [Price], [StateId], [StateName], [CityId], [CityName], [PinCode], [SupplierId], [SupplierLoginId], [SupplierName], [Status], [CreatedDate]) VALUES (7, CAST(89.00 AS Decimal(18, 2)), N'5', N'Hariyana', N'2', N'Hariyana', N'325654', 3, N'krishna007', N'krishna kant', 0, CAST(N'2020-05-28 14:01:25.340' AS DateTime))
INSERT [dbo].[T_DeliveryCharges] ([DeliveryChargesId], [Price], [StateId], [StateName], [CityId], [CityName], [PinCode], [SupplierId], [SupplierLoginId], [SupplierName], [Status], [CreatedDate]) VALUES (8, CAST(40.00 AS Decimal(18, 2)), N'4', N'UP', N'3', N'Meerut', N'110059', 3, N'krishna007', N'krishna kant', 1, CAST(N'2020-05-28 14:29:08.880' AS DateTime))
SET IDENTITY_INSERT [dbo].[T_DeliveryCharges] OFF
SET IDENTITY_INSERT [dbo].[T_Login] ON 

INSERT [dbo].[T_Login] ([LoginId], [Title], [FirstName], [LastName], [EmailId], [MobileNo], [Country], [State], [City], [PinCode], [Address], [ProfileImage], [Profession], [DOB], [AddharNo], [PanNo], [GSTNo], [AddharImage], [PanImage], [BankName], [AccountNo], [BranchName], [BrnachCode], [IFSCCode], [BankAddress], [PayeeName], [UserId], [Password], [UserType], [IsApproved], [Status], [CreatedDate], [SupplierName]) VALUES (1, N'Mr.', N'Admin', N'Admin', N'admin@admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'admin', N'admin', N'admin', 1, 1, CAST(N'2020-05-24 12:22:44.663' AS DateTime), NULL)
INSERT [dbo].[T_Login] ([LoginId], [Title], [FirstName], [LastName], [EmailId], [MobileNo], [Country], [State], [City], [PinCode], [Address], [ProfileImage], [Profession], [DOB], [AddharNo], [PanNo], [GSTNo], [AddharImage], [PanImage], [BankName], [AccountNo], [BranchName], [BrnachCode], [IFSCCode], [BankAddress], [PayeeName], [UserId], [Password], [UserType], [IsApproved], [Status], [CreatedDate], [SupplierName]) VALUES (2, N'Mr.', N'Shree', N'Kant', N'shri@shri', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'shri', N'shri', N'supplier', 1, 1, CAST(N'2020-05-24 12:23:15.970' AS DateTime), N'ShriLal')
INSERT [dbo].[T_Login] ([LoginId], [Title], [FirstName], [LastName], [EmailId], [MobileNo], [Country], [State], [City], [PinCode], [Address], [ProfileImage], [Profession], [DOB], [AddharNo], [PanNo], [GSTNo], [AddharImage], [PanImage], [BankName], [AccountNo], [BranchName], [BrnachCode], [IFSCCode], [BankAddress], [PayeeName], [UserId], [Password], [UserType], [IsApproved], [Status], [CreatedDate], [SupplierName]) VALUES (3, N'Mr.', N'krishna', N'kant', N'krishna@gmail.com', N'9555266308', 17, 3, N'west delhi', N'110059', N'uttam nagar', N'', N'developer', N'31/08/2000', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'krishna007', N'krishna', N'supplier', 1, 1, CAST(N'2020-05-24 14:30:07.930' AS DateTime), N'GhostRider')
INSERT [dbo].[T_Login] ([LoginId], [Title], [FirstName], [LastName], [EmailId], [MobileNo], [Country], [State], [City], [PinCode], [Address], [ProfileImage], [Profession], [DOB], [AddharNo], [PanNo], [GSTNo], [AddharImage], [PanImage], [BankName], [AccountNo], [BranchName], [BrnachCode], [IFSCCode], [BankAddress], [PayeeName], [UserId], [Password], [UserType], [IsApproved], [Status], [CreatedDate], [SupplierName]) VALUES (4, N'Mr.', N'krishna gchj', N'kantghj', N'kdfhrishna@gmail.com', N'9555266308h', 17, 3, N'west delhicm', N'110059', N'uttam nagar cfjgh', N'', N'developer', N'31/08/2000', N'jhngchn', N'cvmnc', N'', N'', N'', N'cvmncv', N'cvmncvn', N'cvmcvn', N'', N'cvmcvm', N'cvmccmv', N'cvmcvm', N'krishna007', N'krishna', N'supplier', 1, 1, CAST(N'2020-05-24 14:32:17.890' AS DateTime), NULL)
INSERT [dbo].[T_Login] ([LoginId], [Title], [FirstName], [LastName], [EmailId], [MobileNo], [Country], [State], [City], [PinCode], [Address], [ProfileImage], [Profession], [DOB], [AddharNo], [PanNo], [GSTNo], [AddharImage], [PanImage], [BankName], [AccountNo], [BranchName], [BrnachCode], [IFSCCode], [BankAddress], [PayeeName], [UserId], [Password], [UserType], [IsApproved], [Status], [CreatedDate], [SupplierName]) VALUES (5, N'Mr.', N'cxgjh', N'ghdcj', N'cghj@gmail.com', N'cghj', 17, 3, N'cgh', N'cghj', N'xfgjc', N'C:\fakepath\e69f1df4-3664-465d-ada4-6f3c389d0e56.jpg', N'cghj', N'chgj', N'dgch', N'cghj', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'admingh', N'admin', N'supplier', 1, 1, CAST(N'2020-05-24 16:17:04.297' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[T_Login] OFF
SET IDENTITY_INSERT [dbo].[T_Menu] ON 

INSERT [dbo].[T_Menu] ([Menuid], [MenuName], [Status], [CreatedDate]) VALUES (1, N'Krishna', 0, CAST(N'2020-05-24 22:04:12.977' AS DateTime))
INSERT [dbo].[T_Menu] ([Menuid], [MenuName], [Status], [CreatedDate]) VALUES (2, N'Krishna', 0, CAST(N'2020-05-25 11:45:52.007' AS DateTime))
INSERT [dbo].[T_Menu] ([Menuid], [MenuName], [Status], [CreatedDate]) VALUES (3, N'xfg', 0, CAST(N'2020-05-25 15:01:50.203' AS DateTime))
INSERT [dbo].[T_Menu] ([Menuid], [MenuName], [Status], [CreatedDate]) VALUES (4, N'Fruits & Vegetables', 1, CAST(N'2020-05-25 16:52:24.943' AS DateTime))
INSERT [dbo].[T_Menu] ([Menuid], [MenuName], [Status], [CreatedDate]) VALUES (5, N'Foodgrains, Oil & Masala', 1, CAST(N'2020-05-25 16:52:35.190' AS DateTime))
INSERT [dbo].[T_Menu] ([Menuid], [MenuName], [Status], [CreatedDate]) VALUES (6, N'Bakery, Cakes & Dairy', 1, CAST(N'2020-05-25 16:52:42.710' AS DateTime))
SET IDENTITY_INSERT [dbo].[T_Menu] OFF
SET IDENTITY_INSERT [dbo].[T_Product] ON 

INSERT [dbo].[T_Product] ([ProductId], [ProductName], [ProductCode], [Brand], [Menu], [Category], [SubCategory], [Offer], [PurchasePrice], [SalePrice], [Discount], [Unit], [DeliveryTime], [Description], [IsFeaturedProducts], [SupplierId], [SupplierLoginId], [Status], [CreatedDate], [UpdatedDate], [SupplierName]) VALUES (1, N'Demo', N'Demo123', 1027, 1, 1, 1, N'new', CAST(2500.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), 5, N'5-7 days', N'this is for tsting', 0, 1, N'admin', 1, CAST(N'2020-05-25 17:41:31.140' AS DateTime), CAST(N'2020-05-25 17:41:31.140' AS DateTime), N'')
INSERT [dbo].[T_Product] ([ProductId], [ProductName], [ProductCode], [Brand], [Menu], [Category], [SubCategory], [Offer], [PurchasePrice], [SalePrice], [Discount], [Unit], [DeliveryTime], [Description], [IsFeaturedProducts], [SupplierId], [SupplierLoginId], [Status], [CreatedDate], [UpdatedDate], [SupplierName]) VALUES (2, N'testing', N'testing007', 1027, 1, 1, 1, N'new', CAST(1500.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 5, N'2-3 days', N'thsi is for tetsing', 0, 3, N'krishna007', 1, CAST(N'2020-05-27 23:16:09.213' AS DateTime), CAST(N'2020-05-27 23:16:09.213' AS DateTime), N'GhostRider')
INSERT [dbo].[T_Product] ([ProductId], [ProductName], [ProductCode], [Brand], [Menu], [Category], [SubCategory], [Offer], [PurchasePrice], [SalePrice], [Discount], [Unit], [DeliveryTime], [Description], [IsFeaturedProducts], [SupplierId], [SupplierLoginId], [Status], [CreatedDate], [UpdatedDate], [SupplierName]) VALUES (3, N'cjh', N'fxj', 1027, 1, 1, 1, N'sale', CAST(45.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), 1, N'5-6 days', N'', 1, 3, N'krishna007', 1, CAST(N'2020-05-27 23:36:14.540' AS DateTime), CAST(N'2020-05-27 23:36:14.540' AS DateTime), N'GhostRider')
SET IDENTITY_INSERT [dbo].[T_Product] OFF
SET IDENTITY_INSERT [dbo].[T_ProductDescription] ON 

INSERT [dbo].[T_ProductDescription] ([ProductDescriptionId], [ProductId], [Description1], [Description2], [Description3], [Description4], [Description5], [Status], [CreatedDate]) VALUES (1, 2, N'product-master/add-edit-product?productid=2&status=existing', N'product-master/add-edit-product?productid=2&status=existing', N'product-master/add-edit-product?productid=2&status=existing', N'product-master/add-edit-product?productid=2&status=existing', N'product-master/add-edit-product?productid=2&status=existing', 1, CAST(N'2020-05-27 23:32:39.660' AS DateTime))
SET IDENTITY_INSERT [dbo].[T_ProductDescription] OFF
SET IDENTITY_INSERT [dbo].[T_ProductImages] ON 

INSERT [dbo].[T_ProductImages] ([ImageId], [ImageName], [ImageUrl], [ProductId], [Status], [CreatedDate]) VALUES (1, N'test', N'1OGB.jpg', 2, 1, CAST(N'2020-05-27 23:17:46.413' AS DateTime))
INSERT [dbo].[T_ProductImages] ([ImageId], [ImageName], [ImageUrl], [ProductId], [Status], [CreatedDate]) VALUES (2, N'cgh', N'2AAc.png', 2, 1, CAST(N'2020-05-27 23:17:54.413' AS DateTime))
SET IDENTITY_INSERT [dbo].[T_ProductImages] OFF
SET IDENTITY_INSERT [dbo].[T_State] ON 

INSERT [dbo].[T_State] ([StateId], [StateName], [StateCode], [CountryId], [Status], [CreatedDate]) VALUES (1, N'New Delhi', N'DL', 1, 0, CAST(N'2020-05-22 16:22:04.950' AS DateTime))
INSERT [dbo].[T_State] ([StateId], [StateName], [StateCode], [CountryId], [Status], [CreatedDate]) VALUES (2, N'Uttar Pradesh', N'UP', 1, 0, CAST(N'2020-05-22 18:00:17.323' AS DateTime))
INSERT [dbo].[T_State] ([StateId], [StateName], [StateCode], [CountryId], [Status], [CreatedDate]) VALUES (3, N'Delhi', N'DL', 17, 1, CAST(N'2020-05-22 23:36:26.030' AS DateTime))
INSERT [dbo].[T_State] ([StateId], [StateName], [StateCode], [CountryId], [Status], [CreatedDate]) VALUES (4, N'UP', N'UP', 17, 1, CAST(N'2020-05-28 13:16:11.860' AS DateTime))
INSERT [dbo].[T_State] ([StateId], [StateName], [StateCode], [CountryId], [Status], [CreatedDate]) VALUES (5, N'Hariyana', N'HR', 17, 1, CAST(N'2020-05-28 13:16:22.870' AS DateTime))
SET IDENTITY_INSERT [dbo].[T_State] OFF
SET IDENTITY_INSERT [dbo].[T_SubCategory] ON 

INSERT [dbo].[T_SubCategory] ([SubCategoryId], [SubCategoryName], [Menu], [Category], [Status], [CreatedDate]) VALUES (1, N'krishna123', 4, 4, 1, CAST(N'2020-05-26 14:41:02.847' AS DateTime))
INSERT [dbo].[T_SubCategory] ([SubCategoryId], [SubCategoryName], [Menu], [Category], [Status], [CreatedDate]) VALUES (2, N'kkk', 6, 9, 1, CAST(N'2020-05-27 22:32:50.060' AS DateTime))
SET IDENTITY_INSERT [dbo].[T_SubCategory] OFF
ALTER TABLE [dbo].[HomeBanner] ADD  DEFAULT ((0)) FOR [IsShow]
GO
ALTER TABLE [dbo].[HomeBanner] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[HomeBanner] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[T_Brand] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[T_Brand] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[T_Unit] ADD  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[T_Unit] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[T_Category]  WITH CHECK ADD FOREIGN KEY([Menu])
REFERENCES [dbo].[T_Menu] ([Menuid])
GO
ALTER TABLE [dbo].[T_City]  WITH CHECK ADD FOREIGN KEY([CountryId])
REFERENCES [dbo].[T_Country] ([CountryId])
GO
ALTER TABLE [dbo].[T_City]  WITH CHECK ADD FOREIGN KEY([StateId])
REFERENCES [dbo].[T_State] ([StateId])
GO
ALTER TABLE [dbo].[T_ProductDescription]  WITH CHECK ADD FOREIGN KEY([ProductId])
REFERENCES [dbo].[T_Product] ([ProductId])
GO
ALTER TABLE [dbo].[T_State]  WITH CHECK ADD FOREIGN KEY([CountryId])
REFERENCES [dbo].[T_Country] ([CountryId])
GO
ALTER TABLE [dbo].[T_SubCategory]  WITH CHECK ADD FOREIGN KEY([Category])
REFERENCES [dbo].[T_Category] ([CategoryId])
GO
ALTER TABLE [dbo].[T_SubCategory]  WITH CHECK ADD FOREIGN KEY([Menu])
REFERENCES [dbo].[T_Menu] ([Menuid])
GO
/****** Object:  StoredProcedure [dbo].[ups_UpdateRecordToTable]    Script Date: 5/28/2020 3:36:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ups_UpdateRecordToTable] 
@TableName varchar(100)=null,
@FieldsWithValue varchar(max),
@WhereCondition varchar(max)
as
	begin
		
		   declare @qry as nvarchar(max)
		   set @qry='update ' + @TableName + ' set '+ @FieldsWithValue
		   if(@WhereCondition<>'') set @qry=@qry+' where ' + @WhereCondition
		   execute(@qry)
	end

GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteRecordFromTable]    Script Date: 5/28/2020 3:36:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[usp_DeleteRecordFromTable]
(
@tablename varchar(100)=null,
@where nvarchar(max)=null
)
as
begin
   declare @qry as nvarchar(max)
   set @qry='delete from '+ @tablename
   if(@where<>'') set @qry=@qry+' where ' + @where
   execute(@qry)
end

GO
/****** Object:  StoredProcedure [dbo].[usp_GetRecordFromTable]    Script Date: 5/28/2020 3:36:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_GetRecordFromTable] 
(
@fileds nvarchar(max) =null,
@tablename varchar(max)=null,
@where nvarchar(max)=null
)
as
begin
   declare @qry as nvarchar(max)
   set @qry='select ' + @fileds + ' from '+ @tablename
   if(@where<>'') set @qry=@qry+' where ' + @where
   execute(@qry)
end

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertRecordToTable]    Script Date: 5/28/2020 3:36:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[usp_InsertRecordToTable]
(
@fileds nvarchar(max) =null,
@values nvarchar(max) =null,
@tablename varchar(100)=null
)
as
begin
   declare @qry as nvarchar(max)
   set @qry='insert into ' + @tablename + ' ( '+ @fileds + ' ) values (' + @values + ' )'   
   execute(@qry)
end

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertRecordToTableReturnID]    Script Date: 5/28/2020 3:36:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_InsertRecordToTableReturnID]
(
@fileds nvarchar(max) =null,
@values nvarchar(max) =null,
@tablename varchar(100)=null,
@Id int output
)
as
begin
   declare @qry as nvarchar(max)
   set @qry='insert into ' + @tablename + ' ( '+ @fileds + ' ) values (' + @values + ' )'   
   execute(@qry)
   SET @Id=@@IDENTITY
   RETURN @Id
end
GO
/****** Object:  StoredProcedure [dbo].[usp_TruncateRecordToTable]    Script Date: 5/28/2020 3:36:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[usp_TruncateRecordToTable]
(
@tablename varchar(100)=null
)
as
begin
   declare @qry as nvarchar(max)
   set @qry='truncate table '+ @tablename   
   execute(@qry)
end


GO
USE [master]
GO
ALTER DATABASE [Gentelella] SET  READ_WRITE 
GO
