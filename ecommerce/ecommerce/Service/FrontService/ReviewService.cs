﻿using ecommerce.Helper.FrontHelper;
using ecommerce.Models.Frontend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecommerce.Service.FrontService
{
    public static class ReviewService
    {
        public static bool InsertProductReviewDetail(ReviewModel model)
        {
            return ReviewHelper.InsertProductReviewDetail(model);
        }

        public static List<ReviewModel> GetReviewDetail(string productid, string whrcon = null)
        {
            return ReviewHelper.GetReviewDetail(productid, whrcon);
        }

        public static bool UpdateReviewPosition(string reviewid, string posval)
        {
            return ReviewHelper.UpdateReviewPosition(reviewid, posval);
        }

        public static bool UpdateReviewIsApproved(string reviewid, string approved)
        {
            return ReviewHelper.UpdateReviewIsApproved(reviewid, approved);
        }
    }
}