﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ecommerce.Models.Frontend.FrontHomeModel;
using static ecommerce.Models.FrontProductModel;
using ecommerce.Helper.FrontHelper;
using static ecommerce.Models.Frontend.FrontUserEnd;
using System.Data;

namespace ecommerce.Service.Front
{
    public static class FrontProductService
    {
        public static List<FrontProduct> GetFrontProductList(string prodtid = null, string menuid = null, string catid = null, string subcatid = null, string productName = null)
        {
            return FrontProductHelper.GetFrontProductList(prodtid, menuid, catid, subcatid, productName);
        }

        public static DataTable GetMenuCatSCatDetails(string menuid = null, string catid = null, string subcatid = null)
        {
            return FrontProductHelper.GetMenuCatSCatDetails(menuid, catid, subcatid);
        }

        public static bool InsertDetailsAddToCart(AddToCart addcart)
        {
            return FrontProductHelper.InsertDetailsAddToCart(addcart);
        }

        public static bool UpdateDetailsAddToCartById(string cartid, string quantity)
        {
            return FrontProductHelper.UpdateDetailsAddToCartById(cartid, quantity);
        }

        public static bool DeleteDetailsAddToCartById(string cartid)
        {
            return FrontProductHelper.DeleteDetailsAddToCartById(cartid);
        }

        public static List<AddToCart> CheckDetailsInCart(string userid = null)
        {
            return FrontProductHelper.CheckDetailsInCart(userid);
        }

        public static Dictionary<int, string> GetFeaturedMenuList()
        {
            return FrontProductHelper.GetFeaturedMenuList();
        }

        public static List<FrontHomeProduct> GetFrontProductByMenuIdList(string menuid, string pincode = null)
        {
            return FrontProductHelper.GetFrontProductByMenuIdList(menuid, pincode);
        }

        public static List<OrderDetail> GetorderList(string userid)
        {
            return FrontProductHelper.GetorderList(userid);
        }
        public static List<OrderDetail> GetorderDetails(string productid)
        {
            return FrontProductHelper.GetorderDetails(productid);
        }
        public static List<OrderDetail> GetorderListbyOrderid(string oderid)
        {
            return FrontProductHelper.GetorderListbyOrderid(oderid);
        }       
    }
}