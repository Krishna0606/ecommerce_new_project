﻿using ecommerce.Helper.FrontHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ecommerce.Models.Frontend.CheckoutModel;

namespace ecommerce.Service.FrontService
{
    public static class CheckoutService
    {
        public static List<CheckoutShippingAddress> GetShippingAddressDetail(int loginid, string userid, string isDefault = null)
        {
            return CheckoutHelper.GetShippingAddressDetail(loginid, userid, isDefault);
        }

        public static bool UpdateShippingAddressDetail(string addressid, int loginid)
        {
            return CheckoutHelper.UpdateShippingAddressDetail(addressid, loginid);
        }

        public static bool InsertBookingOrder(BookingOrder bookingOrder)
        {
            return CheckoutHelper.InsertBookingOrder(bookingOrder);
        }

        public static bool InsertOrderDelieveryAddress(OrderDelieveryAddress delAddress)
        {
            return CheckoutHelper.InsertOrderDelieveryAddress(delAddress);
        }
        public static bool DeleteAddToCartDetailByUserId(string userId)
        {
            return CheckoutHelper.DeleteAddToCartDetailByUserId(userId);
        }
    }
}