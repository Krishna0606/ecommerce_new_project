﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ecommerce.Helper;
using ecommerce.Models.Backend;
using ecommerce.Models.Frontend;

namespace ecommerce.Service
{
    public static class AccountService
    {
        public static bool BackendLogin(string userName, string password, ref string msg, bool rememberMe = false)
        {
            return AccountHelper.BackendLogin(userName, password, ref msg, rememberMe);
        }

        public static bool LogoutBackendLogin()
        {
            return AccountHelper.LogoutBackendLogin();
        }

        public static bool IsBackendLogin(ref BackendLogin lu)
        {
            return AccountHelper.IsBackendLogin(ref lu);
        }

        public static bool FrontendLogin(string userName, string password, ref string msg, bool rememberMe = false)
        {
            return AccountHelper.FrontendLogin(userName, password, ref msg, rememberMe);
        }
        public static bool IsFrontendLogin(ref FrontEndLogin lu)
        {
            return AccountHelper.IsFrontendLogin(ref lu);
        }

        public static bool LogoutFrontendLogin()
        {
            return AccountHelper.LogoutFrontendLogin();
        }

        public static bool ProcessVerifyOtpValid(string emailotp, string mobileotp, string userid, string password, ref string msg)
        {
            return AccountHelper.ProcessVerifyOtpValid(emailotp, mobileotp, userid, password, ref msg);
        }

        public static bool SendVerificationCode(string userid, string password, ref string msg)
        {
            return AccountHelper.SendVerificationCode(userid, password, ref msg);
        }
    }
}