﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ecommerce.Models.Backend.BasicMaster;
using ecommerce.Helper.BackEndHelper;

namespace ecommerce.Service.BackEndService
{
    public static class BasicMasterService
    {
        public static int InsertMenuDetail(string menuName, string isfeatured)
        {
            return BasicMasterHelper.InsertMenuDetail(menuName, isfeatured);
        }
        public static bool UpdateMenuDetail(string menuid, string menuname = null, string isfeatured = null)
        {
            return BasicMasterHelper.UpdateMenuDetail(menuid, menuname, isfeatured);
        }

        public static bool UpdateMenuImage(string menuid, string imageurlname)
        {
            return BasicMasterHelper.UpdateMenuImage(menuid, imageurlname);
        }
        public static List<MenuModel> GetmenuDetail(string menuid = null, bool status = true)
        {
            return BasicMasterHelper.GetmenuDetail(menuid, status);
        }

        public static List<CategoryModel> GetCategoryDetail(string Categoryid = null, bool status = true, string menu = null)
        {
            return BasicMasterHelper.GetCategoryDetail(Categoryid, status, menu);
        }
        public static bool InsertCategoryDetail(string categoryname, string menu)
        {
            return BasicMasterHelper.InsertCategoryDetail(categoryname, menu);
        }
        public static bool UpdateCategoryDetail(string categoryid, string categoryname, string menu)
        {
            return BasicMasterHelper.UpdateCategoryDetail(categoryid, categoryname, menu);
        }

        public static List<SubCategoryModel> GetSubCategoryDetail(string subcatid = null, string menuid = null, string catid = null, bool status = true)
        {
            return BasicMasterHelper.GetSubCategoryDetail(subcatid, menuid, catid, status);
        }
        public static bool InsertSubCategoryDetail(string menuid, string catid, string subcatname)
        {
            return BasicMasterHelper.InsertSubCategoryDetail(menuid, catid, subcatname);
        }
        public static bool UpdateSubCategoryDetail(string subcatid, string menuid, string catid, string subcatname)
        {
            return BasicMasterHelper.UpdateSubCategoryDetail(subcatid, menuid, catid, subcatname);
        }
        public static bool InsertBrandDetail(string BrandName)
        {
            return BasicMasterHelper.InsertBrandDetail(BrandName);
        }
        public static bool UpdateBrandDetail(string brandname = null, string brandid = null)
        {
            return BasicMasterHelper.UpdateBrandDetail(brandname, brandid);
        }
        public static bool InsertUnitDetail(string UnitName)
        {
            return BasicMasterHelper.InsertUnitDetail(UnitName);
        }
        public static bool UpdateUnitDetail(string unitname = null, string unitid = null)
        {
            return BasicMasterHelper.UpdateUnitDetail(unitname, unitid);
        }
        public static List<BrandModel> GetBrandDetail(string brandId = null, bool status = true)
        {
            return BasicMasterHelper.GetBrandDetail(brandId, status);
        }

        public static List<UnitModel> GetUnitDetail(string unitId = null, bool status = true)
        {
            return BasicMasterHelper.GetUnitDetail(unitId, status);
        }
    }
}