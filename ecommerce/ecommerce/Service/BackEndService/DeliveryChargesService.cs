﻿using ecommerce.Helper.BackEndHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ecommerce.Models.Backend.SettingMaster;

namespace ecommerce.Service.BackEndService
{
    public static class DeliveryChargesService
    {
        public static bool InsertDeliveryCharges(DeliveryCharges dcharges)
        {
            return DeliveryChargesHelper.InsertDeliveryCharges(dcharges);
        }
        public static bool UpdateDeliveryCharges(DeliveryCharges dcharges)
        {
            return DeliveryChargesHelper.UpdateDeliveryCharges(dcharges);
        }

        public static bool DeleteDeliveryCharges(DeliveryCharges dcharges)
        {
            return DeliveryChargesHelper.DeleteDeliveryCharges(dcharges);
        }
    }
}