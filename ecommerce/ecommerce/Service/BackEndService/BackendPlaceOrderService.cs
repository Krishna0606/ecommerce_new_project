﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ecommerce.Models.Backend.Dashboard;
using ecommerce.Helper.BackEndHelper;

namespace ecommerce.Service.BackEndService
{
    public static class BackendPlaceOrderService
    {
        public static List<DashboardOrders> GetPlaceOrderDetails(string userid = null, string userType = null)
        {
            return BackendPlaceOrderHelper.GetPlaceOrderDetails(userid, userType);
        }
        public static List<UserLoginDetails> UserLoginDetails()
        {
            return BackendPlaceOrderHelper.UserLoginDetails();
        }
    }
}