﻿using ecommerce.Helper.BackEndHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ecommerce.Models.Backend.ProductMaster;

namespace ecommerce.Service.BackEndService
{
    public static class ProductMasterService
    {
        public static List<Product> GetProductDetail(ProductFilter filter)
        {
            return ProductMasterHelper.GetProductDetail(filter);
        }
        public static int InsertProductDetail(Product product)
        {
            return ProductMasterHelper.InsertProductDetail(product);
        }

        public static bool UpdateProductDetail(Product product)
        {
            return ProductMasterHelper.UpdateProductDetail(product);
        }

        public static bool ArchiveUnArchiveProductDetail(string productid, string status)
        {
            return ProductMasterHelper.ArchiveUnArchiveProductDetail(productid, status);
        }

        public static List<ProductImage> GetProductImagesById(string productid)
        {
            return ProductMasterHelper.GetProductImagesById(productid);
        }
        public static int InsertProductImageDetail(string imagename, string productid)
        {
            return ProductMasterHelper.InsertProductImageDetail(imagename, productid);
        }

        public static bool UpdateProductImageUrl(string productid, string imageid, string imageurlname)
        {
            return ProductMasterHelper.UpdateProductImageUrl(productid, imageid, imageurlname);
        }

        public static bool DeleteProductImage(string imgid, string productid)
        {
            return ProductMasterHelper.DeleteProductImage(imgid, productid);
        }

        public static bool InsertDescriptionDetails(string productid, string[] desclist)
        {
            return ProductMasterHelper.InsertDescriptionDetails(productid, desclist);
        }

        public static List<string> GetDescriptionDetails(string productid)
        {
            return ProductMasterHelper.GetDescriptionDetails(productid);
        }
    }
}