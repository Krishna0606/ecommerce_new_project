﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ecommerce.Helper.BackEndHelper;
using ecommerce.Models.Backend;

namespace ecommerce.Service.BackEndService
{
    public static class MemberRegService
    {
        public static List<BackendLogin> GetMemRegDetail(string memberid = null, bool status = true)
        {
            return MemberRegHelper.GetMemRegDetail(memberid, status);
        }
        public static bool InsertMemberRegDetails(BackendLogin supplier)
        {
            return MemberRegHelper.InsertMemberRegDetails(supplier);
        }
        public static bool UpdateMemberRegDetails(BackendLogin supplier)
        {
            return MemberRegHelper.UpdateMemberRegDetails(supplier);
        }
        public static bool CheckUserIdExist(string userid)
        {
            return MemberRegHelper.CheckUserIdExist(userid);
        }
        public static bool InsertUserRegDetails(string firstname, string lastname, string emailid, string mobileno, string userid, string regpassword)
        {
            return MemberRegHelper.InsertUserRegDetails(firstname, lastname, emailid, mobileno, userid, regpassword);
        }

        public static List<string> GetSupplierAreaList(string pincode)
        {
            return MemberRegHelper.GetSupplierAreaList(pincode);
        }
    }
}