﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ecommerce.Helper.BackEndHelper;
using static ecommerce.Models.Backend.AreaMaster;

namespace ecommerce.Service.BackEndService
{
    public static class AreaMasterService
    {
        public static List<Country> GetCountryDetail(string countryid = null, bool status = true)
        {
            return AreaMasterHelper.GetCountryDetail(countryid, status);
        }
        public static bool InsertCountryDetail(string countryName, string countryCode)
        {
            return AreaMasterHelper.InsertCountryDetail(countryName, countryCode);
        }

        public static bool UpdateCountryDetail(string countryName = null, string countryCode = null, string countryid = null)
        {
            return AreaMasterHelper.UpdateCountryDetail(countryName, countryCode, countryid);
        }

        public static List<State> GetStateDetail(string stateid = null, bool status = true, string countryId = null)
        {
            return AreaMasterHelper.GetStateDetail(stateid, status, countryId);
        }

        public static bool InsertStateDetail(string stateName, string stateCode, string countryId)
        {
            return AreaMasterHelper.InsertStateDetail(stateName, stateCode, countryId);
        }

        public static bool UpdateStateDetail(string stateid, string stateName, string stateCode, string countryId)
        {
            return AreaMasterHelper.UpdateStateDetail(stateid, stateName, stateCode, countryId);
        }

        public static List<City> GetCityDetail(string cityid = null, string stateid = null, bool status = true)
        {
            return AreaMasterHelper.GetCityDetail(cityid, stateid, status);
        }

        public static bool InsertCityDetail(string cityName, string cityCode, string countryId, string stateId)
        {
            return AreaMasterHelper.InsertCityDetail(cityName, cityCode, countryId, stateId);
        }

        public static bool UpdateCityDetail(string cityId, string cityName, string cityCode, string countryId, string stateId)
        {
            return AreaMasterHelper.UpdateCityDetail(cityId, cityName, cityCode, countryId, stateId);
        }
    }
}