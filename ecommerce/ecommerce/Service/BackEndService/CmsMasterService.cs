﻿using ecommerce.Helper.BackEndHelper;
using ecommerce.Models.Backend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecommerce.Service.BackEndService
{
    public static class CmsMasterService
    { 
        public static bool InsertAboutUs(AboutUsModel model)
        {
            return CmsHelper.InsertAboutUs(model);
        }
        public static bool UpdateAboutUs(int? aboutid,AboutUsModel model)
        {
            return CmsHelper.UpdateAboutUs(aboutid, model);
        }
        public static AboutUsModel GetaboutModelbyID(int? aboutid)
        {
            return CmsHelper.GetaboutModelbyID(aboutid);
        }
        public static List<AboutUsModel> Getaboutuslist()
        {
            return CmsHelper.Getaboutuslist();
        }
        public static List<PrivicyPolicyModel> GetaPrivicyPolicylist()
        {
            return CmsHelper.GetaPrivicyPolicylist();
        }
        public static PrivicyPolicyModel GetPrivicyModelbyID(int? privicyid)
        {
            return CmsHelper.GetPrivicyModelbyID(privicyid);
        }
        public static bool UpdatePrivicy(int? privicyid, PrivicyPolicyModel model)
        {
            return CmsHelper.UpdatePrivicy(privicyid, model);
        }
        public static bool Insertprivicy(PrivicyPolicyModel model)
        {
            return CmsHelper.Insertprivicy(model);
        }

    }
}