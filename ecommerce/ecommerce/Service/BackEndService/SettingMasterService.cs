﻿using ecommerce.Helper.BackEndHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ecommerce.Models.Backend.SettingMaster;

namespace ecommerce.Service.BackEndService
{
    public static class SettingMasterService
    {
        public static List<DeliveryCharges> GetDeliveryChargesDetail(int supplierId, string dcid = null, bool status = true)
        {
            return SettingMasterHelper.GetDeliveryChargesDetail(supplierId, dcid, status);
        }
        public static List<HomeBannerModel> GetHomeBannerList(string bannerId = null, string isShow = null, string status = null)
        {
            return SettingMasterHelper.GetHomeBannerList(bannerId, isShow, status);
        }
        public static int InsertBannerDetail(HomeBannerModel banner)
        {
            return SettingMasterHelper.InsertBannerDetail(banner);
        }
        public static bool UpdateBannerImage(string bannerId, string imagename)
        {
            return SettingMasterHelper.UpdateBannerImage(bannerId, imagename);
        }

        public static bool UpdateBannerOrder(string bannerid, string posval)
        {
            return SettingMasterHelper.UpdateBannerOrder(bannerid, posval);
        }
    }
}