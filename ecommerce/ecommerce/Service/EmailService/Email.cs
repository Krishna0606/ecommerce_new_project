﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Razor;
using ecommerce.Helper.DataBase;
using ecommerce.Helper.EmailHelper;
using ecommerce.Models.Common;
using ecommerce.Service.Front;
using static ecommerce.Models.Frontend.FrontUserEnd;

namespace ecommerce.Service.EmailService
{
    public static class Email
    {
        public static bool SendUserRegistEmail(string firstname, string lastname, string emailid, string mobileno, string userid, string regpassword)
        {
            StringBuilder message = new StringBuilder();
            message.Append(Utility.ReadHTMLTemplate("UserRejistrationDetails.html"));
            message.Replace("#CompanyLogo#", (Config.WebsiteUrl + Config.SendEmailcompanylogo));
            message.Replace("#WebsiteUrl#", Config.WebsiteUrl);
            message.Replace("#FullName#", (firstname + " " + lastname));
            message.Replace("#EmailId#", emailid);
            message.Replace("#MobileNo#", mobileno);
            message.Replace("#UserId#", userid);
            message.Replace("#Password#", regpassword);

            string subject = "Your vocal go local registration details from " + Config.WebsiteName;

            if (EmailHelp.SendEmail(Config.AdminEmailService, emailid, subject, message.ToString(), "User Registration"))
            {
                return true;
            }

            return false;
        }
        public static bool SendUserInvoice(string oderid, string useremail, string userAddress, string userlandmark, string userMobileNo, string userCity, string userState, string UserPinCode)
        {
            OrderDetail model = new OrderDetail();
            try
            {
                List<OrderDetail> orderList = FrontProductService.GetorderListbyOrderid(oderid);
                if (orderList != null && orderList.Count > 0)
                {
                    model.OrderList = orderList;
                }
                foreach (var item in model.OrderList)
                {
                    model.Supppierid = item.Supppierid; ;
                    model.SupplierName = item.SupplierName;
                    model.SupplierEmail = item.SupplierEmail;
                    model.UserId = item.UserId;
                    model.UserName = item.UserName;
                    break;
                }
                string Ufileds = "*";
                string Utablename = "T_Login";
                string UwhereCondition = "status=1 and loginid=" + model.UserId + "";
                DataTable dtuser = ConnectToDataBase.GetRecordFromTable(Ufileds, Utablename, UwhereCondition);
                if (dtuser != null && dtuser.Rows.Count > 0)
                {
                    for (int i = 0; i < dtuser.Rows.Count; i++)
                    {
                        model.UserMobile = !string.IsNullOrEmpty(dtuser.Rows[i]["MobileNo"].ToString()) ? dtuser.Rows[i]["MobileNo"].ToString() : string.Empty;
                        model.UserCity = !string.IsNullOrEmpty(dtuser.Rows[i]["City"].ToString()) ? dtuser.Rows[i]["City"].ToString() : string.Empty;
                        model.Userpin = !string.IsNullOrEmpty(dtuser.Rows[i]["PinCode"].ToString()) ? dtuser.Rows[i]["PinCode"].ToString() : string.Empty;
                        model.UserAddress = !string.IsNullOrEmpty(dtuser.Rows[i]["Address"].ToString()) ? dtuser.Rows[i]["Address"].ToString() : string.Empty;
                    }
                }
                string fileds = "*";
                string tablename = "T_Login";
                string whereCondition = "status=1 and loginid=" + model.Supppierid + "";
                DataTable dtsupplior = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtsupplior != null && dtsupplior.Rows.Count > 0)
                {
                    for (int i = 0; i < dtsupplior.Rows.Count; i++)
                    {
                        model.SupplierMobileNo = !string.IsNullOrEmpty(dtsupplior.Rows[i]["MobileNo"].ToString()) ? dtsupplior.Rows[i]["MobileNo"].ToString() : string.Empty;
                        model.SupplierCity = !string.IsNullOrEmpty(dtsupplior.Rows[i]["City"].ToString()) ? dtsupplior.Rows[i]["City"].ToString() : string.Empty;
                        model.SupplierPin = !string.IsNullOrEmpty(dtsupplior.Rows[i]["PinCode"].ToString()) ? dtsupplior.Rows[i]["PinCode"].ToString() : string.Empty;
                        model.SupplierAddress = !string.IsNullOrEmpty(dtsupplior.Rows[i]["Address"].ToString()) ? dtsupplior.Rows[i]["Address"].ToString() : string.Empty;
                        model.SupplierPanNo = !string.IsNullOrEmpty(dtsupplior.Rows[i]["PanNo"].ToString()) ? dtsupplior.Rows[i]["PanNo"].ToString() : string.Empty;
                        model.SupplierGstNo = !string.IsNullOrEmpty(dtsupplior.Rows[i]["GSTNo"].ToString()) ? dtsupplior.Rows[i]["GSTNo"].ToString() : string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            StringBuilder message = new StringBuilder();
            message.Append(Utility.ReadHTMLTemplate("ProductInvoice.html"));
            message.Replace("#CompanyLogo#", (Config.WebsiteUrl + Config.SendEmailcompanylogo));
            message.Replace("#WebsiteUrl#", Config.WebsiteUrl);
            message.Replace("#orderno#", oderid);
            message.Replace("#UserName#", model.UserName);
            message.Replace("#useraddress#", userAddress);
            message.Replace("#landmark#", oderid);
            message.Replace("#landmark#", userlandmark);
            message.Replace("#userCity#", userCity);
            message.Replace("#userpin#", UserPinCode);
            message.Replace("#userPhone#", userMobileNo);
            message.Replace("#userbaddress#", model.UserAddress);
            message.Replace("#landmark#", userlandmark);
            message.Replace("#userbCity#", model.UserCity);
            message.Replace("#userbpin#", model.Userpin);
            message.Replace("#userbPhone#", model.UserMobile);
            message.Replace("#suppliorname#", model.SupplierName);
            message.Replace("#supplioremail#", model.SupplierEmail);
            message.Replace("#suppphone#", model.SupplierMobileNo);
            message.Replace("#supcity#", model.SupplierCity);
            message.Replace("#suppin#", model.SupplierPin);
            message.Replace("#gstno#", model.SupplierGstNo);
            message.Replace("#panno#", model.SupplierPanNo);
            message.Replace("#supaddress#", model.SupplierAddress);
            message.Replace("#Date#", DateTime.Now.ToString("dd/MM/yyyy"));
            string billdiscription = string.Empty;
            decimal grandtotal = 0;
            decimal total = 0;
            int totalproduct = 0;
            foreach (var item in model.OrderList)
            {
                total = Convert.ToDecimal(item.GrandTotal) * Convert.ToDecimal(item.Quantity);
                billdiscription += "<tr height='50'>"
                    + "  <td style='border-right:1px solid #b3b3b3!important;border-bottom:1px solid #b3b3b3;color:#484848;text-align:center;'>" + item.ProductName + "</td>"
                    + " <td style='border-right:1px solid #b3b3b3!important;border-bottom:1px solid #b3b3b3;color:#484848;text-align:center;'>" + item.Unit + "</td>"
                    + " <td style='border-right:1px solid #b3b3b3!important;border-bottom:1px solid #b3b3b3;color:#484848;text-align:center;'>" + item.Quantity + "</td>"
                    + " <td style='border-right:1px solid #b3b3b3!important;border-bottom:1px solid #b3b3b3;color:#484848;text-align:center;'>" + total + "</td>"
                    + " <td style='border-right:1px solid #b3b3b3!important;border-bottom:1px solid #b3b3b3;color:#484848;text-align:center;'>0 %</td>"
                    + " <td style='border-bottom:1px solid #b3b3b3;color:#484848;text-align:center;'>" + total + "</td>";
                billdiscription += "</tr>";
                grandtotal = (grandtotal + Convert.ToDecimal(total));
                totalproduct = totalproduct + 1;
            }
            message.Replace("#billdisriptopn#", billdiscription);
            message.Replace("#grandtotal#", grandtotal.ToString());
            message.Replace("#totalproduct#", totalproduct.ToString());
            string subject = "Your vocal go local Oder Detials details from " + Config.WebsiteName;

            if (EmailHelp.SendEmail(Config.AdminEmailService, useremail, subject, message.ToString(), "Product Invoice"))
            {
                return true;
            }

            return false;
        }
        public static bool SendMobileSms(string mobileNo, string message)
        {
            return EmailHelp.SendMobileSms(mobileNo, message);
        }
    }
}