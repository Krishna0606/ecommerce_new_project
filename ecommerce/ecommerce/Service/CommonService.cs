﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ecommerce.Models.Backend.AreaMaster;
using ecommerce.Helper.BackEndHelper;
using static ecommerce.Models.Backend.BasicMaster;
using ecommerce.Helper;

namespace ecommerce.Service
{
    public static class CommonService
    {
        public static List<Country> GetCountryDetail(string countryid = null, bool status = true)
        {
            return AreaMasterHelper.GetCountryDetail(countryid, status);
        }

        public static List<State> GetStateDetail(string stateid = null, bool status = true, string countryId = null)
        {
            return AreaMasterHelper.GetStateDetail(stateid, status, countryId);
        }

        public static List<City> GetCityDetail(string cityid = null, string stateid = null, bool status = true)
        {
            return AreaMasterHelper.GetCityDetail(cityid, stateid, status);
        }

        public static List<MenuModel> GetMenuList(bool status = true)
        {
            return CommonHelper.GetMenuList(status);
        }

        public static List<CategoryModel> GetCategoryList(string menuid)
        {
            return CommonHelper.GetCategoryList(menuid);
        }

        public static List<StateModel> GetStateList(string stateid = null)
        {
            return CommonHelper.GetStateList(stateid);
        }

        public static List<CityModel> GetCityList(string stateid = null)
        {
            return CommonHelper.GetCityList(stateid);
        }
    }
}