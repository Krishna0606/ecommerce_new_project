﻿using ecommerce.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ecommerce.Models.FrontMenuModel;

namespace ecommerce.Service
{
    public static class FrontMenuService
    {
        public static List<FrontMainCategoryModel> GetFrontMenu()
        {
            return FrontMenuHelper.GetFrontMenu();
        }
    }
}