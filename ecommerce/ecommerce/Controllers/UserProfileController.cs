﻿using ecommerce.Models.Common;
using ecommerce.Models.Frontend;
using ecommerce.Service;
using ecommerce.Service.BackEndService;
using ecommerce.Service.Front;
using ecommerce.Service.FrontService;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using static ecommerce.Models.Frontend.CheckoutModel;
using static ecommerce.Models.Frontend.FrontUserEnd;

namespace ecommerce.Controllers
{
    public class UserProfileController : Controller
    {
        // GET: UserProfile
        public ActionResult ManageAddress()
        {
            ShippingAddress model = new ShippingAddress();

            try
            {
                model.StateList = Common.PopulateState(AreaMasterService.GetStateDetail(null, true, null));
                model.IsDefault = true;
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }

        public ActionResult UserProfile()
        {
            Checkout model = new Checkout();

            try
            {
                CheckoutShippingAddress shipAdd = new CheckoutShippingAddress();

                shipAdd.StateList = Common.PopulateState(AreaMasterService.GetStateDetail(null, true, null));
                shipAdd.IsDefault = true;
                FrontEndLogin lu = new FrontEndLogin();
                if (AccountService.IsFrontendLogin(ref lu))
                {
                    shipAdd.LogId = lu.LoginId;                    
                    shipAdd.FirstName = lu.FirstName;
                    shipAdd.LastName = lu.LastName;
                    shipAdd.EmailId = lu.EmailId;
                    shipAdd.MobileNo = lu.MobileNo;                 

                }
                model.ShippingAddress = shipAdd;
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public ActionResult OrderList()
        {
            OrderDetail model = new OrderDetail();
            try
            {
                FrontEndLogin lu = new FrontEndLogin();
                if (AccountService.IsFrontendLogin(ref lu))
                {
                    List<OrderDetail> orderList = FrontProductService.GetorderList(lu.LoginId.ToString());
                    if (orderList != null && orderList.Count > 0)
                    {
                        model.OrderList = orderList;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        public ActionResult OrderDetails()
        {
            OrderDetail model = new OrderDetail();
            string productid = !string.IsNullOrEmpty(Request.QueryString["Productid"]) ? Request.QueryString["Productid"] : string.Empty;
            List<OrderDetail> OrderDetailsList = FrontProductService.GetorderDetails(productid);
            if (OrderDetailsList != null && OrderDetailsList.Count > 0)
            {
                model.OederNo = OrderDetailsList[0].OederNo;
                model.ProductCode = OrderDetailsList[0].ProductCode;
                model.ProductName = OrderDetailsList[0].ProductName;
                model.DeliverCharges = OrderDetailsList[0].DeliverCharges;
                model.DeliverStatus = OrderDetailsList[0].DeliverStatus;
                model.ProductImgUrl = OrderDetailsList[0].ProductImgUrl;
                model.GrandTotal = OrderDetailsList[0].GrandTotal;
                model.Unit = OrderDetailsList[0].Unit;
                model.SupplierName = OrderDetailsList[0].SupplierName;
                model.UserName = OrderDetailsList[0].UserName;
                model.UserAddress = OrderDetailsList[0].UserAddress;
                model.UserEmailid = OrderDetailsList[0].UserEmailid;
                model.Landmark = OrderDetailsList[0].Landmark;
                model.UserMobile = OrderDetailsList[0].UserMobile;
                model.City = OrderDetailsList[0].City;
                model.State = OrderDetailsList[0].State;
                model.pincode = OrderDetailsList[0].pincode;
            }

            return View(model);
        }
        public JsonResult CreateShippingAddress(ShippingAddress shippingadd)
        {
            List<string> result = new List<string>();

            try
            {
                FrontEndLogin lu = new FrontEndLogin();
                if (AccountService.IsFrontendLogin(ref lu))
                {
                    shippingadd.LogId = lu.LoginId;
                    shippingadd.UserId = lu.UserId;
                    shippingadd.UserName = lu.FirstName + " " + lu.LastName;

                    if (UserProfileService.InsertShippingAddressDetail(shippingadd))
                    {
                        result.Add("true");
                        result.Add(lu.LoginId.ToString()); ;
                        result.Add(lu.UserId);
                    }
                }
                else
                {
                    if (AccountService.LogoutFrontendLogin())
                    {
                        Response.Redirect("/");
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return Json(result);
        }

        public JsonResult GetJsonGetShippingAddress(int loginid, string userid)
        {
            List<string> resultList = GetShippingAddress(loginid, userid);

            return Json(resultList);
        }
        public JsonResult UserprofilepageDetail()
        {
            List<string> result = new List<string>();

            try
            {
                FrontEndLogin lu = new FrontEndLogin();
                if (AccountService.IsFrontendLogin(ref lu))
                {
                    result.Add("true");

                    List<string> shippingAdd = GetShippingAddress(lu.LoginId, lu.UserId);
                    if (shippingAdd.Count > 0)
                    {
                        if (shippingAdd[0] == "true")
                        {
                            result.Add(shippingAdd[1]);
                        }
                        else
                        {
                            result.Add("<p class='text-center text-danger'>No address available!</p>");
                        }
                    }
                }
                else
                {
                    bool isLogout = AccountService.LogoutFrontendLogin();
                    result.Add("false");
                    result.Add("logout");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }

        public List<string> GetShippingAddress(int loginid, string userid)
        {
            List<string> resultList = new List<string>();

            try
            {
                List<CheckoutShippingAddress> shippingAddress = CheckoutService.GetShippingAddressDetail(loginid, userid);
                if (shippingAddress.Count > 0)
                {
                    resultList.Add("true");
                    string result = string.Empty;
                    foreach (var item in shippingAddress)
                    {
                        if (item.IsDefault)
                        {

                            result += "<div class='col-sm-12 form-group adressbox'>";
                            result += "<span class='addresstype'>" + item.AddressType + "</span><span class='dropdowntogle pull-right'><span class='fa fa-ellipsis-v  listmenu'><span class='dropdowntogle-content'><a href='#'>Edit</a><a href='#'>Delete</a></span></span></span>";
                            result += "<p><span style='font-weight:bold;'>" + item.FirstName + " " + item.LastName + "</p>";
                            result += "<p>Email- " + item.EmailId + "</p>";
                            result += "<p>" + item.Address + ", " + item.City + ",<span>(" + item.Landmark + ")</span>,<span>" + item.State + ", India</span>,<span>Pin Code- " + item.PinCode + "</span></p>";                          
                            result += "<p>Contact- " + item.MobileNo + (!string.IsNullOrEmpty(item.AlternateMobileNo) ? "," + item.AlternateMobileNo : string.Empty) + "</p>";
                            result += "</div>";
                        }
                        else
                        {
                            result += "<div class='col-sm-12 form-group adressbox'>";
                            result += "<span class='addresstype'>" + item.AddressType + "</span><span class='dropdowntogle pull-right'><span class='fa fa-ellipsis-v listmenu'><span class='dropdowntogle-content'><a href='#'>Edit</a><a href='#'>Delete</a></span></span></span>";
                            result += "<p><span style='font-weight:bold;'>" + item.FirstName + " " + item.LastName + "</p>";
                            result += "<p>Email- " + item.EmailId + "</p>";
                            result += "<p>" + item.Address + ", " + item.City + ",<span>(" + item.Landmark + ")</span>,<span>" + item.State + ", India</span>,<span>Pin Code- " + item.PinCode + "</span></p>";                           
                            result += "<p>Contact- " + item.MobileNo + (!string.IsNullOrEmpty(item.AlternateMobileNo) ? "," + item.AlternateMobileNo : string.Empty) + "</p>";
                            result += "</div>";
                        }
                    }
                    resultList.Add(result);
                }
                else
                {
                    resultList.Add("false");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return resultList;
        }
    }
}