﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ecommerce.Models.Backend.BasicMaster;
using ecommerce.Service.BackEndService;
using ecommerce.Models.Common;
using System.IO;

namespace ecommerce.Controllers
{
    public class BasicMasterController : Controller
    {
        public ActionResult MenuList(string menuid = null, bool status = true)
        {
            MenuModel model = new MenuModel();

            try
            {
                List<MenuModel> menuList = BasicMasterService.GetmenuDetail(menuid, status);
                if (menuList != null && menuList.Count > 0)
                {
                    model.MenuList = menuList;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public ActionResult AddEditMenu(string menuid = null, bool status = true)
        {
            MenuModel model = new MenuModel();

            try
            {
                if (!string.IsNullOrEmpty(menuid))
                {
                    List<MenuModel> MenuList = BasicMasterService.GetmenuDetail(menuid, status);
                    if (MenuList != null && MenuList.Count > 0)
                    {
                        model.Menuid = MenuList[0].Menuid;
                        model.MenuName = MenuList[0].MenuName;
                        model.Image = MenuList[0].Image;
                        model.IsFeatured = MenuList[0].IsFeatured;
                        model.Status = MenuList[0].Status;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public ActionResult CategoryList(string categoryid = null, bool status = true)
        {
            CategoryModel model = new CategoryModel();

            try
            {
                List<CategoryModel> CategoryList = BasicMasterService.GetCategoryDetail(categoryid, status);
                if (CategoryList != null && CategoryList.Count > 0)
                {
                    model.CategoryList = CategoryList;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public ActionResult AddEditCategory(string categoryid = null, bool status = true)
        {
            CategoryModel model = new CategoryModel();
            try
            {
                model.MenuList = Common.PopulateMenu(BasicMasterService.GetmenuDetail(null, true));
                if (!string.IsNullOrEmpty(categoryid))
                {
                    List<CategoryModel> categoryList = BasicMasterService.GetCategoryDetail(categoryid, status);
                    if (categoryList != null && categoryList.Count > 0)
                    {
                        model.CategoryId = categoryList[0].CategoryId;
                        model.Menu = categoryList[0].Menu;
                        model.CategoryName = categoryList[0].CategoryName;
                        model.Status = categoryList[0].Status;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public ActionResult SubCategoryList(string subcatid = null, bool status = true)
        {
            SubCategoryModel model = new SubCategoryModel();

            try
            {
                List<SubCategoryModel> subCatList = BasicMasterService.GetSubCategoryDetail(subcatid, null, null, status);
                if (subCatList != null && subCatList.Count > 0)
                {
                    model.SubCategoryList = subCatList;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public ActionResult AddEditSubCategory(string subcategoryid = null, bool status = true)
        {
            //SubCategoryModel model = new SubCategoryModel();

            //try
            //{
            //    model.CountryList = Common.PopulateCountry(AreaMasterService.GetCountryDetail(null, true));
            //    model.StateList = Common.PopulateState(AreaMasterService.GetStateDetail(null, true, "0"));

            //    if (!string.IsNullOrEmpty(cityid))
            //    {
            //        List<City> cityList = AreaMasterService.GetCityDetail(cityid, status);
            //        if (cityList != null && cityList.Count > 0)
            //        {
            //            model.CityId = cityList[0].StateId;
            //            model.CityName = cityList[0].CityName;
            //            model.CityCode = cityList[0].CityCode;
            //            model.CountryId = cityList[0].CountryId;
            //            model.StateId = cityList[0].StateId;
            //            model.Status = cityList[0].Status;

            //            model.StateList = Common.PopulateState(AreaMasterService.GetStateDetail(null, true, model.CountryId.ToString()));
            //        }
            //    }
            //}
            //catch (Exception)
            //{

            //    throw;
            //}

            return View();
        }
        public ActionResult BrandList(string brandid = null, bool status = true)
        {
            BrandModel model = new BrandModel();

            try
            {
                List<BrandModel> brandList = BasicMasterService.GetBrandDetail(brandid, status);
                if (brandList != null && brandList.Count > 0)
                {
                    model.BrandList = brandList;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public ActionResult AddEditBrand(string brandid = null, bool status = true)
        {
            BrandModel model = new BrandModel();

            try
            {
                if (!string.IsNullOrEmpty(brandid))
                {
                    List<BrandModel> BrnadList = BasicMasterService.GetBrandDetail(brandid, status);
                    if (BrnadList != null && BrnadList.Count > 0)
                    {
                        model.BrandId = BrnadList[0].BrandId;
                        model.BrandName = BrnadList[0].BrandName;
                        model.Status = BrnadList[0].Status;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public ActionResult UnitList(string unitid = null, bool status = true)
        {

            UnitModel model = new UnitModel();

            try
            {
                List<UnitModel> UnitList = BasicMasterService.GetUnitDetail(unitid, status);
                if (UnitList != null && UnitList.Count > 0)
                {
                    model.UnitList = UnitList;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public ActionResult AddEditUnit(string unitid = null, bool status = true)
        {
            UnitModel model = new UnitModel();

            try
            {
                if (!string.IsNullOrEmpty(unitid))
                {
                    List<UnitModel> unitList = BasicMasterService.GetUnitDetail(unitid, status);
                    if (unitList != null && unitList.Count > 0)
                    {
                        model.UnitId = unitList[0].UnitId;
                        model.UnitName = unitList[0].UnitName;
                        model.Status = unitList[0].Status;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }

        #region [Json Section]       
        public JsonResult ProcessInsertMenu(string menuid, string menuname, string isfeatured)
        {
            List<string> result = new List<string>(); ;

            try
            {
                if (!string.IsNullOrEmpty(menuname))
                {
                    if (string.IsNullOrEmpty(menuid) || menuid == "0")
                    {
                        int menuId = BasicMasterService.InsertMenuDetail(menuname, isfeatured);
                        if (menuId > 0)
                        {
                            result.Add("insert");
                            result.Add(menuId.ToString());
                            TempData["insertedmenuid"] = menuId.ToString();
                            //result.Add("Record inserted successfully.");
                        }
                    }
                    else
                    {
                        if (BasicMasterService.UpdateMenuDetail(menuid, menuname, isfeatured))
                        {
                            result.Add("update");
                            result.Add(menuid);
                            TempData["insertedmenuid"] = menuid;
                            //result.Add("Record updated successfully.");
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(menuid) && menuid != "0")
                    {
                        if (BasicMasterService.UpdateMenuDetail(menuid, null, null))
                        {
                            result.Add("true");
                            result.Add("Record deleted successfully.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }

        public JsonResult InsertMenuImage()
        {
            string returnMsg = string.Empty;
            try
            {
                if (TempData["insertedmenuid"] != null)
                {
                    string menuid = TempData["insertedmenuid"].ToString();
                    if (Request.Files.Count > 0)
                    {
                        HttpFileCollectionBase files = Request.Files;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            string fname = file.FileName;

                            if (!string.IsNullOrEmpty(fname))
                            {
                                Directory.CreateDirectory(Server.MapPath(Utility.GetFullPathStringWithAppData("MenuImages", ("Menu_" + menuid))));

                                string ext = Path.GetExtension(file.FileName);
                                string imageurlname = Utility.GenrateRandomTransactionId(menuid, 3) + ext;
                                string filePath = Server.MapPath(Utility.GetFullPathStringWithAppData("MenuImages", ("Menu_" + menuid)) + imageurlname);

                                if (System.IO.File.Exists(filePath))
                                {
                                    System.IO.File.Delete(filePath);
                                }

                                file.SaveAs(filePath);

                                if (BasicMasterService.UpdateMenuImage(menuid, imageurlname))
                                {
                                    returnMsg = "true";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(returnMsg);
        }
        public JsonResult ProcessInsertCategory(string categoryid, string categoryname, string menu)
        {
            List<string> result = new List<string>(); ;

            try
            {
                if (!string.IsNullOrEmpty(categoryname) && !string.IsNullOrEmpty(menu))
                {
                    if (string.IsNullOrEmpty(categoryid) || categoryid == "0")
                    {
                        if (BasicMasterService.InsertCategoryDetail(categoryname, menu))
                        {
                            result.Add("true");
                            result.Add("Record inserted successfully.");
                        }
                    }
                    else
                    {
                        if (BasicMasterService.UpdateCategoryDetail(categoryid, categoryname, menu))
                        {
                            result.Add("true");
                            result.Add("Record updated successfully.");
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(categoryid) && categoryid != "0")
                    {
                        if (BasicMasterService.UpdateCategoryDetail(categoryid, null, null))
                        {
                            result.Add("true");
                            result.Add("Record deleted successfully.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult ProcessInsertSubCategory(string subcatid, string menuid, string catid, string subcatname)
        {
            List<string> result = new List<string>(); ;

            try
            {
                if (!string.IsNullOrEmpty(menuid) && !string.IsNullOrEmpty(catid) && !string.IsNullOrEmpty(subcatname))
                {
                    if (subcatid == null && string.IsNullOrEmpty(subcatid))
                    {
                        if (BasicMasterService.InsertSubCategoryDetail(menuid, catid, subcatname))
                        {
                            result.Add("true");
                            result.Add("Record inserted successfully.");
                        }
                    }
                    else
                    {
                        if (BasicMasterService.UpdateSubCategoryDetail(subcatid, menuid, catid, subcatname))
                        {
                            result.Add("true");
                            result.Add("Record updated successfully.");
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(subcatid) && subcatid != "0")
                    {
                        if (BasicMasterService.UpdateSubCategoryDetail(subcatid, null, null, null))
                        {
                            result.Add("true");
                            result.Add("Record deleted successfully.");
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Json(result);
        }
        public JsonResult ProcessInsertBrand(string brandid, string brandname)
        {
            List<string> result = new List<string>(); ;

            try
            {
                if (!string.IsNullOrEmpty(brandname))
                {
                    if (string.IsNullOrEmpty(brandid) || brandid == "0")
                    {
                        if (BasicMasterService.InsertBrandDetail(brandname))
                        {
                            result.Add("true");
                            result.Add("Record inserted successfully.");
                        }
                    }
                    else
                    {
                        if (BasicMasterService.UpdateBrandDetail(brandname, brandid))
                        {
                            result.Add("true");
                            result.Add("Record updated successfully.");
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(brandid) && brandid != "0")
                    {
                        if (BasicMasterService.UpdateBrandDetail(null, brandid))
                        {
                            result.Add("true");
                            result.Add("Record deleted successfully.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult ProcessInsertUnit(string unitid, string unitname)
        {
            List<string> result = new List<string>(); ;

            try
            {
                if (!string.IsNullOrEmpty(unitname))
                {
                    if (string.IsNullOrEmpty(unitid) || unitid == "0")
                    {
                        if (BasicMasterService.InsertUnitDetail(unitname))
                        {
                            result.Add("true");
                            result.Add("Record inserted successfully.");
                        }
                    }
                    else
                    {
                        if (BasicMasterService.UpdateUnitDetail(unitname, unitid))
                        {
                            result.Add("true");
                            result.Add("Record updated successfully.");
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(unitid) && unitid != "0")
                    {
                        if (BasicMasterService.UpdateUnitDetail(null, unitid))
                        {
                            result.Add("true");
                            result.Add("Record deleted successfully.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }


        public JsonResult GetSubCategoryById(string subcategoryid)
        {
            List<string> result = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(subcategoryid))
                {
                    SubCategoryModel subCatList = BasicMasterService.GetSubCategoryDetail(subcategoryid, null, null, true).FirstOrDefault();
                    if (subCatList != null && subCatList.SubCategoryId > 0)
                    {
                        result.Add(subCatList.MenuId.ToString());
                        result.Add(subCatList.CategoryId.ToString());
                        result.Add(subCatList.SubCategoryName);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return Json(result);
        }
        #endregion
    }
}