﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ecommerce.Models.Backend.ProductMaster;
using ecommerce.Models.Backend;
using ecommerce.Service;
using ecommerce.Service.BackEndService;
using static ecommerce.Models.Backend.BasicMaster;
using System.Text;
using System.IO;
using ecommerce.Models.Common;

namespace ecommerce.Controllers
{
    public class ProductMasterController : Controller
    {
        public ActionResult ProductList()
        {
            Product model = new Product();

            try
            {
                ProductFilter filter = new ProductFilter();

                BackendLogin lu = new BackendLogin();
                if (AccountService.IsBackendLogin(ref lu))
                {
                    if (lu.UserType.ToLower() == "supplier")
                    {
                        filter.SupplierId = lu.LoginId;
                    }
                }

                List<Product> productList = ProductMasterService.GetProductDetail(filter);
                if (productList != null && productList.Count > 0)
                {
                    model.ProductList = productList;
                    model.SupplierType = lu.UserType;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public ActionResult AddEditProduct(string productid, string status)
        {
            Product model = new Product();

            try
            {
                ProductFilter filter = new ProductFilter();


                BackendLogin lu = new BackendLogin();
                if (AccountService.IsBackendLogin(ref lu))
                {
                    if (!string.IsNullOrEmpty(productid))
                    {
                        filter.ProductId = Convert.ToInt32(productid);
                        if (lu.UserType.ToLower().Trim() != "admin")
                        {
                            filter.SupplierId = lu.LoginId;
                        }
                        model = ProductMasterService.GetProductDetail(filter).FirstOrDefault();

                        model.MenuList = Common.PopulateMenu(BasicMasterService.GetmenuDetail(null, true));
                        model.CategoryList = Common.PopulateCategory(BasicMasterService.GetCategoryDetail(null, true, model.MenuId.ToString()));
                        model.SubCategoryList = Common.PopulateSubCategory(BasicMasterService.GetSubCategoryDetail(null, model.MenuId.ToString(), model.CategoryId.ToString(), true));
                        model.BrandList = Common.PopulateBrand(BasicMasterService.GetBrandDetail(null, true));
                        model.UnitList = Common.PopulateUnit(BasicMasterService.GetUnitDetail(null, true));
                    }
                    else
                    {
                        model.MenuList = Common.PopulateMenu(BasicMasterService.GetmenuDetail(null, true));
                        model.CategoryList = Common.PopulateCategory(BasicMasterService.GetCategoryDetail("0", true, "0"));
                        model.SubCategoryList = Common.PopulateSubCategory(BasicMasterService.GetSubCategoryDetail("0", "0", "0", true));
                        model.BrandList = Common.PopulateBrand(BasicMasterService.GetBrandDetail(null, true));
                        model.UnitList = Common.PopulateUnit(BasicMasterService.GetUnitDetail(null, true));
                    }
                }
                else
                {
                    if (AccountService.LogoutBackendLogin())
                    {
                        Response.Redirect("/backend/login");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return View(model);
        }
        public JsonResult ProcessInsertProduct(Product product)
        {
            List<string> result = new List<string>();

            try
            {
                if (product != null && !string.IsNullOrEmpty(product.ProductName))
                {
                    BackendLogin lu = new BackendLogin();
                    if (AccountService.IsBackendLogin(ref lu))
                    {
                        if (product.ProductId > 0)
                        {
                            if (ProductMasterService.UpdateProductDetail(product))
                            {
                                result.Add("update");
                                //result.Add(product.ProductId.ToString());
                                result.Add("Record updated successfully.");
                            }
                        }
                        else
                        {
                            product.SupplierId = lu.LoginId;
                            product.SupplierLoginId = lu.UserId;
                            product.SupplierName = lu.FirstName + "" + lu.LastName;

                            int lastProdtId = ProductMasterService.InsertProductDetail(product);
                            if (lastProdtId > 0)
                            {
                                result.Add("insert");
                                result.Add(lastProdtId.ToString());
                                result.Add("Record inserted successfully.");
                            }
                        }

                        //int lastProdtId = ProductMasterService.InsertProductDetail(product);
                        //if (lastProdtId > 0)
                        //{
                        //    result.Add("true");
                        //    result.Add(lastProdtId.ToString());
                        //    result.Add("Record inserted successfully.");
                        //}
                    }
                    else
                    {
                        if (AccountService.LogoutBackendLogin())
                        {
                            Response.Redirect("/backend/login");
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Json(result);
        }
        public JsonResult ProcessGetProductDetail(string productid)
        {
            Product product = new Product();

            try
            {
                ProductFilter filter = new ProductFilter();

                if (!string.IsNullOrEmpty(productid))
                {
                    filter.ProductId = Convert.ToInt32(productid);
                    product = ProductMasterService.GetProductDetail(filter).FirstOrDefault();
                }
            }
            catch (Exception)
            {

                throw;
            }


            return Json(product);
        }
        public JsonResult GetMenuList()
        {
            StringBuilder result = new StringBuilder();

            try
            {
                List<MenuModel> menuList = CommonService.GetMenuList(true);
                if (menuList != null && menuList.Count > 0)
                {
                    result.Append("<option value='0'>Select Menu</option>");
                    foreach (var item in menuList)
                    {
                        result.Append("<option value='" + item.Menuid + "'>" + item.MenuName + "</option>");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Json(result.ToString());
        }
        public JsonResult BindCategoryById(string menuid)
        {
            StringBuilder result = new StringBuilder();

            //try
            //{
            //    List<CategoryModel> menuList = CommonService.GetCategoryList(menuid);
            //    if (menuList != null && menuList.Count > 0)
            //    {
            //        result.Append("<option value='0'>Select Menu</option>");
            //        foreach (var item in menuList)
            //        {
            //            result.Append("<option value='" + item.Menuid + "'>" + item.MenuName + "</option>");
            //        }
            //    }
            //}
            //catch (Exception)
            //{
            //    throw;
            //}

            return Json(result.ToString());
        }
        public JsonResult SaveProductImageDetails(string imagename, string productid)
        {
            int result = 0;

            try
            {
                if (!string.IsNullOrEmpty(imagename))
                {
                    int lastImgId = ProductMasterService.InsertProductImageDetail(imagename, productid);

                    if (lastImgId > 0)
                    {
                        result = lastImgId;
                        TempData["insertedimage"] = lastImgId;
                        TempData["productid"] = productid;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Json(result);
        }
        public JsonResult UpdateLastInsertedImageName()
        {
            string returnMsg = string.Empty;
            try
            {
                if (TempData["insertedimage"] != null && TempData["productid"] != null)
                {
                    string imageid = TempData["insertedimage"].ToString();
                    string productid = TempData["productid"].ToString();

                    if (Request.Files.Count > 0)
                    {
                        HttpFileCollectionBase files = Request.Files;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            string fname = file.FileName;

                            if (!string.IsNullOrEmpty(fname))
                            {
                                Directory.CreateDirectory(Server.MapPath(Utility.GetFullPathStringWithAppData("ProductImages", ("Product_" + productid))));

                                string ext = Path.GetExtension(file.FileName);
                                string imageurlname = Utility.GenrateRandomTransactionId(imageid.ToString(), 3) + ext;
                                string filePath = Server.MapPath(Utility.GetFullPathStringWithAppData("ProductImages", ("Product_" + productid)) + imageurlname);

                                if (System.IO.File.Exists(filePath))
                                {
                                    System.IO.File.Delete(filePath);
                                }

                                file.SaveAs(filePath);

                                if (ProductMasterService.UpdateProductImageUrl(productid, imageid, imageurlname))
                                {
                                    returnMsg = "true";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(returnMsg);
        }
        public JsonResult GetAllImagesByProductId(string productid)
        {
            StringBuilder result = new StringBuilder();

            try
            {
                List<ProductImage> imgList = ProductMasterService.GetProductImagesById(productid);
                if (imgList != null && imgList.Count > 0)
                {
                    foreach (var item in imgList)
                    {
                        result.Append("<div class='col-sm-2' style='padding: 0px 30px 15px 0px;'>");
                        result.Append("<div class='row' style='border: 1px solid #ccc;padding: 6px;border-radius: 12px;'>");
                        result.Append("<div class='col-sm-12'>");
                        result.Append("<img src='" + item.ImageUrl + "' class='img-responsive' style='height:90px;width:100%;'>");
                        result.Append("</div>");
                        result.Append("<div class='col-sm-12'>");
                        result.Append("<span class='btn btn-danger btn-sm form-control imgactionevt' style='border-radius:0px;' id='deleteImg_" + item.ImageId + "' data-imgaction='delete' data-imageid='" + item.ImageId + "' data-productid='" + item.ProductId + "'>");
                        result.Append("<span class='fa fa-trash'></span> Remove </span>");
                        result.Append("");
                        result.Append("</div>");
                        result.Append("</div>");
                        result.Append("</div>");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Json(result.ToString());
        }
        public JsonResult ImageActionEvent(string imgid, string productid, string action)
        {
            List<string> returnMsg = new List<string>();

            if (!string.IsNullOrEmpty(imgid) && !string.IsNullOrEmpty(productid) && !string.IsNullOrEmpty(action))
            {
                if (action == "delete")
                {
                    if (ProductMasterService.DeleteProductImage(imgid, productid))
                    {
                        returnMsg.Add("true");
                    }
                }
                else
                {
                }
            }

            return Json(returnMsg);
        }

        public JsonResult InsertDescriptionDetails(string productid, string[] desclist)
        {
            List<string> result = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(productid) && !string.IsNullOrEmpty(desclist[0]))
                {
                    if (ProductMasterService.InsertDescriptionDetails(productid, desclist))
                    {
                        result.Add("true");
                        result.Add("Record updated successfully.");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Json(result);
        }

        [HttpPost]
        public ActionResult AddEditProduct(Product product)
        {
            try
            {
                if (product != null && !string.IsNullOrEmpty(product.ProductName))
                {
                    BackendLogin lu = new BackendLogin();
                    if (AccountService.IsBackendLogin(ref lu))
                    {
                        if (product.ProductId > 0)
                        {
                            if (ProductMasterService.UpdateProductDetail(product))
                            {
                                TempData["RecordInserted"] = "Record updated successfully.";
                                return Redirect("/product-master/add-edit-product?productid=" + product.ProductId + "&status=existing");
                            }
                        }
                        else
                        {
                            product.SupplierId = lu.LoginId;
                            product.SupplierLoginId = lu.UserId;
                            product.SupplierName = lu.FirstName + "" + lu.LastName;

                            int lastProdtId = ProductMasterService.InsertProductDetail(product);
                            if (lastProdtId > 0)
                            {
                                TempData["RecordInserted"] = "Record inserted successfully.";
                                return Redirect("/product-master/add-edit-product?productid=" + lastProdtId + "&status=existing");
                            }
                        }
                    }
                    else
                    {
                        if (AccountService.LogoutBackendLogin())
                        {
                            Response.Redirect("/backend/login");
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return View(product);
        }
        public ActionResult BindCategoryByMenuId(string menuid)
        {
            CategoryModel model = new CategoryModel();
            model.DropDownCategory = Common.PopulateCategory(BasicMasterService.GetCategoryDetail(null, true, menuid));
            return Json(model.DropDownCategory, JsonRequestBehavior.AllowGet);
        }
        public ActionResult BindSubCategoryByCatId(string menuid, string catid)
        {
            SubCategoryModel model = new SubCategoryModel();
            model.DropDownSubCategory = Common.PopulateSubCategory(BasicMasterService.GetSubCategoryDetail(null, menuid, catid, true));
            return Json(model.DropDownSubCategory, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetExistingDescDetail(string productid)
        {
            List<string> strList = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(productid))
                {
                    strList = ProductMasterService.GetDescriptionDetails(productid);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(strList);
        }

        public JsonResult DeleteProductDetail(string productid, string actiontype)
        {
            string result = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(productid))
                {
                    string status = actiontype == "delete" ? "0" : "1";

                    if (ProductMasterService.ArchiveUnArchiveProductDetail(productid, status))
                    {
                        result = "true";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
    }
}