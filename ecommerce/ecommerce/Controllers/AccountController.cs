﻿using ecommerce.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecommerce.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult BackendLogin()
        {
            return View();
        }
        public JsonResult ProcessBackendLogin(string username, string password, string isrember)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;

                bool isSuccessLogin = AccountService.BackendLogin(username, password, ref msg, (isrember == "true" ? true : false));
                if (isSuccessLogin)
                {
                    result.Add("true");
                    result.Add(msg);
                }
                else
                {
                    result.Add("false");
                    result.Add(msg);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult BackendLogOut()
        {
            string result = "false";

            try
            {
                if (AccountService.LogoutBackendLogin())
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult ProcessFrontEndLogin(string userid, string password)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;

                bool isSuccessLogin = AccountService.FrontendLogin(userid, password, ref msg, false);
                if (isSuccessLogin)
                {
                    result.Add("true");
                    result.Add(msg);
                }
                else
                {
                    result.Add("false");
                    result.Add(msg);
                    result.Add("notverified");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult ProcessVerifyOtpValid(string emailotp, string mobileotp, string userid, string password)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;
                if (AccountService.ProcessVerifyOtpValid(emailotp, mobileotp, userid, password, ref msg))
                {
                    result.Add("true");
                }
                else
                {
                    result.Add("false");
                    result.Add(msg);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult SendVerificationCode(string userid, string password)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;
                if (AccountService.SendVerificationCode(userid, password, ref msg))
                {
                    result.Add("true");
                }
                else
                {
                    result.Add("false");
                    result.Add(msg);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
    }
}