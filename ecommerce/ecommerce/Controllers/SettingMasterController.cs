﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ecommerce.Models.Backend.SettingMaster;
using ecommerce.Models.Backend;
using ecommerce.Service;
using ecommerce.Service.BackEndService;
using static ecommerce.Models.Backend.BasicMaster;
using ecommerce.Models.Common;
using System.IO;

namespace ecommerce.Controllers
{
    public class SettingMasterController : Controller
    {
        // GET: SettingMaster
        public ActionResult HomeBannerMaster()
        {
            HomeBannerModel model = new HomeBannerModel();
            try
            {
                model.HomeBannerList = SettingMasterService.GetHomeBannerList();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        public ActionResult AddEditHomeBanner()
        {
            return View();
        }
        public ActionResult DeliveryChargesList()
        {
            DeliveryCharges model = new DeliveryCharges();

            try
            {
                BackendLogin lu = new BackendLogin();
                if (AccountService.IsBackendLogin(ref lu))
                {                        //model.SupplierId = lu.LoginId;
                                         //model.SupplierLoginId = lu.UserId;
                                         //model.SupplierName = lu.FirstName + " " + lu.LastName;

                    List<DeliveryCharges> deliveryChargesList = SettingMasterService.GetDeliveryChargesDetail(lu.LoginId, null, true);
                    if (deliveryChargesList != null && deliveryChargesList.Count > 0)
                    {
                        model.DeliveryChargesList = deliveryChargesList;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return View(model);
        }
        public ActionResult AddEditDeliveryCharges(string dcid = null)
        {
            DeliveryCharges model = new DeliveryCharges();

            if (!string.IsNullOrEmpty(dcid))
            {
                if (!string.IsNullOrEmpty(dcid))
                {
                    BackendLogin lu = new BackendLogin();
                    if (AccountService.IsBackendLogin(ref lu))
                    {
                        model = SettingMasterService.GetDeliveryChargesDetail(lu.LoginId, dcid, true).FirstOrDefault();
                    }
                }
            }

            model.StateList = Common.PopulateState(AreaMasterService.GetStateDetail(null, true, null));
            model.CityList = Common.PopulateCity(AreaMasterService.GetCityDetail(null, model.StateId.ToString(), true));

            return View(model);
        }
        public JsonResult BindDCState()
        {
            string result = string.Empty;

            try
            {
                List<StateModel> stateList = CommonService.GetStateList(null);
                if (stateList != null && stateList.Count > 0)
                {
                    result = "<option value='0'>Select State</option>";
                    foreach (var item in stateList)
                    {
                        result += "<option value=" + item.StateId + ">" + item.StateName + "</option>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult BindDCCity(string stateid)
        {
            DeliveryCharges model = new DeliveryCharges();
            model.CityList = Common.PopulateCity(AreaMasterService.GetCityDetail(null, stateid, true));
            return Json(model.CityList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult InsertDeliveryCharges(DeliveryCharges dcharges)
        {
            List<string> result = new List<string>();
            try
            {
                BackendLogin lu = new BackendLogin();
                if (!AccountService.IsBackendLogin(ref lu))
                {
                    if (AccountService.LogoutBackendLogin())
                    {
                        //Response.Redirect("/backend/login");

                        result.Add("logout");
                        result.Add("/backend/login");
                    }
                }
                else
                {
                    if (dcharges.DeliveryChargesId > 0 && dcharges.StateId > 0 && dcharges.CityId > 0)
                    {
                        if (DeliveryChargesService.UpdateDeliveryCharges(dcharges))
                        {
                            result.Add("true");
                            result.Add("Record updated successfully.");
                        }
                    }
                    else
                    {
                        if (dcharges.DeliveryChargesId == 0 && dcharges.StateId > 0 && dcharges.CityId > 0)
                        {
                            dcharges.SupplierId = lu.LoginId;
                            dcharges.SupplierLoginId = lu.UserId;
                            dcharges.SupplierName = lu.FirstName + " " + lu.LastName;

                            if (DeliveryChargesService.InsertDeliveryCharges(dcharges))
                            {
                                result.Add("true");
                                result.Add("Record inserted successfully.");
                            }
                        }
                        else
                        {
                            if (DeliveryChargesService.DeleteDeliveryCharges(dcharges))
                            {
                                result.Add("true");
                                result.Add("Record deleted successfully.");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult InsertBannerDetail(HomeBannerModel banner)
        {
            string result = string.Empty;

            try
            {
                int bannerId = SettingMasterService.InsertBannerDetail(banner);
                if (bannerId > 0)
                {
                    result = "true";
                    TempData["bannerId"] = bannerId;
                }
                else
                {
                    result = "false";
                }
            }
            catch (Exception ex)
            {
                result = "error";
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult UpdateLastInsertedBannerImageName()
        {
            string result = string.Empty;

            try
            {
                if (TempData["bannerId"] != null && TempData["bannerId"] != null)
                {
                    string bannerId = TempData["bannerId"].ToString();

                    if (Request.Files.Count > 0)
                    {
                        HttpFileCollectionBase files = Request.Files;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            string fname = file.FileName;

                            if (!string.IsNullOrEmpty(fname))
                            {
                                Directory.CreateDirectory(Server.MapPath(Utility.GetFullPathStringWithAppData("BannerImages", ("Banner_" + bannerId))));

                                string ext = Path.GetExtension(file.FileName);
                                string imageurlname = Utility.GenrateRandomTransactionId(bannerId, 3) + ext;
                                string filePath = Server.MapPath(Utility.GetFullPathStringWithAppData("BannerImages", ("Banner_" + bannerId)) + imageurlname);

                                if (System.IO.File.Exists(filePath))
                                {
                                    System.IO.File.Delete(filePath);
                                }

                                file.SaveAs(filePath);

                                if (SettingMasterService.UpdateBannerImage(bannerId, imageurlname))
                                {
                                    result = "true";
                                }
                                else
                                {
                                    result = "false";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = "error";
                ex.ToString();
            }

            return Json(result);
        }

        public JsonResult UpdateBannerOrder(string bannerid, string posval)
        {
            string result = string.Empty;

            try
            {
                if (SettingMasterService.UpdateBannerOrder(bannerid, posval))
                {
                    result = "true";
                }
                else
                {
                    result = "false";
                }
            }
            catch (Exception ex)
            {
                result = "error";
                ex.ToString();
            }

            return Json(result);
        }
    }
}