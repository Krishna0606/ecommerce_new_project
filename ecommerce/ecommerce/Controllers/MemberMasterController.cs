﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ecommerce.Models.Backend;
using ecommerce.Models.Common;
using ecommerce.Service;
using ecommerce.Service.BackEndService;
using ecommerce.Service.EmailService;

namespace ecommerce.Controllers
{
    public class MemberMasterController : Controller
    {
        public ActionResult SupplierList(string memberid = null, bool status = true)
        {
            BackendLogin model = new BackendLogin();

            try
            {
                List<BackendLogin> memList = MemberRegService.GetMemRegDetail(memberid, status);
                if (memList != null && memList.Count > 0)
                {
                    model.MemberList = memList;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public ActionResult SupplierRegistration(string memberid = null, bool status = true)
        {
            BackendLogin model = new BackendLogin();

            model.CountryList = Common.PopulateCountry(CommonService.GetCountryDetail(null, true));
            model.StateList = Common.PopulateState(CommonService.GetStateDetail(null, true, "0"));
            model.CityList = Common.PopulateCity(CommonService.GetCityDetail(null, "0", true));

            if (!string.IsNullOrEmpty(memberid))
            {
                List<BackendLogin> memList = MemberRegService.GetMemRegDetail(memberid, status);
                if (memList != null && memList.Count > 0)
                {
                    model.LoginId = memList[0].LoginId;
                    model.FirstName = memList[0].FirstName;
                    model.LastName = memList[0].LastName;
                    model.EmailId = memList[0].EmailId;
                    model.MobileNo = memList[0].MobileNo;
                    model.CountryId = memList[0].CountryId;
                    model.StateId = memList[0].StateId;
                    model.CityId = memList[0].CityId;
                    model.AreaName = memList[0].AreaName;
                    model.PinCode = memList[0].PinCode;
                    model.Address = memList[0].Address;
                    model.ProfileImage = memList[0].ProfileImage;
                    model.Profession = memList[0].Profession;
                    model.DOB = memList[0].DOB;
                    model.AddharNo = memList[0].AddharNo;
                    model.PanNo = memList[0].PanNo;
                    model.GSTNo = memList[0].GSTNo;
                    model.AddharImage = memList[0].AddharImage;
                    model.PanImage = memList[0].PanImage;
                    model.BankName = memList[0].BankName;
                    model.AccountNo = memList[0].AccountNo;
                    model.BranchName = memList[0].BranchName;
                    model.BrnachCode = memList[0].BrnachCode;
                    model.IFSCCode = memList[0].IFSCCode;
                    model.BankAddress = memList[0].BankAddress;
                    model.PayeeName = memList[0].PayeeName;
                    model.UserId = memList[0].UserId;
                    model.Password = memList[0].Password;
                    //model.CountryId = memList[0].Country;
                    //model.StateId = memList[0].State;
                    //model.City = memList[0].City;
                    model.StateList = Common.PopulateState(CommonService.GetStateDetail(null, true, model.CountryId.ToString()));
                    model.CityList = Common.PopulateCity(CommonService.GetCityDetail(null, model.StateId.ToString(), true));
                }
            }

            return View(model);
        }
        public ActionResult UserProfile()
        {
            return View();
        }
        public JsonResult CreateMemRegistration(BackendLogin supplier)
        {
            List<string> result = new List<string>(); 

            try
            {
                //if (string.IsNullOrEmpty(supplier.LoginId.ToString()) && supplier.LoginId <= 0 && string.IsNullOrEmpty(supplier.ActionType))
                //{
                if (supplier.LoginId == 0)
                {
                    if (MemberRegService.InsertMemberRegDetails(supplier))
                    {
                        result.Add("true");
                        result.Add("Record inserted successfully.");
                    }
                    else
                    {
                        result.Add("false");
                        result.Add("Error Occured!");
                    }
                }
                else if (!string.IsNullOrEmpty(supplier.LoginId.ToString()) && supplier.LoginId > 0 && !string.IsNullOrEmpty(supplier.ActionType))
                {
                    if (MemberRegService.UpdateMemberRegDetails(supplier))
                    {
                        result.Add("true");
                        result.Add("Record updated successfully.");
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(supplier.LoginId.ToString()) && supplier.LoginId > 0)
                    {
                        supplier.Status = false;

                        if (MemberRegService.UpdateMemberRegDetails(supplier))
                        {
                            result.Add("true");
                            result.Add("Record deleted successfully.");
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return Json(result);
        }
        public JsonResult UserRegistration(string firstname, string lastname, string emailid, string mobileno, string userid, string regpassword)
        {
            List<string> result = new List<string>(); ;

            try
            {
                if (!string.IsNullOrEmpty(firstname) && !string.IsNullOrEmpty(lastname) && !string.IsNullOrEmpty(emailid) && !string.IsNullOrEmpty(mobileno) && !string.IsNullOrEmpty(regpassword))
                {
                    if (MemberRegService.InsertUserRegDetails(firstname, lastname, emailid, mobileno, userid, regpassword))
                    {
                        result.Add("true");
                    }
                    else
                    {
                        result.Add("false");
                        //result.Add("Error! there is an error occurred.");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult CheckUserIdExist(string userid)
        {
            List<string> result = new List<string>();

            try
            {
                if (MemberRegService.CheckUserIdExist(userid))
                {
                    result.Add("true");
                    result.Add("Available ! use another login id.");
                }
            }
            catch (Exception)
            {

                throw;
            }

            return Json(result);
        }
    }
}