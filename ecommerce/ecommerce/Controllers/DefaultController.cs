﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using static ecommerce.Models.Frontend.FrontHomeModel;
using static ecommerce.Models.FrontMenuModel;
using static ecommerce.Models.FrontProductModel;
using ecommerce.Models.Common;
using ecommerce.Models.Frontend;
using ecommerce.Service;
using ecommerce.Service.BackEndService;
using ecommerce.Service.Front;
using static ecommerce.Models.Frontend.CheckoutModel;
using ecommerce.Service.FrontService;
using System.Data;
using System.Text.RegularExpressions;
using static ecommerce.Models.Backend.BasicMaster;
using ecommerce.Service.EmailService;

namespace ecommerce.Controllers
{
    public class DefaultController : Controller
    {
        public ActionResult Home()
        {
            FrontHomeProductBind model = new FrontHomeProductBind();

            try
            {
                List<FrontHomeProductBind> tempModel = new List<FrontHomeProductBind>();

                Dictionary<int, string> dicMenu = FrontProductService.GetFeaturedMenuList();
                if (dicMenu != null && dicMenu.Count > 0)
                {
                    foreach (var item in dicMenu)
                    {
                        FrontHomeProductBind objModel = new FrontHomeProductBind();
                        List<FrontHomeProductBind> modellist = new List<FrontHomeProductBind>();
                        List<FrontHomeProduct> productList = new List<FrontHomeProduct>();

                        objModel.MenuId = item.Key;
                        objModel.MenuName = item.Value;

                        productList = FrontProductService.GetFrontProductByMenuIdList(item.Key.ToString());
                        if (productList.Count > 0)
                        {
                            objModel.ProductList = productList;
                            tempModel.Add(objModel);
                        }
                    }
                }

                model.FrontHomeProductBindList = tempModel;
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return View(model);

        }

        public JsonResult HomeAllProductBind(string pincode)
        {
            List<string> result = new List<string>();

            try
            {
                List<MenuModel> menuList = BasicMasterService.GetmenuDetail(null, true).Where(p => p.IsFeatured == true).ToList();

                if (menuList != null && menuList.Count > 0)
                {
                    StringBuilder sbTopCategory = new StringBuilder();
                    StringBuilder sbAllProductMenu = new StringBuilder();
                    StringBuilder sbAllProducts = new StringBuilder();

                    int allProMenuCount = 1;

                    sbTopCategory.Append("<h2>Top Categories</h2><hr>");
                    sbTopCategory.Append("<div class='col-lg-12 col-md-12 col-sm-12 p-0 lm-p-l-0 beyque-p col-xs-12'>");
                    sbTopCategory.Append("<div id='featured_category'>");
                    sbTopCategory.Append("<div class='category_module p-0'>");
                    sbTopCategory.Append("<div id='' class='beyqu-center-column-btn'>");
                    foreach (var menu in menuList)
                    {
                        string menuNameUrl = Regex.Replace(menu.MenuName, @"[^0-9a-zA-Z]+", ",").Replace(",", "-").ToLower();
                        sbTopCategory.Append("<div class='block-cat-wr col-xs-12 col-md-3 pl-5 pr-5'>");
                        sbTopCategory.Append("<a href='/pm/" + menuNameUrl + "/" + menu.Menuid + "'>");
                        sbTopCategory.Append("<img src='" + menu.Image + "' class='img-responsive center-block' alt=''>");
                        sbTopCategory.Append("</a>");
                        sbTopCategory.Append("<h4 class='text-center cat-name-height'>");
                        sbTopCategory.Append("<a href='/pm/" + menuNameUrl + "/" + menu.Menuid + "' class=''>" + menu.MenuName + "</a>");
                        sbTopCategory.Append("</h4>");
                        sbTopCategory.Append("</div>");

                        if (menu.IsFeatured)
                        {
                            //sbAllProductMenu.Append("<ul class='nav nav-tabs collapse footer-collapse text-right' id='cat_tab'>");
                            if (allProMenuCount == 1)
                            {
                                sbAllProductMenu.Append("<li class='active'><a href='#tab-" + allProMenuCount + "' data-toggle='tab'>" + menu.MenuName + "</a></li>");
                            }
                            else
                            {
                                sbAllProductMenu.Append("<li><a href='#tab-" + allProMenuCount + "' data-toggle='tab'>" + menu.MenuName + "</a></li>");
                            }
                            //sbAllProductMenu.Append("</ul>");

                            List<FrontHomeProduct> productList = new List<FrontHomeProduct>();
                            if (!string.IsNullOrEmpty(pincode))
                            {
                                productList = FrontProductService.GetFrontProductByMenuIdList(menu.Menuid.ToString(), pincode);
                            }
                            else
                            {
                                productList = FrontProductService.GetFrontProductByMenuIdList(menu.Menuid.ToString(), pincode);
                            }

                            if (allProMenuCount == 1)
                            {
                                sbAllProducts.Append("<div class='tab-pane fade active in' id='tab-" + allProMenuCount + "'>");
                            }
                            else
                            {
                                sbAllProducts.Append("<div class='tab-pane fade' id='tab-" + allProMenuCount + "'>");
                            }
                            sbAllProducts.Append("<div class='row thummargin'>");
                            //sbAllProducts.Append("<div id='cattab' class='owl-theme'>");                                
                            sbAllProducts.Append("<div id='cattab' class='owl-theme owl-carousel cattabproduct' style='opacity: 1; display: block;'>");

                            sbAllProducts.Append("<div class='owl-wrapper-outer'>");
                            sbAllProducts.Append("<div class='owl-wrapper' style='width: 7854px; left: 0px; display: block;'>");
                            if (productList.Count > 0)
                            {
                                foreach (var product in productList)
                                {
                                    sbAllProducts.Append("<div class='owl-item' style='width: 231px;'>");
                                    sbAllProducts.Append("<div class='product-layout col-xs-12'>");
                                    sbAllProducts.Append("<div class='product-thumb transition'>");
                                    sbAllProducts.Append("<div class='image'>");
                                    if (product.FrontProductImage != null && product.FrontProductImage.Count > 0)
                                    {
                                        sbAllProducts.Append("<a href='" + product.RedirectUrl + "'>");
                                        sbAllProducts.Append("<img src='" + product.FrontProductImage[0].ImageUrl + "' alt='" + product.ProductName + "' title='" + product.ProductName + "' class='img-responsive center-block'>");
                                        sbAllProducts.Append("</a>");
                                    }
                                    if (product.DiscountPer > 0)
                                    {
                                        sbAllProducts.Append("<p class='sale-tag new'>Discount " + product.DiscountPer + "%</p>");
                                    }
                                    sbAllProducts.Append("</div>");
                                    sbAllProducts.Append("<div class='caption text-center'>");
                                    sbAllProducts.Append("<h4>" + product.ProductName + "</h4>");
                                    if (product.DiscountPer > 0)
                                    {
                                        sbAllProducts.Append("<p class='price'><span class='price-new'>" + String.Format("{0:0.00}", product.SalePriceWithDiscount) + "</span> <span class='price-old'>" + product.SalePrice + "</span></p>");
                                    }
                                    else
                                    {
                                        sbAllProducts.Append("<p class='price'><span class='price-new'>" + product.SalePrice + "</span></p>");
                                    }
                                    sbAllProducts.Append("<div class='hidden'>");
                                    sbAllProducts.Append("<span id='frontprodetails_" + product.ProductId + "' class='hidden frontprodetails' data-productid='" + product.ProductId + "' data-productname='" + product.ProductName + "' data-productimgurl='" + (product.FrontProductImage.Count > 0 ? product.FrontProductImage[0].ImageUrl : "~/Content/image/noproduct.png") + "' data-saleprice='" + product.SalePrice + "' data-discountprice='" + product.Discount + "' data-discountper='" + product.DiscountPer + "' data-unitid='" + product.UnitId + "' data-unitname='" + product.UnitName + "'></span>");
                                    sbAllProducts.Append("</div>");
                                    sbAllProducts.Append("<div class='input-group col-xs-12 col-sm-12 qop'>");
                                    sbAllProducts.Append("<label class='control-label col-sm-2 col-xs-2 qlable' for='input-quantity'> Qty </label>");
                                    sbAllProducts.Append("<input type='number' name='quantity' min='1' value='1' step='1' id='fqty_" + product.ProductId + "' class='form-control col-sm-2 col-xs-9 qtyq'>");
                                    sbAllProducts.Append("<button type='button' class='acart frontaddtocart' data-proid='" + product.ProductId + "'>");
                                    sbAllProducts.Append("<i class='fa fa-shopping-basket fasbasket_" + product.ProductId + "' aria-hidden='true'></i>");
                                    sbAllProducts.Append("</button>");
                                    sbAllProducts.Append("</div>");
                                    sbAllProducts.Append("</div>");
                                    sbAllProducts.Append("</div>");
                                    sbAllProducts.Append("</div>");
                                    sbAllProducts.Append("</div>");
                                }
                            }
                            sbAllProducts.Append("</div>");
                            sbAllProducts.Append("</div>");
                            //sbAllProducts.Append("<div class='owl-controls clickable'><div class='owl-buttons'><div class='owl-prev'><i class='fa fa-angle-left' aria-hidden='true'></i></div><div class='owl-next'><i class='fa fa-angle-right' aria-hidden='true'></i></div></div></div>");
                            sbAllProducts.Append("</div>");
                            sbAllProducts.Append("</div>");
                            sbAllProducts.Append("</div>");


                            allProMenuCount = allProMenuCount + 1;
                        }
                    }
                    sbTopCategory.Append("</div>");
                    sbTopCategory.Append("</div>");
                    sbTopCategory.Append("</div>");
                    sbTopCategory.Append("</div>");

                    result.Add(sbTopCategory.ToString());
                    result.Add(sbAllProductMenu.ToString());
                    result.Add(sbAllProducts.ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public ActionResult NewProduct(string menuname, string menuid, string catname, string catid, string subcatname, string subcatid)
        {
            FrontProduct model = new FrontProduct();

            try
            {
                //List<FrontProduct> productList = FrontProductService.GetFrontProductList(null, menuid, catid, subcatid);
                //if (productList != null && productList.Count > 0)
                //{
                //    model.FrontProductList = productList;

                //    //if (!string.IsNullOrEmpty(menuname))
                //    //{
                //    //    model.MenuName = menuname;
                //    //}
                //    //if (!string.IsNullOrEmpty(catname))
                //    //{
                //    //    model.CatName = menuname;
                //    //}
                //    //if (!string.IsNullOrEmpty(subcatname))
                //    //{
                //    //    model.SubCatName = subcatname;
                //    //}
                //}

                //TempData["ProductList"] = model;

                //LeftSideFilter leftSideFilter = new LeftSideFilter();
                ////leftSideFilter.StateFilter = BindStateFilterSection();

                //model.LeftSideFilter = leftSideFilter;
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return View();
        }
        public ActionResult NewProductdetails(string prodid, string prodname, string menuname, string menuid, string catname, string catid, string subcatname, string subcatid)
        {
            FrontProduct model = new FrontProduct();

            try
            {
                List<FrontProduct> productList = FrontProductService.GetFrontProductList(prodid, menuid, catid, subcatid);
                if (productList != null && productList.Count > 0)
                {
                    model.FrontProductList = productList;

                    //model.MenuName = menuname;
                    //model.CatName = catname;
                    //model.SubCatName = subcatname;
                    //model.UnitList = Common.PopulateUnit(BasicMasterService.GetUnitDetail(null, true));
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return View(model);
        }
        public ActionResult PlaceOrder()
        {
            Checkout model = new Checkout();

            try
            {
                CheckoutShippingAddress shipAdd = new CheckoutShippingAddress();

                shipAdd.StateList = Common.PopulateState(AreaMasterService.GetStateDetail(null, true, null));
                shipAdd.IsDefault = true;

                model.ShippingAddress = shipAdd;
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public ActionResult Index()
        {
            FrontHomeProductBind model = new FrontHomeProductBind();

            try
            {
                List<FrontHomeProductBind> tempModel = new List<FrontHomeProductBind>();

                Dictionary<int, string> dicMenu = FrontProductService.GetFeaturedMenuList();
                if (dicMenu != null && dicMenu.Count > 0)
                {
                    foreach (var item in dicMenu)
                    {
                        FrontHomeProductBind objModel = new FrontHomeProductBind();
                        List<FrontHomeProductBind> modellist = new List<FrontHomeProductBind>();
                        List<FrontHomeProduct> productList = new List<FrontHomeProduct>();

                        objModel.MenuId = item.Key;
                        objModel.MenuName = item.Value;

                        productList = FrontProductService.GetFrontProductByMenuIdList(item.Key.ToString());
                        if (productList.Count > 0)
                        {
                            objModel.ProductList = productList;
                            tempModel.Add(objModel);
                        }
                    }
                }

                model.FrontHomeProductBindList = tempModel;
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return View(model);
        }
        public ActionResult Productdetails(string prodid, string prodname, string menuname, string menuid, string catname, string catid, string subcatname, string subcatid)
        {
            FrontProduct model = new FrontProduct();

            try
            {
                List<FrontProduct> productList = FrontProductService.GetFrontProductList(prodid, menuid, catid, subcatid);
                if (productList != null && productList.Count > 0)
                {
                    model.FrontProductList = productList;

                    //model.MenuName = menuname;
                    //model.CatName = catname;
                    //model.SubCatName = subcatname;
                    //model.UnitList = Common.PopulateUnit(BasicMasterService.GetUnitDetail(null, true));
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return View(model);
        }
        public ActionResult Product(string menuname, string menuid, string catname, string catid, string subcatname, string subcatid)
        {
            FrontProduct model = new FrontProduct();

            try
            {
                List<FrontProduct> productList = FrontProductService.GetFrontProductList(null, menuid, catid, subcatid);
                if (productList != null && productList.Count > 0)
                {
                    model.FrontProductList = productList;

                    //if (!string.IsNullOrEmpty(menuname))
                    //{
                    //    model.MenuName = menuname;
                    //}
                    //if (!string.IsNullOrEmpty(catname))
                    //{
                    //    model.CatName = menuname;
                    //}
                    //if (!string.IsNullOrEmpty(subcatname))
                    //{
                    //    model.SubCatName = subcatname;
                    //}
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return View(model);
        }
        public PartialViewResult SignUp(int? way)
        {
            ViewBag.way = way;
            return PartialView();
        }
        public PartialViewResult DropdownMenu()
        {
            FrontMainCategoryModel model = new FrontMainCategoryModel();

            List<FrontMainCategoryModel> menuList = FrontMenuService.GetFrontMenu();
            if (menuList != null && menuList.Count > 0)
            {
                model.FrontMainCategoryList = menuList;
            }

            return PartialView(model);
        }
        public PartialViewResult DirectMenu()
        {
            return PartialView();
        }
        public JsonResult ProcessToAddToCart(AddToCart addcart)
        {
            string result = string.Empty;
            try
            {
                FrontEndLogin lu = new FrontEndLogin();
                if (AccountService.IsFrontendLogin(ref lu))
                {
                    addcart.UserId = lu.LoginId;
                    addcart.UserLoginId = lu.UserId;
                    addcart.UserName = lu.FirstName + " " + lu.LastName;

                    if (FrontProductService.InsertDetailsAddToCart(addcart))
                    {
                        result = "true";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult CheckDetailsInCart(string pagetype = null)
        {
            List<string> result = new List<string>();

            try
            {
                StringBuilder sbCart = new StringBuilder();

                FrontEndLogin lu = new FrontEndLogin();
                if (AccountService.IsFrontendLogin(ref lu))
                {
                    List<AddToCart> cartList = FrontProductService.CheckDetailsInCart(lu.LoginId.ToString());
                    if (cartList != null && cartList.Count > 0)
                    {
                        result.Add("true");
                        result.Add(cartList.Count.ToString());

                        sbCart.Append("<div class='col-sm-12'>");
                        sbCart.Append("<div class='table-responsive'>");
                        sbCart.Append("<table class='table table-responsive table-bordered'>");
                        sbCart.Append("<thead>");
                        sbCart.Append("<tr>");
                        sbCart.Append("<th>Image</th><th style='width:300px' class='text-center'>Product Name</th><th class='text-center'>Rate</th><th class='text-center'>Qut.</th><th class='text-center'>Unit</th><th class='text-center'>Total</th><th class='text-center'>Action</th>");
                        sbCart.Append("</tr>");
                        sbCart.Append("</thead>");
                        sbCart.Append("<tbody>");

                        decimal subTotal = 0;
                        foreach (var item in cartList)
                        {
                            sbCart.Append("<tr id='Row1' class='text-center'>");
                            sbCart.Append("<td><img src='" + item.ProductImgUrl + "' class='img-responsive center-block' style='width: 80px;height: 50px;'></td>");
                            sbCart.Append("<td>" + item.ProductName + "</td>");
                            sbCart.Append("<td><span>&nbsp;" + GetFormattedPrice(item.SalePrice) + "</span></td>");
                            //sbCart.Append("<td><input id='#' class='input-sm' name='#' type='number' min='1' value='1' onchange='CalculatePrice('1')'></td>");
                            sbCart.Append("<td><input id='txtCartQuantity_" + item.CartId + "' class='cartquant' data-cartid='" + item.CartId + "' name='txtCartQuantity_" + item.CartId + "' style='width:70px;' min='1' type='number' value=" + item.Quantity + " /><i class='fa fa-spinner fa-spin hidden spiiner_" + item.CartId + "'  style='color:#ff3366;position: absolute;margin: 6px 0px 0px -20px;'></i></td>");
                            sbCart.Append("<td>" + item.UnitName + "</td>");
                            sbCart.Append("</td>");

                            string pricestring = string.Empty;

                            string price = !string.IsNullOrEmpty(item.DiscountPrice) ? (GetFormattedPrice((Convert.ToDecimal(item.SalePrice) - Convert.ToDecimal(item.DiscountPrice)).ToString())) : "0";
                            if (item.Discount > 0)
                            {
                                pricestring = price + "<br/> <small>You Save Rs.&nbsp;" + GetFormattedPrice(item.DiscountPrice) + "</small>";
                            }
                            else
                            {
                                price = item.SalePrice;
                                pricestring = GetFormattedPrice(item.SalePrice);
                            }

                            string finalRowAmtWitQyt = (Convert.ToDecimal(price) * Convert.ToInt32(item.Quantity)).ToString();

                            sbCart.Append("<td id='totalitemcost1'>Rs.&nbsp;<span id='price_" + item.CartId + "' class='rowprice' data-rowcartid='" + item.CartId + "' data-rowprice='" + pricestring + "'>" + finalRowAmtWitQyt + "</span></td>");
                            sbCart.Append("<td><button class='btn btn-danger btn-xs delrowcart' id='btnDelRowCart_" + item.CartId + "' data-delrowcartid='" + item.CartId + "'><i class='fa fa-trash delrowicon_" + item.CartId + "'></i></button>");
                            sbCart.Append("</td>");
                            sbCart.Append("</tr>");

                            if (item.Discount > 0)
                            {
                                subTotal = subTotal + (item.Quantity * (Convert.ToDecimal(item.SalePrice) - Convert.ToDecimal(item.DiscountPrice)));
                            }
                            else
                            {
                                subTotal = subTotal + (item.Quantity * Convert.ToDecimal(item.SalePrice));
                            }
                        }

                        sbCart.Append("</tbody>");
                        sbCart.Append("<tfoot>");
                        sbCart.Append("<tr>");
                        sbCart.Append("<td colspan='3' rowspan='2'><small>**Actual Delivery Charges computed at checkout</small></td>");
                        sbCart.Append("<td colspan='2'>SubTotal</td>");
                        sbCart.Append("<td colspan='1' id='CartSubTotal'>Rs.&nbsp;<span id='spsubtotal' data-subtotal='" + subTotal + "'>" + subTotal + "</span></td>");
                        sbCart.Append("</tr>");
                        sbCart.Append("<tr>");
                        sbCart.Append("<td colspan='2'><strong>Grand Total</strong></td>");
                        sbCart.Append("<td colspan='1' id='CartGrandTotal'><strong>Rs&nbsp;<span id='sptotal' data-total='" + subTotal + "'>" + subTotal + "</span></strong></td>");
                        sbCart.Append("</tr>");
                        sbCart.Append("</tfoot>");
                        sbCart.Append("</table>");
                        sbCart.Append("</div>");
                        sbCart.Append("<div class='row'>");
                        sbCart.Append("<div class='col-sm-12 text-right'>");
                        sbCart.Append("<div class=''>");
                        sbCart.Append("<a href='/place-order/checkout' id='btnCheckout' class='btn btn-check-out' style='background: #000;padding: 6px;margin-bottom: 12px;color: #fff;'>Proceed to checkout</a>");
                        sbCart.Append("</div>");
                        sbCart.Append("</div>");
                        sbCart.Append("</div>");
                        sbCart.Append("</div>");
                        result.Add(sbCart.ToString());
                        result.Add(subTotal.ToString());
                    }
                    else
                    {
                        result.Add("false");
                        result.Add("0");
                        sbCart.Append("<div class='col-sm-12' style='padding-bottom: 25px;text-align: center;'>");
                        sbCart.Append("<h3>Your basket is empty. Start shopping now!</h3>");
                        sbCart.Append("</div>");

                        result.Add(sbCart.ToString());
                        result.Add("0.00");
                    }
                }
                else
                {
                    if (pagetype == "front")
                    {
                        result.Add("false");
                        result.Add("0");
                        sbCart.Append("<div class='col-sm-12' style='padding-bottom: 25px;text-align: center;'>");
                        sbCart.Append("<h3>Your basket is empty. Start shopping now!</h3>");
                        sbCart.Append("</div>");

                        result.Add(sbCart.ToString());
                        result.Add("0.00");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult CheckFrontUserLogin()
        {
            List<string> result = new List<string>();

            FrontEndLogin lu = new FrontEndLogin();
            if (AccountService.IsFrontendLogin(ref lu))
            {
                result.Add("true");
                result.Add(lu.UserId);
            }
            else
            {
                bool isLogout = AccountService.LogoutFrontendLogin();
                result.Add("false");
            }

            return Json(result);
        }
        public string GetFormattedPrice(string price)
        {
            string modifyPrice = String.Format("{0:0.00}", Convert.ToDecimal(price));

            return modifyPrice;
        }
        public JsonResult LogoutFrontendLogin()
        {
            string result = string.Empty;
            if (AccountService.LogoutFrontendLogin())
            {
                result = "true";
            }

            return Json(result);
        }
        public JsonResult UpdateAddToCart(string cartid, string quantity)
        {
            string result = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(cartid))
                {
                    if (FrontProductService.UpdateDetailsAddToCartById(cartid, quantity))
                    {
                        result = "true";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult DeleteAddToCart(string cartid)
        {
            string result = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(cartid))
                {
                    if (FrontProductService.DeleteDetailsAddToCartById(cartid))
                    {
                        result = "true";
                    }
                    else
                    {
                        result = "false";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }

        #region [Acc. New Design]
        public PartialViewResult MenuCatSubCat()
        {
            FrontMainCategoryModel model = new FrontMainCategoryModel();

            List<FrontMainCategoryModel> menuList = FrontMenuService.GetFrontMenu();
            if (menuList != null && menuList.Count > 0)
            {
                model.FrontMainCategoryList = menuList;
            }

            return PartialView(model);
        }
        public PartialViewResult MobileMenuCatSubCat()
        {
            FrontMainCategoryModel model = new FrontMainCategoryModel();

            List<FrontMainCategoryModel> menuList = FrontMenuService.GetFrontMenu();
            if (menuList != null && menuList.Count > 0)
            {
                model.FrontMainCategoryList = menuList;
            }

            return PartialView(model);
        }
        public PartialViewResult TopCategories()
        {
            FrontMainCategoryModel model = new FrontMainCategoryModel();

            List<FrontMainCategoryModel> menuList = FrontMenuService.GetFrontMenu();
            if (menuList != null && menuList.Count > 0)
            {
                model.FrontMainCategoryList = menuList;
            }

            return PartialView(model);
        }
        public PartialViewResult ProductSideMenu()
        {
            FrontMainCategoryModel model = new FrontMainCategoryModel();

            List<FrontMainCategoryModel> menuList = FrontMenuService.GetFrontMenu();
            if (menuList != null && menuList.Count > 0)
            {
                model.FrontMainCategoryList = menuList;
            }

            return PartialView(model);
        }
        public JsonResult CheckCheckoutPageDetail()
        {
            List<string> result = new List<string>();

            try
            {
                FrontEndLogin lu = new FrontEndLogin();
                if (AccountService.IsFrontendLogin(ref lu))
                {
                    result.Add("true");

                    List<string> shippingAdd = GetShippingAddress(lu.LoginId, lu.UserId);
                    if (shippingAdd.Count > 0)
                    {
                        if (shippingAdd[0] == "true")
                        {
                            result.Add(shippingAdd[1]);
                        }
                        else
                        {
                            result.Add("<p class='text-center text-danger'>No address available!</p>");
                        }
                    }

                    result.Add(BindCartDetailOnCheckoutPage(lu.LoginId));

                    result.Add(BindFinalPriceDetailOnCheckout(lu.LoginId));
                }
                else
                {
                    bool isLogout = AccountService.LogoutFrontendLogin();
                    result.Add("false");
                    result.Add("logout");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public string BindFinalPriceDetailOnCheckout(int loginid)
        {
            StringBuilder sbCart = new StringBuilder();

            try
            {
                if (loginid <= 0)
                {
                    FrontEndLogin lu = new FrontEndLogin();
                    if (AccountService.IsFrontendLogin(ref lu))
                    {
                        loginid = lu.LoginId;
                    }
                    else
                    {
                        bool isLogout = AccountService.LogoutFrontendLogin();
                        sbCart.Append("logout");
                    }
                }

                List<AddToCart> cartList = FrontProductService.CheckDetailsInCart(loginid.ToString());

                if (cartList != null && cartList.Count > 0)
                {
                    decimal totalAmount = 0; decimal saveAmount = 0;
                    foreach (var item in cartList)
                    {
                        if (item.Discount > 0)
                        {
                            saveAmount = saveAmount + (item.Quantity * Convert.ToDecimal(item.DiscountPrice));
                            totalAmount = totalAmount + (item.Quantity * (Convert.ToDecimal(item.SalePrice) - Convert.ToDecimal(item.DiscountPrice)));
                        }
                        else
                        {
                            totalAmount = totalAmount + (item.Quantity * Convert.ToDecimal(item.SalePrice));
                        }
                    }
                    sbCart.Append("<div class='panel panel-default'>");
                    sbCart.Append("<div class='panel-body'>");
                    sbCart.Append("<div class='row'>");
                    sbCart.Append("<div class='col-sm-12 text-center' style='border-bottom: 1px solid #ccc;'>");
                    sbCart.Append("<label style='font-size: 20px;padding-bottom: 10px;'>PRICE DETAILS</label>");
                    sbCart.Append("</div>");
                    sbCart.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;padding-top: 10px;'>");
                    sbCart.Append("<div class='row'>");
                    sbCart.Append("<div class='col-sm-8'>");
                    sbCart.Append("<p>Price(" + cartList.Count + " item)</p>");
                    sbCart.Append("<p>Delivery Charge</p>");
                    sbCart.Append("</div>");
                    sbCart.Append("<div class='col-sm-4'>");
                    sbCart.Append("<p>₹ " + totalAmount + "</p>");
                    sbCart.Append("<p>**</p>");
                    sbCart.Append("</div>");
                    sbCart.Append("</div>");
                    sbCart.Append("</div>");

                    if (saveAmount > 0)
                    {
                        sbCart.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;padding-top: 10px;'>");
                        sbCart.Append("<div class='row'>");
                        sbCart.Append("<div class='col-sm-8'>");
                        sbCart.Append("<p>You Saved</p>");
                        sbCart.Append("</div>");
                        sbCart.Append("<div class='col-sm-4'>");
                        sbCart.Append("<p> ₹ " + saveAmount + "</p>");
                        sbCart.Append("</div>");
                        sbCart.Append("</div>");
                        sbCart.Append("</div>");
                    }

                    sbCart.Append("<div class='col-sm-12'>");
                    sbCart.Append("<div class='row'>");
                    sbCart.Append("<div class='col-sm-8'>");
                    sbCart.Append("<h4>Total Payable</h4>");
                    sbCart.Append("</div>");
                    sbCart.Append("<div class='col-sm-4'>");
                    sbCart.Append("<h4 style='font-weight:bold;'>₹ " + Math.Ceiling(totalAmount).ToString("#.00") + "</h4>");
                    sbCart.Append("</div>");
                    sbCart.Append("</div>");
                    sbCart.Append("</div>");
                    sbCart.Append("</div>");
                    sbCart.Append("</div>");
                    sbCart.Append("</div>");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return sbCart.ToString();
        }
        public string BindCartDetailOnCheckoutPage(int loginid)
        {
            StringBuilder sbCart = new StringBuilder();

            try
            {
                List<AddToCart> cartList = FrontProductService.CheckDetailsInCart(loginid.ToString());

                sbCart.Append("<div class='table-responsive'><table class='table table-responsive table-bordered'>");
                sbCart.Append("<thead><tr>");
                sbCart.Append("<th>Image</th><th style='width:300px' class='text-center'>Product Name</th><th class='text-center'>Rate</th><th class='text-center'>Qut.</th>");
                sbCart.Append("<th class='text-center'>Unit</th><th class='text-center'>Total</th><th class='text-center'>Action</th>");
                sbCart.Append("</tr></thead>");

                if (cartList != null && cartList.Count > 0)
                {
                    decimal totalAmt = 0;

                    sbCart.Append("<tbody>");
                    foreach (var item in cartList)
                    {
                        sbCart.Append("<tr id='Row1' class='text-center'>");
                        sbCart.Append("<td><img src='" + item.ProductImgUrl + "' class='img-responsive center-block' style='width: 80px;height: 50px;'></td>");
                        sbCart.Append("<td>" + item.ProductName + "</td>");
                        sbCart.Append("<td><span>&nbsp;" + item.SalePrice + "</span></td>");
                        sbCart.Append("<td><input id='txtCheckoutCartQuantity_" + item.CartId + "' class='checkoutcartquant' data-cartid='" + item.CartId + "' name='txtCheckoutCartQuantity_" + item.CartId + "' style='width:70px;' min='1' type='number' value='" + item.Quantity + "'><i class='fa fa-spinner fa-spin hidden spiiner_" + item.CartId + "' style='color:#ff3366;position: absolute;margin: 6px 0px 0px -20px;'></i></td>");
                        sbCart.Append("<td>" + item.UnitName + "</td>");

                        decimal price = 0;
                        if (item.Discount > 0)
                        {
                            price = Convert.ToDecimal(item.SalePrice) - Convert.ToDecimal(item.DiscountPrice);

                            totalAmt = totalAmt + (item.Quantity * price);

                            string finalRowAmtWitQyt = (price * item.Quantity).ToString();

                            sbCart.Append("<td id='totalitemcost_" + item.CartId + "'>Rs.&nbsp;<span id='checkoutprice_" + item.CartId + "' class='checkoutrowprice' data-checkoutrowcartid='" + item.CartId + "' data-checkoutrowprice='" + price + "'>" + finalRowAmtWitQyt + "<br> <small>You Save Rs.&nbsp;" + item.DiscountPrice + "</small></span></td>");
                        }
                        else
                        {
                            totalAmt = totalAmt + (item.Quantity * Convert.ToDecimal(item.SalePrice));

                            string finalRowAmtWitQyt = (Convert.ToDecimal(item.SalePrice) * item.Quantity).ToString();

                            sbCart.Append("<td id='totalitemcost_" + item.CartId + "'>Rs.&nbsp;<span id='checkoutprice_" + item.CartId + "' class='checkoutrowprice' data-checkoutrowcartid='" + item.CartId + "' data-checkoutrowprice='" + item.SalePrice + "'>" + finalRowAmtWitQyt + "</span></td>");
                        }

                        sbCart.Append("<td><button class='btn btn-danger btn-xs checkoutdelrowcart' id='btnCheckoutDelRowCart_" + item.CartId + "' data-checkoutdelrowcartid='" + item.CartId + "'><i class='fa fa-trash checkoutdelrowicon_" + item.CartId + "'></i></button></td>");
                        sbCart.Append("</tr>");


                    }
                    sbCart.Append("</tbody>");

                    sbCart.Append("<tfoot><tr>");
                    sbCart.Append("<td colspan='3' rowspan='2'></td>");//<small>**Actual Delivery Charges computed at checkout</small>
                    sbCart.Append("<td colspan='2'>Grand Total</td>");
                    sbCart.Append("<td colspan='1' id='CartSubTotal'>Rs.&nbsp;<span id='checkoutspsubtotal' data-subtotal='" + totalAmt + "'>" + totalAmt + "</span></td>");
                    sbCart.Append("</tr></tfoot>");
                }
                else
                {
                    sbCart.Append("<tbody>");
                    sbCart.Append("<tr id='noproduct'><td colspan='7' class='text-center text-danger'>Cart is empty! <a href='/' class='btn btn-success' style='padding: 2px;'>Continue Shopping</a></td></tr>");
                    sbCart.Append("</tbody>");
                }
                sbCart.Append("</table></div>");

                if (cartList.Count > 0)
                {
                    sbCart.Append("<div class='col-sm-12'>");
                    sbCart.Append("<div class='row'>");
                    sbCart.Append("<div class='col-sm-6'><h3 style='color: #255625;'>Payment Mode : Cash On Delievery</h3></div>");
                    sbCart.Append("<div class='col-sm-6 pull-right'><button class='btn btn-success center-block pull-right' id='btnPlaceOrderContinue'>CONTINUE</button></div>");
                    sbCart.Append("</div></div>");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return sbCart.ToString();
        }
        public List<string> GetShippingAddress(int loginid, string userid)
        {
            List<string> resultList = new List<string>();

            try
            {
                List<CheckoutShippingAddress> shippingAddress = CheckoutService.GetShippingAddressDetail(loginid, userid);
                if (shippingAddress.Count > 0)
                {
                    resultList.Add("true");
                    string result = string.Empty;
                    foreach (var item in shippingAddress)
                    {
                        if (item.IsDefault)
                        {
                            result += "<div class='col-sm-4'>";
                            result += "<div class='col-sm-12' style='padding: 10px;border: 1px solid #ccc; box-shadow: 4px 4px 5px #ccc'>";
                            result += "<p><span style='font-weight:bold;'>" + item.FirstName + " " + item.LastName + " </span> <span id='selectadd_" + item.AddressId + "'  style='border: 1px solid #6f0063;padding: 3px;background: #ff6795;color: #fff;box-shadow: 4px 4px 6px #ff6795;' class='pull-right selectaddclass' data-shipaddid='" + item.AddressId + "'><i class='fa fa-check-circle-o ispinho_" + item.AddressId + "'></i>&nbsp;Default</span></p>";
                            result += "<p>Email- " + item.EmailId + "</p>";
                            result += "<p>" + item.Address + ", " + item.City + "</p>";
                            if (!string.IsNullOrEmpty(item.Landmark))
                            {
                                result += "<p>(" + item.Landmark + ")</p>";
                            }
                            result += "<p>" + item.State + ", India</p>";
                            result += "<p>Pin Code- " + item.PinCode + "</p>";
                            result += "<p>Contact- " + item.MobileNo + (!string.IsNullOrEmpty(item.AlternateMobileNo) ? "," + item.AlternateMobileNo : string.Empty) + "</p>";
                            result += "</div>";
                            result += "</div>";
                        }
                        else
                        {
                            result += "<div class='col-sm-4'>";
                            result += "<div class='col-sm-12' style='padding: 10px;border: 1px solid #ccc; box-shadow: 4px 4px 5px #ccc;'>";
                            result += "<p><span style='font-weight:bold;'>" + item.FirstName + " " + item.LastName + " </span> <span id='selectadd_" + item.AddressId + "' style='cursor:pointer; border: 1px solid #6f0063;padding: 3px;background: #ff6795;color: #fff;box-shadow: 4px 4px 6px #ff6795;' class='pull-right selectaddclass' data-shipaddid='" + item.AddressId + "'><i class='fa fa-edit ispinho_" + item.AddressId + "'></i>&nbsp;Select</span></p>";
                            result += "<p>Email- " + item.EmailId + "</p>";
                            result += "<p>" + item.Address + ", " + item.City + "</p>";
                            if (!string.IsNullOrEmpty(item.Landmark))
                            {
                                result += "<p>(" + item.Landmark + ")</p>";
                            }
                            result += "<p>" + item.State + ", India</p>";
                            result += "<p>Pin Code- " + item.PinCode + "</p>";
                            result += "<p>Contact- " + item.MobileNo + (!string.IsNullOrEmpty(item.AlternateMobileNo) ? "," + item.AlternateMobileNo : string.Empty) + "</p>";
                            result += "</div>";
                            result += "</div>";
                        }
                    }
                    resultList.Add(result);
                }
                else
                {
                    resultList.Add("false");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return resultList;
        }
        public JsonResult GetJsonGetShippingAddress(int loginid, string userid)
        {
            List<string> resultList = GetShippingAddress(loginid, userid);

            return Json(resultList);
        }
        public JsonResult SetDefaultAddress(string addressid)
        {
            List<string> result = new List<string>();

            try
            {
                FrontEndLogin lu = new FrontEndLogin();
                if (AccountService.IsFrontendLogin(ref lu))
                {
                    if (CheckoutService.UpdateShippingAddressDetail(addressid, lu.LoginId))
                    {
                        result.Add("true");
                        result.Add(lu.LoginId.ToString());
                        result.Add(lu.UserId);
                    }
                }
                else
                {
                    bool isLogout = AccountService.LogoutFrontendLogin();
                    result.Add("logout");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult InsertOrderDetails()
        {
            List<string> result = new List<string>();

            try
            {
                FrontEndLogin lu = new FrontEndLogin();
                if (AccountService.IsFrontendLogin(ref lu))
                {
                    List<CheckoutShippingAddress> shippingAddress = CheckoutService.GetShippingAddressDetail(lu.LoginId, lu.UserId, "1");
                    List<AddToCart> cartList = FrontProductService.CheckDetailsInCart(lu.LoginId.ToString());

                    if (shippingAddress.Count > 0)
                    {
                        BookingOrder bookingOrder = new BookingOrder();
                        bool isBookingOrderSuccess = false;

                        if (cartList != null && cartList.Count > 0)
                        {
                            bookingOrder.OrderNo = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15).ToUpper();
                            foreach (var order in cartList)
                            {
                                bookingOrder.ProductId = order.ProductId;
                                bookingOrder.ProductName = order.ProductName;
                                bookingOrder.ProductCode = order.ProductCode;

                                decimal subTotal = 0;
                                if (order.Discount > 0)
                                {
                                    subTotal = Convert.ToDecimal(order.SalePrice) - Convert.ToDecimal(order.DiscountPrice);
                                }
                                else
                                {
                                    subTotal = Convert.ToDecimal(order.SalePrice);
                                }

                                bookingOrder.SubTotal = subTotal;
                                bookingOrder.DeliveryCharges = 0;
                                bookingOrder.GrandTotal = bookingOrder.SubTotal + bookingOrder.DeliveryCharges;
                                bookingOrder.Unit = order.UnitName;
                                bookingOrder.Quantity = order.Quantity;

                                bookingOrder.PaymentType = "Cash";
                                bookingOrder.DelieveryStatus = "Pending";

                                bookingOrder.UserId = lu.LoginId;
                                bookingOrder.UserEmailId = lu.EmailId;
                                bookingOrder.UserName = lu.FirstName + " " + lu.LastName;

                                bookingOrder.SupplierId = order.SupplierId;
                                bookingOrder.SupplierEmailId = order.SupplierEmail;
                                bookingOrder.SupplierName = order.SupplierName;

                                bookingOrder.ProductImgUrl = order.ProductImgUrl;

                                if (CheckoutService.InsertBookingOrder(bookingOrder))
                                {
                                    isBookingOrderSuccess = true;
                                }
                                else
                                {
                                    isBookingOrderSuccess = false;
                                    break;
                                }
                            }

                            if (isBookingOrderSuccess)
                            {
                                if (shippingAddress.Count > 0)
                                {
                                    OrderDelieveryAddress delAddress = new OrderDelieveryAddress();

                                    if (shippingAddress.Count > 0)
                                    {
                                        delAddress.OrderNo = bookingOrder.OrderNo;
                                        delAddress.UserEmailId = shippingAddress[0].EmailId;
                                        delAddress.UserAddress = shippingAddress[0].Address;
                                        delAddress.Landmark = shippingAddress[0].Landmark;
                                        delAddress.MobileNo = shippingAddress[0].MobileNo + (!string.IsNullOrEmpty(shippingAddress[0].AlternateMobileNo) ? "," + shippingAddress[0].AlternateMobileNo : string.Empty);
                                        delAddress.City = shippingAddress[0].City;
                                        delAddress.State = shippingAddress[0].State;
                                        delAddress.PinCode = shippingAddress[0].PinCode;
                                        delAddress.AddressType = shippingAddress[0].AddressType;

                                        if (CheckoutService.InsertOrderDelieveryAddress(delAddress))
                                        {
                                            if (CheckoutService.DeleteAddToCartDetailByUserId(lu.LoginId.ToString()))
                                            {
                                                result.Add("true");
                                            }
                                            if (Email.SendUserInvoice(bookingOrder.OrderNo, delAddress.UserEmailId, delAddress.UserAddress, delAddress.Landmark, delAddress.MobileNo, delAddress.City, delAddress.State, delAddress.PinCode))
                                            {
                                                string msg = "Dear " + bookingOrder.UserName + ",Your booking for " + bookingOrder.ProductName + " " + bookingOrder.Unit + "  is confirmed on Oderid is " + bookingOrder.OrderNo + " On " + DateTime.Now.ToString("dd/MM/yyyy") + "..";
                                                Email.SendMobileSms(delAddress.MobileNo, msg);
                                            }
                                        }
                                        else
                                        {
                                            result.Add("false");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                result.Add("false");
                            }
                        }
                    }
                    else
                    {
                        result.Add("noshippingaddress");
                    }
                }
                else
                {
                    bool isLogout = AccountService.LogoutFrontendLogin();
                    result.Add("logout");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        #endregion

        #region [Get Product Search Data]        
        [HttpPost]
        public JsonResult GetProductSearchData(string prefix)
        {
            List<string> result = new List<string>();

            try
            {
                StringBuilder sbProduct = new StringBuilder();

                List<FrontProduct> productList = FrontProductService.GetFrontProductList(null, null, null, null, prefix);

                if (productList.Count > 0)
                {
                    result.Add("true");
                    foreach (var item in productList.Take(15))
                    {
                        sbProduct.Append("<div class='product-thumb insp-cat col-xs-12'>");
                        sbProduct.Append("<a href='" + item.RedirectUrl + "'>");
                        sbProduct.Append("<div class='searchbr'>");

                        sbProduct.Append("<div class='product-info col-lg-4 col-sm-4 col-xs-5 image' style='height: 100px;'>");
                        sbProduct.Append("<img style='width: 100px;' src='" + (item.FrontProductImage != null ? item.FrontProductImage[0].ImageUrl : "/Content/image/blank-images.png") + "' alt='" + item.ProductName + "' title='" + item.ProductName + "' class='img-responsive'>");
                        sbProduct.Append("</div>");

                        sbProduct.Append("<div class='insp-dis col-lg-8 col-sm-8 col-xs-7 text-left'>");
                        sbProduct.Append("<h4 class='acpage'>" + item.ProductName + "</h4>");
                        if (item.FrontProductDescription.Descripion1 != null)
                        {
                            sbProduct.Append("<p class='list-des'>" + Utility.GetDescription(item.FrontProductDescription.Descripion1, 100) + "...</p>");
                        }
                        else
                        {
                            sbProduct.Append("<p class='list-des'>Description not available!</p>");
                        }
                        sbProduct.Append("<div class='price'>");
                        if (item.Discount > 0)
                        {
                            sbProduct.Append("<span class='price-new'>₹ " + item.SalePriceWithDiscount + "</span> <span class='price-old'>₹ " + item.SalePrice + "</span>");
                        }
                        else
                        {
                            sbProduct.Append("<span class='price-new'>₹ " + item.SalePrice + "</span>");
                        }
                        sbProduct.Append("</div>");
                        sbProduct.Append("</div>");

                        sbProduct.Append("</div>");
                        sbProduct.Append("</a>");
                        sbProduct.Append("</div>");
                    }
                    result.Add(sbProduct.ToString());
                }
                else
                {
                    result.Add("false");
                    sbProduct.Append("<div class='product-thumb insp-cat col-xs-12'>");
                    sbProduct.Append("<div class='searchbr text-center'>");
                    sbProduct.Append("<p style='color: #ff6795;'><i class='fa fa-info-circle aria-hidden='true'></i>&nbsp;Sorry, we counldn't find any product for '" + prefix + "' .</p>");
                    sbProduct.Append("</div>");
                    sbProduct.Append("</div>");
                    result.Add(sbProduct.ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region [Product page filter section]
        //public JsonResult GetLeftSideFilter(string stateid)
        public JsonResult GetLeftSideFilter(string pincode)
        {
            string result = string.Empty;

            try
            {
                //result = BindPriceFilterSection(stateid);
                //result += BindStateFilterSection(stateid);
                //result += BindBrandFilterSection(stateid);
                result = BindPriceFilterSection();
                result += BindAreaFilterSection(pincode);
                result += BindStateFilterSection();
                result += BindBrandFilterSection();
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }

        public string BindAreaFilterSection(string pincode)
        {
            string result = string.Empty;

            try
            {
                List<FrontProduct> proList = TempData["ProductList"] as List<FrontProduct>;

                StringBuilder sbFilter = new StringBuilder();

                var areaList = MemberRegService.GetSupplierAreaList(pincode);

                if (areaList != null && areaList.Count > 0)
                {
                    #region [Bind State]                    

                    sbFilter.Append("<div class='tf-filter-group'>");

                    sbFilter.Append("<div class='tf-filter-group-header ' data-toggle='collapse' href='#tf-filter-panel1'>");
                    sbFilter.Append("<span class='tf-filter-group-title'>Area (" + pincode + ")</span>");
                    sbFilter.Append("<span class='pull-right'><i class='fa fa-caret-up'></i></span>");
                    sbFilter.Append("</div>");

                    sbFilter.Append("<div id='tf-filter-panel1' class='collapse in'>");
                    //sbFilter.Append("<a data-tf-reset='check' class=' tf-filter-reset hide'><i class='fa fa-window-close' aria-hidden='true'></i> Clear</a>");
                    sbFilter.Append("<div class='tf-filter-group-content'>");

                    foreach (var area in areaList)
                    {
                        int procount = proList.Where(p => p.SupplierAreaName == area).Count();

                        if (procount > 0)
                        {
                            sbFilter.Append("<div class='form-check tf-filter-value clearfix text'>");
                            sbFilter.Append("<label class='form-check-label' style='cursor: pointer;'>");

                            //string defChechk = string.Empty;
                            //if (state.StateId.ToString() == stateid)
                            //{
                            //    defChechk = "checked";
                            //}

                            //sbFilter.Append("<input type='checkbox' name='tf_fm' class='form-check-input statecheckbox' data-stateid='" + state.StateId + "' " + defChechk + ">" + state.StateName + "</label>");
                            sbFilter.Append("<input type='checkbox' name='tf_fm' class='form-check-input areacheckbox' data-areaname='" + area + "'>" + area + "</label>");
                            sbFilter.Append("<span class='label label-info pull-right tf-product-total' style='background-color: #6f0063;'>" + procount + "</span>");
                            sbFilter.Append("</div>");
                        }
                    }

                    sbFilter.Append("</div>");
                    sbFilter.Append("</div>");

                    sbFilter.Append("</div>");

                    #endregion
                }

                result = sbFilter.ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            KeepSessions();

            return result;
        }
        public string BindStateFilterSection()
        {
            string result = string.Empty;

            try
            {
                List<FrontProduct> proList = TempData["ProductList"] as List<FrontProduct>;

                StringBuilder sbFilter = new StringBuilder();

                var stateList = AreaMasterService.GetStateDetail(null, true);

                if (stateList != null && stateList.Count > 0)
                {
                    #region [Bind State]                    

                    sbFilter.Append("<div class='tf-filter-group'>");

                    sbFilter.Append("<div class='tf-filter-group-header ' data-toggle='collapse' href='#tf-filter-panel1'>");
                    sbFilter.Append("<span class='tf-filter-group-title'>State</span>");
                    sbFilter.Append("<span class='pull-right'><i class='fa fa-caret-up'></i></span>");
                    sbFilter.Append("</div>");

                    sbFilter.Append("<div id='tf-filter-panel1' class='collapse in'>");
                    //sbFilter.Append("<a data-tf-reset='check' class=' tf-filter-reset hide'><i class='fa fa-window-close' aria-hidden='true'></i> Clear</a>");
                    sbFilter.Append("<div class='tf-filter-group-content'>");

                    foreach (var state in stateList)
                    {
                        int procount = proList.Where(p => Convert.ToInt32(p.SupplierStateId) == state.StateId).Count();

                        if (procount > 0)
                        {
                            sbFilter.Append("<div class='form-check tf-filter-value clearfix text'>");
                            sbFilter.Append("<label class='form-check-label' style='cursor: pointer;'>");

                            //string defChechk = string.Empty;
                            //if (state.StateId.ToString() == stateid)
                            //{
                            //    defChechk = "checked";
                            //}

                            //sbFilter.Append("<input type='checkbox' name='tf_fm' class='form-check-input statecheckbox' data-stateid='" + state.StateId + "' " + defChechk + ">" + state.StateName + "</label>");
                            sbFilter.Append("<input type='checkbox' name='tf_fm' class='form-check-input statecheckbox' data-stateid='" + state.StateId + "'>" + state.StateName + "</label>");
                            sbFilter.Append("<span class='label label-info pull-right tf-product-total' style='background-color: #6f0063;'>" + procount + "</span>");
                            sbFilter.Append("</div>");
                        }
                    }

                    sbFilter.Append("</div>");
                    sbFilter.Append("</div>");

                    sbFilter.Append("</div>");

                    #endregion
                }

                result = sbFilter.ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            KeepSessions();

            return result;
        }
        public string BindBrandFilterSection()
        {
            string result = string.Empty;

            try
            {
                List<FrontProduct> proList = TempData["ProductList"] as List<FrontProduct>;

                //if (!string.IsNullOrEmpty(stateid))
                //{
                //    proList = proList.Where(p => p.SupplierStateId == stateid).ToList();
                //}

                StringBuilder sbFilter = new StringBuilder();

                var brandList = BasicMasterService.GetBrandDetail(null, true);

                if ((brandList != null && brandList.Count > 0) && (proList.Count > 0))
                {
                    #region [Bind State]                    

                    sbFilter.Append("<div class='tf-filter-group'>");

                    sbFilter.Append("<div class='tf-filter-group-header ' data-toggle='collapse' href='#tf-filter-panel1'>");
                    sbFilter.Append("<span class='tf-filter-group-title'>Brand</span>");
                    sbFilter.Append("<span class='pull-right'><i class='fa fa-caret-up'></i></span>");
                    sbFilter.Append("</div>");

                    sbFilter.Append("<div id='tf-filter-panel1' class='collapse in'>");
                    //sbFilter.Append("<a data-tf-reset='check' class=' tf-filter-reset hide'><i class='fa fa-window-close' aria-hidden='true'></i> Clear</a>");
                    sbFilter.Append("<div class='tf-filter-group-content'>");

                    foreach (var brand in brandList)
                    {
                        int procount = proList.Where(p => p.Brand == brand.BrandId).Count();

                        if (procount > 0)
                        {
                            sbFilter.Append("<div class='form-check tf-filter-value clearfix text'>");
                            sbFilter.Append("<label class='form-check-label' style='cursor: pointer;'>");
                            sbFilter.Append("<input type='checkbox' name='tf_fm' class='form-check-input brandcheckbox' data-brandid='" + brand.BrandId + "'>" + brand.BrandName + "</label>");
                            sbFilter.Append("<span class='label label-info pull-right tf-product-total' style='background-color: #6f0063;'>" + procount + "</span>");
                            sbFilter.Append("</div>");
                        }
                    }

                    sbFilter.Append("</div>");
                    sbFilter.Append("</div>");

                    sbFilter.Append("</div>");

                    #endregion
                }

                result = sbFilter.ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            KeepSessions();

            return result;
        }
        private int countDigit(long n)
        {
            int count = 0;
            while (n != 0)
            {
                n = n / 10;
                ++count;
            }
            return count;
        }
        public string BindPriceFilterSection()
        {
            string result = string.Empty;

            try
            {
                List<FrontProduct> proList = TempData["ProductList"] as List<FrontProduct>;

                //if (!string.IsNullOrEmpty(stateid))
                //{
                //    proList = proList.Where(p => p.SupplierStateId == stateid).ToList();
                //}

                StringBuilder sbFilter = new StringBuilder();

                if (proList.Count > 0)
                {
                    List<decimal> ascPriceList = proList.OrderBy(p => p.SalePriceWithDiscount).Select(p => p.SalePriceWithDiscount).ToList();

                    decimal minPrice = ascPriceList[0];
                    decimal maxPrice = ascPriceList[proList.Count() - 1];

                    int setPrice = countDigit(Convert.ToInt32(maxPrice / 100));

                    string minPriceStr = string.Empty;
                    string maxPriceStr = string.Empty;

                    decimal tempMinPice = 0;
                    decimal tempSetPrice = 0;

                    string ppppstr = "10";
                    if (setPrice > 0)
                    {
                        for (int i = 0; i < setPrice; i++)
                        {
                            ppppstr = ppppstr + "0";
                        }
                    }

                    decimal tempPrice = Convert.ToDecimal(ppppstr);
                    decimal newsetmiPrice = tempPrice;

                    if (minPrice != maxPrice)
                    {
                        for (int i = 1; i <= 5; i++)
                        {
                            if (i == 1)
                            {
                                tempSetPrice = minPrice;
                                minPriceStr += "<option value='" + minPrice + "'>₹ " + minPrice + "</option>";

                                if (maxPrice > newsetmiPrice)
                                {
                                    tempMinPice = minPrice + newsetmiPrice;
                                    maxPriceStr += "<option value='" + tempMinPice + "'>₹ " + tempMinPice + "</option>";
                                }
                                else
                                {
                                    tempMinPice = minPrice + tempPrice;
                                    maxPriceStr += "<option value='" + tempMinPice + "'>₹ " + tempMinPice + "</option>";
                                }
                            }
                            else
                            {
                                if (maxPrice > tempMinPice)
                                {

                                    tempMinPice = tempMinPice + 1;

                                    if (maxPrice > newsetmiPrice)
                                    {
                                        minPriceStr += "<option value='" + tempMinPice + "'>₹ " + tempMinPice + "</option>";

                                        if (i == 5)
                                        {
                                            tempMinPice = tempMinPice + (newsetmiPrice - 1);
                                            maxPriceStr += "<option value='" + maxPrice + "'>₹ " + tempMinPice + " +</option>";
                                        }
                                        else
                                        {
                                            tempMinPice = tempMinPice + (newsetmiPrice - 1);
                                            maxPriceStr += "<option value='" + tempMinPice + "'>₹ " + tempMinPice + "</option>";
                                        }
                                    }
                                    else
                                    {
                                        tempSetPrice = tempSetPrice + Convert.ToDecimal(ppppstr);
                                        minPriceStr += "<option value='" + tempSetPrice + "'>₹ " + tempSetPrice + "</option>";

                                        if (i == 5)
                                        {
                                            tempSetPrice = tempSetPrice + (tempPrice - 1);
                                            maxPriceStr += "<option value='" + maxPrice + "'>₹ " + tempSetPrice + " +</option>";
                                        }
                                        else
                                        {
                                            tempSetPrice = tempSetPrice + (tempPrice - 1);
                                            maxPriceStr += "<option value='" + tempSetPrice + "'>₹ " + tempSetPrice + "</option>";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #region [Bind Price]                    

                    sbFilter.Append("<div class='tf-filter-group'>");

                    sbFilter.Append("<div class='tf-filter-group-header ' data-toggle='collapse' href='#tf-filter-panel1'>");
                    sbFilter.Append("<span class='tf-filter-group-title'>Price</span>");
                    sbFilter.Append("<span class='pull-right'><i class='fa fa-caret-up'></i></span>");
                    sbFilter.Append("</div>");

                    sbFilter.Append("<div id='tf-filter-panel1' class='collapse in'>");
                    //sbFilter.Append("<a data-tf-reset='check' class=' tf-filter-reset hide'><i class='fa fa-window-close' aria-hidden='true'></i> Clear</a>");
                    sbFilter.Append("<div class='tf-filter-group-content'>");

                    sbFilter.Append("<div class='row'>");

                    sbFilter.Append("<div class='col-xs-6'>");
                    sbFilter.Append("<select class='form-control' id='ddlMinPrice'>");
                    sbFilter.Append("<option value='0'>Min Price</option>");
                    sbFilter.Append(minPriceStr);
                    sbFilter.Append("</select>");
                    sbFilter.Append("</div>");

                    sbFilter.Append("<div class='col-xs-6'>");
                    sbFilter.Append("<select class='form-control' id='ddlMaxPrice'>");
                    sbFilter.Append("<option value='0'>Max Price</option>");
                    sbFilter.Append(maxPriceStr);
                    sbFilter.Append("</select>");
                    sbFilter.Append("</div>");

                    sbFilter.Append("</div>");

                    sbFilter.Append("</div>");
                    sbFilter.Append("</div>");

                    sbFilter.Append("</div>");

                    #endregion
                }

                result = sbFilter.ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            KeepSessions();

            return result;
        }
        private void KeepSessions()
        {
            TempData.Keep("ProductList");
        }
        public JsonResult GetProductList(string pincode, string menuid, string categryid, string subcategryid)
        {
            string result = string.Empty;

            try
            {
                List<FrontProduct> productList = FrontProductService.GetFrontProductList(null, menuid, categryid, subcategryid);

                TempData["ProductList"] = productList;
                if (productList != null && productList.Count > 0)
                {
                    if (!string.IsNullOrEmpty(pincode))
                    {
                        productList = productList.Where(p => p.SupplierPinCode == pincode).ToList();
                    }

                    result = BindDynamicallyProduct(productList);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }


            return Json(result);
        }

        public JsonResult GetBreadCrumb(string menuid, string categryid, string subcategryid)
        {
            string result = string.Empty;

            try
            {
                result = "<li><a href='/'><i class='fa fa-home'></i></a></li>";

                string menuName = string.Empty; string menuUrl = string.Empty;
                string catName = string.Empty; string catUrl = string.Empty;
                string subCatName = string.Empty; string subCatUrl = string.Empty;

                if (!string.IsNullOrEmpty(menuid))
                {
                    DataTable dtResult = FrontProductService.GetMenuCatSCatDetails(menuid, null, null);

                    if (dtResult.Rows.Count > 0)
                    {
                        menuName = dtResult.Rows[0]["MenuName"].ToString();
                        menuUrl = Regex.Replace(menuName, @"[^0-9a-zA-Z]+", ",").Replace(",", "-").ToLower();
                        result += "<li><a href='/pm/" + menuUrl + "/" + menuid + "'>" + menuName + "</a></li>";
                    }
                }

                if (!string.IsNullOrEmpty(categryid))
                {
                    DataTable dtResult = FrontProductService.GetMenuCatSCatDetails(null, categryid, null);
                    if (dtResult.Rows.Count > 0)
                    {
                        catName = dtResult.Rows[0]["CategoryName"].ToString();
                        catUrl = Regex.Replace(catName, @"[^0-9a-zA-Z]+", ",").Replace(",", "-").ToLower();
                        result += "<li><a href='/pc/" + menuUrl + "/" + menuid + "/" + catUrl + "/" + categryid + "'>" + catName + "</a></li>";
                    }
                }

                if (!string.IsNullOrEmpty(subcategryid))
                {
                    DataTable dtResult = FrontProductService.GetMenuCatSCatDetails(null, null, subcategryid);
                    if (dtResult.Rows.Count > 0)
                    {
                        subCatName = dtResult.Rows[0]["SubCategoryName"].ToString();
                        subCatUrl = Regex.Replace(subCatName, @"[^0-9a-zA-Z]+", ",").Replace(",", "-").ToLower();
                        result += "<li><a href='/psc/" + menuUrl + "/" + menuid + "/" + catUrl + "/" + categryid + "/" + subCatUrl + " / " + subcategryid + "'>" + subCatName + "</a></li>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public string BindDynamicallyProduct(List<FrontProduct> productList)
        {
            StringBuilder sbProduct = new StringBuilder();

            try
            {
                if (productList != null && productList.Count > 0)
                {
                    foreach (var item in productList)
                    {
                        sbProduct.Append("<div class='product-layout product-grid col-lg-3 col-md-4 col-sm-6 col-xs-12'>");
                        sbProduct.Append("<div class='product-thumb transition'>");
                        sbProduct.Append("<div class='image'>");
                        sbProduct.Append("<a href='" + item.RedirectUrl + "'>");
                        if (item.FrontProductImage != null && item.FrontProductImage.Count > 0)
                        {
                            sbProduct.Append("<img src='" + item.FrontProductImage[0].ImageUrl + "' alt='" + item.ProductName + "' title='" + item.ProductName + "' class='img-responsive center-block'>");
                        }
                        else
                        {
                            sbProduct.Append("<img src='~/Content/image/no-image.png' alt='" + item.ProductName + "' title='" + item.ProductName + "' class='img-responsive center-block' />");
                        }
                        sbProduct.Append("</a>");
                        if (item.DiscountPer > 0)
                        {
                            sbProduct.Append("<p class='sale-tag new'>Discount " + item.DiscountPer + "%</p>");
                        }
                        sbProduct.Append("</div>");

                        sbProduct.Append("<div class='hidden'>");
                        sbProduct.Append("<span id='frontaddprodetails_" + item.ProductId + "' class='hidden frontaddprodetails' data-productid='" + item.ProductId + "' data-productname='" + item.ProductName + "' data-productimgurl='" + (item.FrontProductImage.Count > 0 ? item.FrontProductImage[0].ImageUrl : "~/Content/image/noproduct.png") + "' data-saleprice='" + item.SalePrice + "' data-discountprice='" + item.Discount + "' data-discountper='" + item.DiscountPer + "' data-unitid='" + item.Unit + "' data-unitname='" + item.UnitName + "'></span>");
                        sbProduct.Append("</div>");

                        sbProduct.Append("<div class='caption text-center'>");
                        sbProduct.Append("<a href='" + item.RedirectUrl + "'>");
                        sbProduct.Append("<h4>" + item.ProductName + "</h4>");
                        if (!string.IsNullOrEmpty(item.FrontProductDescription.Descripion1))
                        {
                            sbProduct.Append("<p class='showdecinlistview hidden'>" + Utility.GetDescription(item.FrontProductDescription.Descripion1, 160) + " ...</p>");
                        }
                        sbProduct.Append("<p class='price'>");
                        if (item.DiscountPer > 0)
                        {
                            sbProduct.Append("<span class='price-new'>₹ " + String.Format("{0:0.00}", item.SalePriceWithDiscount) + "</span> <span class='price-old'>₹ " + item.SalePrice + "</span>");
                        }
                        else
                        {
                            sbProduct.Append("<span class='price-new'>₹ " + item.SalePrice + "</span>");
                        }
                        sbProduct.Append("</a>");
                        sbProduct.Append("<div class='input-group col-xs-12 col-sm-12 qop'>");
                        sbProduct.Append("<label class='control-label col-sm-2 col-xs-2 qlable' for='input-quantity'> Qty </label>");
                        sbProduct.Append("<input type='number' name='quantity' min='1' value='1' step='1' id='fqty_" + item.ProductId + "' class='form-control col-sm-2 col-xs-9 qtyq' />");
                        sbProduct.Append("<button type='button' class='acart frontaddtocart' data-proid='" + item.ProductId + "'>");
                        sbProduct.Append("<i class='fa fa-shopping-basket fasbasket_" + item.ProductId + "' aria-hidden='true'></i>");
                        sbProduct.Append("</button>");
                        sbProduct.Append("</div>");

                        sbProduct.Append("</div>");
                        sbProduct.Append("</div>");
                        sbProduct.Append("</div>");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            KeepSessions();

            return sbProduct.ToString();
        }
        public JsonResult BindProductByFilterAction(LeftSideFilter filter)
        {
            string result = string.Empty;
            try
            {
                if (filter != null)
                {
                    List<FrontProduct> proList = TempData["ProductList"] as List<FrontProduct>;
                    if (proList != null && proList.Count > 0)
                    {
                        if (filter.StateId != null)
                        {
                            proList = proList.Where(p => filter.StateId.Contains(p.SupplierStateId)).ToList();
                        }

                        if (filter.Area != null)
                        {
                            proList = proList.Where(p => filter.Area.Contains(p.SupplierAreaName)).ToList();
                        }

                        if (filter.BrandId != null)
                        {
                            proList = proList.Where(p => filter.BrandId.Contains(p.Brand.ToString())).ToList();
                        }

                        if (!string.IsNullOrEmpty(filter.MinPrice))
                        {
                            if (filter.MinPrice.ToLower().Trim() != "min price" && filter.MinPrice != "0")
                            {
                                proList = proList.Where(p => p.SalePriceWithDiscount >= Convert.ToDecimal(filter.MinPrice)).ToList();
                            }
                        }

                        if (!string.IsNullOrEmpty(filter.MaxPrice))
                        {
                            if (filter.MaxPrice.ToLower().Trim() != "max price" && filter.MaxPrice != "0")
                            {
                                proList = proList.Where(p => p.SalePriceWithDiscount <= Convert.ToDecimal(filter.MaxPrice)).ToList();
                            }
                        }

                        if (!string.IsNullOrEmpty(filter.SelectedStateId) && (filter.StateId == null && filter.BrandId == null))
                        {
                            proList = proList.Where(p => p.SupplierStateId == filter.SelectedStateId).ToList();
                        }

                        if (filter.StateId == null && filter.BrandId == null && (string.IsNullOrEmpty(filter.MinPrice) || filter.MinPrice == "0") && (string.IsNullOrEmpty(filter.MaxPrice) || filter.MaxPrice == "0") && filter.Area == null)
                        {
                            proList = proList.Where(p => p.SupplierPinCode == filter.PinCode).ToList();
                        }

                        result = BindDynamicallyProduct(proList);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        #endregion

        #region [Home Page Products]
        public PartialViewResult BindALLProudcts()
        {
            return PartialView();
        }
        #endregion

        #region [Review Section]

        public JsonResult GetReviewDetail(string productid)
        {
            string result = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(productid))
                {
                    StringBuilder sbReview = new StringBuilder();

                    List<ReviewModel> ReviewList = ReviewService.GetReviewDetail(productid, " order by pr.Position");
                    if (ReviewList.Count > 0)
                    {
                        foreach (var item in ReviewList)
                        {
                            sbReview.Append("<div class='reviewpara'>");
                            sbReview.Append("<div class='row'>");
                            sbReview.Append("<div class='col-sm-8'>");

                            string irating = string.Empty;
                            for (int i = 1; i <= Convert.ToInt32(item.Rating); i++)
                            {
                                irating = irating + "<i class='fa fa-star'></i>";
                            }

                            sbReview.Append("<p><i class='fa fa-user-circle' aria-hidden='true'></i> " + item.UserName.ToUpper() + " " + irating + "</p>");
                            sbReview.Append("</div>");
                            sbReview.Append("<div class='col-sm-4'><p class='pull-right'>" + item.CreatedDate + "</p></div>");
                            sbReview.Append("<div class='col-sm-12'><p>" + item.ReviewContent + "</p></div>");
                            sbReview.Append("</div>");
                            sbReview.Append("</div>");
                        }

                        result = sbReview.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult InsertReviewDetail(string productid, string name, string email, string content, string rating)
        {
            string result = string.Empty;

            try
            {
                FrontEndLogin lu = new FrontEndLogin();
                if (AccountService.IsFrontendLogin(ref lu))
                {
                    ReviewModel model = new ReviewModel();
                    model.ProductId = Convert.ToInt32(productid);
                    model.UserName = name;
                    model.EmailId = email;
                    model.ReviewContent = content;
                    model.Rating = rating;
                    model.LoginId = lu.LoginId.ToString();
                    if (ReviewService.InsertProductReviewDetail(model))
                    {
                        result = "true";
                    }
                    else
                    {
                        result = "false";
                    }
                }
            }
            catch (Exception ex)
            {
                result = "false";
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        #endregion
    }
}