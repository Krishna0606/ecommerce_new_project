﻿
using ecommerce.Models.Backend;
using ecommerce.Service.BackEndService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecommerce.Controllers
{
    public class CMSController : Controller
    {
        // GET: CMS
        public ActionResult AboutusDetails()
        {
            AboutUsModel model = new AboutUsModel();
            try
            {
                model.AboutUslist = CmsMasterService.Getaboutuslist();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }          
            return View(model);
        }
        public ActionResult AddEditAboutus(int? aboutid)
        {
            AboutUsModel model = new AboutUsModel();
            try
            {
                if (aboutid > 0)
                {
                    model = CmsMasterService.GetaboutModelbyID(aboutid);
                }
            }
            catch(Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult AddEditAboutus(int? aboutid,AboutUsModel model)
        {
            try
            {
                bool isSuccess = false;
                if (!string.IsNullOrEmpty(model.Heading) && !string.IsNullOrEmpty(model.Content))
                {
                    if (aboutid > 0)
                    {
                        isSuccess = CmsMasterService.UpdateAboutUs(aboutid, model);
                        TempData["Msg"] = "Record Updated Successfully ...";
                    }
                    else
                    {
                        isSuccess = CmsMasterService.InsertAboutUs(model);
                        TempData["Msg"] = "Record Added Successfully ...";                        
                    }
                }
            }

            catch (Exception ex)
            {
                ex.ToString();
            }
            return RedirectToAction("AboutusDetails", "CMS");
        }

        public ActionResult PrivicyDetails()
        {
            PrivicyPolicyModel model = new PrivicyPolicyModel();
            try
            {
                model.Privicylist = CmsMasterService.GetaPrivicyPolicylist();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public ActionResult AddEditPrivicy(int? privicyid)
        {
            PrivicyPolicyModel model = new PrivicyPolicyModel();
            try
            {
                if (privicyid > 0)
                {
                    model = CmsMasterService.GetPrivicyModelbyID(privicyid);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult AddEditPrivicy(int? privicyid, PrivicyPolicyModel model)
        {
            try
            {
                bool isSuccess = false;
                if (!string.IsNullOrEmpty(model.Heading) && !string.IsNullOrEmpty(model.Content))
                {
                    if (privicyid > 0)
                    {
                        isSuccess = CmsMasterService.UpdatePrivicy(privicyid, model);
                        TempData["Msg"] = "Record Updated Successfully ...";
                    }
                    else
                    {
                        isSuccess = CmsMasterService.Insertprivicy(model);
                        TempData["Msg"] = "Record Added Successfully ...";
                    }
                }
            }

            catch (Exception ex)
            {
                ex.ToString();
            }
            return RedirectToAction("PrivicyDetails", "CMS");
        }
    }
}