﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using static ecommerce.Models.Backend.BasicMaster;
using ecommerce.Service.BackEndService;

namespace ecommerce.Controllers
{
    public class CommonController : Controller
    {
        public JsonResult BindJsonMenuList(string menuid = null)
        {
            StringBuilder sbMenu = new StringBuilder();

            try
            {
                List<MenuModel> menuList = BasicMasterService.GetmenuDetail(menuid, true);
                if (menuList != null && menuList.Count > 0)
                {
                    sbMenu.Append("<option value='0'>Select Menu</option>");
                    foreach (var item in menuList)
                    {
                        sbMenu.Append("<option value='" + item.Menuid + "'>" + item.MenuName + "</option>");
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(sbMenu.ToString(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult BindJsonCategoryList(string menuid = null)
        {
            StringBuilder sbCategory = new StringBuilder();

            try
            {
                List<CategoryModel> categoryList = BasicMasterService.GetCategoryDetail(null, true, menuid);
                if (categoryList != null && categoryList.Count > 0)
                {
                    sbCategory.Append("<option value='0'>Select Category</option>");
                    foreach (var item in categoryList)
                    {
                        sbCategory.Append("<option value='" + item.CategoryId + "'>" + item.CategoryName + "</option>");
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(sbCategory.ToString(), JsonRequestBehavior.AllowGet);
        }
    }
}