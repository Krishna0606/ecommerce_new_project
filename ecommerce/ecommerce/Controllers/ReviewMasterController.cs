﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ecommerce.Models.Frontend;
using ecommerce.Service.FrontService;

namespace ecommerce.Controllers
{
    public class ReviewMasterController : Controller
    {
        // GET: ReviewMaster
        public ActionResult ReviewList()
        {
            ReviewModel model = new ReviewModel();

            try
            {
                List<ReviewModel> ReviewList = ReviewService.GetReviewDetail(string.Empty, " order by p.ProductName");
                if (ReviewList.Count > 0)
                {
                    model.ReviewList = ReviewList;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return View(model);
        }

        public JsonResult UpdateReviewPosition(string reviewid, string posval)
        {
            string result = string.Empty;

            try
            {
                if (ReviewService.UpdateReviewPosition(reviewid, posval))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }

        public JsonResult UpdateReviewIsApproved(string reviewid, string approved)
        {
            string result = string.Empty;

            try
            {
                if (ReviewService.UpdateReviewIsApproved(reviewid, approved))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
    }
}