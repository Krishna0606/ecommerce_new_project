﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ecommerce.Models.Common;
using ecommerce.Service.BackEndService;
using static ecommerce.Models.Backend.AreaMaster;

namespace ecommerce.Controllers
{
    public class AreaMasterController : Controller
    {
        #region [Country]
        public ActionResult CountryMasterList(string countryid = null, bool status = true)
        {
            Country model = new Country();

            try
            {
                List<Country> countryList = AreaMasterService.GetCountryDetail(countryid, status);
                if (countryList != null && countryList.Count > 0)
                {
                    model.CountryList = countryList;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public ActionResult AddEditCountryMaster(string countryid = null, bool status = true)
        {
            Country model = new Country();

            try
            {
                if (!string.IsNullOrEmpty(countryid))
                {
                    List<Country> countryList = AreaMasterService.GetCountryDetail(countryid, status);
                    if (countryList != null && countryList.Count > 0)
                    {
                        model.CountryId = countryList[0].CountryId;
                        model.CountryName = countryList[0].CountryName;
                        model.CountryCode = countryList[0].CountryCode;
                        model.Status = countryList[0].Status;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        #endregion

        #region [State]
        public ActionResult StateMasterList(string stateid = null, bool status = true)
        {
            State model = new State();

            try
            {
                List<State> stateList = AreaMasterService.GetStateDetail(stateid, status);
                if (stateList != null && stateList.Count > 0)
                {
                    model.StateList = stateList;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public ActionResult AddEditStateMaster(string stateid = null, bool status = true)
        {
            State model = new State();
            try
            {
                model.CountryList = Common.PopulateCountry(AreaMasterService.GetCountryDetail(null, true));
                if (!string.IsNullOrEmpty(stateid))
                {
                    List<State> stateList = AreaMasterService.GetStateDetail(stateid, status);
                    if (stateList != null && stateList.Count > 0)
                    {
                        model.StateId = stateList[0].StateId;
                        model.CountryId = stateList[0].CountryId;
                        model.StateName = stateList[0].StateName;
                        model.StateCode = stateList[0].StateCode;
                        model.Status = stateList[0].Status;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        #endregion

        #region [City]
        public ActionResult CityMasterList(string cityid = null, bool status = true)
        {
            City model = new City();

            try
            {
                List<City> cityList = AreaMasterService.GetCityDetail(cityid, null, status);
                if (cityList != null && cityList.Count > 0)
                {
                    model.CityList = cityList;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public ActionResult AddEditCityMaster(string cityid = null, bool status = true)
        {
            City model = new City();
            try
            {
                model.CountryList = Common.PopulateCountry(AreaMasterService.GetCountryDetail(null, true));
                model.StateList = Common.PopulateState(AreaMasterService.GetStateDetail(null, true, "0"));

                if (!string.IsNullOrEmpty(cityid))
                {
                    List<City> cityList = AreaMasterService.GetCityDetail(cityid, null, status);
                    if (cityList != null && cityList.Count > 0)
                    {
                        model.CityId = cityList[0].CityId;
                        model.CityName = cityList[0].CityName;
                        model.CityCode = cityList[0].CityCode;
                        model.CountryId = cityList[0].CountryId;
                        model.StateId = cityList[0].StateId;
                        model.Status = cityList[0].Status;

                        model.StateList = Common.PopulateState(AreaMasterService.GetStateDetail(null, true, model.CountryId.ToString()));
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }

        public JsonResult BindCity(string stateid)
        {
            City model = new City();
            model.BindCityList = Common.PopulateCity(AreaMasterService.GetCityDetail(null, stateid, true));
            return Json(model.BindCityList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region [Json Section]
        public JsonResult ProcessInsertCountry(string countryid, string countryname, string countrycode)
        {
            List<string> result = new List<string>(); ;

            try
            {
                if (!string.IsNullOrEmpty(countryname) && !string.IsNullOrEmpty(countrycode))
                {
                    if (string.IsNullOrEmpty(countryid) || countryid == "0")
                    {
                        if (AreaMasterService.InsertCountryDetail(countryname, countrycode))
                        {
                            result.Add("true");
                            result.Add("Record inserted successfully.");
                        }
                    }
                    else
                    {
                        if (AreaMasterService.UpdateCountryDetail(countryname, countrycode, countryid))
                        {
                            result.Add("true");
                            result.Add("Record updated successfully.");
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(countryid) && countryid != "0")
                    {
                        if (AreaMasterService.UpdateCountryDetail(null, null, countryid))
                        {
                            result.Add("true");
                            result.Add("Record deleted successfully.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult ProcessInsertState(string stateid, string statename, string statecode, string countryid)
        {
            List<string> result = new List<string>(); ;

            try
            {
                if (!string.IsNullOrEmpty(statename) && !string.IsNullOrEmpty(statecode) && !string.IsNullOrEmpty(countryid))
                {
                    if (string.IsNullOrEmpty(stateid) || stateid == "0")
                    {
                        if (AreaMasterService.InsertStateDetail(statename, statecode, countryid))
                        {
                            result.Add("true");
                            result.Add("Record inserted successfully.");
                        }
                    }
                    else
                    {
                        if (AreaMasterService.UpdateStateDetail(stateid, statename, statecode, countryid))
                        {
                            result.Add("true");
                            result.Add("Record updated successfully.");
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(stateid) && stateid != "0")
                    {
                        if (AreaMasterService.UpdateStateDetail(stateid, null, null, null))
                        {
                            result.Add("true");
                            result.Add("Record deleted successfully.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult ProcessInsertCity(string cityid, string cityname, string citycode, string countryid, string stateId)
        {
            List<string> result = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(cityname) && !string.IsNullOrEmpty(citycode) && !string.IsNullOrEmpty(countryid) && !string.IsNullOrEmpty(stateId))
                {
                    if (string.IsNullOrEmpty(cityid) || cityid == "0")
                    {
                        if (AreaMasterService.InsertCityDetail(cityname, citycode, countryid, stateId))
                        {
                            result.Add("true");
                            result.Add("Record inserted successfully.");
                        }
                    }
                    else
                    {
                        if (AreaMasterService.UpdateCityDetail(cityid, cityname, citycode, countryid, stateId))
                        {
                            result.Add("true");
                            result.Add("Record updated successfully.");
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(cityid) && cityid != "0")
                    {
                        if (AreaMasterService.UpdateCityDetail(cityid, null, null, null, null))
                        {
                            result.Add("true");
                            result.Add("Record deleted successfully.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult BindState(string countryid)
        {
            State model = new State();
            model.BindStateList = Common.PopulateState(AreaMasterService.GetStateDetail(null, true, countryid));
            return Json(model.BindStateList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStateDetails()
        {
            string result = string.Empty;

            try
            {
                List<State> stateList = AreaMasterService.GetStateDetail(null, true);
                if (stateList != null && stateList.Count > 0)
                {
                    result = "<option value=''>Select State</option>";
                    foreach (var item in stateList)
                    {
                        result += "<option value='" + item.StateId + "'>" + item.StateName + "</option>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        #endregion

    }
}