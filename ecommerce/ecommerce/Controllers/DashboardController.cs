﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ecommerce.Models.Backend.Dashboard;
using ecommerce.Models.Backend;
using ecommerce.Service;
using ecommerce.Service.BackEndService;

namespace ecommerce.Controllers
{
    public class DashboardController : Controller
    {
        public ActionResult Index()
        {
            DashboardModel model = new DashboardModel();

            try
            {
                BackendLogin lu = new BackendLogin();
                if (AccountService.IsBackendLogin(ref lu))
                {
                    model.LoginId = lu.LoginId;
                    model.UserName = lu.FirstName + " " + lu.LastName;
                    model.UserId = lu.UserId;
                    model.UserType = lu.UserType;

                    TempData["UserLoginDetails"] = lu;
                }
                else
                {
                    if (AccountService.LogoutBackendLogin())
                    {
                        Response.Redirect("/backend/login");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return View(model);
        }
        public ActionResult BlanckPage()
        {
            return View();
        }
        public PartialViewResult SupplierDashboard()
        {
            DashboardOrders model = new DashboardOrders();

            try
            {
                BackendLogin lu = TempData["UserLoginDetails"] as BackendLogin;

                if (lu != null && lu.LoginId > 0)
                {
                    List<DashboardOrders> orderList = BackendPlaceOrderService.GetPlaceOrderDetails(lu.LoginId.ToString(), lu.UserType);

                    if (orderList.Count > 0)
                    {
                        model.PendingOrderList = orderList.Where(p => p.OrderStatus.ToLower().Trim() == "pending").Take(5).ToList();
                        model.DeliverdOrderList = orderList.Where(p => p.OrderStatus.ToLower().Trim() == "delivered").Take(5).ToList();
                        model.CancelledOrderList = orderList.Where(p => p.OrderStatus.ToLower().Trim() == "cancelled").Take(5).ToList();

                        IncomeCountModel incomeModel = new IncomeCountModel();

                        incomeModel.TotalIncome = orderList.Sum(x => Convert.ToDecimal(x.GrandTotal)).ToString();
                        incomeModel.MonthIncome = orderList.Where(x => x.CreatedDate.Month == DateTime.Now.Month).Sum(x => Convert.ToDecimal(x.GrandTotal)).ToString();
                        incomeModel.TodaYIncome = orderList.Where(x => x.CreatedDate.Day == DateTime.Now.Day).Sum(x => Convert.ToDecimal(x.GrandTotal)).ToString();
                        incomeModel.YearIncome = orderList.Where(x => x.CreatedDate.Year == DateTime.Now.Year).Sum(x => Convert.ToDecimal(x.GrandTotal)).ToString();

                        model.IncomeModel = incomeModel;
                    }
                }
                else
                {
                    if (AccountService.LogoutBackendLogin())
                    {
                        Response.Redirect("/backend/login");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            KeepTempData();

            return PartialView(model);
        }
        public PartialViewResult AdminDashboard()
        {
            DashboardOrders model = new DashboardOrders();

            try
            {
                BackendLogin lu = TempData["UserLoginDetails"] as BackendLogin;

                if (lu != null && lu.LoginId > 0)
                {
                    List<DashboardOrders> orderList = BackendPlaceOrderService.GetPlaceOrderDetails(lu.LoginId.ToString(), lu.UserType);

                    if (orderList.Count > 0)
                    {
                        model.PendingOrderList = orderList.Where(p => p.OrderStatus.ToLower().Trim() == "pending").Take(5).ToList();
                        model.DeliverdOrderList = orderList.Where(p => p.OrderStatus.ToLower().Trim() == "delivered").Take(5).ToList();
                        model.CancelledOrderList = orderList.Where(p => p.OrderStatus.ToLower().Trim() == "cancelled").Take(5).ToList();
                    }

                    IncomeCountModel incomeModel = new IncomeCountModel();

                    incomeModel.TotalIncome = orderList.Sum(x => Convert.ToDecimal(x.GrandTotal)).ToString();
                    incomeModel.MonthIncome = orderList.Where(x => x.CreatedDate.Month == DateTime.Now.Month).Sum(x => Convert.ToDecimal(x.GrandTotal)).ToString();
                    incomeModel.TodaYIncome = orderList.Where(x => x.CreatedDate.Day == DateTime.Now.Day).Sum(x => Convert.ToDecimal(x.GrandTotal)).ToString();
                    incomeModel.YearIncome = orderList.Where(x => x.CreatedDate.Year == DateTime.Now.Year).Sum(x => Convert.ToDecimal(x.GrandTotal)).ToString();

                    model.IncomeModel = incomeModel;

                    List<UserLoginDetails> userLoginList = BackendPlaceOrderService.UserLoginDetails();
                    UserCountModel userModel = new UserCountModel();

                    userModel.TotalUser = userLoginList.Where(x => x.UserType == "user").Count().ToString();
                    userModel.MonthUser = userLoginList.Where(x => x.CreatedDate.Month == DateTime.Now.Month && x.UserType == "user").Count().ToString();
                    userModel.TodayUser = userLoginList.Where(x => x.CreatedDate.Day == DateTime.Now.Day && x.UserType == "user").Count().ToString();
                    userModel.YearUser = userLoginList.Where(x => x.CreatedDate.Year == DateTime.Now.Year && x.UserType == "user").Count().ToString();

                    model.UserCountModel = userModel;

                    SuplierCountModel supplierModel = new SuplierCountModel();

                    supplierModel.TotalSuplier = userLoginList.Where(x => x.UserType == "supplier").Count().ToString();
                    supplierModel.MonthSuplier = userLoginList.Where(x => x.CreatedDate.Month == DateTime.Now.Month && x.UserType == "supplier").Count().ToString();
                    supplierModel.TodaySuplier = userLoginList.Where(x => x.CreatedDate.Day == DateTime.Now.Day && x.UserType == "supplier").Count().ToString();
                    supplierModel.YearSuplier = userLoginList.Where(x => x.CreatedDate.Year == DateTime.Now.Year && x.UserType == "supplier").Count().ToString();

                    model.SuplierCountModel = supplierModel;
                }
                else
                {
                    if (AccountService.LogoutBackendLogin())
                    {
                        Response.Redirect("/backend/login");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            KeepTempData();

            return PartialView(model);
        }
        public ActionResult AllPendingOrderList()
        {
            DashboardOrders model = new DashboardOrders();

            try
            {
                BackendLogin lu = TempData["UserLoginDetails"] as BackendLogin;

                if (lu != null && lu.LoginId > 0)
                {
                    List<DashboardOrders> orderList = BackendPlaceOrderService.GetPlaceOrderDetails(lu.LoginId.ToString(), lu.UserType).Where(p => p.OrderStatus.ToLower().Trim() == "pending").ToList();

                    if (orderList.Count > 0)
                    {
                        model.DashboardOrdersList = orderList;
                    }
                }
                else
                {
                    if (AccountService.LogoutBackendLogin())
                    {
                        Response.Redirect("/backend/login");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            KeepTempData();

            return View(model);
        }
        public ActionResult AllDeviveredOrderList()
        {
            DashboardOrders model = new DashboardOrders();

            try
            {
                BackendLogin lu = TempData["UserLoginDetails"] as BackendLogin;

                if (lu != null && lu.LoginId > 0)
                {
                    List<DashboardOrders> orderList = BackendPlaceOrderService.GetPlaceOrderDetails(lu.LoginId.ToString(), lu.UserType).Where(p => p.OrderStatus.ToLower().Trim() == "delivered").ToList();

                    if (orderList.Count > 0)
                    {
                        model.DashboardOrdersList = orderList;
                    }
                }
                else
                {
                    if (AccountService.LogoutBackendLogin())
                    {
                        Response.Redirect("/backend/login");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            KeepTempData();

            return View(model);
        }
        public ActionResult AllCancelledOrderList()
        {
            DashboardOrders model = new DashboardOrders();

            try
            {
                BackendLogin lu = TempData["UserLoginDetails"] as BackendLogin;

                if (lu != null && lu.LoginId > 0)
                {
                    List<DashboardOrders> orderList = BackendPlaceOrderService.GetPlaceOrderDetails(lu.LoginId.ToString(), lu.UserType).Where(p => p.OrderStatus.ToLower().Trim() == "cancelled").ToList();

                    if (orderList.Count > 0)
                    {
                        model.DashboardOrdersList = orderList;
                    }
                }
                else
                {
                    if (AccountService.LogoutBackendLogin())
                    {
                        Response.Redirect("/backend/login");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            KeepTempData();

            return View(model);
        }
        public ActionResult AllOrderList()
        {
            DashboardOrders model = new DashboardOrders();

            try
            {
                BackendLogin lu = TempData["UserLoginDetails"] as BackendLogin;

                if (lu != null && lu.LoginId > 0)
                {
                    List<DashboardOrders> orderList = BackendPlaceOrderService.GetPlaceOrderDetails(lu.LoginId.ToString(), lu.UserType);

                    if (orderList.Count > 0)
                    {
                        model.DashboardOrdersList = orderList;
                    }
                }
                else
                {
                    if (AccountService.LogoutBackendLogin())
                    {
                        Response.Redirect("/backend/login");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            KeepTempData();

            return View(model);
        }

        public void KeepTempData()
        {
            TempData.Keep("UserLoginDetails");
        }
    }
}