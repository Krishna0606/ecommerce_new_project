﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecommerce.Models.Frontend
{
    public class FrontUserEnd
    {
        public class ShippingAddress
        {
            public int AddressId { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string EmailId { get; set; }
            public string MobileNo { get; set; }
            public string AlternateMobileNo { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string PinCode { get; set; }
            public string Landmark { get; set; }
            public string AddressType { get; set; }
            public bool IsDefault { get; set; }
            public int LogId { get; set; }
            public string UserId { get; set; }
            public string UserName { get; set; }
            public bool Status { get; set; }
            public string StateId { get; set; }
            public List<SelectListItem> StateList { get; set; }
        }

        public class OrderDetail
        {
            public int Orderid { get; set; }
            public string OederNo { get; set; }
            public int Productid { get; set; }
            public string ProductCode { get; set; }
            public string ProductName { get; set; }
            public string ProductImgUrl { get; set; }            
            public decimal DeliverCharges { get; set; }
            public decimal SubTotal { get; set; }
            public decimal GrandTotal { get; set; }
            public string Discount { get; set; }           
            public string Unit { get; set; }
            public string PaymentType { get; set; }
            public string DeliverStatus { get; set; }
            public int Supppierid { get; set; }
            public string SupplierEmail { get; set; }
            public string SupplierName { get; set; }
            public string SupplierAddress { get; set; }
            public string SupplierMobileNo { get; set; }
            public string SupplierCity { get; set; }
            public string SupplierPin { get; set; }
            public string SupplierPanNo { get; set; }
            public string SupplierGstNo { get; set; }
            public int UserId { get; set; }
            public int Quantity { get; set; }
            public string UserLoginId { get; set; }
            public string UserName { get; set; }
            public string UserEmailid { get; set; }
            public string UserAddress { get; set; }
            public string UserCity { get; set; }
            public string Userpin { get; set; }
            public string Landmark { get; set; }
            public string UserMobile { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string pincode { get; set; }
            public List<OrderDetail> OrderList { get; set; }
        }
    }
}