﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecommerce.Models.Frontend
{
    public class CheckoutModel
    {
        public class Checkout
        {
            public CheckoutShippingAddress ShippingAddress { get; set; }
        }

        public class CheckoutShippingAddress
        {
            public int AddressId { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string EmailId { get; set; }
            public string MobileNo { get; set; }
            public string AlternateMobileNo { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string PinCode { get; set; }
            public string Landmark { get; set; }
            public string AddressType { get; set; }
            public bool IsDefault { get; set; }
            public int LogId { get; set; }
            public string UserId { get; set; }
            public string UserName { get; set; }
            public bool Status { get; set; }
            public string StateId { get; set; }
            public List<SelectListItem> StateList { get; set; }
        }

        public class BookingOrder
        {
            public int OrderId { get; set; }
            public string OrderNo { get; set; }

            public int ProductId { get; set; }
            public string ProductName { get; set; }
            public string ProductCode { get; set; }
            public decimal SubTotal { get; set; }
            public decimal DeliveryCharges { get; set; }
            public decimal GrandTotal { get; set; }
            public string Unit { get; set; }
            public int Quantity { get; set; }

            public string PaymentType { get; set; }
            public string DelieveryStatus { get; set; }

            public int SupplierId { get; set; }
            public string SupplierEmailId { get; set; }
            public string SupplierName { get; set; }

            public int UserId { get; set; }
            public string UserEmailId { get; set; }
            public string UserName { get; set; }      
            
            public string ProductImgUrl { get; set; }

            public DateTime CreatedDate { get; set; }
        }

        public class OrderDelieveryAddress
        {
            public int DelieveryAddressId { get; set; }
            public string OrderNo { get; set; }
            public string UserEmailId { get; set; }
            public string UserAddress { get; set; }
            public string Landmark { get; set; }
            public string MobileNo { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string PinCode { get; set; }
            public string AddressType { get; set; }
        }
    }
}