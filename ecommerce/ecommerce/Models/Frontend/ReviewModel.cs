﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecommerce.Models.Frontend
{
    public class ReviewModel
    {
        public int ReviewId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string UserName { get; set; }
        public string EmailId { get; set; }
        public string ReviewContent { get; set; }
        public string Rating { get; set; }
        public string Position { get; set; }
        public bool IsApproved { get; set; }
        public bool Status { get; set; }
        public string CreatedDate { get; set; }
        public string LoginId { get; set; }
        public List<ReviewModel> ReviewList { get; set; }
    }
}