﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ecommerce.Models.FrontProductModel;

namespace ecommerce.Models.Frontend
{
    public class FrontHomeModel
    {
        public class FrontHomeProductBind
        {
            public int MenuId { get; set; }
            public string MenuName { get; set; }
            public List<FrontHomeProduct> ProductList { get; set; }
            public List<FrontHomeProductBind> FrontHomeProductBindList { get; set; }
        }
        public class FrontHomeProduct
        {
            public int ProductId { get; set; }
            public string ProductName { get; set; }
            public string ProductUrl { get; set; }
            public string ProductCode { get; set; }
            public int Brand { get; set; }
            public int UnitId { get; set; }
            public string UnitName { get; set; }
            public int Menu { get; set; }
            public string MenuUrl { get; set; }
            public int Category { get; set; }
            public string CategoryUrl { get; set; }
            public string CategoryName { get; set; }
            public int SubCategory { get; set; }
            public string SubCategoryUrl { get; set; }
            public string SubCategoryName { get; set; }
            public string Offer { get; set; }
            public decimal PurchasePrice { get; set; }
            public decimal SalePrice { get; set; }
            public decimal SalePriceWithDiscount { get; set; }
            public decimal Discount { get; set; }
            public int DiscountPer { get; set; }
            public int SupplierId { get; set; }
            public string SupplierLoginId { get; set; }
            public string SupplierName { get; set; }
            public bool IsFeaturedProducts { get; set; }
            public List<FrontProductImage> FrontProductImage { get; set; }
            public bool Status { get; set; }
            public string RedirectUrl { get; set; }
        }
    }
}