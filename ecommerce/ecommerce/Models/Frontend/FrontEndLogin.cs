﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecommerce.Models.Frontend
{
    public class FrontEndLogin
    {
        public int LoginId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailId { get; set; }
        public string MobileNo { get; set; }        
        public string UserId { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }
        public bool IsApproved { get; set; }
        public bool Status { get; set; }            
        public bool IsWelcome { get; set; }
    }
}