﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecommerce.Models.Backend
{
    public class Dashboard
    {
        public class DashboardModel
        {
            public int LoginId { get; set; }
            public string UserName { get; set; }
            public string UserId { get; set; }
            public string UserType { get; set; }
        }

        public class DashboardOrders
        {
            public int SrNo { get; set; }
            public int OrderId { get; set; }
            public string Image { get; set; }
            public string OrderNo { get; set; }
            public string ProductName { get; set; }
            public string Quantity { get; set; }
            public string UserName { get; set; }
            public string MobileNo { get; set; }
            public string SubTotal { get; set; }
            public string DeliveryCharges { get; set; }
            public string GrandTotal { get; set; }
            public string PaymentMode { get; set; }
            public string OrderStatus { get; set; }
            public string OrderDate { get; set; }   
            public string SupplierName { get; set; }
            public DateTime CreatedDate { get; set; }
            public IncomeCountModel IncomeModel { get; set; }
            public UserCountModel UserCountModel { get; set; }
            public SuplierCountModel SuplierCountModel { get; set; }
            public List<DashboardOrders> DashboardOrdersList { get; set; }
            public List<DashboardOrders> PendingOrderList { get; set; }
            public List<DashboardOrders> DeliverdOrderList { get; set; }
            public List<DashboardOrders> CancelledOrderList { get; set; }            
        }

        public class IncomeCountModel
        {
            public string TotalIncome { get; set; }
            public string TodaYIncome { get; set; }
            public string MonthIncome { get; set; }
            public string YearIncome { get; set; }
        }
        public class UserCountModel
        {
            public string TotalUser { get; set; }
            public string TodayUser { get; set; }
            public string MonthUser { get; set; }
            public string YearUser { get; set; }
        }

        public class SuplierCountModel
        {
            public string TotalSuplier { get; set; }
            public string TodaySuplier { get; set; }
            public string MonthSuplier { get; set; }
            public string YearSuplier { get; set; }
        }

        public class UserLoginDetails
        {
            public bool IsApproved { get; set; }
            public bool Status { get; set; }
            public DateTime CreatedDate { get; set; }
            public bool EmailVerified { get; set; }
            public bool MobileVerified { get; set; }
            public string UserType { get; set; }
        }
    }
}