﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecommerce.Models.Backend
{
    public class BackendLogin
    {
        public int LoginId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailId { get; set; }
        public string MobileNo { get; set; }      
        public string PinCode { get; set; }
        public string Address { get; set; }
        public string ProfileImage { get; set; }
        public string Profession { get; set; }
        public string DOB { get; set; }
        public string AreaName { get; set; }
        public string AddharNo { get; set; }
        public string PanNo { get; set; }
        public string GSTNo { get; set; }
        public string AddharImage { get; set; }
        public string PanImage { get; set; }
        public string BankName { get; set; }
        public string AccountNo { get; set; }
        public string BranchName { get; set; }
        public string BrnachCode { get; set; }
        public string IFSCCode { get; set; }
        public string BankAddress { get; set; }
        public string PayeeName { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string RegPassword { get; set; }
        public string cnfPassword { get; set; }
        public string UserType { get; set; }
        public bool IsApproved { get; set; }
        public bool Status { get; set; }
        public string CreatedDate { get; set; }
        public string ActionType { get; set; }
        public string SupplierName { get; set; }
        public List<BackendLogin> MemberList { get; set; }
        public bool IsShowAll { get; set; }
        public bool IsWelcome { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public List<SelectListItem> CountryList { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public List<SelectListItem> StateList { get; set; }
        public string CityId { get; set; }
        public string CityName { get; set; }
        public List<SelectListItem> CityList { get; set; }
    }    
}