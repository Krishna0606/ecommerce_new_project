﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecommerce.Models.Backend
{
    public class ProductMaster
    {
        public class Product
        {
            public int ProductId { get; set; }
            public string ProductName { get; set; }
            public string ProductCode { get; set; }
            public string Offer { get; set; }
            public decimal PurchasePrice { get; set; }
            public decimal SalePrice { get; set; }
            public decimal Discount { get; set; }
            public int DiscountPer { get; set; }
            public string DeliveryTime { get; set; }
            public string Description { get; set; }
            public bool IsFeaturedProducts { get; set; }
            public int SupplierId { get; set; }
            public string SupplierType { get; set; }
            public string SupplierLoginId { get; set; }
            public string SupplierName { get; set; }
            public bool Status { get; set; }
            public DateTime CreatedDate { get; set; }
            public List<Product> ProductList { get; set; }
            public int MenuId { get; set; }
            public string MenuName { get; set; }
            public int CategoryId { get; set; }
            public string CategoryName { get; set; }
            public int SubCategoryId { get; set; }
            public string SubCategoryName { get; set; }
            public int BrandId { get; set; }
            public string BrandName { get; set; }
            public int UnitId { get; set; }
            public string UnitName { get; set; }
            public List<ProductImage> ProductImage { get; set; }
            public List<SelectListItem> MenuList { get; set; }
            public List<SelectListItem> CategoryList { get; set; }
            public List<SelectListItem> SubCategoryList { get; set; }
            public List<SelectListItem> BrandList { get; set; }
            public List<SelectListItem> UnitList { get; set; }
        }

        public class ProductFilter
        {
            public int SupplierId { get; set; }

            public int ProductId { get; set; }
        }

        public class ProductImage
        {
            public int ImageId { get; set; }
            public string ImageName { get; set; }
            public string ImageUrl { get; set; }
            public int ProductId { get; set; }
            public bool Status { get; set; }
        }

        public class ProductDescription
        {
            public int ProductDescriptionId { get; set; }
            public int ProductId { get; set; }
            public string Description1 { get; set; }
            public string Description2 { get; set; }
            public string Description3 { get; set; }
            public string Description4 { get; set; }
            public string Description5 { get; set; }
            public bool Status { get; set; }
        }
    }
}