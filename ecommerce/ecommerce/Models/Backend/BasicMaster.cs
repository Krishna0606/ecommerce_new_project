﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecommerce.Models.Backend
{
    public class BasicMaster
    {
        public class MenuModel
        {
            public int Menuid { get; set; }
            public string MenuName { get; set; }
            public string Image { get; set; }
            public bool Status { get; set; }
            public bool IsFeatured { get; set; }
            public List<MenuModel> MenuList { get; set; }
        }
        public class CategoryModel
        {
            public int CategoryId { get; set; }
            public string CategoryName { get; set; }
            public bool Status { get; set; }
            public List<CategoryModel> CategoryList { get; set; }
            public int Menu { get; set; }
            public string MenuName { get; set; }
            public List<SelectListItem> MenuList { get; set; }
            public List<SelectListItem> DropDownCategory { get; set; }
        }

        public class SubCategoryModel
        {
            public int SubCategoryId { get; set; }
            public string SubCategoryName { get; set; }
            public int MenuId { get; set; }
            public string MenuName { get; set; }
            public int CategoryId { get; set; }
            public string CategoryName { get; set; }
            public bool Status { get; set; }
            public List<SubCategoryModel> SubCategoryList { get; set; }
            public List<SelectListItem> DropDownSubCategory { get; set; }
        }

        public class StateModel
        {
            public int StateId { get; set; }
            public string StateName { get; set; }
            public string StateCode { get; set; }
            public int CountryId { get; set; }
            public bool Status { get; set; }

            public List<StateModel> StateList { get; set; }
        }

        public class CityModel
        {
            public int CityId { get; set; }
            public string CityName { get; set; }
            public string CityCode { get; set; }
            public int CountryId { get; set; }
            public int StateId { get; set; }
            public bool Status { get; set; }
            public List<CityModel> CityList { get; set; }
        }

        public class BrandModel
        {
            public int BrandId { get; set; }
            public string BrandName { get; set; }
            public bool Status { get; set; }
            public List<BrandModel> BrandList { get; set; }
        }

        public class UnitModel
        {
            public int UnitId { get; set; }
            public string UnitName { get; set; }
            public bool Status { get; set; }
            public List<UnitModel> UnitList { get; set; }
        }
    }
}