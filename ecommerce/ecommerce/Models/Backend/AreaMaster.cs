﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecommerce.Models.Backend
{
    public class AreaMaster
    {
        public class Country
        {
            public int CountryId { get; set; }
            public string CountryName { get; set; }
            public string CountryCode { get; set; }
            public bool Status { get; set; }

            public List<Country> CountryList { get; set; }
        }

        public class State
        {
            public int StateId { get; set; }            
            public string StateName { get; set; }
            public string StateCode { get; set; }
            public bool Status { get; set; }
            public List<State> StateList { get; set; }
            public int CountryId { get; set; }
            public string CountryName { get; set; }
            public List<SelectListItem> BindStateList { get; set; }
            public List<SelectListItem> CountryList { get; set; }
        }

        public class City
        {
            public int CityId { get; set; }
            public string CityName { get; set; }
            public string CityCode { get; set; }
            public bool Status { get; set; }
            public List<City> CityList { get; set; }
            public int CountryId { get; set; }
            public string CountryName { get; set; }
            public List<SelectListItem> CountryList { get; set; }
            public int StateId { get; set; }
            public string StateName { get; set; }
            public List<SelectListItem> StateList { get; set; }
            public List<SelectListItem> BindCityList { get; set; }
        }
    }
}