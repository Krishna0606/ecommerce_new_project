﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecommerce.Models.Backend
{
    public class AboutUsModel
    {
        public int aboutid { get; set; }
        public string Heading { get; set; }
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Content { get; set; }
        public List<AboutUsModel> AboutUslist { get; set; }
    }
    public class PrivicyPolicyModel
    {
        public int privicyid { get; set; }
        public string Heading { get; set; }
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Content { get; set; }
        public List<PrivicyPolicyModel> Privicylist { get; set; }
    }
}