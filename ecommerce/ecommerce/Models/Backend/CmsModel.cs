﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecommerce.Models.Backend
{
    [Table("T_AboutUs")]
    public class T_AboutUs
    {
        [Key]
        public int aboutid { get; set; }
        public string Heading { get; set; }
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Content { get; set; }
    }
    [Table("T_PrivicyPolicy")]
    public class T_PrivicyPolicy
    {
        [Key]
        public int privicyid { get; set; }
        public string Heading { get; set; }
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Content { get; set; }
    }
}