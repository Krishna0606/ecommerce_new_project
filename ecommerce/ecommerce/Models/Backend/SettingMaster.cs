﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecommerce.Models.Backend
{
    public class SettingMaster
    {
        public class HomeBannerModel
        {
            public int BannerId { get; set; }
            public string Heading { get; set; }
            public string Content { get; set; }
            public string ImageName { get; set; }
            public string RedirectUrl { get; set; }
            public bool IsShow { get; set; }
            public string BannerType { get; set; }
            public string BannerOrder { get; set; }
            public bool Status { get; set; }
            public string CreatedDate { get; set; }
            public List<HomeBannerModel> HomeBannerList { get; set; }
        }

        public class DeliveryCharges
        {
            public int DeliveryChargesId { get; set; }
            public decimal Price { get; set; }
            //public string StateId { get; set; }
            public string StateName { get; set; }
            //public string CityId { get; set; }
            public string CityName { get; set; }
            public string PinCode { get; set; }
            public int SupplierId { get; set; }
            public string SupplierLoginId { get; set; }
            public string SupplierName { get; set; }
            public bool Status { get; set; }
            public int StateId { get; set; }
            public int CityId { get; set; }
            public List<SelectListItem> StateList { get; set; }
            public List<SelectListItem> CityList { get; set; }
            public List<DeliveryCharges> DeliveryChargesList { get; set; }
        }
    }
}