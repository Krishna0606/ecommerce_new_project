﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecommerce.Models
{
    public class FrontProductModel
    {
        public class FrontProduct
        {
            public int ProductId { get; set; }
            public string ProductName { get; set; }
            public string ProductNameUrl { get; set; }
            public string ProductCode { get; set; }
            public int Brand { get; set; }
            public int Menu { get; set; }
            public string MenuName { get; set; }
            public string MenuUrl { get; set; }
            public int Category { get; set; }
            public string CategoryName { get; set; }
            public string CategoryNameUrl { get; set; }
            public int SubCategory { get; set; }
            public string SubCategoryName { get; set; }
            public string SubCategoryNameUrl { get; set; }
            public string Offer { get; set; }
            public decimal PurchasePrice { get; set; }
            public decimal SalePrice { get; set; }
            public decimal SalePriceWithDiscount { get; set; }
            public decimal Discount { get; set; }
            public int DiscountPer { get; set; }
            public int Unit { get; set; }
            public string UnitName { get; set; }
            public string DeliveryTime { get; set; }
            public string Description { get; set; }
            public bool IsFeaturedProducts { get; set; }
            public int SupplierId { get; set; }
            public string SupplierLoginId { get; set; }
            public string SupplierName { get; set; }
            public string SupplierAreaName { get; set; }
            public bool Status { get; set; }
            public List<FrontProduct> FrontProductList { get; set; }
            public FrontProductDescription FrontProductDescription { get; set; }
            public List<FrontProductImage> FrontProductImage { get; set; }
            public int UnitId { get; set; }
            public List<SelectListItem> UnitList { get; set; }
            //public string MenuName { get; set; }
            //public string CatName { get; set; }
            //public string SubCatName { get; set; }
            public string SupplierStateId { get; set; }
            public string SupplierPinCode { get; set; }
            public string RedirectUrl { get; set; }
            public LeftSideFilter LeftSideFilter { get; set; }
        }

        public class LeftSideFilter
        {
            public string SelectedStateId { get; set; }
            public string MinPrice { get; set; }
            public string MaxPrice { get; set; }
            public string[] StateId { get; set; }
            public string[] BrandId { get; set; }
            public string PinCode { get; set; }
            public string[] Area { get; set; }
        }

        public class FrontProductDescription
        {
            public string Descripion1 { get; set; }
            public string Descripion2 { get; set; }
            public string Descripion3 { get; set; }
            public string Descripion4 { get; set; }
            public string Descripion5 { get; set; }
        }

        public class FrontProductImage
        {
            public int ImageId { get; set; }
            public string ImageName { get; set; }
            public string ImageUrl { get; set; }
        }

        public class AddToCart
        {
            public int CartId { get; set; }
            public int ProductId { get; set; }
            public string ProductName { get; set; }
            public string ProductCode { get; set; }
            public string ProductImgUrl { get; set; }
            public string SalePrice { get; set; }
            public int Quantity { get; set; }
            public int Discount { get; set; }
            public string DiscountPrice { get; set; }
            public int UnitId { get; set; }
            public string UnitName { get; set; }
            public int SupplierId { get; set; }
            public string SupplierEmail { get; set; }
            public string SupplierName { get; set; }
            public int UserId { get; set; }
            public string UserLoginId { get; set; }
            public string UserName { get; set; }
            public string CartType { get; set; }
            public bool Status { get; set; }
        }

        public class UnitModel
        {
            public int Unitid { get; set; }
            public string UnitName { get; set; }
            public bool Status { get; set; }
        }
    }
}