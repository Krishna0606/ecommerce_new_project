﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ecommerce.Models.Backend.AreaMaster;
using static ecommerce.Models.Backend.BasicMaster;

namespace ecommerce.Models.Common
{
    public static class Common
    {
        public static List<SelectListItem> PopulateCountry(List<Country> countryList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in countryList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.CountryName.ToString(),
                    Value = item.CountryId.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateMenu(List<MenuModel> menuList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in menuList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.MenuName.ToString(),
                    Value = item.Menuid.ToString()
                });
            }

            return items;
        }

        public static List<SelectListItem> PopulateCategory(List<CategoryModel> catList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in catList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.CategoryName.ToString(),
                    Value = item.CategoryId.ToString()
                });
            }

            return items;
        }

        public static List<SelectListItem> PopulateSubCategory(List<SubCategoryModel> subCatList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in subCatList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.SubCategoryName.ToString(),
                    Value = item.SubCategoryId.ToString()
                });
            }

            return items;
        }

        public static List<SelectListItem> PopulateBrand(List<BrandModel> brandList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in brandList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.BrandName.ToString(),
                    Value = item.BrandId.ToString()
                });
            }

            return items;
        }

        public static List<SelectListItem> PopulateUnit(List<UnitModel> unitList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in unitList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.UnitName.ToString(),
                    Value = item.UnitId.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateState(List<State> stateList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in stateList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.StateName.ToString(),
                    Value = item.StateId.ToString()
                });
            }

            return items;
        }

        public static List<SelectListItem> PopulateCity(List<City> cityList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in cityList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.CityName.ToString(),
                    Value = item.CityId.ToString()
                });
            }

            return items;
        }
    }
}