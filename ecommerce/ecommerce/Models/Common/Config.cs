﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ecommerce.Models.Common
{
    public static class Config
    {
        public static string WebsiteName
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websitename"]); }
        }

        public static string WebsiteUrl
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websiteurl"]); }
        }

        public static string AdminEmail
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["adminemail"]); }
        }

        public static string AdminEmailService
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["adminemailservice"]); }
        }
        public static string AdminEmailServicePassword
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["adminemailservicepass"]); }
        }

        public static string CompanyLogo
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["companylogo"]); }
        }

        public static string SendEmailcompanylogo
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["sendemailcompanylogo"]); }
        }

        public static string ImageNotFound
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["imagenotfound"]); }
        }

        public static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["Ecommerce"].ConnectionString; }
        }
    }
}