﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace ecommerce.Models.Common
{
    public static class Utility
    {
        public static string GetFullPathStringWithAppData(string path, string folderName)
        {
            string fullPath = "/App_Data/";

            if (!string.IsNullOrEmpty(path))
            {
                fullPath = fullPath + path + "/" + folderName + "/";
            }

            return fullPath.Replace("//", "/");
        }
        public static string GetImagePath(string imageName, string folderName, int id)
        {
            return !string.IsNullOrEmpty(imageName) ? "/GetFile.ashx?path=" + folderName + "/" + id + "/" + imageName : string.Empty;
        }
        public static string GetImagePath(string imageName, string folderName, string innerfoldername)
        {
            return !string.IsNullOrEmpty(imageName) ? "/GetFile.ashx?path=" + folderName + "/" + innerfoldername + "/" + imageName : string.Empty;
        }
        public static string GenrateRandomTransactionId(string prefixString, int SizeLimit, string charCombo = null)
        {
            try
            {
                char[] chars = !string.IsNullOrEmpty(charCombo) ? charCombo.ToCharArray() : "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
                byte[] data = new byte[SizeLimit];
                using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
                {
                    crypto.GetBytes(data);
                }

                StringBuilder result = new StringBuilder(SizeLimit);

                if (!string.IsNullOrWhiteSpace(prefixString))
                {
                    result.Append(prefixString);
                }

                foreach (byte b in data)
                {
                    result.Append(chars[b % (chars.Length)]);
                }

                return result.ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return string.Empty;
        }
        public static string GetDescription(string description, int? length = null)
        {
            string publicDescription = string.Empty;

            if (!string.IsNullOrEmpty(description))
            {
                publicDescription = description;

                publicDescription = publicDescription.Replace("\r\n", "<br/>").Replace("\n", "<br/>").Replace("amp;", "  ");

                if (length != null && publicDescription.Length > length)
                {
                    publicDescription = publicDescription.Remove((int)length);

                    if (publicDescription.LastIndexOf(" ") > 0)
                    {
                        publicDescription = publicDescription.Remove(publicDescription.LastIndexOf(" "));
                    }
                }
            }

            return publicDescription;
        }
        public static string ReadHTMLTemplate(string fileName)
        {
            using (StreamReader streamReader = new StreamReader(HttpContext.Current.Server.MapPath("~") + "\\" + "/EmailTemplates" + "/" + fileName))
            {
                try
                {
                    string xmlString = streamReader.ReadToEnd();
                    return xmlString;
                }
                finally
                {
                    streamReader.Close();
                    streamReader.Dispose();
                }
            }
        }
    }
}