﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecommerce.Models
{
    public class FrontMenuModel
    {
        public class FrontMainCategoryModel
        {
            public string MenuName { get; set; }
            public string Image { get; set; }
            public int MenuId { get; set; }
            public List<FrontMainCategoryModel> FrontMainCategoryList { get; set; }
            public List<FrontCategoryModel> FrontCategoryList { get; set; }
        }

        public class FrontCategoryModel
        {
            public string CategoryName { get; set; }
            public int CategoryId { get; set; }
            public List<FrontSubCategoryModel> FrontSubCategoryList { get; set; }
        }

        public class FrontSubCategoryModel
        {
            public string SubCategoryName { get; set; }
            public int SubCategoryId { get; set; }
        }
    }
}