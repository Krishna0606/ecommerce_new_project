﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ecommerce
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //=======================Back End Section===================================

            routes.MapRoute(name: "BackendLogin", url: "backend/login", defaults: new { controller = "Account", action = "BackendLogin" });
            routes.MapRoute(name: "BackDashboard", url: "dashboard", defaults: new { controller = "Dashboard", action = "Index" });

            routes.MapRoute(name: "CountryMasterList", url: "area-master/country-list", defaults: new { controller = "AreaMaster", action = "CountryMasterList" });
            routes.MapRoute(name: "AddEditCountryMaster", url: "area-master/add-edit-country", defaults: new { controller = "AreaMaster", action = "AddEditCountryMaster" });

            routes.MapRoute(name: "StateMasterList", url: "area-master/state-list", defaults: new { controller = "AreaMaster", action = "StateMasterList" });
            routes.MapRoute(name: "AddEditStateMaster", url: "area-master/add-edit-state", defaults: new { controller = "AreaMaster", action = "AddEditStateMaster" });

            routes.MapRoute(name: "CityMasterList", url: "area-master/city-list", defaults: new { controller = "AreaMaster", action = "CityMasterList" });
            routes.MapRoute(name: "AddEditCityMaster", url: "area-master/add-edit-city", defaults: new { controller = "AreaMaster", action = "AddEditCityMaster" });

            routes.MapRoute(name: "SupplierList", url: "member-master/member-list", defaults: new { controller = "MemberMaster", action = "SupplierList" });
            routes.MapRoute(name: "SupplierRegistration", url: "member-master/add-new-member", defaults: new { controller = "MemberMaster", action = "SupplierRegistration" });

            routes.MapRoute(name: "BasicMasterList", url: "basic-master/menu-list", defaults: new { controller = "BasicMaster", action = "MenuList" });
            routes.MapRoute(name: "AddEditMenuMaster", url: "basic-master/add-edit-menu", defaults: new { controller = "BasicMaster", action = "AddEditMenu" });

            routes.MapRoute(name: "aboutuslist", url: "cms-master/aboutus-details", defaults: new { controller = "CMS", action = "AboutusDetails" });
            routes.MapRoute(name: "AddEditaboutMaster", url: "cms-master/add-edit-aboutus", defaults: new { controller = "CMS", action = "AddEditAboutus" });

            routes.MapRoute(name: "privicylist", url: "cms-master/privacy-policy-details", defaults: new { controller = "CMS", action = "PrivicyDetails" });
            routes.MapRoute(name: "AddEditprivicyMaster", url: "cms-master/add-edit-privacy-policy", defaults: new { controller = "CMS", action = "AddEditPrivicy" });

            routes.MapRoute(name: "BasicMastercategoryList", url: "basic-master/category-list", defaults: new { controller = "BasicMaster", action = "CategoryList" });
            routes.MapRoute(name: "AddEditcategoryMaster", url: "basic-master/add-edit-category", defaults: new { controller = "BasicMaster", action = "AddEditCategory" });

            routes.MapRoute(name: "SubCategoryList", url: "basic-master/sub-category-list", defaults: new { controller = "BasicMaster", action = "SubCategoryList" });
            routes.MapRoute(name: "AddEditSubCategory", url: "basic-master/add-edit-sub-category", defaults: new { controller = "BasicMaster", action = "AddEditSubCategory" });

            routes.MapRoute(name: "BasicMasterBrandList", url: "basic-master/brand-list", defaults: new { controller = "BasicMaster", action = "BrandList" });
            routes.MapRoute(name: "AddEditBrandMaster", url: "basic-master/add-edit-brand", defaults: new { controller = "BasicMaster", action = "AddEditBrand" });

            routes.MapRoute(name: "BasicMasterUnitList", url: "basic-master/Unit-list", defaults: new { controller = "BasicMaster", action = "UnitList" });
            routes.MapRoute(name: "AddEditUnitMaster", url: "basic-master/add-edit-Unit", defaults: new { controller = "BasicMaster", action = "AddEditUnit" });

            routes.MapRoute(name: "ProductList", url: "product-master/product-list", defaults: new { controller = "ProductMaster", action = "ProductList" });
            routes.MapRoute(name: "AddEditProduct", url: "product-master/add-edit-product", defaults: new { controller = "ProductMaster", action = "AddEditProduct" });

            routes.MapRoute(name: "HomeBannerMaster", url: "setting-master/home-banner", defaults: new { controller = "SettingMaster", action = "HomeBannerMaster" });
            routes.MapRoute(name: "AddEditHomeBanner", url: "setting-master/add-edit-home-banner", defaults: new { controller = "SettingMaster", action = "AddEditHomeBanner" });

            routes.MapRoute(name: "DeliveryChargesList", url: "setting-master/delivery-charges-list", defaults: new { controller = "SettingMaster", action = "DeliveryChargesList" });
            routes.MapRoute(name: "AddEditDeliveryCharges", url: "setting-master/add-edit-delivery-charges", defaults: new { controller = "SettingMaster", action = "AddEditDeliveryCharges" });

            routes.MapRoute(name: "AllOrderList", url: "product-order-history/all-order", defaults: new { controller = "Dashboard", action = "AllOrderList" });
            routes.MapRoute(name: "AllPendingOrderList", url: "product-order-history/pending-order", defaults: new { controller = "Dashboard", action = "AllPendingOrderList" });
            routes.MapRoute(name: "AllDeviveredOrderList", url: "product-order-history/delivered-order", defaults: new { controller = "Dashboard", action = "AllDeviveredOrderList" });
            routes.MapRoute(name: "AllCancelledOrderList", url: "product-order-history/cancelled-order", defaults: new { controller = "Dashboard", action = "AllCancelledOrderList" });

            routes.MapRoute(name: "ReviewList", url: "review-master/review-list", defaults: new { controller = "ReviewMaster", action = "ReviewList" });
            //=======================Front End Section==================================
            routes.MapRoute(name: "ManageAddress", url: "account/addresses", defaults: new { controller = "UserProfile", action = "ManageAddress" });

            routes.MapRoute(name: "FrontAllMenuProduct", url: "pm/all-products", defaults: new { controller = "Default", action = "NewProduct" });
            routes.MapRoute(name: "FrontProductMenu", url: "pm/{menuname}/{menuid}", defaults: new { controller = "Default", action = "NewProduct" });
            routes.MapRoute(name: "FrontProductCat", url: "pc/{menuname}/{menuid}/{catname}/{catid}", defaults: new { controller = "Default", action = "NewProduct" });
            routes.MapRoute(name: "FrontProductSubCat", url: "psc/{menuname}/{menuid}/{catname}/{catid}/{subcatname}/{subcatid}", defaults: new { controller = "Default", action = "NewProduct" });

            routes.MapRoute(name: "FrontProductDetailPageMenu", url: "pd/{prodid}/{prodname}/{menuname}/{menuid}", defaults: new { controller = "Default", action = "NewProductdetails" });
            routes.MapRoute(name: "FrontProductDetailPageCat", url: "pd/{prodid}/{prodname}/{menuname}/{menuid}/{catname}/{catid}", defaults: new { controller = "Default", action = "NewProductdetails" });
            routes.MapRoute(name: "FrontProductDetailPageSubCat", url: "pd/{prodid}/{prodname}/{menuname}/{menuid}/{catname}/{catid}/{subcatname}/{subcatid}", defaults: new { controller = "Default", action = "NewProductdetails" });

            routes.MapRoute(name: "PlaceOrder", url: "place-order/checkout", defaults: new { controller = "Default", action = "PlaceOrder" });

            //routes.MapRoute(name: "FrontProductMenu", url: "pm/{menuname}/{menuid}", defaults: new { controller = "Default", action = "Product" });
            //routes.MapRoute(name: "FrontProductCat", url: "pc/{menuname}/{menuid}/{catname}/{catid}", defaults: new { controller = "Default", action = "Product" });
            //routes.MapRoute(name: "FrontProductSubCat", url: "pc/{menuname}/{menuid}/{catname}/{catid}/{subcatname}/{subcatid}", defaults: new { controller = "Default", action = "Product" });
            //checkout/init?loginFlow=false
            //routes.MapRoute(name: "FrontProductDetailPage", url: "pd/{prodid}/{prodname}/{menuname}/{menuid}/{catname}/{catid}/{subcatname}/{subcatid}", defaults: new { controller = "Default", action = "Productdetails" });

            routes.MapRoute(name: "Default", url: "{controller}/{action}/{id}", defaults: new { controller = "Default", action = "Home", id = UrlParameter.Optional });
        }
    }
}
