﻿using ecommerce.Service.BackEndService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ecommerce.Models.Backend.BasicMaster;
using static ecommerce.Models.FrontMenuModel;

namespace ecommerce.Helper
{
    public static class FrontMenuHelper
    {
        public static List<FrontMainCategoryModel> GetFrontMenu()
        {
            List<FrontMainCategoryModel> frontMenuList = new List<FrontMainCategoryModel>();

            try
            {
                List<MenuModel> menuList = BasicMasterService.GetmenuDetail(null, true);

                if (menuList != null && menuList.Count > 0)
                {
                    foreach (var mainmenu in menuList)
                    {
                        FrontMainCategoryModel result = new FrontMainCategoryModel();

                        result.MenuId = mainmenu.Menuid;
                        result.MenuName = mainmenu.MenuName;
                        result.Image = mainmenu.Image;

                        List<FrontCategoryModel> bindCategoryList = new List<FrontCategoryModel>();

                        List<CategoryModel> categoryList = BasicMasterService.GetCategoryDetail(null, true, mainmenu.Menuid.ToString());
                        if (categoryList != null && categoryList.Count > 0)
                        {
                            foreach (var itemcat in categoryList)
                            {
                                FrontCategoryModel cat = new FrontCategoryModel();
                                cat.CategoryId = itemcat.CategoryId;
                                cat.CategoryName = itemcat.CategoryName;

                                List<FrontSubCategoryModel> bindSubCategoryList = new List<FrontSubCategoryModel>();
                                List<SubCategoryModel> subCatList = BasicMasterService.GetSubCategoryDetail(null, mainmenu.Menuid.ToString(), itemcat.CategoryId.ToString(), true);
                                if (subCatList != null && subCatList.Count > 0)
                                {
                                    foreach (var itemsubcat in subCatList)
                                    {
                                        FrontSubCategoryModel subcat = new FrontSubCategoryModel();
                                        subcat.SubCategoryId = itemsubcat.SubCategoryId;
                                        subcat.SubCategoryName = itemsubcat.SubCategoryName;
                                        bindSubCategoryList.Add(subcat);
                                    }
                                }

                                cat.FrontSubCategoryList = bindSubCategoryList;
                                bindCategoryList.Add(cat);
                            }
                        }

                        result.FrontCategoryList = bindCategoryList;

                        frontMenuList.Add(result);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }


            return frontMenuList;
        }
    }
}