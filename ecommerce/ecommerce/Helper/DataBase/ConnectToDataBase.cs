﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ecommerce.Models.Common;

namespace ecommerce.Helper.DataBase
{
    public static class ConnectToDataBase
    {
        private static SqlConnection Connection = new SqlConnection(Config.ConnectionString);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection()
        {
            if (Connection.State == ConnectionState.Closed)
            {
                Connection.Open();
            }
        }
        public static void CloseConnection()
        {
            if (Connection.State == ConnectionState.Open)
            {
                Connection.Close();
            }
        }
        public static DataTable GetRecordFromTable(string fileds, string tablename, string whereCondition)
        {
            try
            {
                Command = new SqlCommand("usp_GetRecordFromTable", Connection);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("fileds", fileds);
                Command.Parameters.AddWithValue("tablename", tablename);
                Command.Parameters.AddWithValue("where", whereCondition);

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "CommonTable");
                ObjDataTable = ObjDataSet.Tables["CommonTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ObjDataTable;
        }
        public static bool InsertRecordToTable(string fileds, string fieldValue, string tableName)
        {
            try
            {
                Command = new SqlCommand("usp_InsertRecordToTable", Connection);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("fileds", fileds);
                Command.Parameters.AddWithValue("values", fieldValue);
                Command.Parameters.AddWithValue("tablename", tableName);

                OpenConnection();
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static int InsertRecordToTableReturnID(string fileds, string fieldValue, string tableName)
        {
            try
            {
                Command = new SqlCommand("usp_InsertRecordToTableReturnID", Connection);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.Add(new SqlParameter("@fileds", fileds));
                Command.Parameters.Add(new SqlParameter("@values", fieldValue));
                Command.Parameters.Add(new SqlParameter("@tablename", tableName));
                Command.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                OpenConnection();
                int isSuccess = Command.ExecuteNonQuery();
                string id = Command.Parameters["@Id"].Value.ToString();
                CloseConnection();

                if (isSuccess > 0)
                {
                    return Convert.ToInt32(id);
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }
        public static bool UpdateRecordIntoAnyTable(string tableName, string fieldsWithValue, string whereCondition)
        {
            try
            {
                Command = new SqlCommand("ups_UpdateRecordToTable", Connection);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("TableName", tableName);
                Command.Parameters.AddWithValue("FieldsWithValue", fieldsWithValue);
                Command.Parameters.AddWithValue("WhereCondition", whereCondition);

                OpenConnection();
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool DeleteRecordFromAnyTable(string tableName, string whereCondition)
        {
            try
            {
                Command = new SqlCommand("usp_DeleteRecordFromTable", Connection);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("tablename", tableName);
                Command.Parameters.AddWithValue("where", whereCondition);

                OpenConnection();
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
    }
}