﻿using ecommerce.Helper.DataBase;
using ecommerce.Models.Common;
using EmailSmsLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace ecommerce.Helper.EmailHelper
{
    public static class EmailHelp
    {
        public static bool SendEmail(string emailFrom, string emailTo, string subject, string body, string emailType, string cc = null, string bcc = null)
        {
            bool issent = false;

            try
            {
                if (EmailService.SendEmail(Config.AdminEmailService, Config.AdminEmailServicePassword, emailFrom, emailTo, subject, body, emailType, cc, bcc))
                {
                    if (InsertEMailing(emailType, emailFrom, emailTo, cc, string.Empty, subject, body))
                    {
                        issent = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return issent;
        }
        public static bool SendVarificationCode(string firstname, string lastname, string emailTo, string mobileno, string emailOTP, string MoileOTP)
        {
            try
            {
                StringBuilder message = new StringBuilder();
                message.Append(Utility.ReadHTMLTemplate("EmailOTPVarification.html"));
                message.Replace("#CompanyLogo#", (Config.WebsiteUrl + Config.SendEmailcompanylogo));
                message.Replace("#WebsiteUrl#", Config.WebsiteUrl);
                message.Replace("#FullName#", (firstname + " " + lastname));
                message.Replace("#VarificationCode#", emailOTP);

                string subject = "Your vocal go local registration varification OTP code : " + Config.WebsiteName;

                if (EmailHelp.SendEmail(Config.AdminEmailService, emailTo, subject, message.ToString(), "User Registration Varification OTP Code"))
                {
                    if (InsertEMailing("Varification OTP Code", Config.AdminEmailService, emailTo, string.Empty, string.Empty, subject, message.ToString()))
                    {
                        string mobileMsg = MoileOTP + " is your OTP in Vocal Go Local to authenticate yourself to continue login. OTP valid for 5 minutes.";

                        string result = SmsService.SendMobileSms(mobileno, mobileMsg);

                        if (!string.IsNullOrEmpty(result))
                        {
                            if (InsertMobileSmsRecordDetail("8511177312", mobileno, result, mobileMsg))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool InsertEMailing(string emailType, string emailFrom, string emailTo, string mailCC, string mailBCC, string subject, string body)
        {
            try
            {
                string fileds = "EMailType,MailFrom,MailTo,MailCC,MailBCC,SmtpClient,Subject,UserID,Pass,Body,Status,Regards";
                string fieldValue = "'" + emailType + "','" + emailFrom + "','" + emailTo + "','" + mailCC + "','" + mailBCC + "','smtp.gmail.com','" + subject + "','" + Config.AdminEmailService + "','norevert@rwt19','" + body + "','Sent','VocalGoLocal'";
                string tableName = "T_EMailing";

                if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                string fileds = "EMailType,MailFrom,MailTo,MailCC,MailBCC,SmtpClient,Subject,UserID,Pass,Body,Status,Regards";
                string fieldValue = "'" + emailType + "','" + emailFrom + "','" + emailTo + "','" + mailCC + "','" + mailBCC + "','smtp.gmail.com','" + subject + "','" + Config.AdminEmailService + "','norevert@rwt19','" + body + "','Failed','VocalGoLocal'";
                string tableName = "T_EMailing";

                if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                {
                    return true;
                }

                ex.ToString();
            }

            return false;
        }
        public static bool SendMobileSms(string toMobileNo, string message)
        {
            try
            {
                string result = SmsService.SendMobileSms(toMobileNo, message);

                if (!string.IsNullOrEmpty(result))
                {
                    if (InsertMobileSmsRecordDetail("8511177312", toMobileNo, result, message))
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {


                ex.ToString();
            }

            return false;
        }
        public static bool InsertMobileSmsRecordDetail(string fromMobile, string toMobileNo, string successMsg, string msgBody)
        {
            try
            {
                string[] msg = successMsg.Split(':');

                if (msg.Count() == 2)
                {
                    string fileds = "MessageId,FromMobile,ToMobile,MsgBody,Status";
                    string fieldValue = "'" + msg[1] + "','" + fromMobile + "','" + toMobileNo + "','" + msgBody + "','Sent'";
                    string tableName = "T_MobileSmsRecord";

                    if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                string fileds = "MessageId,FromMobile,ToMobile,MsgBody,Status";
                string fieldValue = "'','" + fromMobile + "','" + toMobileNo + "','" + successMsg + "','Failed'";
                string tableName = "T_MobileSmsRecord";

                if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                {
                    return true;
                }

                throw;
            }

            return false;
        }
    }
}