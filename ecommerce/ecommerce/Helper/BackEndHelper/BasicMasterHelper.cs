﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static ecommerce.Models.Backend.BasicMaster;
using ecommerce.Helper.DataBase;
using ecommerce.Models.Common;

namespace ecommerce.Helper.BackEndHelper
{
    public static class BasicMasterHelper
    {
        public static List<MenuModel> GetmenuDetail(string menuid = null, bool status = true)
        {
            List<MenuModel> menuList = new List<MenuModel>();

            try
            {
                string fileds = "*";
                string tablename = "T_Menu";
                string whereCondition = "Status=" + (status == true ? 1 : 0);
                if (!string.IsNullOrEmpty(menuid))
                {
                    whereCondition += " and Menuid=" + menuid;
                }

                DataTable dtMenu = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtMenu != null && dtMenu.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMenu.Rows.Count; i++)
                    {
                        MenuModel result = new MenuModel();
                        result.Menuid = Convert.ToInt32(dtMenu.Rows[i]["Menuid"].ToString());
                        result.MenuName = dtMenu.Rows[i]["MenuName"].ToString();
                        result.Image = !string.IsNullOrEmpty(dtMenu.Rows[i]["Image"].ToString()) ? Utility.GetImagePath(dtMenu.Rows[i]["Image"].ToString(), "MenuImages", ("Menu_" + result.Menuid)) : string.Empty;
                        result.Status = dtMenu.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        result.IsFeatured = dtMenu.Rows[i]["IsFeatured"].ToString().ToLower() == "true" ? true : false;
                        menuList.Add(result);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }


            return menuList.OrderBy(p => p.MenuName).ToList();
        }
        public static int InsertMenuDetail(string menuname, string isfeatured)
        {
            int menuId = 0;
            try
            {
                if (!string.IsNullOrEmpty(menuname))
                {
                    string fileds = "MenuName,IsFeatured";
                    string fieldValue = "'" + menuname + "'," + (isfeatured == "true" ? 1 : 0) + "";
                    string tableName = "T_Menu";

                    menuId = ConnectToDataBase.InsertRecordToTableReturnID(fileds, fieldValue, tableName);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return menuId;
        }

        public static bool UpdateMenuImage(string menuid, string imageurlname)
        {
            try
            {
                string fieldsWithValue = "Image='" + imageurlname + "'";
                string whereCondition = "Menuid=" + menuid;
                string tableName = "T_Menu";

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool UpdateMenuDetail(string menuid, string menuname = null, string isfeatured = null)
        {
            try
            {
                if (!string.IsNullOrEmpty(menuname))
                {
                    string fieldsWithValue = "MenuName='" + menuname + "',IsFeatured=" + (isfeatured == "true" ? 1 : 0) + "";
                    string whereCondition = "Menuid=" + menuid;
                    string tableName = "T_Menu";

                    if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                    {
                        return true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(menuid))
                    {
                        string fieldsWithValue = "Status=0";
                        string whereCondition = "Menuid=" + menuid;
                        string tableName = "T_Menu";

                        if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        public static List<CategoryModel> GetCategoryDetail(string categoryid = null, bool status = true, string menu = null)
        {
            List<CategoryModel> CategoryList = new List<CategoryModel>();

            try
            {
                string fileds = "s.CategoryId,s.CategoryName,s.Menu,s.Status,c.MenuName";
                string tablename = "T_Category s inner join T_Menu c on c.Menuid=s.Menu";
                string whereCondition = "s.Status=" + (status == true ? 1 : 0);
                if (!string.IsNullOrEmpty(categoryid))
                {
                    whereCondition += " and s.CategoryId=" + categoryid;
                }

                if (!string.IsNullOrEmpty(menu))
                {
                    whereCondition += " and s.Menu=" + menu;
                }

                DataTable dtCategory = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtCategory != null && dtCategory.Rows.Count > 0)
                {
                    for (int i = 0; i < dtCategory.Rows.Count; i++)
                    {
                        CategoryModel result = new CategoryModel();
                        result.CategoryId = Convert.ToInt32(dtCategory.Rows[i]["CategoryId"].ToString());
                        result.CategoryName = dtCategory.Rows[i]["CategoryName"].ToString();
                        result.Menu = Convert.ToInt32(dtCategory.Rows[i]["Menu"].ToString());
                        result.MenuName = dtCategory.Rows[i]["MenuName"].ToString();
                        result.Status = dtCategory.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        CategoryList.Add(result);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }


            return CategoryList.OrderBy(p => p.CategoryName).ToList();
        }
        public static bool InsertCategoryDetail(string categoryname, string menu)
        {
            try
            {
                if (!string.IsNullOrEmpty(categoryname) && !string.IsNullOrEmpty(menu))
                {
                    string fileds = "CategoryName,Menu";
                    string fieldValue = "'" + categoryname + "'," + Convert.ToInt32(menu) + "";
                    string tableName = "T_Category";

                    if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        public static bool UpdateCategoryDetail(string categoryid, string categoryname, string menu)
        {
            try
            {
                if (!string.IsNullOrEmpty(categoryname) && !string.IsNullOrEmpty(menu))
                {
                    string fieldsWithValue = "CategoryName='" + categoryname + "',Menu=" + menu + "";
                    string whereCondition = "CategoryId=" + categoryid;
                    string tableName = "T_Category";

                    if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                    {
                        return true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(categoryid))
                    {
                        string fieldsWithValue = "Status=0";
                        string whereCondition = "CategoryId=" + categoryid;
                        string tableName = "T_Category";

                        if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        public static List<SubCategoryModel> GetSubCategoryDetail(string subcatid = null, string menuid = null, string catid = null, bool status = true)
        {
            List<SubCategoryModel> subCategoryList = new List<SubCategoryModel>();

            try
            {
                string fileds = "m.MenuName, c.CategoryName, sc.SubCategoryId,sc.SubCategoryName,sc.Menu,sc.Category, sc.Status";
                string tablename = "T_Menu m INNER JOIN T_SubCategory sc ON m.Menuid = sc.Menu INNER JOIN T_Category  c ON m.Menuid = c.Menu AND sc.Category = c.CategoryId";
                string whereCondition = "m.Status=" + (status == true ? 1 : 0);
                if (!string.IsNullOrEmpty(subcatid))
                {
                    whereCondition += " and sc.SubCategoryId=" + subcatid;
                }

                if (!string.IsNullOrEmpty(catid))
                {
                    whereCondition += " and sc.Category=" + catid;
                }

                if (!string.IsNullOrEmpty(menuid))
                {
                    whereCondition += " and sc.Menu=" + menuid;
                }

                DataTable dtSubCategory = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtSubCategory != null && dtSubCategory.Rows.Count > 0)
                {
                    for (int i = 0; i < dtSubCategory.Rows.Count; i++)
                    {
                        SubCategoryModel result = new SubCategoryModel();
                        result.SubCategoryId = Convert.ToInt32(dtSubCategory.Rows[i]["SubCategoryId"].ToString());
                        result.SubCategoryName = dtSubCategory.Rows[i]["SubCategoryName"].ToString();
                        result.MenuId = Convert.ToInt32(dtSubCategory.Rows[i]["Menu"].ToString());
                        result.MenuName = dtSubCategory.Rows[i]["MenuName"].ToString();
                        result.CategoryId = Convert.ToInt32(dtSubCategory.Rows[i]["Category"].ToString());
                        result.CategoryName = dtSubCategory.Rows[i]["CategoryName"].ToString();
                        result.Status = dtSubCategory.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        subCategoryList.Add(result);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }


            return subCategoryList.OrderBy(p => p.CategoryName).ToList();
        }
        public static bool InsertSubCategoryDetail(string menuid, string catid, string subcatname)
        {
            try
            {
                if (!string.IsNullOrEmpty(menuid) && !string.IsNullOrEmpty(catid) && !string.IsNullOrEmpty(subcatname))
                {
                    string fileds = "SubCategoryName,Menu,Category";
                    string fieldValue = "'" + subcatname + "'," + menuid + "," + catid + "";
                    string tableName = "T_SubCategory";

                    if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        public static bool UpdateSubCategoryDetail(string subcatid, string menuid, string catid, string subcatname)
        {
            try
            {
                if (!string.IsNullOrEmpty(menuid) && !string.IsNullOrEmpty(catid) && !string.IsNullOrEmpty(subcatname))
                {
                    string fieldsWithValue = "SubCategoryName='" + subcatname + "',Menu=" + menuid + ",Category=" + catid + "";
                    string whereCondition = "SubCategoryId=" + subcatid;
                    string tableName = "T_SubCategory";

                    if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                    {
                        return true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(subcatid))
                    {
                        string fieldsWithValue = "Status=0";
                        string whereCondition = "SubCategoryId=" + subcatid;
                        string tableName = "T_SubCategory";

                        if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        public static bool InsertBrandDetail(string brandname)
        {
            try
            {
                if (!string.IsNullOrEmpty(brandname))
                {
                    string fileds = "BrandName";
                    string fieldValue = "'" + brandname + "'";
                    string tableName = "T_Brand";

                    if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        public static bool UpdateBrandDetail(string brandname = null, string brandid = null)
        {
            try
            {
                if (!string.IsNullOrEmpty(brandname))
                {
                    string fieldsWithValue = "BrandName='" + brandname + "'";
                    string whereCondition = "BrandId=" + brandid;
                    string tableName = "T_Brand";

                    if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                    {
                        return true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(brandid))
                    {
                        string fieldsWithValue = "Status=0";
                        string whereCondition = "BrandId=" + brandid;
                        string tableName = "T_Brand";

                        if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        public static bool InsertUnitDetail(string unitname)
        {
            try
            {
                if (!string.IsNullOrEmpty(unitname))
                {
                    string fileds = "Unit";
                    string fieldValue = "'" + unitname + "'";
                    string tableName = "T_Unit";

                    if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }

        public static bool UpdateUnitDetail(string unitname = null, string unitid = null)
        {
            try
            {
                if (!string.IsNullOrEmpty(unitname))
                {
                    string fieldsWithValue = "Unit='" + unitname + "'";
                    string whereCondition = "Unitid=" + unitid;
                    string tableName = "T_Unit";

                    if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                    {
                        return true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(unitid))
                    {
                        string fieldsWithValue = "Status=0";
                        string whereCondition = "Unitid=" + unitid;
                        string tableName = "T_Unit";

                        if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        public static List<BrandModel> GetBrandDetail(string brandId = null, bool status = true)
        {
            List<BrandModel> result = new List<BrandModel>();

            try
            {
                string fileds = "*";
                string tablename = "T_Brand";
                string whereCondition = "Status=" + (status == true ? 1 : 0);
                if (!string.IsNullOrEmpty(brandId))
                {
                    whereCondition += " and BrandId=" + brandId;
                }

                DataTable dtBrand = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtBrand != null && dtBrand.Rows.Count > 0)
                {
                    for (int i = 0; i < dtBrand.Rows.Count; i++)
                    {
                        BrandModel brand = new BrandModel();
                        brand.BrandId = Convert.ToInt32(dtBrand.Rows[i]["BrandId"].ToString());
                        brand.BrandName = dtBrand.Rows[i]["BrandName"].ToString();
                        brand.Status = dtBrand.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        result.Add(brand);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static List<UnitModel> GetUnitDetail(string unitId = null, bool status = true)
        {
            List<UnitModel> result = new List<UnitModel>();

            try
            {
                string fileds = "*";
                string tablename = "T_Unit";
                string whereCondition = "Status=" + (status == true ? 1 : 0);
                if (!string.IsNullOrEmpty(unitId))
                {
                    whereCondition += " and Unitid=" + unitId;
                }

                DataTable dtBrand = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtBrand != null && dtBrand.Rows.Count > 0)
                {
                    for (int i = 0; i < dtBrand.Rows.Count; i++)
                    {
                        UnitModel brand = new UnitModel();
                        brand.UnitId = Convert.ToInt32(dtBrand.Rows[i]["Unitid"].ToString());
                        brand.UnitName = dtBrand.Rows[i]["Unit"].ToString();
                        brand.Status = dtBrand.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        result.Add(brand);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
    }
}