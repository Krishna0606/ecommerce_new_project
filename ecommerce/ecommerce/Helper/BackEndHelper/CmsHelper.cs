﻿using ecommerce.Models.Backend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecommerce.Helper.BackEndHelper
{
    public class CmsHelper
    {
        public static bool InsertAboutUs(AboutUsModel model)
        {
            try
            {
                using (DatebaseEntity db = new DatebaseEntity())
                {
                    T_AboutUs About = new T_AboutUs();

                    About.Heading = model.Heading;
                    About.Content = model.Content;
                    db.AboutUs.Add(About);
                    if (db.SaveChanges() > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        public static T_AboutUs GetAboutbyid(int? aboutid, DatebaseEntity ef)
        {
            return (from p in ef.AboutUs where p.aboutid == aboutid select p).FirstOrDefault();
        }
        public static bool UpdateAboutUs(int? aboutid, AboutUsModel model)
        {
            bool isSuccess = false;
            try
            {
                using (DatebaseEntity db = new DatebaseEntity())
                {
                    var Aboutus = GetAboutbyid(aboutid, db);
                    Aboutus.Heading = model.Heading;
                    Aboutus.Content = model.Content;
                    if (db.SaveChanges() > 0)
                    {
                        isSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return isSuccess;
        }
        public static AboutUsModel GetaboutModelbyID(int? aboutid)
        {
            AboutUsModel tempItem = new AboutUsModel();

            try
            {
                using (DatebaseEntity ef = new DatebaseEntity())
                {
                    var query = from p in ef.AboutUs where p.aboutid == aboutid orderby p.aboutid select p;

                    int totalCount = query.Count();

                    if (totalCount > 0)
                    {
                        foreach (var item in query)
                        {
                            tempItem.aboutid = item.aboutid;
                            tempItem.Heading = item.Heading;
                            tempItem.Content = item.Content.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return tempItem;
        }

        public static List<AboutUsModel> Getaboutuslist()
        {
            List<AboutUsModel> objaboutmodel = new List<AboutUsModel>();

            try
            {
                using (DatebaseEntity ef = new DatebaseEntity())
                {
                    var query = from p in ef.AboutUs orderby p.aboutid select p;

                    int totalCount = query.Count();

                    if (totalCount > 0)
                    {
                        foreach (var item in query)
                        {
                            var tempItem = new AboutUsModel();
                            tempItem.aboutid = item.aboutid;
                            tempItem.Heading = item.Heading;                           
                            tempItem.Content = item.Content;
                            objaboutmodel.Add(tempItem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return objaboutmodel;
        }

        public static T_PrivicyPolicy Getprivicybyid(int? privicyid, DatebaseEntity ef)
        {
            return (from p in ef.PrivicyPolicies where p.privicyid == privicyid select p).FirstOrDefault();
        }
        public static bool Insertprivicy(PrivicyPolicyModel model)
        {
            try
            {
                using (DatebaseEntity db = new DatebaseEntity())
                {
                    T_PrivicyPolicy privicyPolicy = new T_PrivicyPolicy();

                    privicyPolicy.Heading = model.Heading;
                    privicyPolicy.Content = model.Content;
                    db.PrivicyPolicies.Add(privicyPolicy);
                    if (db.SaveChanges() > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        public static bool UpdatePrivicy(int? privicyid, PrivicyPolicyModel model)
        {
            bool isSuccess = false;
            try
            {
                using (DatebaseEntity db = new DatebaseEntity())
                {
                    var Privicy = Getprivicybyid(privicyid, db);
                    Privicy.Heading = model.Heading;
                    Privicy.Content = model.Content;
                    if (db.SaveChanges() > 0)
                    {
                        isSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return isSuccess;
        }
        public static List<PrivicyPolicyModel> GetaPrivicyPolicylist()
        {
            List<PrivicyPolicyModel> objprivivcymodel = new List<PrivicyPolicyModel>();

            try
            {
                using (DatebaseEntity ef = new DatebaseEntity())
                {
                    var query = from p in ef.PrivicyPolicies orderby p.privicyid select p;

                    int totalCount = query.Count();

                    if (totalCount > 0)
                    {
                        foreach (var item in query)
                        {
                            var tempItem = new PrivicyPolicyModel();
                            tempItem.privicyid = item.privicyid;
                            tempItem.Heading = item.Heading;
                            tempItem.Content = item.Content;
                            objprivivcymodel.Add(tempItem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return objprivivcymodel;
        }
        public static PrivicyPolicyModel GetPrivicyModelbyID(int? privicyid)
        {
            PrivicyPolicyModel tempItem = new PrivicyPolicyModel();

            try
            {
                using (DatebaseEntity ef = new DatebaseEntity())
                {
                    var query = from p in ef.PrivicyPolicies where p.privicyid == privicyid orderby p.privicyid select p;

                    int totalCount = query.Count();

                    if (totalCount > 0)
                    {
                        foreach (var item in query)
                        {
                            tempItem.privicyid = item.privicyid;
                            tempItem.Heading = item.Heading;
                            tempItem.Content = item.Content.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return tempItem;
        }


    }
}