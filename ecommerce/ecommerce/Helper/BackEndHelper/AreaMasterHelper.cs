﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using ecommerce.Helper.DataBase;
using static ecommerce.Models.Backend.AreaMaster;

namespace ecommerce.Helper.BackEndHelper
{
    public static class AreaMasterHelper
    {
        #region [Country]        
        public static List<Country> GetCountryDetail(string countryid = null, bool status = true)
        {
            List<Country> countryList = new List<Country>();

            try
            {
                string fileds = "CountryId,CountryName,CountryCode,Status";
                string tablename = "T_Country";
                string whereCondition = "Status=" + (status == true ? 1 : 0);
                if (!string.IsNullOrEmpty(countryid))
                {
                    whereCondition += " and CountryId=" + countryid;
                }

                DataTable dtCountry = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtCountry != null && dtCountry.Rows.Count > 0)
                {
                    for (int i = 0; i < dtCountry.Rows.Count; i++)
                    {
                        Country result = new Country();
                        result.CountryId = Convert.ToInt32(dtCountry.Rows[i]["CountryId"].ToString());
                        result.CountryName = dtCountry.Rows[i]["CountryName"].ToString();
                        result.CountryCode = dtCountry.Rows[i]["CountryCode"].ToString();
                        result.Status = dtCountry.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;

                        countryList.Add(result);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }


            return countryList.OrderBy(p => p.CountryName).ToList();
        }
        public static bool InsertCountryDetail(string countryName, string countryCode)
        {
            try
            {
                if (!string.IsNullOrEmpty(countryName) && !string.IsNullOrEmpty(countryCode))
                {
                    string fileds = "CountryName,CountryCode";
                    string fieldValue = "'" + countryName + "','" + countryCode + "'";
                    string tableName = "T_Country";

                    if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        public static bool UpdateCountryDetail(string countryName = null, string countryCode = null, string countryid = null)
        {
            try
            {
                if (!string.IsNullOrEmpty(countryName) && !string.IsNullOrEmpty(countryCode))
                {
                    string fieldsWithValue = "CountryName='" + countryName + "',CountryCode='" + countryCode + "'";
                    string whereCondition = "CountryId=" + countryid;
                    string tableName = "T_Country";

                    if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                    {
                        return true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(countryid))
                    {
                        string fieldsWithValue = "Status=0";
                        string whereCondition = "CountryId=" + countryid;
                        string tableName = "T_Country";

                        if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        #endregion

        #region [State]        
        public static List<State> GetStateDetail(string stateid = null, bool status = true, string countryId = null)
        {
            List<State> stateList = new List<State>();

            try
            {
                string fileds = "s.StateId,s.StateName,s.StateCode,s.CountryId,s.Status,c.CountryName";
                string tablename = "T_State s inner join T_Country c on c.CountryId=s.CountryId";
                string whereCondition = "s.Status=" + (status == true ? 1 : 0);
                if (!string.IsNullOrEmpty(stateid))
                {
                    whereCondition += " and s.StateId=" + stateid;
                }

                if (!string.IsNullOrEmpty(countryId))
                {
                    whereCondition += " and s.CountryId=" + countryId;
                }

                DataTable dtState = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtState != null && dtState.Rows.Count > 0)
                {
                    for (int i = 0; i < dtState.Rows.Count; i++)
                    {
                        State result = new State();
                        result.StateId = Convert.ToInt32(dtState.Rows[i]["StateId"].ToString());
                        result.StateName = dtState.Rows[i]["StateName"].ToString();
                        result.StateCode = dtState.Rows[i]["StateCode"].ToString();
                        result.CountryId = Convert.ToInt32(dtState.Rows[i]["CountryId"].ToString());
                        result.CountryName = dtState.Rows[i]["CountryName"].ToString();
                        result.Status = dtState.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;

                        stateList.Add(result);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }


            return stateList.OrderBy(p => p.StateName).ToList();
        }
        public static bool InsertStateDetail(string stateName, string stateCode, string countryId)
        {
            try
            {
                if (!string.IsNullOrEmpty(stateName) && !string.IsNullOrEmpty(stateCode) && !string.IsNullOrEmpty(countryId))
                {
                    string fileds = "StateName,StateCode,CountryId";
                    string fieldValue = "'" + stateName + "','" + stateCode + "'," + Convert.ToInt32(countryId) + "";
                    string tableName = "T_State";

                    if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        public static bool UpdateStateDetail(string stateid, string stateName, string stateCode, string countryId)
        {
            try
            {
                if (!string.IsNullOrEmpty(stateName) && !string.IsNullOrEmpty(stateCode) && !string.IsNullOrEmpty(countryId))
                {
                    string fieldsWithValue = "StateName='" + stateName + "',StateCode='" + stateCode + "',CountryId=" + countryId + "";
                    string whereCondition = "StateId=" + stateid;
                    string tableName = "T_State";

                    if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                    {
                        return true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(stateid))
                    {
                        string fieldsWithValue = "Status=0";
                        string whereCondition = "StateId=" + stateid;
                        string tableName = "T_State";

                        if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        #endregion

        #region [City]     
        public static List<City> GetCityDetail(string cityid = null, string stateid = null, bool status = true)
        {
            List<City> cityList = new List<City>();

            try
            {
                string fileds = "c.CityId,c.CityName,c.CityCode,c.CountryId,c.StateId,c.Status,co.CountryName,s.StateName";
                string tablename = "T_City c inner join T_Country co on co.CountryId=c.CountryId inner join T_State s on s.StateId=c.StateId";
                string whereCondition = "c.Status=" + (status == true ? 1 : 0);
                if (!string.IsNullOrEmpty(cityid))
                {
                    whereCondition += " and c.CityId=" + cityid;
                }

                if (!string.IsNullOrEmpty(stateid))
                {
                    whereCondition += " and c.StateId=" + stateid;
                }

                DataTable dtState = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtState != null && dtState.Rows.Count > 0)
                {
                    for (int i = 0; i < dtState.Rows.Count; i++)
                    {
                        City result = new City();
                        result.CityId = Convert.ToInt32(dtState.Rows[i]["CityId"].ToString());
                        result.CityName = dtState.Rows[i]["CityName"].ToString();
                        result.CityCode = dtState.Rows[i]["CityCode"].ToString();
                        result.CountryName = dtState.Rows[i]["CountryName"].ToString();
                        result.StateName = dtState.Rows[i]["StateName"].ToString();
                        result.CountryId = Convert.ToInt32(dtState.Rows[i]["CountryId"].ToString());
                        result.StateId = Convert.ToInt32(dtState.Rows[i]["StateId"].ToString());
                        result.Status = dtState.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;

                        cityList.Add(result);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }


            return cityList.OrderBy(p => p.CityName).ToList();
        }
        public static bool InsertCityDetail(string cityName, string cityCode, string countryId, string stateId)
        {
            try
            {
                if (!string.IsNullOrEmpty(cityName) && !string.IsNullOrEmpty(cityCode) && !string.IsNullOrEmpty(countryId) && !string.IsNullOrEmpty(stateId))
                {
                    string fileds = "CityName,CityCode,CountryId,StateId";
                    string fieldValue = "'" + cityName + "','" + cityCode + "'," + Convert.ToInt32(countryId) + "," + Convert.ToInt32(stateId) + "";
                    string tableName = "T_City";

                    if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        public static bool UpdateCityDetail(string cityId, string cityName, string cityCode, string countryId, string stateId)
        {
            try
            {
                if (!string.IsNullOrEmpty(cityName) && !string.IsNullOrEmpty(cityCode) && !string.IsNullOrEmpty(countryId) && !string.IsNullOrEmpty(stateId))
                {
                    string fieldsWithValue = "CityName='" + cityName + "',CityCode='" + cityCode + "',CountryId=" + countryId + ",StateId=" + stateId + "";
                    string whereCondition = "CityId=" + cityId;
                    string tableName = "T_City";

                    if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                    {
                        return true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(cityId))
                    {
                        string fieldsWithValue = "Status=0";
                        string whereCondition = "CityId=" + cityId;
                        string tableName = "T_City";

                        if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        #endregion
    }
}