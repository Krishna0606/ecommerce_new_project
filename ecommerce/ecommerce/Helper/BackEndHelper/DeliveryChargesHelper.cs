﻿using ecommerce.Helper.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ecommerce.Models.Backend.SettingMaster;

namespace ecommerce.Helper.BackEndHelper
{
    public static class DeliveryChargesHelper
    {
        public static bool InsertDeliveryCharges(DeliveryCharges dcharges)
        {
            try
            {
                string fileds = "Price,StateId,StateName,CityId,CityName,PinCode,SupplierId,SupplierLoginId,SupplierName";
                string fieldValue = "" + dcharges.Price + ",'" + dcharges.StateId + "','" + dcharges.StateName + "','" + dcharges.CityId + "','" + dcharges.StateName + "','" + dcharges.PinCode + "'," + dcharges.SupplierId + ",'" + dcharges.SupplierLoginId + "','" + dcharges.SupplierName + "'";
                string tableName = "T_DeliveryCharges";

                if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool UpdateDeliveryCharges(DeliveryCharges dcharges)
        {
            try
            {
                string fieldsWithValue = "Price=" + dcharges.Price + ",StateId='" + dcharges.StateId + "',StateName='" + dcharges.StateName + "',CityId='" + dcharges.CityId + "',CityName='" + dcharges.CityName + "',PinCode='" + dcharges.PinCode + "'";
                string whereCondition = "DeliveryChargesId=" + dcharges.DeliveryChargesId;
                string tableName = "T_DeliveryCharges";

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return false;
        }

        public static bool DeleteDeliveryCharges(DeliveryCharges dcharges)
        {
            try
            {
                string fieldsWithValue = "Status=0";
                string whereCondition = "DeliveryChargesId=" + dcharges.DeliveryChargesId;
                string tableName = "T_DeliveryCharges";

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return false;
        }
    }
}