﻿using ecommerce.Helper.DataBase;
using ecommerce.Models.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static ecommerce.Models.Backend.SettingMaster;

namespace ecommerce.Helper.BackEndHelper
{
    public static class SettingMasterHelper
    {
        public static List<DeliveryCharges> GetDeliveryChargesDetail(int supplierId, string dcid = null, bool status = true)
        {
            List<DeliveryCharges> deliveryChargesList = new List<DeliveryCharges>();

            try
            {
                string fileds = "*";
                string tablename = "T_DeliveryCharges";
                string whereCondition = "Status=1 and SupplierId=" + supplierId;
                if (!string.IsNullOrEmpty(dcid))
                {
                    whereCondition += " and DeliveryChargesId=" + dcid;
                }

                DataTable dtDeliveryCharges = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtDeliveryCharges != null && dtDeliveryCharges.Rows.Count > 0)
                {
                    for (int i = 0; i < dtDeliveryCharges.Rows.Count; i++)
                    {
                        DeliveryCharges delCharges = new DeliveryCharges();

                        delCharges.DeliveryChargesId = Convert.ToInt32(dtDeliveryCharges.Rows[i]["DeliveryChargesId"].ToString());
                        delCharges.Price = Convert.ToDecimal(dtDeliveryCharges.Rows[i]["Price"].ToString());
                        delCharges.StateId = Convert.ToInt32(dtDeliveryCharges.Rows[i]["StateId"].ToString());
                        delCharges.StateName = dtDeliveryCharges.Rows[i]["StateName"].ToString();
                        delCharges.CityId = Convert.ToInt32(dtDeliveryCharges.Rows[i]["CityId"].ToString());
                        delCharges.CityName = dtDeliveryCharges.Rows[i]["CityName"].ToString();
                        delCharges.PinCode = dtDeliveryCharges.Rows[i]["PinCode"].ToString();
                        delCharges.SupplierId = Convert.ToInt32(dtDeliveryCharges.Rows[i]["SupplierId"].ToString());
                        delCharges.SupplierLoginId = dtDeliveryCharges.Rows[i]["SupplierLoginId"].ToString();
                        delCharges.SupplierName = dtDeliveryCharges.Rows[i]["SupplierName"].ToString();
                        delCharges.Status = dtDeliveryCharges.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;

                        deliveryChargesList.Add(delCharges);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return deliveryChargesList;
        }
        public static List<HomeBannerModel> GetHomeBannerList(string bannerId = null, string isShow = null, string status = null)
        {
            List<HomeBannerModel> result = new List<HomeBannerModel>();

            try
            {
                string fileds = "*";
                string tablename = "T_Banner";
                string whereCondition = "Status=" + (!string.IsNullOrEmpty(status) ? status : "1");

                if (!string.IsNullOrEmpty(bannerId))
                {
                    whereCondition += " and BannerId=" + bannerId;
                }

                if (!string.IsNullOrEmpty(isShow))
                {
                    whereCondition += " and IsShow=" + isShow;
                }

                DataTable dtBanner = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtBanner != null && dtBanner.Rows.Count > 0)
                {
                    for (int i = 0; i < dtBanner.Rows.Count; i++)
                    {
                        HomeBannerModel banner = new HomeBannerModel();
                        banner.BannerId = Convert.ToInt32(dtBanner.Rows[i]["BannerId"].ToString());
                        banner.Heading = !string.IsNullOrEmpty(dtBanner.Rows[i]["Heading"].ToString()) ? dtBanner.Rows[i]["Heading"].ToString() : string.Empty;
                        banner.Content = !string.IsNullOrEmpty(dtBanner.Rows[i]["Content"].ToString()) ? dtBanner.Rows[i]["Content"].ToString() : string.Empty;
                        banner.ImageName = !string.IsNullOrEmpty(dtBanner.Rows[i]["ImageName"].ToString()) ? Utility.GetImagePath(dtBanner.Rows[i]["ImageName"].ToString(), "BannerImages", ("Banner_" + banner.BannerId)) : string.Empty;
                        banner.RedirectUrl = !string.IsNullOrEmpty(dtBanner.Rows[i]["RedirectUrl"].ToString()) ? dtBanner.Rows[i]["RedirectUrl"].ToString() : string.Empty;
                        banner.BannerType = !string.IsNullOrEmpty(dtBanner.Rows[i]["BannerType"].ToString()) ? dtBanner.Rows[i]["BannerType"].ToString() : string.Empty;
                        banner.BannerOrder = !string.IsNullOrEmpty(dtBanner.Rows[i]["BannerOrder"].ToString()) ? dtBanner.Rows[i]["BannerOrder"].ToString() : string.Empty;
                        banner.IsShow = dtBanner.Rows[i]["IsShow"].ToString().ToLower().Trim() == "true" ? true : false;
                        banner.Status = dtBanner.Rows[i]["Status"].ToString().ToLower().Trim() == "true" ? true : false;
                        banner.CreatedDate = (Convert.ToDateTime(dtBanner.Rows[i]["CreatedDate"].ToString())).ToString("dd MM yyyy");
                        result.Add(banner);//
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static int InsertBannerDetail(HomeBannerModel banner)
        {
            try
            {
                string fileds = "Heading,Content,RedirectUrl,IsShow,BannerType,BannerOrder";
                string fieldValue = "'" + banner.Heading + "','" + banner.Content + "','" + banner.RedirectUrl + "'," + (banner.IsShow == true ? 1 : 0) + ",'" + banner.BannerType + "','" + banner.BannerOrder + "'";
                string tableName = "T_Banner";

                return ConnectToDataBase.InsertRecordToTableReturnID(fileds, fieldValue, tableName);
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }
        public static bool UpdateBannerImage(string bannerId, string imagename)
        {
            try
            {
                string tableName = "T_Banner";
                string fieldsWithValue = "ImageName='" + imagename + "'";
                string whereCondition = "BannerId=" + bannerId;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool UpdateBannerOrder(string bannerid, string posval)
        {
            try
            {
                string tableName = "T_Banner";
                string fieldsWithValue = "BannerOrder='" + posval + "'";
                string whereCondition = "BannerId=" + bannerid;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
    }
}