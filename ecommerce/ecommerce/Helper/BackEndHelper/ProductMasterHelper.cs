﻿using ecommerce.Helper.DataBase;
using ecommerce.Models.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using static ecommerce.Models.Backend.ProductMaster;

namespace ecommerce.Helper.BackEndHelper
{
    public static class ProductMasterHelper
    {
        public static List<Product> GetProductDetail(ProductFilter filter)
        {
            List<Product> productList = new List<Product>();
            try
            {
                string fileds = "p.ProductId,p.ProductName,p.ProductCode,p.Brand,p.Menu,m.MenuName,p.Category,sc.SubCategoryName, p.SubCategory,p.Offer,p.PurchasePrice,p.SalePrice,p.Discount,p.DiscountPer,p.Unit,p.DeliveryTime,p.IsFeaturedProducts,p.SupplierId,p.SupplierLoginId,p.Status,p.SupplierName,c.CategoryName,u.Unit as UnitName";
                string tablename = "T_Product p inner join T_Menu m on m.Menuid =p.Menu  left join T_Category c on c.CategoryId =p.Category left join T_SubCategory sc on sc.SubCategoryId =p.SubCategory left join T_Unit u on u.Unitid=p.Unit";
                string whereCondition = "p.Status=1";

                if (filter.ProductId > 0)
                {
                    whereCondition += " and p.ProductId=" + filter.ProductId;
                }

                if (filter.SupplierId > 0)
                {
                    whereCondition += " and p.SupplierId=" + filter.SupplierId;
                }

                DataTable dtProduct = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtProduct != null && dtProduct.Rows.Count > 0)
                {
                    for (int i = 0; i < dtProduct.Rows.Count; i++)
                    {
                        Product product = new Product();

                        product.ProductId = Convert.ToInt32(dtProduct.Rows[i]["ProductId"].ToString());
                        product.ProductName = dtProduct.Rows[i]["ProductName"].ToString();
                        product.ProductCode = dtProduct.Rows[i]["ProductCode"].ToString();
                        product.BrandId = Convert.ToInt32(dtProduct.Rows[i]["Brand"].ToString());
                        product.MenuId = Convert.ToInt32(dtProduct.Rows[i]["Menu"].ToString());
                        product.MenuName = dtProduct.Rows[i]["MenuName"].ToString();
                        product.CategoryId = Convert.ToInt32(dtProduct.Rows[i]["Category"].ToString());
                        product.CategoryName = dtProduct.Rows[i]["CategoryName"].ToString();
                        product.SubCategoryId = Convert.ToInt32(dtProduct.Rows[i]["SubCategory"].ToString());
                        product.SubCategoryName = dtProduct.Rows[i]["SubCategoryName"].ToString();
                        product.Offer = dtProduct.Rows[i]["Offer"].ToString();
                        product.PurchasePrice = Convert.ToDecimal(dtProduct.Rows[i]["PurchasePrice"].ToString());
                        product.SalePrice = Convert.ToDecimal(dtProduct.Rows[i]["SalePrice"].ToString());
                        product.Discount = !string.IsNullOrEmpty(dtProduct.Rows[i]["Discount"].ToString()) ? Convert.ToDecimal(dtProduct.Rows[i]["Discount"].ToString()) : 0;
                        product.DiscountPer = !string.IsNullOrEmpty(dtProduct.Rows[i]["DiscountPer"].ToString()) ? Convert.ToInt32(dtProduct.Rows[i]["DiscountPer"].ToString()) : 0;
                        product.UnitId = Convert.ToInt32(dtProduct.Rows[i]["Unit"].ToString());
                        product.UnitName = dtProduct.Rows[i]["UnitName"].ToString();
                        product.DeliveryTime = dtProduct.Rows[i]["DeliveryTime"].ToString();
                        //product.Description = dtProduct.Rows[i]["Description"].ToString();
                        product.IsFeaturedProducts = dtProduct.Rows[i]["IsFeaturedProducts"].ToString().ToLower() == "true" ? true : false;
                        product.SupplierId = Convert.ToInt32(dtProduct.Rows[i]["SupplierId"].ToString());
                        product.SupplierLoginId = dtProduct.Rows[i]["SupplierLoginId"].ToString();
                        product.SupplierName = dtProduct.Rows[i]["SupplierName"].ToString();
                        product.Status = dtProduct.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        product.ProductImage = GetProductImagesById(product.ProductId.ToString());
                        productList.Add(product);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return productList;
        }
        public static int InsertProductDetail(Product product)
        {
            int rutProductId = 0;

            try
            {
                if (product != null && !string.IsNullOrEmpty(product.ProductCode))
                {
                    if (product.DiscountPer > 0)
                    {
                        product.Discount = (product.SalePrice * product.DiscountPer) / 100;
                    }

                    string fileds = "ProductName,ProductCode,Brand,Menu,Category,SubCategory,Offer,PurchasePrice,SalePrice,Discount,DiscountPer,Unit,DeliveryTime,IsFeaturedProducts,SupplierId,SupplierLoginId,SupplierName";
                    string fieldValue = "'" + product.ProductName + "','" + product.ProductCode + "'," + product.BrandId + "," + product.MenuId + "," + product.CategoryId + "," + product.SubCategoryId + ",'" + product.Offer + "'," + product.PurchasePrice + "," + product.SalePrice + "," + product.Discount + "," + product.DiscountPer + "," + product.UnitId + ",'" + product.DeliveryTime + "'," + (product.IsFeaturedProducts == false ? 0 : 1) + "," + product.SupplierId + ",'" + product.SupplierLoginId + "','" + product.SupplierName + "'";
                    string tableName = "T_Product";

                    //" + product.Description.Replace("'", "''") + "

                    int productId = ConnectToDataBase.InsertRecordToTableReturnID(fileds, fieldValue, tableName);

                    if (productId > 0)
                    {
                        rutProductId = productId;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return rutProductId;
        }
        public static bool UpdateProductDetail(Product product)
        {
            try
            {
                if (product.DiscountPer > 0)
                {
                    product.Discount = (product.SalePrice * product.DiscountPer) / 100;
                }

                string tableName = "T_Product";
                string fieldsWithValue = "ProductName='" + product.ProductName + "',ProductCode='" + product.ProductCode + "',Brand=" + product.BrandId + ",Menu=" + product.MenuId + ",Category=" + product.CategoryId + ",SubCategory=" + product.SubCategoryId + ",Offer='" + product.Offer + "',PurchasePrice=" + product.PurchasePrice + ",SalePrice=" + product.SalePrice + ",Discount=" + product.Discount + ",DiscountPer=" + product.DiscountPer + ",Unit=" + product.UnitId + ",DeliveryTime='" + product.DeliveryTime + "',IsFeaturedProducts=" + (product.IsFeaturedProducts.ToString().ToLower() == "false" ? 0 : 1) + ",UpdatedDate=getdate()";
                string whereCondition = "ProductId=" + product.ProductId;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return false;
        }
        public static bool ArchiveUnArchiveProductDetail(string productid, string status)
        {
            try
            {

                string tableName = "T_Product";
                string fieldsWithValue = "Status=" + status;
                string whereCondition = "ProductId=" + productid;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return false;
        }
        public static List<ProductImage> GetProductImagesById(string productid)
        {
            List<ProductImage> imageList = new List<ProductImage>();
            try
            {
                string fileds = "*";
                string tablename = "T_ProductImages";
                string whereCondition = "Status=1 and ProductId=" + productid;

                DataTable dtImage = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtImage != null && dtImage.Rows.Count > 0)
                {
                    for (int i = 0; i < dtImage.Rows.Count; i++)
                    {
                        ProductImage image = new ProductImage();

                        image.ImageId = Convert.ToInt32(dtImage.Rows[i]["ImageId"].ToString());
                        image.ImageName = dtImage.Rows[i]["ImageName"].ToString();
                        image.ProductId = Convert.ToInt32(dtImage.Rows[i]["ProductId"].ToString());
                        image.ImageUrl = Utility.GetImagePath(dtImage.Rows[i]["ImageUrl"].ToString(), "ProductImages", ("Product_" + image.ProductId));
                        image.Status = dtImage.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;

                        imageList.Add(image);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return imageList;
        }
        public static int InsertProductImageDetail(string imagename, string productid)
        {
            try
            {
                string fileds = "ImageName,ProductId";
                string fieldValue = "'" + imagename + "'," + productid + "";
                string tableName = "T_ProductImages";

                int imageId = ConnectToDataBase.InsertRecordToTableReturnID(fileds, fieldValue, tableName);

                if (imageId > 0)
                {
                    return imageId;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return 0;
        }
        public static bool UpdateProductImageUrl(string productid, string imageid, string imageurlname)
        {
            try
            {
                if (!string.IsNullOrEmpty(productid) && !string.IsNullOrEmpty(imageid) && !string.IsNullOrEmpty(imageurlname))
                {
                    string tableName = "T_ProductImages";
                    string fieldsWithValue = "ImageUrl='" + imageurlname + "'";
                    string whereCondition = "ImageId=" + imageid + " and ProductId=" + productid;

                    if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return false;
        }
        public static bool DeleteProductImage(string imgid, string productid)
        {
            try
            {
                if (!string.IsNullOrEmpty(imgid) && !string.IsNullOrEmpty(productid))
                {
                    string tableName = "T_ProductImages";
                    string whereCondition = "ImageId=" + imgid + " and ProductId=" + productid;

                    if (ConnectToDataBase.DeleteRecordFromAnyTable(tableName, whereCondition))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return false;
        }
        public static bool InsertDescriptionDetails(string productid, string[] desclist)
        {
            try
            {
                bool isSuccess = false;
                for (int i = 1; i <= desclist.Count(); i++)
                {
                    if (!string.IsNullOrEmpty(desclist[i - 1]))
                    {
                        string fileds = "ProductId,Description" + i;
                        string fieldValue = "" + productid + ",'" + desclist[i - 1].Replace("'", "''") + "'";
                        string tableName = "T_ProductDescription";

                        DataTable dtProduct = ConnectToDataBase.GetRecordFromTable("ProductDescriptionId,Description" + i, "T_ProductDescription", "ProductId=" + productid);
                        if (dtProduct != null && dtProduct.Rows.Count > 0)
                        {
                            if (ConnectToDataBase.UpdateRecordIntoAnyTable("T_ProductDescription", "Description" + i + "='" + desclist[i - 1] + "'", "ProductId=" + productid + " and ProductDescriptionId=" + dtProduct.Rows[0]["ProductDescriptionId"].ToString()))
                            {
                                isSuccess = true;
                            }
                            else
                            {
                                isSuccess = false;
                            }
                        }
                        else
                        {
                            if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                            {
                                isSuccess = true;
                            }
                            else
                            {
                                isSuccess = false;
                            }
                        }
                    }
                }

                if (isSuccess)
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }

        public static List<string> GetDescriptionDetails(string productid)
        {
            List<string> strResult = new List<string>();

            try
            {
                string fileds = "*";
                string tablename = "T_ProductDescription";
                string whereCondition = "ProductId=" + productid;

                DataTable dtDescProduct = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtDescProduct != null && dtDescProduct.Rows.Count > 0)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        strResult.Add(dtDescProduct.Rows[0]["Description" + (i + 1)].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return strResult;
        }
    }
}