﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using ecommerce.Helper.DataBase;
using ecommerce.Helper.EmailHelper;
using ecommerce.Models.Backend;
using ecommerce.Models.Common;

namespace ecommerce.Helper.BackEndHelper
{
    public static class MemberRegHelper
    {
        public static List<BackendLogin> GetMemRegDetail(string memberid = null, bool status = true)
        {
            List<BackendLogin> memList = new List<BackendLogin>();
            try
            {
                //string fileds = "l.*, co.CountryName, co.CountryCode, s.StateName,s.StateCode ,c.CityName, c.CityCode";
                //string tablename = "T_Login l inner JOIN   T_City c on c.CityId=l.City inner JOIN   T_Country co on co.CountryId=l.Country inner JOIN   T_State s on s.StateId=l.State";
                //string whereCondition = "l.Status=" + (status == true ? 1 : 0) + " and l.IsApproved=1 and l.UserType='supplier'";

                string fileds = "l.*, co.CountryName, co.CountryCode, s.StateName,s.StateCode ,c.CityName, c.CityCode";
                string tablename = "T_Login l left JOIN   T_City c on c.CityId=l.City left JOIN   T_Country co on co.CountryId=l.Country left JOIN   T_State s on s.StateId=l.State";
                string whereCondition = "l.Status=" + (status == true ? 1 : 0) + " and l.IsApproved=1 and l.UserType='supplier'";

                if (!string.IsNullOrEmpty(memberid))
                {
                    whereCondition += " and l.LoginId=" + memberid;
                }

                DataTable dtMember = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtMember != null && dtMember.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMember.Rows.Count; i++)
                    {
                        BackendLogin result = new BackendLogin();
                        result.LoginId = Convert.ToInt32(dtMember.Rows[i]["LoginId"].ToString());
                        result.Title = !string.IsNullOrEmpty(dtMember.Rows[i]["Title"].ToString()) ? dtMember.Rows[i]["Title"].ToString() : string.Empty;
                        result.FirstName = !string.IsNullOrEmpty(dtMember.Rows[i]["FirstName"].ToString()) ? dtMember.Rows[i]["FirstName"].ToString() : string.Empty;
                        result.LastName = !string.IsNullOrEmpty(dtMember.Rows[i]["LastName"].ToString()) ? dtMember.Rows[i]["LastName"].ToString() : string.Empty;
                        result.EmailId = !string.IsNullOrEmpty(dtMember.Rows[i]["EmailId"].ToString()) ? dtMember.Rows[i]["EmailId"].ToString() : string.Empty;
                        result.MobileNo = !string.IsNullOrEmpty(dtMember.Rows[i]["MobileNo"].ToString()) ? dtMember.Rows[i]["MobileNo"].ToString() : string.Empty;
                        result.CountryId = !string.IsNullOrEmpty(dtMember.Rows[i]["Country"].ToString()) ? Convert.ToInt32(dtMember.Rows[i]["Country"].ToString()) : 0;
                        result.CountryName = !string.IsNullOrEmpty(dtMember.Rows[i]["CountryName"].ToString()) ? dtMember.Rows[i]["CountryName"].ToString() : string.Empty;
                        result.StateId = !string.IsNullOrEmpty(dtMember.Rows[i]["State"].ToString()) ? Convert.ToInt32(dtMember.Rows[i]["State"].ToString()) : 0;
                        result.StateName = !string.IsNullOrEmpty(dtMember.Rows[i]["StateName"].ToString()) ? dtMember.Rows[i]["StateName"].ToString() : string.Empty;
                        result.CityId = !string.IsNullOrEmpty(dtMember.Rows[i]["City"].ToString()) ? dtMember.Rows[i]["City"].ToString() : "0";
                        result.CityName = !string.IsNullOrEmpty(dtMember.Rows[i]["CityName"].ToString()) ? dtMember.Rows[i]["CityName"].ToString() : string.Empty;
                        result.AreaName = !string.IsNullOrEmpty(dtMember.Rows[i]["AreaName"].ToString()) ? dtMember.Rows[i]["AreaName"].ToString() : string.Empty;
                        result.PinCode = !string.IsNullOrEmpty(dtMember.Rows[i]["PinCode"].ToString()) ? dtMember.Rows[i]["PinCode"].ToString() : string.Empty;
                        result.Address = !string.IsNullOrEmpty(dtMember.Rows[i]["Address"].ToString()) ? dtMember.Rows[i]["Address"].ToString() : string.Empty;
                        result.ProfileImage = !string.IsNullOrEmpty(dtMember.Rows[i]["ProfileImage"].ToString()) ? dtMember.Rows[i]["ProfileImage"].ToString() : string.Empty;
                        result.Profession = !string.IsNullOrEmpty(dtMember.Rows[i]["Profession"].ToString()) ? dtMember.Rows[i]["Profession"].ToString() : string.Empty;
                        result.DOB = !string.IsNullOrEmpty(dtMember.Rows[i]["DOB"].ToString()) ? dtMember.Rows[i]["DOB"].ToString() : string.Empty;
                        result.AddharNo = !string.IsNullOrEmpty(dtMember.Rows[i]["AddharNo"].ToString()) ? dtMember.Rows[i]["AddharNo"].ToString() : string.Empty;
                        result.PanNo = !string.IsNullOrEmpty(dtMember.Rows[i]["PanNo"].ToString()) ? dtMember.Rows[i]["PanNo"].ToString() : string.Empty;
                        result.GSTNo = !string.IsNullOrEmpty(dtMember.Rows[i]["GSTNo"].ToString()) ? dtMember.Rows[i]["GSTNo"].ToString() : string.Empty;
                        result.AddharImage = !string.IsNullOrEmpty(dtMember.Rows[i]["AddharImage"].ToString()) ? dtMember.Rows[i]["AddharImage"].ToString() : string.Empty;
                        result.PanImage = !string.IsNullOrEmpty(dtMember.Rows[i]["PanImage"].ToString()) ? dtMember.Rows[i]["PanImage"].ToString() : string.Empty;
                        result.BankName = !string.IsNullOrEmpty(dtMember.Rows[i]["BankName"].ToString()) ? dtMember.Rows[i]["BankName"].ToString() : string.Empty;
                        result.AccountNo = !string.IsNullOrEmpty(dtMember.Rows[i]["AccountNo"].ToString()) ? dtMember.Rows[i]["AccountNo"].ToString() : string.Empty;
                        result.BranchName = !string.IsNullOrEmpty(dtMember.Rows[i]["BranchName"].ToString()) ? dtMember.Rows[i]["BranchName"].ToString() : string.Empty;
                        result.BrnachCode = !string.IsNullOrEmpty(dtMember.Rows[i]["BrnachCode"].ToString()) ? dtMember.Rows[i]["BrnachCode"].ToString() : string.Empty;
                        result.IFSCCode = !string.IsNullOrEmpty(dtMember.Rows[i]["IFSCCode"].ToString()) ? dtMember.Rows[i]["IFSCCode"].ToString() : string.Empty;
                        result.BankAddress = !string.IsNullOrEmpty(dtMember.Rows[i]["BankAddress"].ToString()) ? dtMember.Rows[i]["BankAddress"].ToString() : string.Empty;
                        result.PayeeName = !string.IsNullOrEmpty(dtMember.Rows[i]["PayeeName"].ToString()) ? dtMember.Rows[i]["PayeeName"].ToString() : string.Empty;
                        result.UserId = !string.IsNullOrEmpty(dtMember.Rows[i]["UserId"].ToString()) ? dtMember.Rows[i]["UserId"].ToString() : string.Empty;
                        result.Password = !string.IsNullOrEmpty(dtMember.Rows[i]["Password"].ToString()) ? dtMember.Rows[i]["Password"].ToString() : string.Empty;
                        result.UserType = !string.IsNullOrEmpty(dtMember.Rows[i]["UserType"].ToString()) ? dtMember.Rows[i]["UserType"].ToString() : string.Empty;
                        result.IsApproved = dtMember.Rows[i]["IsApproved"].ToString().ToLower() == "true" ? true : false;
                        result.Status = dtMember.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        result.CreatedDate = dtMember.Rows[i]["CreatedDate"].ToString();
                        memList.Add(result);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return memList;
        }
        public static bool InsertMemberRegDetails(BackendLogin supplier)
        {
            try
            {
                if (supplier != null && !string.IsNullOrEmpty(supplier.UserId) && !string.IsNullOrEmpty(supplier.Password))
                {
                    string tableName = "T_Login";
                    string fileds = "Title,FirstName,LastName,EmailId,MobileNo,Country,State,City,PinCode,Address ,ProfileImage ,Profession ,DOB ,AddharNo ,PanNo  ,GSTNo  ,AddharImage ,PanImage ,BankName ,AccountNo,BranchName ,BrnachCode ,IFSCCode,BankAddress ,PayeeName ,UserId ,Password ,UserType,IsApproved,SupplierName,EmailVerified,MobileVerified,AreaName";
                    string fieldValue = "'Mr.','" + supplier.FirstName + "','" + supplier.LastName + "','" + supplier.EmailId + "','" + supplier.MobileNo + "'," + supplier.CountryId + "," + supplier.StateId + ",'" + supplier.CityId + "','" + supplier.PinCode + "','" + supplier.Address + "','" + supplier.ProfileImage + "','" + supplier.Profession + "','" + supplier.DOB + "','" + supplier.AddharNo + "','" + supplier.PanNo + "','" + supplier.GSTNo + "','" + supplier.AddharImage + "','" + supplier.PanImage + "','" + supplier.BankName + "','" + supplier.AccountNo + "','" + supplier.BranchName + "','" + supplier.BrnachCode + "','" + supplier.IFSCCode + "','" + supplier.BankAddress + "','" + supplier.PayeeName + "','" + supplier.UserId + "','" + supplier.Password + "','supplier',1,'" + (supplier.FirstName + " " + supplier.LastName) + "',1,1,'" + supplier.AreaName + "'";

                    if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return false;
        }
        public static bool UpdateMemberRegDetails(BackendLogin supplier)
        {
            try
            {
                if (!string.IsNullOrEmpty(supplier.LoginId.ToString()) && supplier.LoginId > 0 && string.IsNullOrEmpty(supplier.ActionType))
                {
                    string fieldsWithValue = "Status=" + (supplier.Status == true ? 1 : 0);
                    string whereCondition = "LoginId=" + supplier.LoginId;
                    string tableName = "T_Login";

                    if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                    {
                        return true;
                    }
                }
                else if (!string.IsNullOrEmpty(supplier.LoginId.ToString()) && supplier.LoginId > 0 && !string.IsNullOrEmpty(supplier.ActionType))
                {
                    string fieldsWithValue = "FirstName='" + supplier.FirstName + "',LastName='" + supplier.LastName + "',EmailId='" + supplier.EmailId + "',MobileNo='" + supplier.MobileNo + "',Country=" + supplier.CountryId + ",State=" + supplier.StateId + ",City='" + supplier.CityId + "',PinCode='" + supplier.PinCode + "',Address='" + supplier.Address + "',DOB='" + supplier.DOB + "',AddharNo='" + supplier.AddharNo + "',PanNo='" + supplier.PanNo + "',BankName='" + supplier.BankName + "',AccountNo='" + supplier.AccountNo + "',BranchName='" + supplier.BranchName + "',BrnachCode='" + supplier.BrnachCode + "',IFSCCode='" + supplier.IFSCCode + "',BankAddress='" + supplier.BankAddress + "',PayeeName='" + supplier.PayeeName + "', AreaName='" + supplier.AreaName + "'";
                    string whereCondition = "LoginId=" + supplier.LoginId;
                    string tableName = "T_Login";

                    if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        public static bool CheckUserIdExist(string userid)
        {
            try
            {
                string fileds = "LoginId,UserId";
                string tablename = "T_Login";
                string whereCondition = "UserId='" + userid + "'";

                DataTable dtMember = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);

                if (dtMember != null && dtMember.Rows.Count > 0)
                {
                    return true;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return false;
        }

        public static bool InsertUserRegDetails(string firstname, string lastname, string emailid, string mobileno, string userid, string regpassword)
        {
            try
            {
                string emailOTP = Utility.GenrateRandomTransactionId("", 6, "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
                string moileOTP = Utility.GenrateRandomTransactionId("", 6, "1234567890");

                string tableName = "T_Login";
                string fileds = "Title,FirstName,LastName,EmailId,MobileNo,Password,UserId,UserType,IsApproved,EmailOTP,MobileOTP";
                string fieldValue = "'Mr.','" + firstname + "','" + lastname + "','" + emailid + "','" + mobileno + "','" + regpassword + "','" + userid + "','user',0,'" + emailOTP + "','" + moileOTP + "'";
                if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                {
                    if (AccountHelper.IsMailSmsSent(userid, regpassword))
                    {
                        return SendVarificationCode(firstname, lastname, emailid, mobileno, userid, emailOTP, moileOTP);
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }

            return false;
        }

        public static bool SendVarificationCode(string firstname, string lastname, string emailid, string mobileno, string userid, string emailOTP, string moileOTP)
        {
            try
            {
                string tempEmailOTP = string.Empty; string tempMoileOTP = string.Empty;

                if (string.IsNullOrEmpty(emailOTP) && string.IsNullOrEmpty(moileOTP))
                {
                    tempEmailOTP = Utility.GenrateRandomTransactionId("", 6, "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
                    tempMoileOTP = Utility.GenrateRandomTransactionId("", 6, "1234567890");

                    string fieldsWithValue = "EmailOTP=" + tempEmailOTP + ",MobileOTP=" + tempMoileOTP + "";
                    string whereCondition = "UserId='" + userid + "' and EmailId='" + emailid + "' and MobileNo='" + mobileno + "'";
                    string tableName = "T_Login";

                    bool isUpdated = ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition);
                }
                else
                {
                    tempEmailOTP = emailOTP;
                    tempMoileOTP = moileOTP;
                }

                if (EmailHelp.SendVarificationCode(firstname, lastname, emailid, mobileno, tempEmailOTP, tempMoileOTP))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static List<string> GetSupplierAreaList(string pincode)
        {
            List<string> result = new List<string>();

            try
            {
                string fileds = "*";
                string tablename = "T_Login";
                string whereCondition = "Status=1 and IsApproved=1";

                if (!string.IsNullOrEmpty(pincode))
                {
                    whereCondition += " and PinCode='" + pincode + "'";
                }

                DataTable dtMember = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtMember != null && dtMember.Rows.Count > 0)
                {
                    var set = new HashSet<string>();

                    for (int i = 0; i < dtMember.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(dtMember.Rows[i]["AreaName"].ToString()) && !set.Contains(dtMember.Rows[i]["AreaName"].ToString().ToLower().Trim()))
                        {
                            result.Add(!string.IsNullOrEmpty(dtMember.Rows[i]["AreaName"].ToString()) ? dtMember.Rows[i]["AreaName"].ToString() : string.Empty);
                            set.Add(dtMember.Rows[i]["AreaName"].ToString().ToLower().Trim());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
    }
}