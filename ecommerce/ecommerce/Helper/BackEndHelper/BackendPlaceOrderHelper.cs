﻿using ecommerce.Helper.DataBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static ecommerce.Models.Backend.Dashboard;

namespace ecommerce.Helper.BackEndHelper
{
    public static class BackendPlaceOrderHelper
    {
        public static List<DashboardOrders> GetPlaceOrderDetails(string userid = null, string userType = null)
        {
            List<DashboardOrders> orderList = new List<DashboardOrders>();

            try
            {
                string fileds = "*";
                string tablename = "T_BookingOrder";
                string whereCondition = string.Empty;

                if (!string.IsNullOrEmpty(userType))
                {
                    if (userType.ToLower().Trim() != "admin")
                    {
                        whereCondition = "SupplierId=" + userid + " order by OrderId desc";
                    }
                    else
                    {
                        tablename += " order by OrderId desc";
                    }
                }

                DataTable dtOrder = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtOrder.Rows.Count > 0)
                {
                    for (int i = 0; i < dtOrder.Rows.Count; i++)
                    {
                        DashboardOrders order = new DashboardOrders();
                        order.SrNo = i + 1;
                        order.OrderId = !string.IsNullOrEmpty(dtOrder.Rows[i]["OrderId"].ToString()) ? Convert.ToInt32(dtOrder.Rows[i]["OrderId"].ToString()) : 0;
                        order.OrderNo = !string.IsNullOrEmpty(dtOrder.Rows[i]["OrderNo"].ToString()) ? dtOrder.Rows[i]["OrderNo"].ToString() : string.Empty;
                        order.ProductName = !string.IsNullOrEmpty(dtOrder.Rows[i]["ProductName"].ToString()) ? dtOrder.Rows[i]["ProductName"].ToString() : string.Empty;
                        order.Quantity = !string.IsNullOrEmpty(dtOrder.Rows[i]["Quantity"].ToString()) ? dtOrder.Rows[i]["Quantity"].ToString() : string.Empty;
                        order.UserName = !string.IsNullOrEmpty(dtOrder.Rows[i]["UserName"].ToString()) ? dtOrder.Rows[i]["UserName"].ToString() : string.Empty;
                        order.MobileNo = "";
                        order.SubTotal = !string.IsNullOrEmpty(dtOrder.Rows[i]["SubTotal"].ToString()) ? dtOrder.Rows[i]["SubTotal"].ToString() : string.Empty;
                        order.DeliveryCharges = !string.IsNullOrEmpty(dtOrder.Rows[i]["DeliveryCharges"].ToString()) ? dtOrder.Rows[i]["DeliveryCharges"].ToString() : string.Empty;
                        order.GrandTotal = ((Convert.ToInt32(order.Quantity) * Convert.ToDecimal(order.SubTotal)) + Convert.ToDecimal(order.DeliveryCharges)).ToString();
                        order.PaymentMode = !string.IsNullOrEmpty(dtOrder.Rows[i]["PaymentType"].ToString()) ? dtOrder.Rows[i]["PaymentType"].ToString() : string.Empty;
                        order.OrderStatus = !string.IsNullOrEmpty(dtOrder.Rows[i]["DelieveryStatus"].ToString()) ? dtOrder.Rows[i]["DelieveryStatus"].ToString() : string.Empty;
                        order.OrderDate = !string.IsNullOrEmpty(dtOrder.Rows[i]["CreatedDate"].ToString()) ? dtOrder.Rows[i]["CreatedDate"].ToString() : string.Empty;
                        order.Image = !string.IsNullOrEmpty(dtOrder.Rows[i]["ProductImgUrl"].ToString()) ? dtOrder.Rows[i]["ProductImgUrl"].ToString() : string.Empty;
                        if (string.IsNullOrEmpty(order.Image))
                        {
                            order.Image = "/Content/image/no-image.png";
                        }
                        order.SupplierName = !string.IsNullOrEmpty(dtOrder.Rows[i]["SupplierName"].ToString()) ? dtOrder.Rows[i]["SupplierName"].ToString() : string.Empty;
                        order.CreatedDate = !string.IsNullOrEmpty(dtOrder.Rows[i]["CreatedDate"].ToString()) ? Convert.ToDateTime(dtOrder.Rows[i]["CreatedDate"].ToString()) : DateTime.Now;

                        orderList.Add(order);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return orderList;
        }

        public static List<UserLoginDetails> UserLoginDetails()
        {
            List<UserLoginDetails> model = new List<UserLoginDetails>();

            try
            {
                string fileds = "*";
                string tablename = "T_Login";
                string whereCondition = "Status=1";

                DataTable dtLogin = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtLogin.Rows.Count > 0)
                {
                    for (int i = 0; i < dtLogin.Rows.Count; i++)
                    {
                        UserLoginDetails login = new UserLoginDetails();

                        login.IsApproved = dtLogin.Rows[i]["IsApproved"].ToString().ToLower().Trim() == "true" ? true : false;
                        login.Status = dtLogin.Rows[i]["Status"].ToString().ToLower().Trim() == "true" ? true : false;
                        login.CreatedDate = !string.IsNullOrEmpty(dtLogin.Rows[i]["CreatedDate"].ToString()) ? Convert.ToDateTime(dtLogin.Rows[i]["CreatedDate"].ToString()) : DateTime.Now;
                        login.EmailVerified = dtLogin.Rows[i]["EmailVerified"].ToString().ToLower().Trim() == "true" ? true : false;
                        login.MobileVerified = dtLogin.Rows[i]["MobileVerified"].ToString().ToLower().Trim() == "true" ? true : false;
                        login.UserType = !string.IsNullOrEmpty(dtLogin.Rows[i]["UserType"].ToString()) ? dtLogin.Rows[i]["UserType"].ToString().ToLower().Trim() : string.Empty;

                        model.Add(login);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return model;
        }
    }
}