﻿using ecommerce.Helper.DataBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static ecommerce.Models.Frontend.CheckoutModel;

namespace ecommerce.Helper.FrontHelper
{
    public static class CheckoutHelper
    {
        public static List<CheckoutShippingAddress> GetShippingAddressDetail(int loginid, string userid, string isDefault = null)
        {
            List<CheckoutShippingAddress> resultList = new List<CheckoutShippingAddress>();

            try
            {
                string fileds = "*";
                string tablename = "T_ShippingAddress";
                string whereCondition = "Status=1";

                if (loginid > 0)
                {
                    whereCondition += " and LogId=" + loginid;
                }

                if (!string.IsNullOrEmpty(userid))
                {
                    whereCondition += " and userid='" + userid + "'";
                }

                if (!string.IsNullOrEmpty(isDefault))
                {
                    whereCondition += " and IsDefault=" + isDefault;
                }

                DataTable dtShippingAdd = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtShippingAdd != null && dtShippingAdd.Rows.Count > 0)
                {
                    for (int i = 0; i < dtShippingAdd.Rows.Count; i++)
                    {
                        CheckoutShippingAddress shipp = new CheckoutShippingAddress();

                        shipp.AddressId = Convert.ToInt32(dtShippingAdd.Rows[i]["AddressId"].ToString());
                        shipp.FirstName = dtShippingAdd.Rows[i]["FirstName"].ToString();
                        shipp.LastName = dtShippingAdd.Rows[i]["LastName"].ToString();
                        shipp.EmailId = dtShippingAdd.Rows[i]["EmailId"].ToString();
                        shipp.MobileNo = dtShippingAdd.Rows[i]["MobileNo"].ToString();
                        shipp.AlternateMobileNo = dtShippingAdd.Rows[i]["AlternateMobileNo"].ToString();
                        shipp.Address = dtShippingAdd.Rows[i]["Address"].ToString();
                        shipp.City = dtShippingAdd.Rows[i]["City"].ToString();
                        shipp.State = dtShippingAdd.Rows[i]["State"].ToString();
                        shipp.StateId = dtShippingAdd.Rows[i]["StateId"].ToString();
                        shipp.PinCode = dtShippingAdd.Rows[i]["PinCode"].ToString();
                        shipp.Landmark = dtShippingAdd.Rows[i]["Landmark"].ToString();
                        shipp.AddressType = dtShippingAdd.Rows[i]["AddressType"].ToString();
                        shipp.IsDefault = dtShippingAdd.Rows[i]["IsDefault"].ToString() == "True" ? true : false;
                        shipp.LogId = Convert.ToInt32(dtShippingAdd.Rows[i]["LogId"].ToString());
                        shipp.UserId = dtShippingAdd.Rows[i]["UserId"].ToString();
                        shipp.UserName = dtShippingAdd.Rows[i]["UserName"].ToString();
                        shipp.Status = dtShippingAdd.Rows[i]["Status"].ToString() == "True" ? true : false;
                        resultList.Add(shipp);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return resultList;
        }
        public static bool UpdateShippingAddressDetail(string addressid, int loginid)
        {
            try
            {
                if (!string.IsNullOrEmpty(addressid))
                {
                    string fieldsWithValue = "IsDefault=0";
                    string whereCondition = "LogId=" + loginid;
                    string tableName = "T_ShippingAddress";

                    if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                    {
                        string fieldsWithValue1 = "IsDefault=1";
                        string whereCondition1 = "AddressId=" + addressid + " and LogId=" + loginid;
                        string tableName1 = "T_ShippingAddress";

                        if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName1, fieldsWithValue1, whereCondition1))
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return false;
        }

        public static bool InsertBookingOrder(BookingOrder bookingOrder)
        {
            try
            {
                string fileds = "OrderNo,ProductId,ProductName,ProductCode,SubTotal,DeliveryCharges,GrandTotal,Unit,Quantity,PaymentType,DelieveryStatus,SupplierId,SupplierEmailId,SupplierName,UserId,UserEmailId,UserName,ProductImgUrl";
                string fieldValue = "'" + bookingOrder.OrderNo + "'," + bookingOrder.ProductId + ",'" + bookingOrder.ProductName + "','" + bookingOrder.ProductCode + "'," + bookingOrder.SubTotal + "," + bookingOrder.DeliveryCharges + "," + bookingOrder.GrandTotal + ",'" + bookingOrder.Unit + "'," + bookingOrder.Quantity + ",'" + bookingOrder.PaymentType + "','" + bookingOrder.DelieveryStatus + "'," + bookingOrder.SupplierId + ",'" + bookingOrder.SupplierEmailId + "','" + bookingOrder.SupplierName + "'," + bookingOrder.UserId + ",'" + bookingOrder.UserEmailId + "','" + bookingOrder.UserName + "','" + bookingOrder.ProductImgUrl + "'";
                string tableName = "T_BookingOrder";

                if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool InsertOrderDelieveryAddress(OrderDelieveryAddress delAddress)
        {
            try
            {
                string fileds = "OrderNo,UserEmailId,UserAddress,Landmark,MobileNo,City,State,PinCode,AddressType";
                string fieldValue = "'" + delAddress.OrderNo + "','" + delAddress.UserEmailId + "','" + delAddress.UserAddress + "','" + delAddress.Landmark + "','" + delAddress.MobileNo + "','" + delAddress.City + "','" + delAddress.State + "','" + delAddress.PinCode + "','" + delAddress.AddressType + "'";
                string tableName = "T_OrderDelieveryAddress";

                if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool DeleteAddToCartDetailByUserId(string userId)
        {
            try
            {
                string tableName = "T_AddToCart";
                string whereCondition = "UserId=" + userId;

                if (ConnectToDataBase.DeleteRecordFromAnyTable(tableName, whereCondition))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
    }
}