﻿using ecommerce.Helper.DataBase;
using ecommerce.Models.Frontend;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ecommerce.Helper.FrontHelper
{
    public static class ReviewHelper
    {
        public static bool InsertProductReviewDetail(ReviewModel model)
        {
            try
            {
                string fileds = "ProductId,UserName,EmailId,ReviewContent,Rating,LoginId";
                string fieldValue = "" + model.ProductId + ",'" + model.UserName + "','" + model.EmailId + "','" + model.ReviewContent + "','" + model.Rating + "','" + model.LoginId + "'";
                string tableName = "T_ProductReview";

                if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static List<ReviewModel> GetReviewDetail(string productid, string whrcon = null)
        {
            List<ReviewModel> result = new List<ReviewModel>();

            try
            {
                string fields = "pr.*, p.ProductName";
                string tablename = "T_ProductReview pr inner join T_Product p on p.ProductId=pr.ProductId";
                string whereCon = "pr.Status=1";

                if (!string.IsNullOrEmpty(productid))
                {
                    whereCon += " and pr.IsApproved=1 and pr.ProductId=" + productid;
                }

                if (!string.IsNullOrEmpty(whrcon))
                {
                    whereCon += whrcon;
                }

                DataTable dtReview = ConnectToDataBase.GetRecordFromTable(fields, tablename, whereCon);
                if (dtReview.Rows.Count > 0)
                {
                    for (int i = 0; i < dtReview.Rows.Count; i++)
                    {
                        ReviewModel item = new ReviewModel();
                        item.ReviewId = Convert.ToInt32(dtReview.Rows[i]["ReviewId"].ToString());
                        item.ProductId = Convert.ToInt32(dtReview.Rows[i]["ProductId"].ToString());
                        item.ProductName = dtReview.Rows[i]["ProductName"].ToString();
                        item.UserName = dtReview.Rows[i]["UserName"].ToString();
                        item.EmailId = dtReview.Rows[i]["EmailId"].ToString();
                        item.ReviewContent = dtReview.Rows[i]["ReviewContent"].ToString();
                        item.Rating = dtReview.Rows[i]["Rating"].ToString();
                        item.Position = dtReview.Rows[i]["Position"].ToString();
                        item.IsApproved = dtReview.Rows[i]["IsApproved"].ToString().ToLower() == "true" ? true : false;
                        item.Status = dtReview.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        item.CreatedDate = (Convert.ToDateTime(dtReview.Rows[i]["CreatedDate"].ToString())).ToString("dd MMMM yyyy");
                        result.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }

        public static bool UpdateReviewPosition(string reviewid, string posval)
        {
            try
            {
                string tableName = "T_ProductReview";
                string fieldsWithValue = "Position=" + posval;
                string whereCondition = "ReviewId=" + reviewid;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool UpdateReviewIsApproved(string reviewid, string approved)
        {
            try
            {
                string tableName = "T_ProductReview";
                string fieldsWithValue = "IsApproved=" + (approved == "yes" ? 1 : 0);
                string whereCondition = "ReviewId=" + reviewid;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
    }
}