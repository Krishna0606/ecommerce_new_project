﻿using ecommerce.Helper.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ecommerce.Models.Frontend.FrontUserEnd;

namespace ecommerce.Helper.FrontHelper
{
    public static class UserProfileHelper
    {
        public static bool InsertShippingAddressDetail(ShippingAddress shippingadd)
        {
            try
            {
                if (shippingadd != null)
                {
                    bool isSuccess = UpdateShippingDefaultAddress(shippingadd.LogId);

                    string fileds = "FirstName,LastName,EmailId,MobileNo,AlternateMobileNo,Address,City,StateId,State,PinCode,Landmark,AddressType,IsDefault,LogId,UserId,UserName";
                    string fieldValue = "'" + shippingadd.FirstName + "','" + shippingadd.LastName + "','" + shippingadd.EmailId + "','" + shippingadd.MobileNo + "','" + shippingadd.AlternateMobileNo + "','" + shippingadd.Address + "','" + shippingadd.City + "','" + shippingadd.StateId + "','" + shippingadd.State + "','" + shippingadd.PinCode + "','" + shippingadd.Landmark + "','" + shippingadd.AddressType + "','" + shippingadd.IsDefault + "','" + shippingadd.LogId + "','" + shippingadd.UserId + "','" + shippingadd.UserName + "'";
                    string tableName = "T_ShippingAddress";

                    if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }

        public static bool UpdateShippingDefaultAddress(int logId)
        {
            string fieldsWithValue = "IsDefault=0";
            string whereCondition = "LogId=" + logId;
            string tableName = "T_ShippingAddress";

            if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
            {
                return true;
            }

            return false;
        }
    }
}