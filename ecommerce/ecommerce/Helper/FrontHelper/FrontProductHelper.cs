﻿using ecommerce.Helper.DataBase;
using ecommerce.Models.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using static ecommerce.Models.Frontend.FrontHomeModel;
using static ecommerce.Models.Frontend.FrontUserEnd;
using static ecommerce.Models.FrontProductModel;

namespace ecommerce.Helper.FrontHelper
{
    public static class FrontProductHelper
    {
        public static List<FrontProduct> GetFrontProductList(string prodtid = null, string menuid = null, string catid = null, string subcatid = null, string productName = null)
        {
            List<FrontProduct> result = new List<FrontProduct>();

            try
            {
                string fileds = "p.*, l.State as SupplierStateId, l.PinCode as SupplierPinCode,l.AreaName as SupplierAreaName";
                string tablename = "T_Product p inner join T_Login l on l.LoginId=p.SupplierId";
                string whereCondition = "p.Status=1";

                if (!string.IsNullOrEmpty(prodtid))
                {
                    whereCondition += " and p.ProductId=" + prodtid;
                }

                if (!string.IsNullOrEmpty(menuid))
                {
                    whereCondition += " and p.Menu=" + menuid;
                }

                if (!string.IsNullOrEmpty(catid))
                {
                    whereCondition += " and p.Category=" + catid;
                }

                if (!string.IsNullOrEmpty(subcatid))
                {
                    whereCondition += " and p.SubCategory=" + subcatid;
                }

                if (!string.IsNullOrEmpty(productName))
                {
                    whereCondition += " and LOWER(p.ProductName) like '%" + productName.ToLower().Trim() + "%'";
                }

                DataTable dtProduct = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtProduct != null && dtProduct.Rows.Count > 0)
                {
                    for (int i = 0; i < dtProduct.Rows.Count; i++)
                    {
                        FrontProduct product = new FrontProduct();
                        product.ProductId = Convert.ToInt32(dtProduct.Rows[i]["ProductId"].ToString());
                        product.ProductName = dtProduct.Rows[i]["ProductName"].ToString();
                        product.ProductNameUrl = Regex.Replace(product.ProductName, @"[^0-9a-zA-Z]+", ",").Replace(",", "-").ToLower();
                        product.ProductCode = dtProduct.Rows[i]["ProductCode"].ToString();
                        product.Brand = Convert.ToInt32(dtProduct.Rows[i]["Brand"].ToString());
                        product.Menu = Convert.ToInt32(dtProduct.Rows[i]["Menu"].ToString());
                        product.MenuName = GetProductMenu(product.ProductId, product.Menu.ToString());
                        product.MenuUrl = Regex.Replace(product.MenuName, @"[^0-9a-zA-Z]+", ",").Replace(",", "-").ToLower();
                        product.Category = Convert.ToInt32(dtProduct.Rows[i]["Category"].ToString());
                        product.CategoryName = GetProductCategory(product.ProductId, product.Category.ToString());
                        product.CategoryNameUrl = Regex.Replace(product.CategoryName, @"[^0-9a-zA-Z]+", ",").Replace(",", "-").ToLower();
                        product.SubCategory = Convert.ToInt32(dtProduct.Rows[i]["SubCategory"].ToString());
                        product.SubCategoryName = GetProductSubCategory(product.ProductId, product.SubCategory.ToString());
                        product.SubCategoryNameUrl = Regex.Replace(product.SubCategoryName, @"[^0-9a-zA-Z]+", ",").Replace(",", "-").ToLower();
                        product.Offer = dtProduct.Rows[i]["Offer"].ToString();
                        product.PurchasePrice = Convert.ToDecimal(dtProduct.Rows[i]["PurchasePrice"].ToString());
                        product.SalePrice = Convert.ToDecimal(dtProduct.Rows[i]["SalePrice"].ToString());
                        product.Discount = !string.IsNullOrEmpty(dtProduct.Rows[i]["Discount"].ToString()) ? Convert.ToDecimal(dtProduct.Rows[i]["Discount"].ToString()) : 0;
                        product.DiscountPer = !string.IsNullOrEmpty(dtProduct.Rows[i]["DiscountPer"].ToString()) ? Convert.ToInt32(dtProduct.Rows[i]["DiscountPer"].ToString()) : 0;
                        if (product.DiscountPer > 0)
                        {
                            product.SalePriceWithDiscount = product.SalePrice - product.Discount;
                        }
                        product.Unit = Convert.ToInt32(dtProduct.Rows[i]["Unit"].ToString());
                        product.UnitName = GetProductUnit(product.ProductId, product.Unit.ToString());
                        product.DeliveryTime = dtProduct.Rows[i]["DeliveryTime"].ToString();
                        //product.Description = dtProduct.Rows[i]["Description"].ToString();
                        product.IsFeaturedProducts = dtProduct.Rows[i]["IsFeaturedProducts"].ToString().ToLower() == "true" ? true : false;
                        product.SupplierId = Convert.ToInt32(dtProduct.Rows[i]["SupplierId"].ToString());
                        product.SupplierLoginId = dtProduct.Rows[i]["SupplierLoginId"].ToString();
                        product.SupplierName = dtProduct.Rows[i]["SupplierName"].ToString();
                        product.SupplierAreaName = !string.IsNullOrEmpty(dtProduct.Rows[i]["SupplierAreaName"].ToString()) ? dtProduct.Rows[i]["SupplierAreaName"].ToString() : string.Empty;
                        product.Status = dtProduct.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        product.FrontProductDescription = GetProductDescription(product.ProductId);
                        product.FrontProductImage = GetProductImages(product.ProductId);
                        product.RedirectUrl = "/pd/" + product.ProductId + "/" + product.ProductNameUrl + "/" + product.MenuUrl + "/" + product.Menu;

                        if (product.Category > 0)
                        {
                            product.RedirectUrl = product.RedirectUrl + "/" + product.CategoryNameUrl + "/" + product.Category;
                        }

                        if (product.SubCategory > 0)
                        {
                            product.RedirectUrl = product.RedirectUrl + "/" + product.SubCategoryNameUrl + "/" + product.SubCategory;
                        }

                        product.SupplierStateId = dtProduct.Rows[i]["SupplierStateId"].ToString();
                        product.SupplierPinCode = dtProduct.Rows[i]["SupplierPinCode"].ToString();
                        result.Add(product);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }


            return result;
        }

        public static DataTable GetMenuCatSCatDetails(string menuid = null, string catid = null, string subcatid = null)
        {
            DataTable dtMCSC = new DataTable();
            try
            {
                string fileds = string.Empty;
                string tablename = string.Empty;
                string whereCondition = string.Empty;

                if (!string.IsNullOrEmpty(menuid))
                {
                    fileds = "Menuid,MenuName";
                    tablename = "T_Menu";
                    whereCondition = "Menuid=" + menuid;
                }

                if (!string.IsNullOrEmpty(catid))
                {
                    fileds = "CategoryId,CategoryName";
                    tablename = "T_Category";
                    whereCondition = "CategoryId=" + catid;
                }

                if (!string.IsNullOrEmpty(subcatid))
                {
                    fileds = "SubCategoryId,SubCategoryName";
                    tablename = "T_SubCategory";
                    whereCondition = "SubCategoryId=" + subcatid;
                }

                dtMCSC = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return dtMCSC;
        }
        public static FrontProductDescription GetProductDescription(int productid)
        {
            FrontProductDescription decResult = new FrontProductDescription();

            try
            {
                string fileds = "*";
                string tablename = "T_ProductDescription";
                string whereCondition = "Status=1 and ProductId=" + productid;

                DataTable dtProduct = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtProduct != null && dtProduct.Rows.Count > 0)
                {
                    decResult.Descripion1 = dtProduct.Rows[0]["Description1"].ToString();
                    decResult.Descripion2 = dtProduct.Rows[0]["Description2"].ToString();
                    decResult.Descripion3 = dtProduct.Rows[0]["Description3"].ToString();
                    decResult.Descripion4 = dtProduct.Rows[0]["Description4"].ToString();
                    decResult.Descripion5 = dtProduct.Rows[0]["Description5"].ToString();
                }
            }
            catch (Exception)
            {

                throw;
            }

            return decResult;
        }
        public static List<FrontProductImage> GetProductImages(int productid)
        {
            List<FrontProductImage> imgResult = new List<FrontProductImage>();

            try
            {
                string fileds = "*";
                string tablename = "T_ProductImages";
                string whereCondition = "Status=1 and ProductId=" + productid;

                DataTable dtImage = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtImage != null && dtImage.Rows.Count > 0)
                {
                    for (int i = 0; i < dtImage.Rows.Count; i++)
                    {
                        FrontProductImage img = new FrontProductImage();
                        img.ImageId = Convert.ToInt32(dtImage.Rows[i]["ImageId"].ToString());
                        img.ImageName = dtImage.Rows[i]["ImageName"].ToString();
                        img.ImageUrl = Utility.GetImagePath(dtImage.Rows[i]["ImageUrl"].ToString(), "ProductImages", "Product_" + productid);
                        imgResult.Add(img);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return imgResult;
        }
        public static string GetProductMenu(int productid, string menuid = null)
        {
            string result = string.Empty;

            try
            {
                string fileds = "*";
                string tablename = "T_Menu";
                string whereCondition = "Status=1";
                if (!string.IsNullOrEmpty(menuid))
                {
                    whereCondition += " and Menuid =" + menuid;
                }
                else
                {
                    whereCondition += " and Menuid = (select Menu from T_Product where ProductId=" + productid + " and Status=1)";
                }

                DataTable dtCategory = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtCategory != null && dtCategory.Rows.Count > 0)
                {
                    for (int i = 0; i < dtCategory.Rows.Count; i++)
                    {
                        //UnitModel unit = new UnitModel();
                        //unit.Unitid = Convert.ToInt32(dtImage.Rows[i]["ImageId"].ToString());
                        result = !string.IsNullOrEmpty(dtCategory.Rows[i]["MenuName"].ToString()) ? dtCategory.Rows[i]["MenuName"].ToString() : string.Empty;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }
        public static string GetProductCategory(int productid, string catid = null)
        {
            string result = string.Empty;

            try
            {
                string fileds = "*";
                string tablename = "T_Category";
                string whereCondition = "Status=1";
                if (!string.IsNullOrEmpty(catid))
                {
                    whereCondition += " and CategoryId =" + catid;
                }
                else
                {
                    whereCondition += " and CategoryId = (select Category from T_Product where ProductId=" + productid + " and Status=1)";
                }

                DataTable dtCategory = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtCategory != null && dtCategory.Rows.Count > 0)
                {
                    for (int i = 0; i < dtCategory.Rows.Count; i++)
                    {
                        //UnitModel unit = new UnitModel();
                        //unit.Unitid = Convert.ToInt32(dtImage.Rows[i]["ImageId"].ToString());
                        result = !string.IsNullOrEmpty(dtCategory.Rows[i]["CategoryName"].ToString()) ? dtCategory.Rows[i]["CategoryName"].ToString() : string.Empty;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }
        public static string GetProductSubCategory(int productid, string subcatid = null)
        {
            string result = string.Empty;

            try
            {
                string fileds = "*";
                string tablename = "T_SubCategory";
                string whereCondition = "Status=1";
                if (!string.IsNullOrEmpty(subcatid))
                {
                    whereCondition += " and SubCategoryId =" + subcatid;
                }
                else
                {
                    whereCondition += " and SubCategoryId = (select SubCategory from T_Product where ProductId=" + productid + " and Status=1)";
                }

                DataTable dtSubCategory = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtSubCategory != null && dtSubCategory.Rows.Count > 0)
                {
                    for (int i = 0; i < dtSubCategory.Rows.Count; i++)
                    {
                        //UnitModel unit = new UnitModel();
                        //unit.Unitid = Convert.ToInt32(dtImage.Rows[i]["ImageId"].ToString());
                        result = !string.IsNullOrEmpty(dtSubCategory.Rows[i]["SubCategoryName"].ToString()) ? dtSubCategory.Rows[i]["SubCategoryName"].ToString() : string.Empty;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }
        public static string GetProductUnit(int productid, string unitid = null)
        {
            string result = string.Empty;

            try
            {
                string fileds = "*";
                string tablename = "T_Unit";
                string whereCondition = "Status=1";
                if (!string.IsNullOrEmpty(unitid))
                {
                    whereCondition += " and Unitid =" + unitid;
                }
                else
                {
                    whereCondition += " and Unitid = (select Unit from T_Product where ProductId=" + productid + " and Status=1)";
                }

                DataTable dtUnit = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtUnit != null && dtUnit.Rows.Count > 0)
                {
                    for (int i = 0; i < dtUnit.Rows.Count; i++)
                    {
                        //UnitModel unit = new UnitModel();
                        //unit.Unitid = Convert.ToInt32(dtImage.Rows[i]["ImageId"].ToString());
                        result = !string.IsNullOrEmpty(dtUnit.Rows[i]["Unit"].ToString()) ? dtUnit.Rows[i]["Unit"].ToString() : string.Empty;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }
        public static List<AddToCart> CheckDetailsInCart(string userid = null)
        {
            List<AddToCart> cart = new List<AddToCart>();

            try
            {
                string fileds = "*";
                string tablename = "T_AddToCart";
                string whereCondition = "Status=1 and UserId=" + userid;

                DataTable dtCart = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtCart != null && dtCart.Rows.Count > 0)
                {
                    for (int i = 0; i < dtCart.Rows.Count; i++)
                    {
                        AddToCart itmcart = new AddToCart();
                        itmcart.CartId = Convert.ToInt32(dtCart.Rows[i]["CartId"].ToString());
                        itmcart.ProductId = Convert.ToInt32(dtCart.Rows[i]["ProductId"].ToString());
                        itmcart.ProductName = dtCart.Rows[i]["ProductName"].ToString();
                        itmcart.ProductCode = dtCart.Rows[i]["ProductCode"].ToString();
                        itmcart.ProductImgUrl = dtCart.Rows[i]["ProductImgUrl"].ToString();
                        itmcart.SalePrice = dtCart.Rows[i]["SalePrice"].ToString();
                        itmcart.Discount = !string.IsNullOrEmpty(dtCart.Rows[i]["Discount"].ToString()) ? Convert.ToInt32(dtCart.Rows[i]["Discount"].ToString()) : 0;
                        itmcart.DiscountPrice = dtCart.Rows[i]["DiscountPrice"].ToString();
                        itmcart.UnitId = Convert.ToInt32(dtCart.Rows[i]["UnitId"].ToString());
                        itmcart.UnitName = dtCart.Rows[i]["UnitName"].ToString();
                        itmcart.UserId = Convert.ToInt32(dtCart.Rows[i]["UserId"].ToString());
                        itmcart.UserLoginId = dtCart.Rows[i]["UserLoginId"].ToString();
                        itmcart.UserName = dtCart.Rows[i]["UserName"].ToString();
                        itmcart.Quantity = !string.IsNullOrEmpty(dtCart.Rows[i]["Quantity"].ToString()) ? Convert.ToInt32(dtCart.Rows[i]["Quantity"].ToString()) : 0;
                        itmcart.SupplierId = !string.IsNullOrEmpty(dtCart.Rows[i]["SupplierId"].ToString()) ? Convert.ToInt32(dtCart.Rows[i]["SupplierId"].ToString()) : 0;
                        itmcart.SupplierEmail = !string.IsNullOrEmpty(dtCart.Rows[i]["SupplierEmailId"].ToString()) ? dtCart.Rows[i]["SupplierEmailId"].ToString() : string.Empty;
                        itmcart.SupplierName = !string.IsNullOrEmpty(dtCart.Rows[i]["SupplierName"].ToString()) ? dtCart.Rows[i]["SupplierName"].ToString() : string.Empty;
                        cart.Add(itmcart);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return cart;
        }
        public static bool InsertDetailsAddToCart(AddToCart addcart)
        {
            try
            {
                //if (!string.IsNullOrEmpty(addcart.Discount) && addcart.Discount != "0.00")
                //{
                //    addcart.DiscountPrice = String.Format("{0:0.00}", ((Convert.ToDecimal(addcart.SalePrice) * Convert.ToDecimal(addcart.Discount)) / 100));
                //}

                List<int> existing = CheckProductExistInCart(addcart.ProductId, addcart.UserId);

                if (existing.Count == 0)
                {
                    List<string> productDetail = GetSupplierDelByProductId(addcart.ProductId.ToString());

                    string fileds = "ProductId,ProductName,ProductImgUrl,SalePrice,Discount,DiscountPrice,UnitId,UnitName,UserId,UserLoginId,UserName,Quantity,SupplierId,ProductCode,SupplierName,SupplierEmailId";
                    string fieldValue = "" + addcart.ProductId + ",'" + addcart.ProductName + "','" + addcart.ProductImgUrl + "','" + addcart.SalePrice + "','" + addcart.Discount + "','" + addcart.DiscountPrice + "'," + addcart.UnitId + ",'" + addcart.UnitName + "'," + addcart.UserId + ",'" + addcart.UserLoginId + "','" + addcart.UserName + "'," + addcart.Quantity + "," + productDetail[0] + ",'" + productDetail[1] + "','" + productDetail[2] + "','" + productDetail[3] + "'";
                    string tableName = "T_AddToCart";

                    if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName))
                    {
                        return true;
                    }
                }
                else
                {
                    string tableName = "T_AddToCart";
                    string fieldsWithValue = "Quantity=" + (existing[1] + addcart.Quantity);
                    string whereCondition = "CartId=" + existing[0];

                    if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }


            return false;
        }
        public static List<string> GetSupplierDelByProductId(string productid)
        {
            List<string> result = new List<string>();

            try
            {
                string fileds = "SupplierId,ProductCode";
                string tablename = "T_Product";
                string whereCondition = "Status=1";

                if (!string.IsNullOrEmpty(productid))
                {
                    whereCondition += " and ProductId=" + productid;
                }

                DataTable dtProduct = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtProduct.Rows.Count > 0)
                {
                    string supplierid = !string.IsNullOrEmpty(dtProduct.Rows[0]["SupplierId"].ToString()) ? dtProduct.Rows[0]["SupplierId"].ToString() : "";

                    result.Add(supplierid);
                    result.Add(!string.IsNullOrEmpty(dtProduct.Rows[0]["ProductCode"].ToString()) ? dtProduct.Rows[0]["ProductCode"].ToString() : "");

                    List<string> strSupplier = GetLoginDelByLoginId(supplierid);
                    if (strSupplier.Count > 0)
                    {
                        result.Add(strSupplier[0] + " " + strSupplier[1]);
                        result.Add(strSupplier[2]);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        //public static string GetImageByProductId(string productId)
        //{
        //    string result = string.Empty;

        //    try
        //    {
        //        string fileds = "ImageUrl";
        //        string tablename = "T_ProductImages";
        //        string whereCondition = "Status=1";

        //        if (!string.IsNullOrEmpty(productId))
        //        {
        //            whereCondition += " and ProductId=" + productId;
        //        }

        //        DataTable dtProduct = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
        //        if (dtProduct.Rows.Count > 0)
        //        {
        //            string imageUrl = !string.IsNullOrEmpty(dtProduct.Rows[0]["ImageUrl"].ToString()) ? dtProduct.Rows[0]["ImageUrl"].ToString() : string.Empty;

        //            if (!string.IsNullOrEmpty(imageUrl))
        //            {
        //                result = Utility.GetImagePath(imageUrl, "ProductImages", "Product_" + productId);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        throw;
        //    }

        //    return result;
        //}
        public static List<string> GetSupplierDelBySupplierId(string supplierId)
        {
            List<string> result = new List<string>();

            try
            {
                string fileds = "FirstName,LastName,EmailId";
                string tablename = "T_Login";
                string whereCondition = "Status=1 and UserType='supplier'";

                if (!string.IsNullOrEmpty(supplierId))
                {
                    whereCondition += " and LoginId=" + supplierId;
                }

                DataTable dtProduct = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtProduct.Rows.Count > 0)
                {
                    result.Add(!string.IsNullOrEmpty(dtProduct.Rows[0]["FirstName"].ToString()) ? dtProduct.Rows[0]["FirstName"].ToString() : "");
                    result.Add(!string.IsNullOrEmpty(dtProduct.Rows[0]["LastName"].ToString()) ? dtProduct.Rows[0]["LastName"].ToString() : "");
                    result.Add(!string.IsNullOrEmpty(dtProduct.Rows[0]["EmailId"].ToString()) ? dtProduct.Rows[0]["EmailId"].ToString() : "");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }

        public static List<string> GetLoginDelByLoginId(string loginId)
        {
            List<string> result = new List<string>();

            try
            {
                string fileds = "FirstName,LastName,EmailId";
                string tablename = "T_Login";
                string whereCondition = "Status=1";

                if (!string.IsNullOrEmpty(loginId))
                {
                    whereCondition += " and LoginId=" + loginId;
                }

                DataTable dtProduct = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtProduct.Rows.Count > 0)
                {
                    result.Add(!string.IsNullOrEmpty(dtProduct.Rows[0]["FirstName"].ToString()) ? dtProduct.Rows[0]["FirstName"].ToString() : "");
                    result.Add(!string.IsNullOrEmpty(dtProduct.Rows[0]["LastName"].ToString()) ? dtProduct.Rows[0]["LastName"].ToString() : "");
                    result.Add(!string.IsNullOrEmpty(dtProduct.Rows[0]["EmailId"].ToString()) ? dtProduct.Rows[0]["EmailId"].ToString() : "");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static List<int> CheckProductExistInCart(int proid, int userid)
        {
            List<int> result = new List<int>();

            try
            {
                string fileds = "*";
                string tablename = "T_AddToCart";
                string whereCondition = "ProductId=" + proid + " and UserId=" + userid;

                DataTable dtCartDel = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);

                if (dtCartDel.Rows.Count > 0)
                {
                    result.Add(Convert.ToInt32(dtCartDel.Rows[0]["CartId"].ToString()));
                    result.Add(Convert.ToInt32(dtCartDel.Rows[0]["Quantity"].ToString()));
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static bool UpdateDetailsAddToCartById(string cartid, string quantity)
        {
            string tableName = "T_AddToCart";
            string fieldsWithValue = "Quantity=" + quantity;
            string whereCondition = "CartId=" + cartid;

            if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
            {
                return true;
            }

            return false;
        }

        public static bool DeleteDetailsAddToCartById(string cartid)
        {
            try
            {
                string tableName = "T_AddToCart";
                string whereCondition = "CartId=" + cartid;

                if (ConnectToDataBase.DeleteRecordFromAnyTable(tableName, whereCondition))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static Dictionary<int, string> GetFeaturedMenuList()
        {
            Dictionary<int, string> dicMenu = new Dictionary<int, string>();

            try
            {
                string fileds = "Menuid,MenuName";
                string tablename = "T_Menu";
                string whereCondition = "IsFeatured=1 and Status=1";

                DataTable dtMenu = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtMenu != null && dtMenu.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMenu.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(dtMenu.Rows[i]["Menuid"].ToString()))
                        {
                            dicMenu.Add(Convert.ToInt32(dtMenu.Rows[i]["Menuid"].ToString()), dtMenu.Rows[i]["MenuName"].ToString());
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return dicMenu;
        }
        public static List<FrontHomeProduct> GetFrontProductByMenuIdList(string menuid, string pincode = null)
        {
            List<FrontHomeProduct> result = new List<FrontHomeProduct>();

            try
            {
                if (!string.IsNullOrEmpty(menuid))
                {
                    string fileds = "p.ProductId,p.ProductName,p.ProductCode,p.Brand,p.Menu,p.Category,p.SubCategory,p.Unit,p.Offer,p.PurchasePrice,p.SalePrice,p.Discount,p.DiscountPer,p.IsFeaturedProducts,p.SupplierId,p.SupplierLoginId,p.SupplierName,l.State as SupplierStateId,l.PinCode as SupplierPinCode,p.Status,c.CategoryName,sc.SubCategoryName ";
                    string tablename = "T_Product p inner join T_Login l on l.LoginId=p.SupplierId  left join T_Category c on c.CategoryId=p.Category left join T_SubCategory sc on sc.SubCategoryId=p.SubCategory";
                    string whereCondition = "p.Status=1 and p.IsFeaturedProducts=1";

                    if (!string.IsNullOrEmpty(menuid))
                    {
                        whereCondition += " and p.Menu=" + menuid;
                    }

                    if (!string.IsNullOrEmpty(pincode))
                    {
                        whereCondition += " and l.PinCode=" + pincode;
                    }

                    DataTable dtProduct = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                    if (dtProduct != null && dtProduct.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtProduct.Rows.Count; i++)
                        {
                            FrontHomeProduct product = new FrontHomeProduct();
                            product.ProductId = Convert.ToInt32(dtProduct.Rows[i]["ProductId"].ToString());
                            product.ProductName = dtProduct.Rows[i]["ProductName"].ToString();
                            product.ProductUrl = Regex.Replace(product.ProductName, @"[^0-9a-zA-Z]+", ",").Replace(",", "-").ToLower();
                            product.ProductCode = dtProduct.Rows[i]["ProductCode"].ToString();
                            product.Brand = Convert.ToInt32(dtProduct.Rows[i]["Brand"].ToString());
                            product.UnitId = Convert.ToInt32(dtProduct.Rows[i]["Unit"].ToString());
                            product.UnitName = GetProductUnit(product.ProductId, product.UnitId.ToString());
                            product.Menu = Convert.ToInt32(dtProduct.Rows[i]["Menu"].ToString());
                            product.MenuUrl = Regex.Replace(GetProductMenu(product.ProductId, product.Menu.ToString()), @"[^0-9a-zA-Z]+", ",").Replace(",", "-").ToLower();
                            product.Category = Convert.ToInt32(dtProduct.Rows[i]["Category"].ToString());
                            product.CategoryUrl = Regex.Replace(GetProductCategory(product.ProductId, product.Category.ToString()), @"[^0-9a-zA-Z]+", ",").Replace(",", "-").ToLower();
                            product.SubCategory = Convert.ToInt32(dtProduct.Rows[i]["SubCategory"].ToString());
                            product.SubCategoryUrl = Regex.Replace(GetProductSubCategory(product.ProductId, product.SubCategory.ToString()), @"[^0-9a-zA-Z]+", ",").Replace(",", "-").ToLower();
                            product.CategoryName = dtProduct.Rows[i]["CategoryName"].ToString();
                            product.SubCategoryName = dtProduct.Rows[i]["SubCategoryName"].ToString();
                            product.Offer = dtProduct.Rows[i]["Offer"].ToString();
                            product.PurchasePrice = Convert.ToDecimal(dtProduct.Rows[i]["PurchasePrice"].ToString());
                            product.SalePrice = Convert.ToDecimal(dtProduct.Rows[i]["SalePrice"].ToString());
                            product.DiscountPer = !string.IsNullOrEmpty(dtProduct.Rows[i]["DiscountPer"].ToString()) ? Convert.ToInt32(dtProduct.Rows[i]["DiscountPer"].ToString()) : 0;
                            product.Discount = !string.IsNullOrEmpty(dtProduct.Rows[i]["Discount"].ToString()) ? Convert.ToDecimal(dtProduct.Rows[i]["Discount"].ToString()) : 0;
                            if (product.DiscountPer > 0)
                            {
                                product.SalePriceWithDiscount = product.SalePrice - product.Discount;
                            }
                            product.IsFeaturedProducts = dtProduct.Rows[i]["IsFeaturedProducts"].ToString().ToLower() == "true" ? true : false;
                            product.SupplierId = Convert.ToInt32(dtProduct.Rows[i]["SupplierId"].ToString());
                            product.SupplierLoginId = dtProduct.Rows[i]["SupplierLoginId"].ToString();
                            product.SupplierName = dtProduct.Rows[i]["SupplierName"].ToString();
                            product.Status = dtProduct.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                            product.FrontProductImage = GetProductImages(product.ProductId);
                            product.RedirectUrl = "/pd/" + product.ProductId + "/" + product.ProductUrl + "/" + product.MenuUrl + "/" + product.Menu + "/" + product.CategoryUrl + "/" + product.Category + "/" + product.SubCategoryUrl + "/" + product.SubCategory + "";
                            result.Add(product);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static List<OrderDetail> GetorderList(string userid)
        {
            List<OrderDetail> result = new List<OrderDetail>();

            try
            {
                string fileds = "*";
                string tablename = "T_BookingOrder";
                string whereCondition = "userid=" + userid + " order by OrderId desc";
                DataTable dtorder = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtorder != null && dtorder.Rows.Count > 0)
                {
                    for (int i = 0; i < dtorder.Rows.Count; i++)
                    {
                        OrderDetail model = new OrderDetail();
                        model.Orderid = Convert.ToInt32(dtorder.Rows[i]["OrderId"].ToString());
                        model.Productid = Convert.ToInt32(dtorder.Rows[i]["ProductId"].ToString());
                        model.ProductName = dtorder.Rows[i]["ProductName"].ToString();
                        model.ProductCode = dtorder.Rows[i]["ProductCode"].ToString();
                        model.Unit = dtorder.Rows[i]["Unit"].ToString();
                        model.SupplierName = dtorder.Rows[i]["SupplierName"].ToString();
                        model.DeliverStatus = dtorder.Rows[i]["DelieveryStatus"].ToString();
                        model.ProductImgUrl = dtorder.Rows[i]["ProductImgUrl"].ToString();
                        model.GrandTotal = Convert.ToDecimal(dtorder.Rows[i]["GrandTotal"].ToString());
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }
        public static List<OrderDetail> GetorderListbyOrderid(string orderid)
        {
            List<OrderDetail> result = new List<OrderDetail>();

            try
            {
                string fileds = "*";
                string tablename = "T_BookingOrder";
                string whereCondition = "orderNo='" + orderid + "'";
                DataTable dtorder = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtorder != null && dtorder.Rows.Count > 0)
                {
                    for (int i = 0; i < dtorder.Rows.Count; i++)
                    {
                        OrderDetail model = new OrderDetail();
                        model.Orderid = Convert.ToInt32(dtorder.Rows[i]["OrderId"].ToString());
                        model.Supppierid = Convert.ToInt32(dtorder.Rows[i]["SupplierId"].ToString());
                        model.Productid = Convert.ToInt32(dtorder.Rows[i]["ProductId"].ToString());
                        model.Quantity = Convert.ToInt32(dtorder.Rows[i]["Quantity"].ToString());
                        model.ProductName = dtorder.Rows[i]["ProductName"].ToString();
                        model.ProductCode = dtorder.Rows[i]["ProductCode"].ToString();
                        model.Unit = dtorder.Rows[i]["Unit"].ToString();
                        model.UserName = dtorder.Rows[i]["UserName"].ToString();
                        model.UserId = Convert.ToInt32(dtorder.Rows[i]["UserId"].ToString());
                        model.SupplierName = dtorder.Rows[i]["SupplierName"].ToString();
                        model.SupplierEmail = dtorder.Rows[i]["SupplierEmailId"].ToString();
                        model.DeliverStatus = dtorder.Rows[i]["DelieveryStatus"].ToString();
                        model.ProductImgUrl = dtorder.Rows[i]["ProductImgUrl"].ToString();
                        model.GrandTotal = Convert.ToDecimal(dtorder.Rows[i]["GrandTotal"].ToString());
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }
        public static List<OrderDetail> GetorderDetails(string productid)
        {
            List<OrderDetail> result = new List<OrderDetail>();

            try
            {
                string fileds = "b.OrderNo,b.Productid,b.ProductName,b.UserName,b.Productcode,b.SubTotal,b.DeliveryCharges,b.GrandTotal,b.Unit,b.DelieveryStatus,b.SupplierName,b.ProductImgUrl,s.UserAddress,s.UserEmailid,s.Landmark,s.MobileNo,s.City,s.State,s.Pincode";
                string tablename = "T_BookingOrder b INNER JOIN T_OrderDelieveryAddress s ON b.OrderNo = s.OrderNo";
                string whereCondition = "Productid=" + productid + "";
                DataTable dtorder = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtorder != null && dtorder.Rows.Count > 0)
                {
                    for (int i = 0; i < dtorder.Rows.Count; i++)
                    {
                        OrderDetail model = new OrderDetail();
                        model.OederNo = dtorder.Rows[i]["OrderNo"].ToString();
                        model.Productid = Convert.ToInt32(dtorder.Rows[i]["ProductId"].ToString());
                        model.ProductName = dtorder.Rows[i]["ProductName"].ToString();
                        model.ProductCode = dtorder.Rows[i]["ProductCode"].ToString();
                        model.Unit = dtorder.Rows[i]["Unit"].ToString();
                        model.SupplierName = dtorder.Rows[i]["SupplierName"].ToString();
                        model.DeliverStatus = dtorder.Rows[i]["DelieveryStatus"].ToString();
                        model.ProductImgUrl = dtorder.Rows[i]["ProductImgUrl"].ToString();
                        model.UserAddress = dtorder.Rows[i]["UserAddress"].ToString();
                        model.UserEmailid = dtorder.Rows[i]["UserEmailid"].ToString();
                        model.Landmark = dtorder.Rows[i]["Landmark"].ToString();
                        model.UserMobile = dtorder.Rows[i]["MobileNo"].ToString();
                        model.City = dtorder.Rows[i]["City"].ToString();
                        model.State = dtorder.Rows[i]["State"].ToString();
                        model.pincode = dtorder.Rows[i]["Pincode"].ToString();
                        model.UserName = dtorder.Rows[i]["UserName"].ToString();
                        model.GrandTotal = Convert.ToDecimal(dtorder.Rows[i]["GrandTotal"].ToString());
                        model.DeliverCharges = Convert.ToDecimal(dtorder.Rows[i]["DeliveryCharges"].ToString());
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }

        public static OrderDetail GetSupplirDetialbyid(string suppiorid)
        {
            OrderDetail model = new OrderDetail();

            try
            {
                string fileds = "*";
                string tablename = "T_Login";
                string whereCondition = "status=1 and loginid=" + suppiorid + "";

                DataTable dtsupplior = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtsupplior != null && dtsupplior.Rows.Count > 0)
                {
                    for (int i = 0; i < dtsupplior.Rows.Count; i++)
                    {
                        model.SupplierMobileNo = !string.IsNullOrEmpty(dtsupplior.Rows[i]["MobileNo"].ToString()) ? dtsupplior.Rows[i]["MobileNo"].ToString() : string.Empty;
                        model.SupplierCity = !string.IsNullOrEmpty(dtsupplior.Rows[i]["City"].ToString()) ? dtsupplior.Rows[i]["City"].ToString() : string.Empty;
                        model.SupplierPin = !string.IsNullOrEmpty(dtsupplior.Rows[i]["PinCode"].ToString()) ? dtsupplior.Rows[i]["PinCode"].ToString() : string.Empty;
                        model.SupplierAddress = !string.IsNullOrEmpty(dtsupplior.Rows[i]["Address"].ToString()) ? dtsupplior.Rows[i]["Address"].ToString() : string.Empty;
                        model.SupplierPanNo = !string.IsNullOrEmpty(dtsupplior.Rows[i]["PanNo"].ToString()) ? dtsupplior.Rows[i]["PanNo"].ToString() : string.Empty;
                        model.SupplierGstNo = !string.IsNullOrEmpty(dtsupplior.Rows[i]["GSTNo"].ToString()) ? dtsupplior.Rows[i]["GSTNo"].ToString() : string.Empty;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return model;
        }
    }
}