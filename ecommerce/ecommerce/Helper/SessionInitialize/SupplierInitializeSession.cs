﻿using ecommerce.Models.Backend;
using ecommerce.Models.Frontend;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ecommerce.Helper.SessionInitialize
{
    public static class SupplierInitializeSession
    {
        public static void InitializeBackendLoginSession(DataTable dtSuplier, bool isWelcome = false)
        {
            if (dtSuplier != null && dtSuplier.Rows.Count > 0)
            {
                BackendLogin login = new BackendLogin();
                login.LoginId = Convert.ToInt32(dtSuplier.Rows[0]["LoginId"].ToString());
                login.Title = dtSuplier.Rows[0]["Title"].ToString();
                login.FirstName = dtSuplier.Rows[0]["FirstName"].ToString();
                login.LastName = dtSuplier.Rows[0]["LastName"].ToString();
                login.EmailId = dtSuplier.Rows[0]["EmailId"].ToString();
                login.MobileNo = dtSuplier.Rows[0]["MobileNo"].ToString();
                login.CountryId = !string.IsNullOrEmpty(dtSuplier.Rows[0]["Country"].ToString()) ? Convert.ToInt32(dtSuplier.Rows[0]["Country"].ToString()) : 0;
                login.StateId = !string.IsNullOrEmpty(dtSuplier.Rows[0]["State"].ToString()) ? Convert.ToInt32(dtSuplier.Rows[0]["State"].ToString()) : 0;
                login.CityId = !string.IsNullOrEmpty(dtSuplier.Rows[0]["City"].ToString()) ? dtSuplier.Rows[0]["City"].ToString() : "0";
                login.PinCode = dtSuplier.Rows[0]["PinCode"].ToString();
                login.Address = dtSuplier.Rows[0]["Address"].ToString();
                login.ProfileImage = dtSuplier.Rows[0]["ProfileImage"].ToString();
                login.Profession = dtSuplier.Rows[0]["Profession"].ToString();
                login.DOB = dtSuplier.Rows[0]["DOB"].ToString();
                login.AddharNo = dtSuplier.Rows[0]["AddharNo"].ToString();
                login.PanNo = dtSuplier.Rows[0]["PanNo"].ToString();
                login.GSTNo = dtSuplier.Rows[0]["GSTNo"].ToString();
                login.AddharImage = dtSuplier.Rows[0]["AddharImage"].ToString();
                login.PanImage = dtSuplier.Rows[0]["PanImage"].ToString();
                login.BankName = dtSuplier.Rows[0]["BankName"].ToString();
                login.AccountNo = dtSuplier.Rows[0]["AccountNo"].ToString();
                login.BranchName = dtSuplier.Rows[0]["BranchName"].ToString();
                login.BrnachCode = dtSuplier.Rows[0]["BrnachCode"].ToString();
                login.IFSCCode = dtSuplier.Rows[0]["IFSCCode"].ToString();
                login.BankAddress = dtSuplier.Rows[0]["BankAddress"].ToString();
                login.PayeeName = dtSuplier.Rows[0]["PayeeName"].ToString();
                login.UserId = dtSuplier.Rows[0]["UserId"].ToString();
                login.Password = dtSuplier.Rows[0]["Password"].ToString();
                login.UserType = dtSuplier.Rows[0]["UserType"].ToString();
                login.IsApproved = dtSuplier.Rows[0]["IsApproved"].ToString().ToLower() == "true" ? true : false;
                login.Status = dtSuplier.Rows[0]["Status"].ToString().ToLower() == "true" ? true : false;
                login.IsShowAll = login.UserType.ToLower() == "admin" ? true : false;
                login.IsWelcome = isWelcome;
                login.SupplierName = dtSuplier.Rows[0]["SupplierName"].ToString();
                HttpContext.Current.Session["supplier"] = login;


                //login.Title = dtSuplier.Rows[0]["Title"].ToString();
                //login.FirstName = dtSuplier.Rows[0]["FirstName"].ToString();
                //login.LastName = dtSuplier.Rows[0]["LastName"].ToString();
                //login.UserId = dtSuplier.Rows[0]["UserId"].ToString();
                //login.Password = dtSuplier.Rows[0]["Password"].ToString();
                //login.EmailId = dtSuplier.Rows[0]["EmailId"].ToString();
                //login.MobileNo = dtSuplier.Rows[0]["MobileNo"].ToString();
                //login.BusinessEmailId = dtSuplier.Rows[0]["BusinessEmailId"].ToString();
                //login.BusinessMobileNo = dtSuplier.Rows[0]["BusinessMobileNo"].ToString();
                //login.ProfileImage = dtSuplier.Rows[0]["ProfileImage"].ToString();
                //login.Country = !string.IsNullOrEmpty(dtSuplier.Rows[0]["Country"].ToString()) ? Convert.ToInt32(dtSuplier.Rows[0]["Country"].ToString()) : 0;
                //login.State = !string.IsNullOrEmpty(dtSuplier.Rows[0]["State"].ToString()) ? Convert.ToInt32(dtSuplier.Rows[0]["State"].ToString()) : 0;
                //login.City = dtSuplier.Rows[0]["City"].ToString();
                //login.PinCode = dtSuplier.Rows[0]["PinCode"].ToString();
                //login.UserType = dtSuplier.Rows[0]["UserType"].ToString();
                //login.GSTNo = dtSuplier.Rows[0]["GSTNo"].ToString();
                //login.IsApproved = dtSuplier.Rows[0]["IsApproved"].ToString().ToLower() == "true" ? true : false;
                //login.Status = dtSuplier.Rows[0]["Status"].ToString().ToLower() == "true" ? true : false;
                //login.IsWelcome = isWelcome;
                //HttpContext.Current.Session["supplier"] = login;
            }
        }
        public static void ReInitializeBackendLoginSession(bool isWelcome)
        {
            BackendLogin olu = new BackendLogin();
            olu = (BackendLogin)HttpContext.Current.Session["supplier"];
            olu.IsWelcome = isWelcome;
            HttpContext.Current.Session["supplier"] = olu;
        }
        public static void InitializeFrontendLoginSession(DataTable dtSuplier, bool isWelcome = false)
        {
            if (dtSuplier != null && dtSuplier.Rows.Count > 0)
            {
                FrontEndLogin login = new FrontEndLogin();
                login.LoginId = Convert.ToInt32(dtSuplier.Rows[0]["LoginId"].ToString());                
                login.FirstName = dtSuplier.Rows[0]["FirstName"].ToString();
                login.LastName = dtSuplier.Rows[0]["LastName"].ToString();
                login.EmailId = dtSuplier.Rows[0]["EmailId"].ToString();
                login.MobileNo = dtSuplier.Rows[0]["MobileNo"].ToString();                
                login.UserId = dtSuplier.Rows[0]["UserId"].ToString();
                login.Password = dtSuplier.Rows[0]["Password"].ToString();
                login.UserType = dtSuplier.Rows[0]["UserType"].ToString();
                login.IsApproved = dtSuplier.Rows[0]["IsApproved"].ToString().ToLower() == "true" ? true : false;
                login.Status = dtSuplier.Rows[0]["Status"].ToString().ToLower() == "true" ? true : false;                
                login.IsWelcome = isWelcome;                
                HttpContext.Current.Session["frontuser"] = login;
            }
        }

        public static void ReInitializeFrontendLoginSession(bool isWelcome)
        {
            FrontEndLogin olu = new FrontEndLogin();
            olu = (FrontEndLogin)HttpContext.Current.Session["frontuser"];
            olu.IsWelcome = isWelcome;
            HttpContext.Current.Session["frontuser"] = olu;
        }
    }
}