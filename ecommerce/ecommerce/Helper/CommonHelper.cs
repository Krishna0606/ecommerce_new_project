﻿using ecommerce.Helper.DataBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static ecommerce.Models.Backend.BasicMaster;

namespace ecommerce.Helper
{
    public static class CommonHelper
    {
        public static DataTable GetRecordFromTable(string fileds, string tablename, string whereCondition)
        {
            return ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
        }
        public static List<MenuModel> GetMenuList(bool status = true)
        {
            List<MenuModel> menuList = new List<MenuModel>();
            try
            {
                string fileds = "*";
                string tablename = "T_Menu";
                string whereCondition = "Status=" + (status == true ? 1 : 0);

                DataTable dtMenu = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtMenu.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMenu.Rows.Count; i++)
                    {
                        MenuModel menu = new MenuModel();
                        menu.Menuid = Convert.ToInt32(dtMenu.Rows[i]["Menuid"].ToString());
                        menu.MenuName = dtMenu.Rows[i]["MenuName"].ToString();
                        menu.Status = dtMenu.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        menuList.Add(menu);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return menuList;
        }
        public static List<CategoryModel> GetCategoryList(string menuid)
        {
            List<CategoryModel> menuList = new List<CategoryModel>();
            try
            {
                string fileds = "*";
                string tablename = "T_Category";
                string whereCondition = "Status=1 and Menu=" + menuid;

                //DataTable dtCategory = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                //if (dtMenu.Rows.Count > 0)
                //{
                //    for (int i = 0; i < dtMenu.Rows.Count; i++)
                //    {
                //        CategoryModel menu = new CategoryModel();
                //        menu.Menuid = Convert.ToInt32(dtMenu.Rows[i]["Menuid"].ToString());
                //        menu.MenuName = dtMenu.Rows[i]["MenuName"].ToString();
                //        menu.Status = dtMenu.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                //        menuList.Add(menu);
                //    }
                //}
            }
            catch (Exception)
            {

                throw;
            }

            return menuList;
        }
        public static List<StateModel> GetStateList(string stateid = null)
        {
            List<StateModel> stateList = new List<StateModel>();
            try
            {
                string fileds = "*";
                string tablename = "T_State";
                string whereCondition = "Status=1";

                DataTable dtState = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtState.Rows.Count > 0)
                {
                    for (int i = 0; i < dtState.Rows.Count; i++)
                    {
                        StateModel state = new StateModel();
                        state.StateId = Convert.ToInt32(dtState.Rows[i]["StateId"].ToString());
                        state.StateName = dtState.Rows[i]["StateName"].ToString();
                        state.StateCode = dtState.Rows[i]["StateCode"].ToString();
                        state.CountryId = Convert.ToInt32(dtState.Rows[i]["CountryId"].ToString());
                        state.Status = dtState.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        stateList.Add(state);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return stateList;
        }
        public static List<CityModel> GetCityList(string stateid = null)
        {
            List<CityModel> cityList = new List<CityModel>();
            try
            {
                string fileds = "*";
                string tablename = "T_City";
                string whereCondition = "Status=1";

                if (!string.IsNullOrEmpty(stateid))
                {
                    whereCondition += " and StateId=" + stateid;
                }

                DataTable dtCity = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition);
                if (dtCity.Rows.Count > 0)
                {
                    for (int i = 0; i < dtCity.Rows.Count; i++)
                    {
                        CityModel city = new CityModel();
                        city.CityId = Convert.ToInt32(dtCity.Rows[i]["CityId"].ToString());
                        city.CityName = dtCity.Rows[i]["CityName"].ToString();
                        city.CityCode = dtCity.Rows[i]["CityCode"].ToString();
                        city.CountryId = Convert.ToInt32(dtCity.Rows[i]["CountryId"].ToString());
                        city.StateId = Convert.ToInt32(dtCity.Rows[i]["StateId"].ToString());
                        city.Status = dtCity.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        cityList.Add(city);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return cityList;
        }
    }
}