﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using ecommerce.Helper.DataBase;
using ecommerce.Helper.EmailHelper;
using ecommerce.Helper.SessionInitialize;
using ecommerce.Models.Backend;
using ecommerce.Models.Common;
using ecommerce.Models.Frontend;
using ecommerce.Service.EmailService;

namespace ecommerce.Helper
{
    public static class AccountHelper
    {
        public static bool BackendLogin(string userName, string password, ref string msg, bool rememberMe = false)
        {
            bool result = false;

            try
            {
                DataTable dt = CommonHelper.GetRecordFromTable("*", "T_Login", "UserId='" + userName + "' and Password='" + password + "'");
                if (dt != null && dt.Rows.Count > 0)
                {
                    bool isActive = dt.Rows[0]["Status"].ToString().ToLower() == "true" ? true : false;

                    if (isActive)
                    {
                        SupplierInitializeSession.InitializeBackendLoginSession(dt, true);
                        if (rememberMe)
                        {
                            HttpCookie cook = new HttpCookie("supcookie");
                            cook.Value = dt.Rows[0]["LoginId"].ToString();
                            cook.Expires = DateTime.Now.AddYears(1);
                            HttpContext.Current.Response.Cookies.Add(cook);
                        }

                        msg = dt.Rows[0]["LoginId"].ToString();

                        result = true;
                    }
                    else
                    {
                        msg = "Your login suspended by administrator !";
                        result = false;
                    }
                }
                else
                {
                    msg = "Incorrect username and password !";
                    result = false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }

        public static bool LogoutBackendLogin()
        {
            HttpContext.Current.Session.Remove("supplier");
            if (HttpContext.Current.Request.Cookies["supcookie"] != null)
            {
                HttpContext.Current.Response.Cookies["supcookie"].Expires = DateTime.Now.AddYears(-1);
            }

            return true;
        }

        public static bool IsBackendLogin(ref BackendLogin lu)
        {
            //BackendLogin objLoginUser = new BackendLogin();
            bool isLogin = IsBackendLoginSuccess(ref lu);

            if (isLogin)
            {
                if (lu.IsWelcome)
                {
                    SupplierInitializeSession.ReInitializeBackendLoginSession(lu.IsWelcome);
                }
            }

            return isLogin;
        }
        public static bool IsBackendLoginSuccess(ref BackendLogin login)
        {
            bool value = false;
            try
            {
                if (HttpContext.Current.Session["supplier"] == null && HttpContext.Current.Request.Cookies["supcookie"] != null)
                {
                    int loginId = Convert.ToInt32(HttpContext.Current.Request.Cookies["u"].Value);

                    DataTable dt = CommonHelper.GetRecordFromTable("*", "T_Login", "LoginId=" + loginId);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        SupplierInitializeSession.InitializeBackendLoginSession(dt);
                    }
                }

                if (HttpContext.Current.Session["supplier"] != null)
                {
                    login = (BackendLogin)HttpContext.Current.Session["supplier"];
                    value = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return value;
        }

        public static bool IsFrontendLogin(ref FrontEndLogin lu)
        {
            bool isLogin = IsFrontndLoginSuccess(ref lu);

            if (isLogin)
            {
                if (lu.IsWelcome)
                {
                    SupplierInitializeSession.ReInitializeFrontendLoginSession(lu.IsWelcome);
                }
            }

            return isLogin;
        }

        public static bool IsFrontndLoginSuccess(ref FrontEndLogin login)
        {
            bool value = false;
            try
            {
                if (HttpContext.Current.Session["frontuser"] == null && HttpContext.Current.Request.Cookies["fusercookie"] != null)
                {
                    int loginId = Convert.ToInt32(HttpContext.Current.Request.Cookies["fu"].Value);

                    DataTable dt = CommonHelper.GetRecordFromTable("*", "T_Login", "LoginId=" + loginId);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        SupplierInitializeSession.InitializeFrontendLoginSession(dt);
                    }
                }

                if (HttpContext.Current.Session["frontuser"] != null)
                {
                    login = (FrontEndLogin)HttpContext.Current.Session["frontuser"];
                    value = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return value;
        }

        public static bool LogoutFrontendLogin()
        {
            HttpContext.Current.Session.Remove("frontuser");
            if (HttpContext.Current.Request.Cookies["fusercookie"] != null)
            {
                HttpContext.Current.Response.Cookies["fusercookie"].Expires = DateTime.Now.AddYears(-1);
            }

            return true;
        }

        public static bool FrontendLogin(string userName, string password, ref string msg, bool rememberMe = false)
        {
            bool result = false;

            try
            {
                DataTable dt = CommonHelper.GetRecordFromTable("*", "T_Login", "UserId='" + userName + "' and Password='" + password + "' and UserType='user'");
                if (dt != null && dt.Rows.Count > 0)
                {
                    bool isActive = dt.Rows[0]["Status"].ToString().ToLower() == "true" ? true : false;
                    bool IsApproved = dt.Rows[0]["IsApproved"].ToString().ToLower() == "true" ? true : false;
                    bool emailVerified = dt.Rows[0]["EmailVerified"].ToString().ToLower() == "true" ? true : false;
                    bool mobileVerified = dt.Rows[0]["MobileVerified"].ToString().ToLower() == "true" ? true : false;

                    if (isActive)
                    {
                        if (emailVerified == true && mobileVerified == true)
                        {
                            if (IsApproved)
                            {
                                SupplierInitializeSession.InitializeFrontendLoginSession(dt, true);
                                if (rememberMe)
                                {
                                    HttpCookie cook = new HttpCookie("fusercookie");
                                    cook.Value = dt.Rows[0]["LoginId"].ToString();
                                    cook.Expires = DateTime.Now.AddYears(1);
                                    HttpContext.Current.Response.Cookies.Add(cook);
                                }

                                msg = dt.Rows[0]["LoginId"].ToString();

                                result = true;
                            }
                            else
                            {
                                msg = "Your login not activated, please verify your email and mobile number !";
                                result = false;
                            }
                        }
                        else
                        {
                            msg = "Your login not activated, please verify your email and mobile number !";
                            result = false;
                        }
                    }
                    else
                    {
                        msg = "Your login suspended by administrator !";
                        result = false;
                    }
                }
                else
                {
                    msg = "Incorrect username and password !";
                    result = false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static bool ProcessVerifyOtpValid(string emailotp, string mobileotp, string userid, string password, ref string msg)
        {
            try
            {
                bool emailVarified = false; bool mobileVarified = false;

                DataTable dt = CommonHelper.GetRecordFromTable("*", "T_Login", "UserId='" + userid + "' and Password='" + password + "'");

                if (dt.Rows.Count > 0)
                {
                    string emailOTP = !string.IsNullOrEmpty(dt.Rows[0]["EmailOTP"].ToString()) ? dt.Rows[0]["EmailOTP"].ToString() : string.Empty;
                    string mobileOTP = !string.IsNullOrEmpty(dt.Rows[0]["MobileOTP"].ToString()) ? dt.Rows[0]["MobileOTP"].ToString() : string.Empty;

                    if (emailOTP == emailotp)
                    {
                        emailVarified = true;
                    }

                    if (mobileOTP == mobileotp)
                    {
                        mobileVarified = true;
                    }

                    if (emailVarified == true && mobileVarified == true)
                    {
                        if (UpdateSingnUpVarificationDel(emailotp, mobileotp, userid, password))
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (!emailVarified)
                        {
                            msg = "Incorrect email otp! Enter valid otp.";
                            return false;
                        }
                        else
                        {
                            msg = "Incorrect mobile otp! Enter valid otp.";
                            return false;
                        }
                    }
                }
                else
                {
                    msg = "Incorrect username and password !";
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        private static bool UpdateSingnUpVarificationDel(string emailotp, string mobileotp, string userid, string password)
        {
            try
            {
                string fieldsWithValue = "IsApproved=1,EmailVerified=1,MobileVerified=1,EmailOTP=null,MobileOTP=null";
                string whereCondition = "EmailOTP='" + emailotp + "' and MobileOTP='" + mobileotp + "' and UserId='" + userid + "' and Password='" + password + "'";
                string tableName = "T_Login";

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                {
                    if (IsMailSmsSent(userid, password))
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool IsMailSmsSent(string userid, string password)
        {
            try
            {
                DataTable dt = CommonHelper.GetRecordFromTable("*", "T_Login", "UserId='" + userid + "' and Password='" + password + "'");

                if (dt.Rows.Count > 0)
                {
                    string firstname = !string.IsNullOrEmpty(dt.Rows[0]["FirstName"].ToString()) ? dt.Rows[0]["FirstName"].ToString() : string.Empty;
                    string lastname = !string.IsNullOrEmpty(dt.Rows[0]["LastName"].ToString()) ? dt.Rows[0]["LastName"].ToString() : string.Empty;
                    string emailid = !string.IsNullOrEmpty(dt.Rows[0]["EmailId"].ToString()) ? dt.Rows[0]["EmailId"].ToString() : string.Empty;
                    string mobileno = !string.IsNullOrEmpty(dt.Rows[0]["MobileNo"].ToString()) ? dt.Rows[0]["MobileNo"].ToString() : string.Empty;
                    string regpassword = !string.IsNullOrEmpty(dt.Rows[0]["Password"].ToString()) ? dt.Rows[0]["Password"].ToString() : string.Empty;
                    string regUserid = !string.IsNullOrEmpty(dt.Rows[0]["UserId"].ToString()) ? dt.Rows[0]["UserId"].ToString() : string.Empty;

                    if (Email.SendUserRegistEmail(firstname, lastname, emailid, mobileno, regUserid, regpassword))
                    {
                        string msg = "Dear " + firstname + " " + lastname + ", Your UserId:" + userid + " and Password:" + regpassword + ". Thank you for register with VocalGoLocal, If any support to email us at info@richaworldtravels.com or call +917949109999";
                        if (Email.SendMobileSms(mobileno, msg))
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool SendVerificationCode(string userid, string password, ref string msg)
        {
            try
            {
                DataTable dt = CommonHelper.GetRecordFromTable("*", "T_Login", "UserId='" + userid + "' and Password='" + password + "'");

                if (dt.Rows.Count > 0)
                {
                    string tempEmailOTP = Utility.GenrateRandomTransactionId("", 6, "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
                    string tempMoileOTP = Utility.GenrateRandomTransactionId("", 6, "1234567890");

                    string firstname = !string.IsNullOrEmpty(dt.Rows[0]["FirstName"].ToString()) ? dt.Rows[0]["FirstName"].ToString() : string.Empty;
                    string lastname = !string.IsNullOrEmpty(dt.Rows[0]["LastName"].ToString()) ? dt.Rows[0]["LastName"].ToString() : string.Empty;
                    string emailid = !string.IsNullOrEmpty(dt.Rows[0]["EmailId"].ToString()) ? dt.Rows[0]["EmailId"].ToString() : string.Empty;
                    string mobileno = !string.IsNullOrEmpty(dt.Rows[0]["MobileNo"].ToString()) ? dt.Rows[0]["MobileNo"].ToString() : string.Empty;
                    string regpassword = !string.IsNullOrEmpty(dt.Rows[0]["Password"].ToString()) ? dt.Rows[0]["Password"].ToString() : string.Empty;
                    string regUserid = !string.IsNullOrEmpty(dt.Rows[0]["UserId"].ToString()) ? dt.Rows[0]["UserId"].ToString() : string.Empty;
                    string loginid = !string.IsNullOrEmpty(dt.Rows[0]["LoginId"].ToString()) ? dt.Rows[0]["LoginId"].ToString() : string.Empty;

                    string fieldsWithValue = "EmailOTP='" + tempEmailOTP + "', MobileOTP='" + tempMoileOTP + "'";
                    string whereCondition = "LoginId=" + loginid;
                    string tableName = "T_Login";

                    if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition))
                    {
                        if (EmailHelp.SendVarificationCode(firstname, lastname, emailid, mobileno, tempEmailOTP, tempMoileOTP))
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    msg = "User id and password incorrect !";
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
    }
}