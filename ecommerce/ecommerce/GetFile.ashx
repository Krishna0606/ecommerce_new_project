﻿<%@ WebHandler Language="C#" Class="GetFile" %>

using System;
using System.Web;
using System.IO;
using System.Drawing;

public class GetFile : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
            FileInfo fi = new FileInfo(context.Server.MapPath("~/App_Data/" + context.Request.QueryString["path"]));

            if (fi.Exists)
            {
                string extension = fi.Extension.ToLower().Replace(".", string.Empty);

                switch (extension)
                {
                    case "jpg":
                    case "tif":
                    case "gif":
                    case "png":
                    case "raw":
                    case "jpeg":
                        if (!string.IsNullOrEmpty(context.Request.QueryString["width"]))
                        {
                            FileStream fileStream = null;
                            try
                            {
                                fileStream = fi.Open(FileMode.Open);

                                ////Calculate New Width and Height
                                using (Bitmap bitMap = new Bitmap(fileStream))
                                {
                                    try
                                    {
                                        int iOldWidth = bitMap.Width;
                                        int iOldHeight = bitMap.Height;

                                        double dHeightUnitsPerOneWidthUnit = iOldHeight / (double)iOldWidth;

                                        int iNewWidth = Convert.ToInt32(context.Request.QueryString["width"]);
                                        int iNewHeight = Convert.ToInt32(dHeightUnitsPerOneWidthUnit * iNewWidth);

                                        ////Internal Processing            
                                        using (Image image = Image.FromStream(fileStream))
                                        {
                                            try
                                            {
                                                Rectangle rectangle = new Rectangle(0, 0, iNewWidth, iNewHeight);

                                                using (Image canvas = new Bitmap(iNewWidth, iNewHeight))
                                                {
                                                    try
                                                    {
                                                        using (Graphics graphic = Graphics.FromImage(canvas))
                                                        {
                                                            try
                                                            {
                                                                graphic.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                                                                graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                                                                graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                                                                graphic.Clear(Color.White);

                                                                graphic.DrawImage(image, rectangle);

                                                                using (MemoryStream str = new MemoryStream())
                                                                {
                                                                    try
                                                                    {
                                                                        switch (extension)
                                                                        {
                                                                            case "jpg":
                                                                            case "raw":
                                                                            case "jpeg":
                                                                            default:
                                                                                canvas.Save(str, System.Drawing.Imaging.ImageFormat.Jpeg);
                                                                                break;
                                                                            case "tif":
                                                                                canvas.Save(str, System.Drawing.Imaging.ImageFormat.Tiff);
                                                                                break;
                                                                            case "gif":
                                                                                canvas.Save(str, System.Drawing.Imaging.ImageFormat.Gif);
                                                                                break;
                                                                            case "png":
                                                                                canvas.Save(str, System.Drawing.Imaging.ImageFormat.Png);
                                                                                break;
                                                                        }

                                                                        str.WriteTo(context.Response.OutputStream);
                                                                        context.Response.ContentType = fi.Extension;
                                                                        context.Response.End();
                                                                    }
                                                                    finally
                                                                    {
                                                                        str.Close();
                                                                        str.Dispose();
                                                                    }
                                                                }
                                                            }
                                                            finally
                                                            {
                                                                graphic.Dispose();
                                                            }
                                                        }
                                                    }
                                                    finally
                                                    {
                                                        canvas.Dispose();
                                                    }
                                                }
                                            }
                                            finally
                                            {
                                                image.Dispose();
                                            }
                                        }
                                    }
                                    finally
                                    {
                                        bitMap.Dispose();
                                    }
                                }
                            }
                            finally
                            {
                                if (fileStream != null)
                                {
                                    fileStream.Close();
                                    fileStream.Dispose();
                                }
                            }
                        }
                        else
                        {
                            using (Image img = Image.FromFile(fi.FullName))
                            {
                                using (MemoryStream str = new MemoryStream())
                                {
                                    try
                                    {
                                        switch (extension)
                                        {
                                            case "jpg":
                                            case "raw":
                                            case "jpeg":
                                            default:
                                                img.Save(str, System.Drawing.Imaging.ImageFormat.Jpeg);
                                                break;
                                            case "tif":
                                                img.Save(str, System.Drawing.Imaging.ImageFormat.Tiff);
                                                break;
                                            case "gif":
                                                img.Save(str, System.Drawing.Imaging.ImageFormat.Gif);
                                                break;
                                            case "png":
                                                img.Save(str, System.Drawing.Imaging.ImageFormat.Png);
                                                break;
                                        }

                                        str.WriteTo(context.Response.OutputStream);
                                        context.Response.ContentType = fi.Extension;
                                        context.Response.End();
                                    }
                                    finally
                                    {
                                        str.Close();
                                        str.Dispose();
                                        img.Dispose();
                                    }
                                }
                            }
                        }

                        break;
                    default:
                        using (FileStream fileStream = File.OpenRead(fi.FullName))
                        {
                            using (MemoryStream memStream = new MemoryStream())
                            {
                                try
                                {
                                    memStream.SetLength(fileStream.Length);
                                    fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);

                                    memStream.WriteTo(context.Response.OutputStream);
                                    if (fi.Extension.Replace(".", "").ToLower() == "pdf")
                                    {
                                        context.Response.ContentType = "application/pdf";
                                        context.Response.AddHeader("Content-Disposition", "filename=" + fi.Name);
                                    }
                                    else if (fi.Extension.Replace(".", "").ToLower() == "htm")
                                    {
                                        context.Response.AddHeader("Content-Disposition", "filename=" + fi.Name);
                                    }
                                    else
                                    {
                                        context.Response.ContentType = "application/" + fi.Extension.Replace(".", "");
                                        context.Response.AddHeader("Content-Disposition", "attachment; filename=" + fi.Name);
                                    }

                                    context.Response.End();
                                }
                                finally
                                {
                                    fileStream.Close();
                                    fileStream.Dispose();

                                    memStream.Close();
                                    memStream.Dispose();
                                }
                            }
                        }

                        break;
                }
            }
            else
            {
                context.Response.Clear();
                context.Response.StatusCode = 404;
            }
        }
    
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}