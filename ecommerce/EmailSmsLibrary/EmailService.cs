﻿using System;
using System.Net.Mail;

namespace EmailSmsLibrary
{
    public class EmailService
    {
        public static bool SendEmail(string emailUserName, string emailPassword, string emailFrom, string emailTo, string subject, string body, string emailType, string cc = null, string bcc = null)
        {
            bool issent = false;

            try
            {
                using (MailMessage message = new MailMessage(emailFrom, emailTo, subject, body))
                {
                    if (cc != null) { message.CC.Add(cc); }
                    if (bcc != null) { message.Bcc.Add(bcc); }

                    message.ReplyToList.Add(new MailAddress(emailFrom));
                    message.IsBodyHtml = true;
                    using (SmtpClient emailClient = new SmtpClient("smtp.gmail.com"))
                    {
                        emailClient.Port = 587;
                        emailClient.EnableSsl = true;
                        emailClient.UseDefaultCredentials = true;
                        emailClient.Credentials = new System.Net.NetworkCredential(emailUserName, emailPassword);
                        emailClient.Send(message);
                        issent = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return issent;
        }
    }
}
